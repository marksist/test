/****************************************
  UCL Goodymat Device, prototype 1.00
  Based on Arduino YUN
  Unicorn College
  Author: Marek Beranek
  Last update: 2017/01/14
****************************************/

#include <Bridge.h> // The library simplifies communication between the Linux processor and microcontroller, part of the Bridge library for Yun devices, created by Arduino
#include <Process.h> // The library enables to run Linux processes on the Linux processor, part of the Bridge library for Yun devices, created by Arduino
#include <FileIO.h> // The library represents interface to the Linux file system, part of the Bridge library for Yun devices, created by Arduino
#include <NewPing.h> // The library for the ultrasonic sensor HC-SR04, created by Tim Eckel (https://bitbucket.org/teckel12/arduino-new-ping/wiki/Home)
#include "LedControl.h" // The library for the MAX7221 and MAX7219 LED display drivers, created by Eberhard Fahle (https://github.com/wayoda/LedControl)
#include "oidc.h" // UU OIDC credentials token
#include "uuthing.h" // UU Thing credentials

boolean debug = false; // If debug is true, sound level measurements and initiall rest call status within setup are logged; Can be overloaded by settings in config.txt

/********* PIN DEFINITION *********/
#define SOUNDSENSOR A0
#define BUTTONPIN 2
#define TRIGPIN 8 //Ultrasonic sensor
#define ECHOPIN 9 //Ultrasonic sensor

/********* SCRIPTS *********/
#define LOGINSCRIPT "/mnt/sda1/login.py"
#define BUYSCRIPT "/mnt/sda1/buy.py"

/********* LOG FILE *********/
#define LOG_FILE "/mnt/sda1/log.txt"

/********* CONFIG FILE *********/
#define CONFIG_FILE "/mnt/sda1/config.txt"

/********* LED MATRIX 8x8 *********/
#define NUMBER_OF_LED_MATRIXES 1
#define MAX_SCALE 8
LedControl ledMatrix=LedControl(5,7,6,1);  // Pins: DIN,CLK,CS, # of Display connected
int matrix[8][8] {
  {0,0,0,0,0,0,0,0},
  {0,0,0,0,0,0,0,0},  
  {0,0,0,0,0,0,0,0},
  {0,0,0,0,0,0,0,0},
  {0,0,0,0,0,0,0,0},
  {0,0,0,0,0,0,0,0},
  {0,0,0,0,0,0,0,0},
  {0,0,0,0,0,0,0,0},
  };

/********* SOUND SENSOR *********/
#define SAMPLE_WINDOW 50
#define MAX_PEAK 80
unsigned int minLevel = 50; // Can be overloaded by settings in config.txt

/********* ULTRASONIC SENSOR *********/
unsigned int minDistance = 100; // Can be overloaded by settings in config.txt. Distance in cm.
#define MAX_DISTANCE 500 // Max distance for ultrasonic measuring
NewPing ultrasonic(TRIGPIN, ECHOPIN, MAX_DISTANCE);

/********* GLOBAL VARIABLES *********/
boolean connected2UU = false;
int measuredDistance = 0;
unsigned int buttonState = 0;

/********* SETUP *********/
void setup() {  
  // Init button pin as input pin
  pinMode(BUTTONPIN, INPUT);

  // Init LED Matrix
  ledMatrix.shutdown(0,false);  // Wake up displays
  ledMatrix.setIntensity(0, 2);  // Set intensity levels
  ledMatrix.clearDisplay(0);  // Clear Displays

  // Init ultrasonic sensor
  pinMode(TRIGPIN, OUTPUT);
  pinMode(ECHOPIN, INPUT);

  // Init Bridge and Serial
  Bridge.begin();
  Serial.begin(9600);
  FileSystem.begin();

  // Initialize variables stored in the data store on the Linux processor
  Bridge.put("UU_OIDC_CREDENTIALS_TOKEN", "");
  Bridge.put("UU_THING_AC1", "");
  Bridge.put("UU_THING_AC2", "");
  Bridge.put("REST_CALL_STATUS", "");

  // Store credentials in the data store on the Linux processor
  storeCredentials2Linux();
  
  drawInit();
  
  loadSettingsValues(); // Load settings from config.txt
 
  int linuxResponse = 0;
  char lbuffer[256];
  Bridge.get("REST_CALL_STATUS", lbuffer, 256); // Read recieved REST call status from the data store on the Linux processsor

  // Call login.py script stored in the SD card on the Linux processor
  while (String(lbuffer).length() == 0) {
    Process p;
    p.begin("python");
    p.addParameter(LOGINSCRIPT);
    p.run();      
    drawInProgressDynamic();
    delay(50);

    Bridge.get("REST_CALL_STATUS", lbuffer, 256);
  }

  if (String(lbuffer) == "200") // Ready
  {
    drawEmoji("CONN");
    connected2UU = true;
  } else  // Something went wrong
  {
    drawEmoji("NOCONN");
    connected2UU = false;

    debug && logMessage("login.py REST_CALL_STATUS: " + String(lbuffer));
  }  
}

/********* LOOP *********/
void loop() {  
  if (connected2UU) { // If the device is connected to the Internet (logged to application Goodymat)
    Bridge.put("REST_CALL_STATUS", "");

    // Read state of the button
    buttonState = digitalRead(BUTTONPIN);  
    if (buttonState == HIGH) { // Button is pressed, order "O1" product and display man emoji on LED matrix
      drawEmoji("MAN");
      buyViaIot("O1");
    } else {
      measuredDistance = measureDistance();
      if (measuredDistance > 0 && measuredDistance <= minDistance) {
        measureSoundLevel();
      } else {
        drawInProgressDynamic();
        ledMatrix.clearDisplay(0);
      }
    }
  } else {
    delay(50);
  }
}

/********* LOGGING FUNCTION *********/
boolean logMessage(String message) {
  // Send message to Serial port
  Serial.println(message);
  Serial.flush();

  // Log message to log.txt file
  File logFile = FileSystem.open(LOG_FILE, FILE_APPEND);
  if (logFile) {
    logFile.println(message);
    logFile.close();  
    return true;
  } else {
    return false;
  }
}

/********* LOAD SETTINGS VALUES *********/
void loadSettingsValues() {
  char character;
  String settingsName;
  String settingsValue;

  File settings = FileSystem.open(CONFIG_FILE, FILE_READ);
  if (settings) {
    while (settings.available()) {
      character = settings.read();
      while((settings.available()) && (character != '[')){
        character = settings.read();
      }
   
      character = settings.read();
      while((settings.available()) && (character != '=')){
        settingsName = settingsName + character;
        character = settings.read();
      }
    
      character = settings.read();
      while((settings.available()) && (character != ']')){
        settingsValue = settingsValue + character;
        character = settings.read();
      }
      
      if(character == ']'){   
        // Apply the value to the parameter
        applySetting(settingsName,settingsValue);
      
        // Reset Strings
        settingsName = "";
        settingsValue = "";
      }
    }
  
    // Close the file
    settings.close();
  } else {
    drawEmoji("ERR");
    debug && logMessage("Error opening config.txt");
  }
}

void applySetting(String settingsName, String settingsValue) {
  if (settingsName == "DEBUG") {
    (settingsValue.toInt() == 1) ? debug = true : debug = false;
  }

  if (settingsName == "DISTANCE") {
    minDistance = settingsValue.toInt();
  }

  if (settingsName == "MIN_LEVEL") {
    minLevel = settingsValue.toInt();
  }
}

/********* CREDENTIALS FUNCTIONS *********/
void storeCredentials2Linux() {
  // Store UU OIDC credentials token in the data store on the Linux processor
  Bridge.put("UU_OIDC_CREDENTIALS_TOKEN", UU_OIDC_CREDENTIALS_TOKEN);
  
  // Store uuThing credentials in the data store on the Linux processor
  Bridge.put("UU_THING_AC1", UU_THING_AC1);
  Bridge.put("UU_THING_AC2", UU_THING_AC2);  
}

/********* LED MATRIX FUNCTIONS *********/
void drawEmoji(String emoji) {
  const byte EMOJIS[6][8] = {
    {
      B00000100,
      B00010010,
      B00001001,
      B11001001,
      B11001001,
      B00001001,
      B00010010,
      B00000100
    },
    {
      B10000001,
      B01000010,
      B00100100,
      B00011000,
      B00011000,
      B00100100,
      B01000010,
      B10000001
    },
    {
      B11111100,
      B00100110,
      B01001011,
      B01000011,
      B01000011,
      B01001011,
      B00100110,
      B11111100
    },
    {
      B00001100,
      B01111000,
      B01011000,
      B00011000,
      B01111000,
      B01011110,
      B00001110,
      B00001000
    },
    {
      B00010000,
      B00100000,
      B01000000,
      B00100000,
      B00010000,
      B00001000,
      B00000100,
      B00000010
    },
    {
      B00000000,
      B01000000,
      B00100100,
      B00010000,
      B00010000,
      B00100100,
      B01000000,
      B00000000
    }    
  };

  ledMatrix.clearDisplay(0);
  for (int i=0; i < 8; i++) {
    if (emoji == "CONN") {
      ledMatrix.setRow(0,i,EMOJIS[0][i]);
    } else if (emoji == "NOCONN") {
      ledMatrix.setRow(0,i,EMOJIS[1][i]);
    } else if (emoji == "MAN") {
      ledMatrix.setRow(0,i,EMOJIS[2][i]);      
    } else if (emoji == "DOG") {
      ledMatrix.setRow(0,i,EMOJIS[3][i]);      
    } else if (emoji == "SUCCESS") {
      ledMatrix.setRow(0,i,EMOJIS[4][i]);
    } else if (emoji == "ERR") {
      ledMatrix.setRow(0,i,EMOJIS[5][i]);
    }
  }

  if (emoji != "NOCONN") {
    delay(1000);
    ledMatrix.clearDisplay(0);
  }
}

void drawInit() {
  ledMatrix.clearDisplay(0);
  ledMatrix.setLed(0,0,0,true);
}

void drawInProgress() {
  ledMatrix.clearDisplay(0);
  ledMatrix.setLed(0,2,4,true);
  ledMatrix.setLed(0,3,4,true);
  ledMatrix.setLed(0,4,4,true);
  ledMatrix.setLed(0,5,4,true);
}

void drawInProgressDynamic() {
  ledMatrix.clearDisplay(0);
  ledMatrix.setLed(0,3,4,true);
  delay(100);
  ledMatrix.clearDisplay(0);
  ledMatrix.setLed(0,4,4,true);
  delay(100);
  ledMatrix.clearDisplay(0);
  delay(100);
}

void addNewSampleIntoMatrix(int value) {
  const int ROW = 7;
  int col;
  
  for (col = 0; col <= MAX_SCALE; col++)
  {
    (col >= value) ? matrix[ROW][col] = 0 : matrix[ROW][col] = 1; // If col >= value, blank these pixels, otherwise draw pixels
  }
}

void resampleMatrix() {
  int row, col;

  for (row = 0; row < MAX_SCALE; row++) {
    for (col = 0; col < MAX_SCALE; col++) {
      matrix[row][col] = matrix[row+1][col];
    }
  }
}

void displayMatrix() {
  int row, col, value;
  bool light;

  for (row = 0; row < MAX_SCALE; row++) {
    for (col = 0; col < MAX_SCALE; col++) {
      value = matrix[row][col];
      (value == 0) ? light = false : light = true;
      ledMatrix.setLed(0, row, col, light);      
    }
  }
}

/********* BUSINESS USE CASE *********/
void buyViaIot(String product) {
  drawInProgress();
  
  // Store credentials in the data store on the Linux processor
  storeCredentials2Linux();

  // Call buyviot.py script stored in the SD card on the Linux processor
  Process p;                         
  p.begin("python");  
  p.addParameter(BUYSCRIPT);
  p.addParameter(product);
  p.run();

  char lbuffer[256];
  Bridge.get("REST_CALL_STATUS", lbuffer, 256);
  String response = "";

  // Print response to Serial
  while (p.available()) {
    char c = p.read();
    response = response + c;
  }

  (debug) && logMessage("Operation: " + product + ", response: " + response);

  if (String(lbuffer) == "200") {
    drawEmoji("SUCCESS");
  } else {
    drawEmoji("ERR");
  }
}

/********* MEASUREMENT FUNCTIONS *********/
long measureDistance() {
  long duration;
  int distance;
  
  duration = ultrasonic.ping_median(5);
  distance = ultrasonic.convert_cm(duration);
  return distance;
}

void measureSoundLevel() {
  unsigned long startMillis = millis(); // Start of sample window
  unsigned int peakToPeak = 0;   // Peak-to-peak level
  unsigned int sample = 0;
  unsigned int i = 0;
  unsigned int signalMax = 0;
  unsigned int signalMin = 1024;

  // Collect data for 50 mS
  while (millis() - startMillis < SAMPLE_WINDOW)
  {
    sample = analogRead(SOUNDSENSOR);
    if (sample < 1024)  // Toss out spurious readings
    {
       if (sample > signalMax)
       {
          signalMax = sample;  // Save just the max levels
       }
       else if (sample < signalMin)
       {
          signalMin = sample;  // Save just the min levels
       }
    }
  }
  
  peakToPeak = signalMax - signalMin;  // max - min = peak-peak amplitude

  // Map 1v p-p level to the max scale of the display
  int displayPeak = map(peakToPeak, 0, MAX_PEAK, 0, MAX_SCALE);

  debug && logMessage("Peak: " + String(peakToPeak) + "; display peak: " + String(displayPeak) + "; min: " + String(signalMin) + "; max: " + String(signalMax));

  if (displayPeak > 0) { // If sound is measured, add sample and display in matrix
    resampleMatrix();
    addNewSampleIntoMatrix(displayPeak);
    displayMatrix();
  } else { // If no sound is measured, draw "in progress" on LED matrix
    drawInit();
  }

  // If measured sound reached required level, order "O2" product and display dog emoji on LED matrix
  if (peakToPeak >= minLevel) {
    drawEmoji("DOG");
    buyViaIot("O2");
  }
}
