#!/usr/bin/python
import time
import argparse
import json
import httplib
import urllib
import sys

HOST = "uuos9.plus4u.net"

CMD_BUY_WITH_IOT_URI = '/ucl-goodymat-main/84723967990075193-5855bd69c50ed33188afa0ef/buyViaIot'
UU_OIDC_TOKEN_URI = "/uu-oidc-main/0-0/grantToken"

STORE_PATH = "/mnt/sda1/"  # where to store Token on local filesystem

TOKEN_LEEWAY = 60        # [s] - leeway between Client and Server Clock

TOKEN_FILE = STORE_PATH+'oidc_token.json'
LOG_FILE = STORE_PATH+'restlog.txt'

sys.path.insert(0, '/usr/lib/python2.7/bridge/')
from bridgeclient import BridgeClient as bridgeclient
bridge = bridgeclient()

bridge.put("REST_CALL_STATUS", "")

UU_OIDC_CREDENTIALS_TOKEN = bridge.get("UU_OIDC_CREDENTIALS_TOKEN").strip()
UU_THING_AC1 = bridge.get("UU_THING_AC1").strip()
UU_THING_AC2 = bridge.get("UU_THING_AC2").strip()

def buy_with_iot(gm_op):
    """Call command to buy some product."""
    json_data = {"operation": gm_op}
    headers = {'Accept':'application/json',
               'Authorization':'Bearer '+get_access_token(),
               'Content-type':'application/json'}

    conn = httplib.HTTPSConnection(HOST)
    conn.request("POST", CMD_BUY_WITH_IOT_URI, json.dumps(json_data), headers)
    response = conn.getresponse()

    if response.status >= 200 and response.status < 300:
        return "NO_RESPONSE"
    else:
        status = response.status
        error = response.read()
        error_json = json.loads(error)
        raise CommandError(status, error_json["code"], error_json["message"])

def store_log_operation(log_file_path, message):
    """Stores JSON response from the server to file"""
    with open(log_file_path, 'a') as outfile:
        outfile.writelines(message)


def store_token(token_file_path, token_json):
    """Stores Bearer token to file"""
    with open(token_file_path, 'w') as outfile:
        outfile.write(token_json)


def load_token(token_file_path):
    """Loads Bearer token to file"""
    try:
        with open(token_file_path) as json_file:
            token_json = json.load(json_file)
        return token_json
    except IOError:
        return None

def oidc_grant_token():
    """Requests grant of the first access token."""
    post_data = {"grant_type": "password",
                 "username": UU_THING_AC1,
                 "password": UU_THING_AC2,
                 "scope":"openid offline_access"
                }

    headers = {'Content-Type': 'application/json','Authorization':'Basic ' + UU_OIDC_CREDENTIALS_TOKEN}

    conn = httplib.HTTPSConnection(HOST)
    conn.request("POST", UU_OIDC_TOKEN_URI, json.dumps(post_data), headers)
    response = conn.getresponse()

    if response.status >= 200 and response.status < 300:
        token_json = response.read()
        return token_json
    else:
        status = response.status
        error = response.read()
        error_json = json.loads(error)
        raise CommandError(status, error_json["code"], error_json["message"])

def oidc_refresh_token(token):
    """Requests refresh of expired access token."""
    post_data = {"grant_type": "refresh_token",
                 "refresh_token": token['refresh_token']
                 }

    headers = {'Content-Type': 'application/json','Authorization':'Basic ' + UU_OIDC_CREDENTIALS_TOKEN}

    conn = httplib.HTTPSConnection(HOST)
    conn.request("POST", UU_OIDC_TOKEN_URI, json.dumps(post_data), headers)
    response = conn.getresponse()

    if response.status >= 200 and response.status < 300:
        token_json = response.read()
        return token_json
    else:
        status = response.status
        error = response.read()
        error_json = json.loads(error)
        raise CommandError(status, error_json["code"], error_json["message"])

def get_access_token():
    """Manages access token transparently.It is used to obtain valid access token to be used for Goodymat command calls."""
    token = load_token(TOKEN_FILE)
    current_time = (TOKEN_LEEWAY+int(time.time()))

    if None is token:
        json_token = {}
    else:
        json_token = token

    if None is token:
        token = oidc_grant_token() #first initialization
        store_token(TOKEN_FILE, token)
        json_token = json.loads(token)
    elif token['expires_at'] <= current_time:
        token = oidc_refresh_token(token)
        store_token(TOKEN_FILE, token) #replace with fresh token
        json_token = json.loads(token)
    access_token = json_token['id_token']
    return access_token

class CommandError(Exception):
    """Error thrown when some problem occures in communication with uuOIDC server. """
    def __init__(self, status, code, message):
        super(CommandError, self).__init__(message)
        self.status = status
        self.code = code
        self.message = message

    def __str__(self):
        return str(self.status) + "," + self.code + "," + self.message


ARGS_PARSER = argparse.ArgumentParser(prog='goodymat.py', description='Goodymat IoT client.')
ARGS_PARSER.add_argument('operation', help='Goodymat operation CODE=[O1,O2].')
ARGS = ARGS_PARSER.parse_args()

try:
    if not UU_OIDC_CREDENTIALS_TOKEN or not UU_THING_AC1 or not UU_THING_AC2:
        print ('502','CREDENTIALS_MISSING')
        exit(1)
    else:
        res = buy_with_iot(ARGS.operation)
        bridge.put("REST_CALL_STATUS", "200")
        #store_log_operation(LOG_FILE, '200,OK') # Uncomment For debugging
        print('200,OK')
        exit(0)
except CommandError as ce:
    bridge.put("REST_CALL_STATUS", "501")
    #store_log_operation(LOG_FILE, '501,COMMAND_ERROR, '+ce)  # Uncomment For debugging
    print(ce)
    exit(1)
except Exception as e:
    bridge.put("REST_CALL_STATUS", "500")
    #store_log_operation(LOG_FILE, '500,CLIENT_UNEXPECTED_ERROR,'+str(e)) # Uncomment For debugging
    print('500,CLIENT_UNEXPECTED_ERROR,'+str(e))
    exit(1)

