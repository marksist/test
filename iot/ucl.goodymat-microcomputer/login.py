#!/usr/bin/python
import json
import httplib
import urllib
import sys
import os

HOST = "uuos9.plus4u.net"

UU_OIDC_TOKEN_URI = "/uu-oidc-main/0-0/grantToken"

TOKEN_STORE_PATH = "/mnt/sda1/"  # where to store Token on local filesystem
TOKEN_FILE = TOKEN_STORE_PATH+'oidc_token.json'

sys.path.insert(0, '/usr/lib/python2.7/bridge/')
from bridgeclient import BridgeClient as bridgeclient
bridge = bridgeclient()

bridge.put("REST_CALL_STATUS", "")

UU_OIDC_CREDENTIALS_TOKEN = bridge.get("UU_OIDC_CREDENTIALS_TOKEN").strip()
UU_THING_AC1 = bridge.get("UU_THING_AC1").strip()
UU_THING_AC2 = bridge.get("UU_THING_AC2").strip()

def store_token(token_file_path, token_json):
    """Stores Bearer token to file"""
    with open(token_file_path, 'w') as outfile:
        outfile.write(token_json)

def oidc_grant_token():
    """Requests grant of the first access token."""
    post_data = {"grant_type": "password",
                 "username": UU_THING_AC1,
                 "password": UU_THING_AC2,
                 "scope":"openid offline_access"
                }

    headers = {'Content-Type': 'application/json','Authorization':'Basic ' + UU_OIDC_CREDENTIALS_TOKEN}

    conn = httplib.HTTPSConnection(HOST)
    conn.request("POST", UU_OIDC_TOKEN_URI, json.dumps(post_data), headers)
    response = conn.getresponse()


    if response.status >= 200 and response.status < 300:
        token_json = response.read()
        return token_json
    else:
        status = response.status
        error_json = response.json()
        raise CommandError(status, error_json["code"], error_json["message"])

class CommandError(Exception):
    """Error thrown when some problem occures in communication with uuOIDC server. """
    def __init__(self, status, code, message):
        super(CommandError, self).__init__(message)
        self.status = status
        self.code = code
        self.message = message

    def __str__(self):
        return str(self.status) + "," + self.code + "," + self.message

try:
    if not UU_OIDC_CREDENTIALS_TOKEN or not UU_THING_AC1 or not UU_THING_AC2:
        print('502','CREDENTIALS_MISSING')
        exit(1)
    else:
        if os.path.exists(TOKEN_FILE):
            os.remove(TOKEN_FILE)
        token = oidc_grant_token() #first initialization
        store_token(TOKEN_FILE, token)
        bridge.put("REST_CALL_STATUS", "200")
        print('200,OK')
        exit(0)
except Exception as e:
    bridge.put("REST_CALL_STATUS", "500")
    print('500,CLIENT_UNEXPECTED_ERROR,'+str(e))
    exit(1)
