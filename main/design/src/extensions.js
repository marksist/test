﻿UU5.Common.environment.extensions.uclGoodymat = {
  cache: {},

  _loadServerCall: function (callName, dtoIn) {
    var env = UU5.UclGoodymat.environment;
    var url = env.applicationUri.concat('/', env.tenantId, '/web/', dtoIn.uri, '/', callName);
    if (UU5.Common.environment.extensions.uclGoodymat.cache[url]) {
      dtoIn.done(UU5.Common.environment.extensions.uclGoodymat.cache[url]);
    } else {
      $.ajax({
        url: url,
        xhrFields: { withCredentials: true },
        type: 'get',
        cache: false,
        contentType: false
      }).done((function (doneDtoIn) {
        var dtoOut = typeof doneDtoIn == 'string' ? JSON.parse(doneDtoIn) : doneDtoIn;
        UU5.Common.environment.extensions.uclGoodymat.cache[url] = dtoOut;
        dtoIn.done(dtoOut);
      }).bind(this)).fail(function (failDtoIn) {
        dtoIn.fail(typeof failDtoIn == 'string' ? JSON.parse(failDtoIn) : failDtoIn);
      });
    }
  },

  // calls for page must contain uri, uri is not dependent on own component
  loadPage: function(uri, dtoIn) {
    dtoIn = dtoIn || {};
    dtoIn.uri = dtoIn.uri || uri;
    this._loadServerCall('getPageData', dtoIn);
  },

  loadIndexPageData: function (dtoIn) {
    UU5.Common.environment.extensions.uclGoodymat.loadPage('template', dtoIn);
  }
};
