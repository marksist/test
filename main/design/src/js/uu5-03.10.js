/*
 * UU5
 */

"use strict";

var UU5 = UU5 || {};



'use strict';

var UU5 = UU5 || {};
UU5.Common = UU5.Common || {};

'use strict';

var UU5 = UU5 || {};
UU5.Common = UU5.Common || {};

UU5.Common.baseMixin = {

  statics: {
    UU5_Common_baseMixin: {
      warnings: {
        bsContainerStyleNotUsed: 'Property bsContainerStyle was not used. Component was not covered itself by container.',
        bsColWidthNotUsed: 'Property bsColWidth was not used. Component was not covered itself by column.'
      },
      errors: {
        idMissing: 'Id %s was not set.',
        parentMissing: 'Parent was not found in props.',
        childrenAreNotIndexedByParent: 'Children are not indexed by parent %s.',
        tagIsWrong: 'Wrong tag %s - element was not found.',
        notRequiredMixin: 'Registered mixin %s needs to require mixin %s in component %s!',
        invalidParentTagName: 'Parent %s is not %s.',
        invalidParentType: 'Parent %s has not function %s.'
      }
    }
  },

  propTypes: {
    id: React.PropTypes.string,
    name: React.PropTypes.string,
    title: React.PropTypes.string,
    className: React.PropTypes.string,
    mainAttrs: React.PropTypes.object,
    parent: React.PropTypes.object,
    ref_: React.PropTypes.func,

    // UU5.Layout.container
    bsContainerStyle: React.PropTypes.oneOf(['standard', 'fluid']),

    // UU5.Layout.row
    display: React.PropTypes.oneOf(['standard', 'flex', 'flex-full']),

    // UU5.Layout.column
    bsColWidth: React.PropTypes.oneOfType([React.PropTypes.shape({
      xs: React.PropTypes.number,
      sm: React.PropTypes.number,
      md: React.PropTypes.number,
      lg: React.PropTypes.number
    }), React.PropTypes.string // see bootstrap col width
    ])
  },

  getDefaultProps: function () {
    return {
      id: null,
      name: null,
      title: null,
      className: null,
      mainAttrs: null,
      parent: null,
      ref_: null,

      // UU5.Layout.container
      bsContainerStyle: null,

      // UU5.Layout.row
      display: 'standard',

      // UU5.Layout.column
      bsColWidth: null
    };
  },

  getInitialState: function () {
    // initialize
    this.registerMixin('UU5_Common_baseMixin');
    this.id = this.props.id === null ? this.generateUUID() : this.props.id;
    // state
    return null;
  },

  componentDidMount: function () {
    if (this.props.bsContainerStyle && !this.hasUU5_Layout_containerMixin) {
      var container = this.getParentByType('hasUU5_Layout_containerMixin');
      if (container && this.props.bsContainerStyle !== container.props.bsContainerStyle) {
        this.showWarning('bsContainerStyleNotUsed', null, {
          mixinName: 'UU5_Common_baseMixin',
          context: {
            containerStyle: container.props.bsContainerStyle,
            componentStyle: this.props.bsContainerStyle
          }
        });
      }
    }

    if (this.props.bsColWidth && !this.hasUU5_Layout_columnMixin) {
      var column = this.getParentByType('hasUU5_Layout_columnMixin');
      if (column && this.props.bsColWidth !== column.props.bsColWidth) {
        this.showWarning('bsColWidthNotUsed', null, {
          mixinName: 'UU5_Common_baseMixin',
          context: {
            columnWidth: column.props.bsColWidth,
            componentWidth: this.props.bsColWidth
          }
        });
      }
    }

    if (typeof this.props.ref_ === 'function') {
      this.props.ref_(this);
    }
  },

  componentWillReceiveProps: function (nextProps) {
    // set id only if it was set in nextProps
    if (nextProps.id !== undefined && nextProps.id !== null && nextProps.id !== this.getId()) {
      var parent = this.getParent();
      // if parent support rendered children register - methods unregisterRenderedChild, registerRenderedChild
      if (parent && parent.unregisterRenderedChild) {
        var index = this.getIndex();
        parent.unregisterRenderedChild(this);
        this.id = nextProps.id;
        parent.registerRenderedChild && parent.registerRenderedChild(this, index);
      } else {
        this.id = nextProps.id;
      }
    }
  },

  componentWillUnmount: function () {
    // if parent support rendered children register - unregisterRenderedChild
    var parent = this.getParent();
    parent && parent.unregisterRenderedChild && parent.unregisterRenderedChild(this);
  },

  // Interface

  hasUU5_Common_baseMixin: function () {
    return this.hasMixin('UU5_Common_baseMixin');
  },

  getEnvironment: function () {
    return UU5.Common.environment;
  },

  getTagName: function () {
    return this.constructor.tagName || 'unknownTagName';
  },

  getLayoutType: function () {
    return this.constructor.layoutType || 'unknownLayoutType';
  },

  getMixinRegister: function () {
    return this.mixinRegister = this.mixinRegister || [];
  },

  registerMixin: function (mixinName) {
    this.getMixinRegister().push(mixinName);

    var component = this;
    var requiredMixins = this.constructor[mixinName].requiredMixins;
    if (requiredMixins && requiredMixins.length) {
      requiredMixins.forEach(function (mixin) {
        if (!component.hasMixin(mixin)) {
          component.showError('notRequiredMixin', [mixinName, mixin, component.getTagName()], {
            mixinName: 'UU5_Common_baseMixin',
            context: { requiredMixins: requiredMixins }
          });
        }
      });
    }

    return this;
  },

  hasMixin: function (mixinName) {
    return this.getMixinRegister().indexOf(mixinName) !== -1;
  },

  getClassName: function (item, mixinName) {
    var classNames = mixinName ? this.constructor[mixinName] ? this.constructor[mixinName].classNames : null : this.constructor.classNames;

    return classNames && item ? classNames[item] : classNames;
  },

  getDefault: function (item, mixinName) {
    var defaults = mixinName ? this.constructor[mixinName] ? this.constructor[mixinName].defaults : null : this.constructor.defaults;

    return defaults && item ? defaults[item] : defaults;
  },

  getLSI: function (item, mixinName) {
    var lsi = mixinName ? this.constructor[mixinName] ? this.constructor[mixinName].lsi : null : this.constructor.lsi;

    return lsi && item ? lsi[item] : lsi;
  },

  getLSIComponent: function (item, mixin) {
    return React.createElement(UU5.Bricks.lsi, { lsi: this.getLSI(item, mixin) });
  },

  getLSIValue: function (item, mixin) {
    return this.getLSIItemByLanguage( this.getLSI(item, mixin));
  },

  getLimit: function (item, mixinName) {
    var limits = mixinName ? this.getEnvironment().limits[mixinName] : this.constructor[mixinName] ? this.constructor[mixinName].limits : this.constructor.limits;

    return limits && item ? limits[item] : limits;
  },

  getError: function (item, mixinName) {
    var errors = mixinName ? this.constructor[mixinName] ? this.constructor[mixinName].errors || {} : {} : this.constructor.errors || {};
    return item ? errors[item] || item : errors;
  },

  getWarning: function (item, mixinName) {
    var warnings = mixinName ? this.constructor[mixinName] ? this.constructor[mixinName].warnings || {} : {} : this.constructor.warnings || {};
    return item ? warnings[item] || item : warnings;
  },

  getCallName: function (item, mixinName) {
    var calls = mixinName ? this.constructor[mixinName] ? this.constructor[mixinName].calls || {} : {} : this.constructor.calls || {};
    return item ? calls[item] || null : calls;
  },

  getId: function () {
    var id = this.id;
    !id && this.showError('idMissing', id, {
      mixinName: 'UU5_Common_baseMixin'
    });
    return id;
  },

  getName: function () {
    return this.props.name;
  },

  getTitle: function () {
    return this.props.title;
  },

  getProps: function (prop) {
    return this.props ? prop ? this.props[prop] : this.props : null;
  },

  getUU5_Common_baseMixinProps: function () {
    return {
      id: this.getId(),
      name: this.getName(),
      title: this.getTitle(),
      className: this.props.className,
      mainAttrs: this.props.mainAttrs,
      parent: this.getParent(),
      bsContainerStyle: this.props.bsContainerStyle,
      display: this.props.display,
      bsColWidth: this.props.bsColWidth
    };
  },

  getUU5_Common_baseMixinPropsToPass: function (suffix) {
    suffix = suffix || 'inner';

    var props = {
      id: this.getId() + '-' + suffix,
      name: this.getName() ? this.getName() + '-' + suffix : null,
      title: this.getTitle(),
      className: this.getFullClassName(),
      parent: this,
      bsContainerStyle: this.props.bsContainerStyle,
      display: this.props.display,
      bsColWidth: this.props.bsColWidth
    };

    var mainAttrs = this.props.mainAttrs;
    var extend = true;
    if (mainAttrs && mainAttrs.className) {
      extend && (mainAttrs = $.extend(true, {}, mainAttrs)) && (extend = false);
      delete mainAttrs.className;
    }
    if (this.getTitle()) {
      extend && (mainAttrs = $.extend(true, {}, mainAttrs));
      mainAttrs.title = this.getTitle() || mainAttrs.title;
    }

    props.mainAttrs = mainAttrs;

    return props;
  },

  // mixinNames = mixins to choose from
  getMainPropsToPass: function (mixinNames) {
    var params = [true, {}];

    var component = this;
    mixinNames = mixinNames || this.getMixinRegister();
    mixinNames.forEach(function (mixinName) {
      var propsToPassFunction = component['get' + mixinName + 'PropsToPass'];
      propsToPassFunction && params.push(propsToPassFunction());
    });

    return $.extend.apply(null, params);
  },

  getMainAttrs: function () {
    var newMainAttrs = $.extend(true, {}, this.getUU5_Common_baseMixinProps().mainAttrs, {title: this.props.title});
    this.buildMainAttrs && (newMainAttrs = this.buildMainAttrs(newMainAttrs));
    return newMainAttrs;
  },

  getWidth: function () {
    return $(this.findDOMNode()).width();
  },

  getInnerWidth: function () {
    return $(this.findDOMNode()).innerWidth();
  },

  getOuterWidth: function (withMargin) {
    return $(this.findDOMNode()).outerWidth(!!withMargin);
  },

  getHeight: function () {
    return $(this.findDOMNode()).height();
  },

  getInnerHeight: function () {
    return $(this.findDOMNode()).innerHeight();
  },

  getOuterHeight: function (withMargin) {
    return $(this.findDOMNode()).outerHeight(!!withMargin);
  },

  getParent: function () {
    return this.props.parent;
  },

  getParentByType: function (typeFunction) {
    var parent = this.getParent && this.getParent();
    while (parent && (typeof parent[typeFunction] !== 'function' || !parent[typeFunction]())) {
      parent = parent.getParent && parent.getParent();
    }
    return parent;
  },

  getFileName: function (path) {
    return path.replace(/^.*[\\\/]/, '');
  },

  checkParentTagName: function (parentTagNames) {
    parentTagNames = Array.isArray(parentTagNames) ? parentTagNames : [parentTagNames];
    var parent = this.getParent();
    var currentParentTagName = parent && parent.getTagName();
    var result = !currentParentTagName || parentTagNames.indexOf(currentParentTagName) == -1;
    if (result) {
      this.showError('invalidParentTagName', [currentParentTagName, parentTagNames.join(' or ')], {
        mixinName: 'UU5_Common_baseMixin'
      });
    }
    return result;
  },

  checkParentType: function (typeFunction) {
    var parent = this.getParent();
    var parentTypeFunction = parent && parent[typeFunction];
    var result = parentTypeFunction !== 'function' || !parentTypeFunction();
    if (result) {
      this.showError('invalidParentType', [parent ? parent.getTagName() : null, typeFunction], {
        mixinName: 'UU5_Common_baseMixin'
      });
    }
    return result;
  },

  getIndex: function () {
    var parent = this.getParent();
    var index = null;
    // parent should support children index - method getChildIndexById
    if (parent && parent.getChildIndexById) {
      index = parent.getChildIndexById(this.getId());
    } else {
      this.showError('childrenAreNotIndexedByParent', parent ? parent.getTagName() : null, {
        mixinName: 'UU5_Common_baseMixin',
        context: { parent: parent }
      });
    }
    return index;
  },

  // Component helpers for React

  getChildTag: function (child) {
    // react child type
    return child.type;
  },

  getChildDisplayName: function (child) {
    var tag = this.getChildTag(child);
    return typeof tag === 'function' ? tag.displayName : tag;
  },

  getChildTagName: function (child) {
    // UU5 tagNames or standard DOM tags ('div', 'span',...)
    var tag = this.getChildTag(child);
    return typeof tag === 'function' ? tag.tagName : tag;
  },

  getFullClassName: function (className) {
    var component = this;
    var myClassName = className || 'main';
    var classArray = [];
    var classes = {
      main: this.getClassName(myClassName),
      props: this.props.className,
      mainAttrs: this.props.mainAttrs && this.props.mainAttrs.className
    };

    this.getMixinRegister().forEach(function (v) {
      var mixinClassName = v + '_' + myClassName;
      classes[mixinClassName] = component.getClassName(myClassName, v);
      classArray.push(mixinClassName);
    });
    classArray.push(myClassName);
    classArray.push('props');
    classArray.push('mainAttrs');

    return this.buildClasses(classes, classArray);
  },

  // languagesString = 'cs-CZ,en;q=0.6,sk;q=0.8' => [{language: 'cs', location: 'cs-cz', q: 1.0}, {language: 'sk', q:
  // 0.8}, {language: 'en', q: 0.6}] languagesString = 'cs' => [{language: 'cs', q: 1.0}] languagesString =
  // 'en;q=0.6,sk;q=0.8' => [{language: 'sk', q: 0.8}, {language: 'en', q: 0.6}]
  sortLanguages: function (languagesString) {
    // languagesString = 'cs-CZ,en;q=0.6,sk;q=0.8' => languagesSplitter = ['cs-CZ', 'en;q=0.6', 'sk;q=0.8']
    var languagesSplitter = languagesString.toLowerCase().split(',');

    var languages = {};
    languagesSplitter.forEach(function (lang) {
      var language = {};

      var langOpt = lang.split(';');
      var langStr = langOpt[0];
      var q = 1; // quality factor
      if (langOpt.length > 1) {
        langStr = langOpt[0];
        q = parseFloat(langOpt[1].split('=')[1]);
      }

      var langStrSplitter = langStr.split('-');
      language.language = langStrSplitter[0];
      langStrSplitter[1] && (language.location = langStr);

      if (languages[lang]) {
        languages[lang].q < q && (languages[lang].q = q);
      } else {
        language.q = q;
        languages[lang] = language;
      }
    });

    // languagesArray = [{language: 'cs', location: 'cs-CZ', q: 1.0}, {language: 'en', q: 0.6}, {language: 'sk', q:
    // 0.8}]
    var languagesArray = Object.keys(languages).map(function (lang) {
      return languages[lang];
    });

    // [{language: 'cs', location: 'cs-CZ', q: 1.0}, {language: 'sk', q: 0.8}, {language: 'en', q: 0.6}]
    return languagesArray.sort(function (lang1, lang2) {
      var result = 0;
      if (lang1.q < lang2.q) {
        result = 1;
      } else if (lang1.q > lang2.q) {
        result = -1;
      }
      return result;
    });
  },

  setEnvironmentLanguages: function (languages) {
    this.getEnvironment().languages = this.sortLanguages(languages);
    return this;
  },

  getEnvironmentLanguages: function () {
    return this.getEnvironment().languages;
  },

  setGlobalLanguage: function (language) {
    this.setEnvironmentLanguages(language);
    if (UU5.Common.lsiMixin) {
      $(window).trigger(UU5.Common.lsiMixin.statics.UU5_Common_lsiMixin.lsiEvent, language);
    }
    return this;
  },

  getLSIItemByLanguage: function (lsi, languages) {
    languages = languages || this.getEnvironmentLanguages();
    var result = null;

    if (lsi) {
      var keys = Object.keys(lsi);
      var resLang = keys[0];

      for (var i = 0; i < languages.length; i++) {
        var lang = languages[i];

        if (lsi[lang.location]) {
          resLang = lang.location;
          break;
        } else if (lsi[lang.language]) {
          resLang = lang.language;
          break;
        } else {
          var lsiKeys = keys.filter(function (key) {
            return key.match("^" + lang.language);
          });
          if (lsiKeys.length) {
            resLang = lsiKeys[0];
            break;
          }
        }
      }

      result = lsi[resLang];
    }

    return result;
  },

  generateUUID: function () {
    var d = new Date().getTime();
    if (window.performance && typeof window.performance.now === 'function') {
      d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c == 'x' ? r : r & 0x3 | 0x8).toString(16);
    });
  },

  buildClasses: function (classes, keys) {
    var className = '';
    classes && keys.forEach(function (v) {
      classes[v] && (className += ' ' + classes[v]);
    });
    return className.trim();
  },

  isInClasses: function (classes, regExp) {
    var classesArray = classes ? classes.split(' ') : [];
    var result = false;
    while (!result && classesArray.length) {
      classesArray[0].match(regExp) && (result = true);
      classesArray.shift();
    }
    return result;
  },

  buildCounterCallback: function (callback, count) {
    /*
     Method wrap (function) callback by newCallBack.
     If newCallBack is used, increase closureCounter
     but call callback just if closureCounter === count.
     You can use this 'tricky' method, when you want to call callback
     just once but you have to send it to several methods.
     See examples!!!
     */
    var newCallback = null;
    if (typeof callback === 'function') {
      var closureCounter = 0;
      newCallback = function () {
        closureCounter++;
        closureCounter === count && callback();
      };
    }
    return newCallback;
  },

  checkTag: function (tag) {
    return UU5.Tools.checkTag(tag);
  },

  stringToObjectType: function (string, type, prefix) {
    var calculated = prefix || window;
    if (typeof string === 'string') {
      var sArray = string.split('.');
      while (calculated && sArray.length > 0) {
        calculated = calculated[sArray.shift()];
      }
    }
    if (typeof calculated !== type) {
      this.showError('tagIsWrong', string, {
        mixinName: 'UU5_Common_baseMixin',
        context: {
          notFoundObject: calculated,
          notFoundObjectType: typeof calculated,
          checkedType: type,
          prefix: prefix
        }
      });
      calculated = null;
    }
    return calculated;
  },

  childToBodyItem: function (child) {
    return { tag: this.getChildTagName(child), props: $.extend(true, {}, child.props) };
  },

  switchChildrenToBody: function (props) {
    var newProps = $.extend(true, {}, props || this.props);
    var children = newProps.children;
    newProps.children = null;
    newProps.body = React.Children.map(children, this.childToBodyItem);
    return newProps;
  },

  findDOMNode: function () {
    return ReactDOM.findDOMNode(this);
  },

  exportToObject: function () {
    return {
      tag: this.getTagName(),
      props: $.extend(true, {}, this.props),
      state: $.extend(true, {}, this.state)
    };
  },

  formatString: function (string, stringParams) {
    var result;
    stringParams = stringParams && !Array.isArray(stringParams) ? [stringParams] : stringParams;

    if (string.indexOf('%s') > -1 || string.indexOf('%d') > -1) {
      result = this._replaceParamsInString(string, stringParams);
    } else {
      result = this._setParamsToString(string, stringParams);
    }
    return result;
  },

  // msgParams could be array of params or just one param
  showError: function (msgKey, msgParams, opt) {
    opt = opt || {};
    var msg = this.getError(msgKey, opt.mixinName);
    console.error.apply(null, this._getConsoleLogParams(msg, msgParams, opt.context));
    return this;
  },

  // msgParams could be array of params or just one param
  showWarning: function (msgKey, msgParams, opt) {
    opt = opt || {};
    var msg = this.getWarning(msgKey, opt.mixinName);
    console.warn.apply(null, this._getConsoleLogParams(msg, msgParams, opt.context));
    return this;
  },

  replaceByHardSpace: function (text, language) {
    var replacer = language ? UU5.Common.environment.hardSpace.lsiReplacer[language] : null;
    replacer = replacer || this.getLSIItemByLanguage(UU5.Common.environment.hardSpace.lsiReplacer);
    return replacer ? replacer(text) : text;
  },

  // Overriding Functions

  // Component Specific Helpers

  _replaceParamsInString: function (string, stringParams) {
    var component = this;
    var i = 0;
    return string.replace(/%((%)|s|d)/g, function (match) {
      // match is the matched format, e.g. %s, %d
      var val = null;
      if (match[2]) {
        val = match[2];
      } else if (stringParams) {
        val = stringParams[i];
        // A switch statement so that the formatter can be extended. Default is %s
        switch (match) {
          case '%d':
            var parsedVal = parseFloat(val);
            if (isNaN(parsedVal)) {
              // cannot use showWarning because of this method is used in showWarning !!!
              console.warn(component.getTagName(), 'Value ' + val + ' is not number!', {
                string: string,
                stringParams: stringParams
              });
              val = '%d';
            }
            break;
        }
        i++;
      }
      return val;
    });
  },

  _setParamsToString: function (string, stringParams) {
    return string.replace(/{(\d+)}/g, function (match, number) {
      return stringParams && typeof stringParams[number] != 'undefined' ? stringParams[number] : match;
    });
  },

  _getConsoleLogParams: function (msg, msgParams, context) {
    var title = this.getTagName() + ' [' + this.getId() + ']:';
    var msgWithParams = msg ? this.formatString(msg, msgParams) : msgParams || null;

    context = context || {};
    context.thisTagName = this.getTagName();
    context.this = this;
    context.thisProps = this.props;
    context.thisState = this.state;

    return [title, msgWithParams, context];
  }
};

'use strict';

var UU5 = UU5 || {};
UU5.Common = UU5.Common || {};

UU5.Common.elementaryMixin = {

  statics: {
    UU5_Common_elementaryMixin: {
      requiredMixins: ['UU5_Common_baseMixin'],
      classNames: {
        hidden: 'UU5_Common-hidden',
        selected: 'UU5_Common-selected',
        disabled: 'UU5_Common-disabled',
        disabledCover: 'UU5_Common-disabledCover',
        disabledCoverTransparent: 'UU5_Common-disabledCoverTransparent'
      }
    }
  },

  propTypes: {
    hidden: React.PropTypes.bool,
    disabled: React.PropTypes.bool,
    selected: React.PropTypes.bool
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      hidden: false,
      disabled: false,
      selected: false
    };
  },

  getInitialState: function () {
    // initialize
    this.registerMixin('UU5_Common_elementaryMixin');
    // state
    return {
      hidden: this.props.hidden,
      disabled: this.props.disabled,
      selected: this.props.selected
    };
  },

  componentWillReceiveProps: function (nextProps) {
    this.setState({
      hidden: nextProps.hidden,
      disabled: nextProps.disabled,
      selected: nextProps.selected
    });
  },

  // Interface

  hasUU5_Common_elementaryMixin: function () {
    return this.hasMixin('UU5_Common_elementaryMixin');
  },

  getUU5_Common_elementaryMixinProps: function () {
    return {
      hidden: this.isHidden(),
      disabled: this.isDisabled(),
      selected: this.isSelected()
    };
  },

  getUU5_Common_elementaryMixinPropsToPass: function () {
    return this.getUU5_Common_elementaryMixinProps();
  },

  setHiddenValue: function (value, setStateCallback) {
    this.setState({hidden: value}, setStateCallback);
    return this;
  },

  hide: function (setStateCallback) {
    if (typeof this.hide_ === 'function') {
      this.hide_(setStateCallback);
    } else {
      this.setHiddenValue(true, setStateCallback);
    }
    return this;
  },

  show: function (setStateCallback) {
    if (typeof this.show_ === 'function') {
      this.show_(setStateCallback);
    } else {
      this.setHiddenValue(false, setStateCallback);
    }
    return this;
  },

  isHidden: function () {
    return this.state.hidden;
  },

  toggleHide: function (setStateCallback) {
    if (typeof this.toggleHide_ === 'function') {
      this.toggleHide_(setStateCallback);
    } else {
      this.setState(function (state) {
        return {hidden: !state.hidden};
      }, setStateCallback);
    }
    return this;
  },

  setDisabledValue: function (value, setStateCallback) {
    this.setState({disabled: value}, setStateCallback);
    return this;
  },

  disable: function (setStateCallback) {
    if (typeof this.disable_ === 'function') {
      this.disable_(setStateCallback);
    } else {
      this.setDisabledValue(true, setStateCallback);
    }
    return this;
  },

  enable: function (setStateCallback) {
    if (typeof this.enable_ === 'function') {
      this.enable_(setStateCallback);
    } else {
      this.setDisabledValue(false, setStateCallback);
    }
    return this;
  },

  isDisabled: function () {
    return this.state.disabled;
  },

  toggleDisable: function (setStateCallback) {
    if (typeof this.toggleDisable_ === 'function') {
      this.toggleDisable_(setStateCallback);
    } else {
      this.setState(function (state) {
        return {disabled: !state.disabled};
      }, setStateCallback);
    }
    return this;
  },

  setSelectedValue: function (value, setStateCallback) {
    this.setState({selected: value}, setStateCallback);
    return this;
  },

  select: function (setStateCallback) {
    if (typeof this.select_ === 'function') {
      this.select_(setStateCallback);
    } else {
      this.setSelectedValue(true, setStateCallback);
    }
    return this;
  },

  deselect: function (setStateCallback) {
    if (typeof this.deselect_ === 'function') {
      this.deselect_(setStateCallback);
    } else {
      this.setSelectedValue(false, setStateCallback);
    }
    return this;
  },

  isSelected: function () {
    return this.state.selected;
  },

  toggleSelect: function (setStateCallback) {
    if (typeof this.toggleSelect_ === 'function') {
      this.toggleSelect_(setStateCallback);
    } else {
      this.setState(function (state) {
        return {selected: !state.selected};
      }, setStateCallback);
    }
    return this;
  },

  buildMainAttrs: function (mainAttrs) {
    var newMainAttrs = $.extend(true, {}, mainAttrs || (this.getUU5_Common_baseMixinProps && this.getUU5_Common_baseMixinProps().mainAttrs), {title: this.props.title});
    newMainAttrs.className = this.getHiddenClassName(this.getFullClassName());
    newMainAttrs.className = this.getDisabledClassName(newMainAttrs.className);
    newMainAttrs.className = this.getSelectedClassName(newMainAttrs.className);
    return newMainAttrs;
  },

  getHiddenClassName: function (className) {
    var hiddenClassName = '';
    if (this.isHidden()) {
      hiddenClassName = this.getClassName('hidden') || this.getClassName(null, 'UU5_Common_elementaryMixin').hidden;
    }
    return className ? (className + ' ' + hiddenClassName).trim() : hiddenClassName;
  },

  getDisabledClassName: function (className) {
    var disabledClassName = '';
    if (this.isDisabled()) {
      disabledClassName = this.getClassName('disabled') || this.getClassName(null, 'UU5_Common_elementaryMixin').disabled;
    }
    return className ? (className + ' ' + disabledClassName).trim() : disabledClassName;
  },

  getSelectedClassName: function (className) {
    var selectedClassName = '';
    if (this.isSelected()) {
      selectedClassName = this.getClassName('selected') || this.getClassName(null, 'UU5_Common_elementaryMixin').selected;
    }
    return className ? (className + ' ' + selectedClassName).trim() : selectedClassName;
  },

  getDisabledCover: function () {
    var className = this.getClassName('disabledCover') || this.getClassName(null, 'UU5_Common_elementaryMixin').disabledCover;
    return this.isDisabled() ? React.createElement('span', {className: className}) : null;
  },

  getDisabledCoverTransparent: function () {
    var className = this.getClassName('disabledCoverTransparent') || this.getClassName(null, 'UU5_Common_elementaryMixin').disabledCoverTransparent;
    return this.isDisabled() ? React.createElement('span', {className: className}) : null;
  }

  // Overriding Functions

  // Component Specific Helpers

};

'use strict';

var UU5 = UU5 || {};
UU5.Common = UU5.Common || {};

UU5.Common.levelMixin = {

  statics: {
    UU5_Common_levelMixin: {
      requiredMixins: ['UU5_Common_baseMixin'],
      defaults: {
        maxLevel: 6
      },
      warnings: {
        levelMismatch: 'Component level %s is lower than parent level %s.',
        levelMax: 'Maximum level of component is 6 but is set %d.'
      }
    }
  },

  propTypes: {
    level: React.PropTypes.oneOf(['0', '1', '2', '3', '4', '5', '6', 0, 1, 2, 3, 4, 5, 6])
  },

  getDefaultProps: function () {
    return {
      level: null
    };
  },

  getInitialState: function () {
    // initialize
    this.registerMixin('UU5_Common_levelMixin');
    // state
    return {
      level: null
    };
  },

  componentWillMount: function () {
    this.setState({ level: this.checkLevel() });
  },

  componentWillReceiveProps: function (nextProps) {
    this.getLevel() !== nextProps.level && this.setState({ level: this.checkLevel() });
  },

  // Interface

  hasUU5_Common_levelMixin: function () {
    return this.hasMixin('UU5_Common_levelMixin');
  },

  getLevel: function () {
    return this.state.level;
  },

  getUU5_Common_levelMixinProps: function () {
    return {
      level: this.props.level
    };
  },

  getUU5_Common_levelMixinPropsToPass: function () {
    return this.getUU5_Common_levelMixinProps();
  },

  shouldIncreaseLevel: function () {
    return !this.constructor.isDummyLevel && (
        this.getHeader && this.getHeader() ||
        this.getFooter && this.getFooter()
      );
  },

  checkLevel: function () {
    var level = typeof this.props.level === 'string' ? parseInt(this.props.level) : this.props.level;
    var maxLevel = this.getDefault('maxLevel', 'UU5_Common_levelMixin');
    var parentLevelComponent = this.getParentByType('hasUU5_Common_levelMixin');
    while (parentLevelComponent && parentLevelComponent.constructor.isDummyLevel) {
      parentLevelComponent = parentLevelComponent.getParentByType('hasUU5_Common_levelMixin');
    }

    if (level > maxLevel) {
      this.showWarning(
        'levelMax', level, {
          mixinName: 'UU5_Common_levelMixin',
          context: {
            parent: {
              tagName: parentLevelComponent && parentLevelComponent.getTagName(),
              component: parentLevelComponent
            }
          }
        }
      );
      level = maxLevel;
    }

    if (parentLevelComponent) {
      var parentLevel = parentLevelComponent.getLevel();
      if (this.shouldIncreaseLevel && this.shouldIncreaseLevel()) {
        level = level || (
            parentLevel >= maxLevel ? maxLevel : parentLevel + 1
          );
        if (level <= parentLevel) {
          this.showWarning(
            'levelMismatch', [level, parentLevel], {
              mixinName: 'UU5_Common_levelMixin',
              context: {
                parent: {
                  tagName: parentLevelComponent.getTagName(),
                  component: parentLevelComponent
                }
              }
            }
          );
        }
      } else {
        level = level || parentLevel;
        if (level < parentLevel) {
          this.showWarning(
            'levelMismatch', [level, parentLevel], {
              mixinName: 'UU5_Common_levelMixin',
              context: {
                parent: {
                  tagName: parentLevelComponent.getTagName(),
                  component: parentLevelComponent
                }
              }
            }
          );
        }
      }
    }

    return level;
  }

  // Overriding Functions

  // Component Specific Helpers

};

'use strict';

var UU5 = UU5 || {};
UU5.Common = UU5.Common || {};

UU5.Common.contentMixin = {

  statics: {
    UU5_Common_contentMixin: {
      requiredMixins: ['UU5_Common_baseMixin'],
      defaults: {
        standardMode: 'standard',
        outlineMode: 'outline'
      },
      errors: {
        unexpectedContentType: 'Type "%s" of content property is unexpected.',
        dynamicOnly: 'Method %s can be used just for dynamic content.',
        insertedChildIdNotExists: 'Child with ID %s does not exist.',
        indexNotSet: 'In this case index has to be specified.',
        addRenderedChildToIdList_IdDuplicity: 'There is duplicity ID %s in adding rendered child to the list of children by ID.',
        addRenderedChildToNameList_IdDuplicity: 'There is Name duplicity in adding rendered child to the list children by Name.'
      }
    }
  },

  propTypes: {
    content: React.PropTypes.oneOfType([
    // content body:[{tag:'',props:{}},{tag:'',props:{}},...]
    React.PropTypes.arrayOf(React.PropTypes.shape({
      tag: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.element]),
      props: React.PropTypes.object
    })),
    // content bodyItem:{tag:'',props{}}
    React.PropTypes.shape({
      tag: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.element]),
      props: React.PropTypes.arrayOf(React.PropTypes.object)
    }),
    // content items:{tag:'',propsArray:[{},{},{},...]}
    React.PropTypes.shape({
      tag: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.element]),
      propsArray: React.PropTypes.arrayOf(React.PropTypes.object)
    }),
    // content node
    React.PropTypes.node]),
    ignoreInnerHTML: React.PropTypes.bool,
    ignoreSpaces: React.PropTypes.bool,
    ignoreGrammar: React.PropTypes.bool,
    dynamic: React.PropTypes.bool,
    mode: React.PropTypes.oneOf(['standard', 'outline'])
  },

  getDefaultProps: function () {
    return {
      content: null,
      ignoreInnerHTML: false,
      ignoreSpaces: false,
      ignoreGrammar: false,
      dynamic: false,
      mode: 'standard'
    };
  },

  getInitialState: function () {
    // initialize
    this.registerMixin('UU5_Common_contentMixin');

    this.renderedChildren = []; // [renderedChild,renderedChild,...]
    this.renderedChildrenIdList = {}; // {id:renderedChild,id:renderedChild,...}
    this.renderedChildrenNameList = {}; // {name:renderedChild,name:renderedChild,...}

    // state
    var state = {
      mode: this.props.mode
    };

    if (this.isDynamic()) {
      state.children = this.buildChildren(this.props);
      state.filter = null;
      state.filteredProps = null;
      state.sorter = null;
      state.sortedIds = null;
    }

    return state;
  },

  componentWillMount: function () {
    this.isDynamic() && this.setChildren(this.buildChildren());
  },

  componentWillReceiveProps: function (nextProps) {
    this.isDynamic() && this.setChildren(this.buildChildren(nextProps));
    nextProps.mode !== this.props.mode && this.setState({ mode: nextProps.mode });
  },

  // Interface

  hasUU5_Common_contentMixin: function () {
    return this.hasMixin('UU5_Common_contentMixin');
  },

  isDynamic: function () {
    return this.props.dynamic;
  },

  getContent: function () {
    return this.props.content;
  },

  getUU5_Common_contentMixinProps: function () {
    return {
      content: this.getContent(),
      dynamic: this.isDynamic()
    };
  },

  getUU5_Common_contentMixinPropsToPass: function () {
    return $.extend(true, {}, this.getUU5_Common_contentMixinProps());
  },

  expandChildProps: function (prevChild, childIndex) {
    var newChildProps = prevChild.props;
    newChildProps = $.extend(true, {}, newChildProps);

    var isUU5Child = typeof prevChild.type === 'function';

    if (isUU5Child) {
      newChildProps.parent = this;

      this.isDynamic() && (newChildProps.id = newChildProps.id || this.generateUUID());

      if (typeof this.expandChildProps_ === 'function') {
        var tempChild = React.cloneElement(prevChild, newChildProps);
        newChildProps = this.expandChildProps_(tempChild, childIndex);
      }
    }

    newChildProps.key = newChildProps.id || childIndex;

    if (isUU5Child) {
      newChildProps.ref = function (renderedChild) {
        renderedChild && this.registerRenderedChild(renderedChild, childIndex);
      }.bind(this);
    }

    return newChildProps;
  },

  buildChild: function (childTag, childProps, children) {
    return React.createElement(this.checkTag(childTag), childProps, children);
  },

  cloneChild: function (child, props) {
    var clonedChild = React.cloneElement(child, props);
    if (this.expandChild_) {
      clonedChild = this.expandChild_(clonedChild, props.key);
    }
    return clonedChild;
  },

  buildStringToChildren: function (string) {
    var stringChildren = string;
    if (typeof string === 'string' && !this.props.ignoreInnerHTML) {
      stringChildren = UU5.Tools.getChildrenFromHTML(string);
    }
    return stringChildren;
  },

  buildNodeChildren: function (children, childPropsExpander) {
    var content = this;
    var newChildren = [];
    React.Children.forEach(children, function (child, i) {
      if (child) {
        if (content.shouldChildRender(child)) {
          if (typeof child === 'object') {
            var newChildProps = childPropsExpander(child, i);
            var newChild = content.cloneChild(child, newChildProps);

            // text
          } else {
            // it does not need to cover just spaces by textCorrector
            if (typeof child === 'number' || child.trim() === '') {
              newChild = child;
            } else {
              newChild = React.createElement(UU5.Bricks.textCorrector, {
                text: child,
                ignoreSpaces: content.props.ignoreSpaces,
                ignoreGrammar: content.props.ignoreGrammar,
                language: content.props.language,
                key: i
              });
              if (content.expandChild_) {
                newChild = content.expandChild_(newChild, i);
              }
            }
          }
          newChildren.push(newChild);
        }
      }
    });

    return newChildren;
  },

  shouldChildRender: function (child) {
    var result = true;
    if (typeof this.shouldChildRender_ === 'function') {
      result = this.shouldChildRender_(child);
    }
    return result;
  },

  buildChildren: function (contentProps, childPropsExpander) {
    var content = this;
    var children = null;

    if (typeof this.buildChildren_ === 'function') {
      children = this.buildChildren_(contentProps, childPropsExpander);
    } else {
      contentProps = contentProps || this.props;
      childPropsExpander = childPropsExpander || this.expandChildProps;
      var contentType = this._getContentType(contentProps.content);

      switch (contentType) {
        case 'bodyItem':
          var bodyItemChild = content.buildChild(contentProps.content.tag, contentProps.content.props);
          content.shouldChildRender(bodyItemChild) && (children = [content.cloneChild(bodyItemChild, childPropsExpander(bodyItemChild, 0))]);
          break;
        case 'body':
          children = [];
          contentProps.content.forEach(function (bodyItem, i) {
            var child = content.buildChild(bodyItem.tag, bodyItem.props);
            child = content.cloneChild(child, childPropsExpander(child, i));
            content.shouldChildRender(child) && children.push(child);
          });
          break;
        case 'items':
          var tag = content.checkTag(contentProps.content.tag);
          children = [];
          contentProps.content.propsArray.forEach(function (props, i) {
            var child = content.buildChild(tag, props);
            child = content.cloneChild(child, childPropsExpander(child, i));

            content.shouldChildRender(child) && children.push(child);
          });
          break;
        case 'string':
          var stringChildren = content.buildStringToChildren(contentProps.content);
          children = this.buildNodeChildren(stringChildren, childPropsExpander);
          break;
        case 'node':
          var nodeChildren = React.Children.map(contentProps.content, function (node) {
            return typeof node === 'string' ? content.buildStringToChildren(node) : node;
          });
          children = this.buildNodeChildren(nodeChildren, childPropsExpander);
          break;
        case 'children':
        default:
          if (contentProps.children) {
            children = this.buildNodeChildren(contentProps.children, childPropsExpander);
          }
      }
    }

    return children;
  },

  getStandardChildren: function () {
    return this.isDynamic() ? this.state.children : this.buildChildren();
  },

  getOutlineChildren: function () {
    return React.createElement(UU5.Bricks.outline, { element: this, key: 0 });
  },

  getChildren: function () {
    var children = null;

    switch (this.getMode()) {
      case this.getDefault('standardMode', 'UU5_Common_contentMixin'):
        children = this.getStandardChildren();
        break;
      case this.getDefault('outlineMode', 'UU5_Common_contentMixin'):
        children = this.getOutlineChildren();
        break;
    }

    return children;
  },

  getRenderedChildren: function () {
    return this.renderedChildren;
  },

  setRenderedChildren: function (childrenIndexList) {
    this.renderedChildren = childrenIndexList;
    return this;
  },

  addRenderedChild: function (renderedChild, index) {
    if (index === undefined || index === null) {
      this.showError('indexNotSet', null, {
        mixinName: 'UU5_Common_contentMixin',
        context: {
          index: index,
          renderedChild: {
            tagName: renderedChild.getTagName(),
            id: renderedChild.getId(),
            component: renderedChild
          }
        }
      });
    } else {
      this.getRenderedChildren().splice(index, 0, renderedChild);
    }
    return this;
  },

  removeRenderedChild: function (renderedChild) {
    this.getRenderedChildren().splice(renderedChild.getIndex(), 1);
    return this;
  },

  getRenderedChildrenIdList: function () {
    return this.renderedChildrenIdList;
  },

  setRenderedChildrenIdList: function (childrenIdList) {
    this.renderedChildrenIdList = childrenIdList;
    return this;
  },

  addRenderedChildToIdList: function (renderedChild) {
    var id = renderedChild.getId();
    if (!this.getRenderedChildById(id)) {
      this.getRenderedChildrenIdList()[id] = renderedChild;
    } else if (this.getRenderedChildById(id) !== renderedChild) {
      this.showError('addRenderedChildToIdList_IdDuplicity', id, {
        mixinName: 'UU5_Common_contentMixin',
        context: {
          renderedChildrenIdList: this.getRenderedChildrenIdList(),
          renderedChild: {
            tagName: renderedChild.getTagName(),
            id: renderedChild.getId(),
            component: renderedChild
          }
        }
      });
    }
    return this;
  },

  removeRenderedChildFromIdList: function (renderedChild) {
    delete this.getRenderedChildrenIdList()[renderedChild.getId()];
    return this;
  },

  getRenderedChildrenNameList: function () {
    return this.renderedChildrenNameList;
  },

  setRenderedChildrenNameList: function (childrenNameList) {
    this.renderedChildrenNameList = childrenNameList;
    return this;
  },

  addRenderedChildToNameList: function (renderedChild) {
    var name = renderedChild.getName();
    if (name) {
      var nameList = this.getRenderedChildrenNameList();
      if (!nameList[name] || nameList[name].map(function (rChild) {
        return rChild.getId();
      }).indexOf(renderedChild.getId()) === -1) {

        nameList[name] = nameList[name] || [];
        nameList[name].push(renderedChild);
      }
    }
    return this;
  },

  removeRenderedChildFromNameList: function (renderedChild) {
    var nameChildren = this.getRenderedChildrenByName(renderedChild.getName());
    if (nameChildren) {
      var childIndex = nameChildren.map(function (rChild) {
        return rChild.getId();
      }).indexOf(renderedChild.getId());

      childIndex > -1 && nameChildren.splice(childIndex, 1);
    }
    return this;
  },

  getChildIndexById: function (childId) {
    var childIndex;

    if (typeof this.getChildIndexById_ === 'function') {
      childIndex = this.getChildIndexById_(childId);
    } else {
      var children = this.getRenderedChildren();

      var index = childId && children.map(function (child) {
        return child.getId() === childId;
      }).indexOf(true);

      childIndex = index === -1 ? null : index;
    }

    return childIndex;
  },

  getRenderedChildById: function (childId) {
    var renderedChild;

    if (typeof this.getRenderedChildById_ === 'function') {
      renderedChild = this.getRenderedChildById_(childId);
    } else {
      renderedChild = this.getRenderedChildrenIdList()[childId] || null;
    }

    return renderedChild;
  },

  getRenderedChildrenByName: function (childName) {
    return this.getRenderedChildrenNameList()[childName] || null;
  },

  getRenderedChildByName: function (childName) {
    var renderedChild;

    if (typeof this.getRenderedChildByName_ === 'function') {
      renderedChild = this.getRenderedChildByName_(childName);
    } else {
      var nameChildren = this.getRenderedChildrenByName(childName);
      renderedChild = nameChildren ? nameChildren[0] : null;
    }

    return renderedChild;
  },

  getRenderedChildByIndex: function (index) {
    var renderedChild;

    if (typeof this.getRenderedChildByIndex_ === 'function') {
      renderedChild = this.getRenderedChildByIndex_(index);
    } else {
      renderedChild = this.getRenderedChildren()[index] || null;
    }

    return renderedChild;
  },

  getRenderedChildByTagName: function (tagName) {
    var foundChild = null;

    if (typeof this.getRenderedChildByTagName_ === 'function') {
      foundChild = this.getRenderedChildByTagName_(tagName);
    } else {
      this.eachRenderedChild(function (renderedChild) {
        var condition = renderedChild.getTagName() === tagName;
        condition && (foundChild = renderedChild);
        return !condition; // false <=> end of cycle
      });
    }

    return foundChild;
  },

  getFirstRenderedChild: function () {
    var renderedChild = null;

    if (typeof this.getFirstRenderedChild_ === 'function') {
      renderedChild = this.getFirstRenderedChild_();
    } else {
      renderedChild = this.getRenderedChildByIndex(0);
    }

    return renderedChild;
  },

  getLastRenderedChild: function () {
    var renderedChild = null;

    if (typeof this.getLastRenderedChild_ === 'function') {
      renderedChild = this.getLastRenderedChild_();
    } else {
      renderedChild = this.getRenderedChildByIndex(this.getRenderedChildren().length - 1);
    }

    return renderedChild;
  },

  eachRenderedChild: function (callback) {
    // function callbackFunction( renderedChild, renderedChildIndex );
    if (typeof this.eachRenderedChild_ === 'function') {
      this.eachRenderedChild_(callback);
    } else {
      var renderedChildren = this.getRenderedChildren();

      // same as: for(var i = 0; i < renderedChildren.length; i++) {
      //          var renderedChild = renderedChildren[i];
      for (var i = 0, renderedChild; renderedChild = renderedChildren[i]; ++i) {
        var result = callback(renderedChild, i);
        if (result === false) {
          break;
        }
      }
    }
    return this;
  },

  registerRenderedChild: function (renderedChild, index) {
    if (renderedChild.hasUU5_Common_baseMixin && !this.getRenderedChildById(renderedChild.getId())) {
      this.addRenderedChild(renderedChild, index);
      this.addRenderedChildToIdList(renderedChild);
      this.addRenderedChildToNameList(renderedChild);
    }
    return this;
  },

  unregisterRenderedChild: function (renderedChild) {
    this.removeRenderedChild(renderedChild);
    this.removeRenderedChildFromIdList(renderedChild);
    this.removeRenderedChildFromNameList(renderedChild);
    return this;
  },

  // dynamic functions

  setChildren: function (newChildren, setStateCallback) {
    if (this.isDynamic()) {
      if (typeof this.setChildren_ === 'function') {
        this.setChildren_(newChildren, setStateCallback);
      } else {
        this.setState({ children: newChildren }, setStateCallback);
      }
    } else {
      this.showError('dynamicOnly', 'setChildren', {
        mixinName: 'UU5_Common_contentMixin'
      });
    }
    return this;
  },

  /**
   * Inserts child into container. If position is set, child will be added to that position.
   * Only for dynamic container.
   * Can be overridden by insertChild_ function.
   *
   * @param {object} childBodyItem - Object with tag of the element and props. E.g. {tag: '...', props: {...}}
   * @param {object} opt - Additional parameters.
   * @param {number} opt.position - Position where the child should be placed to. If not set, child is added
   *                                at the end of children list.
   * @param {boolean} opt.shouldUpdate - If false, setState is not called.
   * @param {function} opt.setStateCallback - Callback is called after setState.
   * @returns {object} this
   */
  insertChild: function (childBodyItem, opt) {
    /*
     opt:{
     position: number // Position where the child should be placed to.
     // If not set, child is added at the end of children list.
     shouldUpdate: bool // call setState -> render, default value is true
     callbackFunction: function // function callbackFunction(newRenderedChild)
     }
     */
    if (this.isDynamic()) {
      if (typeof this.insertChild_ === 'function') {
        this.insertChild_(childBodyItem, opt);
      } else {
        opt = opt || {};

        var children = this.getChildren();
        var newChild = this.buildChild(childBodyItem.tag, childBodyItem.props);
        if (this.shouldChildRender(newChild)) {
          newChild = this.cloneChild(newChild, this.expandChildProps(newChild, opt.position));

          if (typeof opt.position === 'number') {
            children.splice(opt.position, 0, newChild);
          } else {
            children.push(newChild);
          }

          if (opt.shouldUpdate === undefined || opt.shouldUpdate) {
            this.setChildren(children, typeof opt.setStateCallback === 'function' && function () {
              opt.setStateCallback(this.getRenderedChildById(newChild.props.id));
            }.bind(this));
          }
        }
      }
    } else {
      this.showError('dynamicOnly', 'insertChild', {
        mixinName: 'UU5_Common_contentMixin'
      });
    }

    return this;
  },

  /**
   * Inserts child into container at position before another child by ID.
   * Only for dynamic container.
   * Can be overridden by insertChildBefore_ function.
   *
   * @param {object} childBodyItem - Object with tag of the element and props. E.g. {tag: '...', props: {...}}
   * @param {object} opt - Additional parameters.
   * @param {string} opt.childAfterId - ID of the child before which the new child will be placed. If not set, the new
   *                                    child will be placed at the beginning.
   * @param {boolean} opt.shouldUpdate - If false, setState is not called.
   * @param {function} opt.setStateCallback - Callback is called after setState.
   * @returns {object} this
   */
  insertChildBefore: function (childBodyItem, opt) {
    if (this.isDynamic()) {
      if (typeof this.insertChildBefore_ === 'function') {
        this.insertChildBefore_(childBodyItem, opt);
      } else {
        opt = opt || {};

        var childIndex;
        if (opt.childAfterId) {
          var renderedChild = this.getRenderedChildById(opt.childAfterId);
          childIndex = renderedChild.getIndex();
        } else {
          childIndex = 0;
        }

        if (childIndex === null) {
          this.showError('insertedChildIdNotExists', opt.childAfterId, {
            mixinName: 'UU5_Common_contentMixin'
          });
        } else {
          this.insertChild(childBodyItem, $.extend({}, opt, { position: childIndex }));
        }
      }
    } else {
      this.showError('dynamicOnly', 'insertChildBefore', {
        mixinName: 'UU5_Common_contentMixin'
      });
    }

    return this;
  },

  /**
   * Inserts child into container at position after another child by ID.
   * Only for dynamic container.
   * Can be overridden by insertChildAfter_ function.
   *
   * @param {object} childBodyItem - Object with tag of the element and props. E.g. {tag: '...', props: {...}}
   * @param {object} opt - Additional parameters.
   * @param {string} opt.childBeforeId - ID of child after which the new child will be placed. If not set, the new
   *                                     child will be placed at the end.
   * @param {boolean} opt.shouldUpdate - If false, setState is not called.
   * @param {function} opt.setStateCallback - Callback is called after setState.
   * @returns {object} this
   */
  insertChildAfter: function (childBodyItem, opt) {
    if (this.isDynamic()) {
      if (typeof this.insertChildAfter_ === 'function') {
        this.insertChildAfter_(childBodyItem, opt);
      } else {
        // reset position value and copy opt
        opt = $.extend({}, opt, { position: undefined }) || {};

        if (opt.childBeforeId) {
          var renderedChild = this.getRenderedChildById(opt.childBeforeId);
          var childIndex = renderedChild.getIndex();
          childIndex !== null && (opt.position = childIndex + 1);
        }

        if (opt.childBeforeId && opt.position === undefined) {
          this.showError('insertedChildIdNotExists', opt.childBeforeId, {
            mixinName: 'UU5_Common_contentMixin'
          });
        } else {
          this.insertChild(childBodyItem, opt);
        }
      }
    } else {
      this.showError('dynamicOnly', 'insertChildAfter', {
        mixinName: 'UU5_Common_contentMixin'
      });
    }

    return this;
  },

  /**
   * Update child props. New props will be merged with old props, so it is possible to change just one property or all
   * of them.
   * Only for dynamic container.
   * Can be overridden by updateChild_ function.
   *
   * TODO: maybe function updateProps() should be in baseMixin (only if parent is dynamic container)
   *
   * @param {string} childId - ID of child, which props are being updated.
   * @param {object} newProps - Props which are being changed. It is not necessary to set all of the props.
   * @param {object} opt - Additional parameters.
   * @param {boolean} opt.shouldUpdate - If false, setState is not called.
   * @param {function} opt.setStateCallback - Callback is called after setState.
   * @returns {object} this
   */
  updateChild: function (childId, newProps, opt) {
    if (this.isDynamic()) {
      if (typeof this.updateChild_ === 'function') {
        this.updateChild_(childId, newProps, opt);
      } else {
        opt = opt || {};

        var renderedChild = this.getRenderedChildById(childId);
        var childIndex = renderedChild.getIndex();

        var children = this.getChildren();
        children[childIndex] = this.cloneChild(renderedChild, this.expandChildProps(newProps, childIndex));

        (opt.shouldUpdate === undefined || opt.shouldUpdate) && this.setChildren(children, opt.setStateCallback);
      }
    } else {
      this.showError('dynamicOnly', 'updateChild', {
        mixinName: 'UU5_Common_contentMixin'
      });
    }

    return this;
  },

  /**
   * Replace child by tag and props on the position.
   * Only for dynamic container.
   * Can be overridden by replaceChild_ function.
   *
   * TODO: maybe function replace() should be in baseMixin (only if parent is dynamic container)
   *
   * @param {string} childId - ID of child, which props are being updated.
   * @param {object} childBodyItem - Object with tag of the element and props. E.g. {tag: '...', props: {...}}
   * @param {object} opt - Additional parameters.
   * @param {boolean} opt.shouldUpdate - If false, setState is not called.
   * @param {function} opt.setStateCallback - Callback is called after setState.
   * @returns {object} this
   */
  replaceChild: function (childId, childBodyItem, opt) {
    if (this.isDynamic()) {
      if (typeof this.replaceChild_ === 'function') {
        this.replaceChild_(childId, childBodyItem, opt);
      } else {
        opt = opt || {};

        var renderedChild = this.getRenderedChildById(childId);
        var childIndex = renderedChild.getIndex();

        var children = this.getChildren();
        var newChild = this.buildChild(childBodyItem.tag, childBodyItem.props);
        if (this.shouldChildRender(newChild)) {
          children[childIndex] = this.cloneChild(newChild, this.expandChildProps(newChild, childIndex));

          (opt.shouldUpdate === undefined || opt.shouldUpdate) && this.setChildren(children, opt.setStateCallback);
        }
      }
    } else {
      this.showError('dynamicOnly', 'replaceChild', {
        mixinName: 'UU5_Common_contentMixin'
      });
    }

    return this;
  },

  /**
   * Deletes child from children list.
   * Only for dynamic container.
   * Can be overridden by deleteChild_ function.
   *
   * TODO: maybe function delete() should be in baseMixin (only if parent is dynamic container)
   *
   * @param {string} childId - ID of child, which props are being deleted.
   * @param {object} opt - Additional parameters.
   * @param {boolean} opt.shouldUpdate - If false, setState is not called.
   * @param {function} opt.setStateCallback - Callback is called after setState.
   * @returns {object} this
   */
  deleteChild: function (childId, opt) {
    if (this.isDynamic()) {
      if (typeof this.deleteChild_ === 'function') {
        this.deleteChild_(childId, opt);
      } else {
        opt = opt || {};

        var renderedChild = this.getRenderedChildById(childId);
        var childIndex = renderedChild.getIndex();

        var children = this.getChildren();
        children.splice(childIndex, 1);

        (opt.shouldUpdate === undefined || opt.shouldUpdate) && this.setChildren(children, opt.setStateCallback);
      }
    } else {
      this.showError('dynamicOnly', 'deleteChild', {
        mixinName: 'UU5_Common_contentMixin'
      });
    }

    return this;
  },

  /**
   * Deletes all children.
   * Only for dynamic container.
   * Can be overridden by clearChildren_ function.
   *
   * @param {object} opt - Additional parameters.
   * @param {boolean} opt.shouldUpdate - If false, setState is not called.
   * @param {function} opt.setStateCallback - Callback is called after setState.
   * @returns {object} this
   */
  clearChildren: function (opt) {
    if (this.isDynamic()) {
      if (typeof this.clearChildren_ === 'function') {
        this.clearChildren_(opt);
      } else {
        opt = opt || {};

        var children = this.getChildren();
        children.length = 0;

        (opt.shouldUpdate === undefined || opt.shouldUpdate) && this.setChildren(children, opt.setStateCallback);
      }
    } else {
      this.showError('dynamicOnly', 'clearChildren', {
        mixinName: 'UU5_Common_contentMixin'
      });
    }

    return this;
  },

  setFilter: function (filter, setStateCallback) {
    this.setState({ filter: filter, filteredProps: this._getFilteredChildrenProps(filter) }, setStateCallback);
    return this;
  },

  resetFilter: function (setStateCallback) {
    this.setState({ filter: null, filteredProps: null }, setStateCallback);
    return this;
  },

  setSorter: function (sorter, setStateCallback) {
    this.setState({ sorter: sorter, sortedIds: this._getSortedChildIds(sorter) }, setStateCallback);
    return this;
  },

  resetSorter: function (setStateCallback) {
    this.setState({ sorter: null, sortedIds: null }, setStateCallback);
    return this;
  },

  setFilterAndSorter: function (filter, sorter, setStateCallback) {
    this.setState({
      filter: filter, filteredProps: this._getFilteredChildrenProps(filter),
      sorter: sorter, sortedIds: this._getSortedChildIds(sorter)
    }, setStateCallback);

    return this;
  },

  resetFilterAndSorter: function (setStateCallback) {
    this.setState({
      filter: null, filteredProps: null,
      sorter: null, sortedIds: null
    }, setStateCallback);

    return this;
  },

  getFilteredSorterChildren: function (children) {
    var container = this;
    var newChildren = [];

    children = children || this.getChildren();

    if (children) {
      children = Array.isArray(children) ? children : [children];
      children.forEach(function (child) {
        var childId = child.props.id;
        var index = container.state.sortedIds && container.state.sortedIds.indexOf(childId);

        if (!container.state.sortedIds || index > -1) {
          var newProps = container.state.filteredProps && container.state.filteredProps[childId];

          if (!container.state.filteredProps || newProps) {
            newProps && Object.keys(newProps).length !== 0 && (child = React.cloneElement(child, $.extend(true, {}, child.props, newProps)));

            if (typeof index === 'number') {
              newChildren[index] = child;
            } else {
              newChildren.push(child);
            }
          }
        }
      });
    }

    return newChildren;
  },

  // Mode
  setStandardMode: function (setStateCallback) {
    this.setState({ mode: this.getDefault('standardMode', 'UU5_Common_contentMixin') }, setStateCallback);
    return this;
  },

  setOutlineMode: function (setStateCallback) {
    this.setState({ mode: this.getDefault('outlineMode', 'UU5_Common_contentMixin') }, setStateCallback);
    return this;
  },

  getMode: function () {
    return this.state.mode;
  },

  isStandardMode: function () {
    return this.getMode() === this.getDefault('standardMode', 'UU5_Common_contentMixin');
  },

  isOutlineMode: function () {
    return this.getMode() === this.getDefault('outlineMode', 'UU5_Common_contentMixin');
  },

  // Overriding Functions

  // Component Specific Helpers

  _getContentType: function (content) {
    var type = null; // one of ['children','body','item','bodyItem','node','string']

    if (!content) {
      // children
      type = 'children';
    } else if (Array.isArray(content)) {
      // body or node
      type = content[0] && content[0].tag ? 'body' : 'node';
    } else if (content && typeof content === 'object') {
      // bodyItem, items or node
      type = content.tag ? content.propsArray ? 'items' : 'bodyItem' : 'node';
    } else if (typeof content === 'string') {
      type = 'string';
    } else {
      this.showError('unexpectedContentType', typeof content, {
        mixinName: 'UU5_Common_contentMixin',
        context: {
          content: content
        }
      });
    }

    return type;
  },

  _getFilteredChildrenProps: function (filter) {
    var filteredChildrenProps = {};

    this.eachRenderedChild(function (renderedChild, i) {
      var result = filter(renderedChild, i);
      if (result) {
        result === true && (result = {});
      } else {
        result = { hidden: true };
      }
      filteredChildrenProps[renderedChild.getId()] = result;
    });

    return filteredChildrenProps;
  },

  _getSortedChildIds: function (sorter) {
    var sortedChildren = this.getRenderedChildren().sort(sorter);
    return sortedChildren.map(function (renderedChild) {
      return renderedChild.getId();
    });
  }
};

'use strict';

var UU5 = UU5 || {};
UU5.Common = UU5.Common || {};

UU5.Common.extensionsMixin = {

  statics: {
    UU5_Common_extensionsMixin: {
      requiredMixins: ['UU5_Common_baseMixin'],
      errors: {
        extensionsNotFound: 'Property extensions was not set.',
        callsNotFound: 'Variable calls was not found in statics.',
        callNameNotFound: 'Call key %s was not found in calls.',
        callNotFound: 'Call %s was not found in extensions.'
      }
    }
  },

  propTypes: {
    extensions: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.object
    ])
  },

  getDefaultProps: function () {
    return {
      extensions: null
    };
  },

  getInitialState: function () {
    // initialize
    this.registerMixin('UU5_Common_extensionsMixin');
    // state
    return {
      extensions: {}
    };
  },

  componentWillMount: function () {
    this._setExtensions(this.props.extensions);
  },

  componentWillReceiveProps: function (nextProps) {
    this._setExtensions(nextProps.extensions);
  },

  // Interface

  hasUU5_Common_extensionsMixin: function () {
    return this.hasMixin('UU5_Common_extensionsMixin');
  },

  getUU5_Common_extensionsMixinProps: function () {
    return {
      extensions: this.getExtensions()
    };
  },

  getUU5_Common_extensionsMixinPropsToPass: function () {
    return this.getUU5_Common_extensionsMixinProps();
  },

  getExtensions: function () {
    return this.state.extensions;
  },

  getCall: function (item, mixinName) {
    var calls = mixinName
      ? this.constructor[mixinName]
      ? this.constructor[mixinName].calls
      : null
      : this.constructor.calls;

    var callName = calls && calls[item];
    var extensions = this.getExtensions();
    var call = null;

    if (!extensions) {
      this.showError('extensionsNotFound', null, { mixinName: 'UU5_Common_extensionsMixin' });
    } else if (!calls) {
      this.showError('callsNotFound', null, {
        mixinName: 'UU5_Common_extensionsMixin',
        context: {
          constructor: this.constructor
        }
      });
    } else if (!callName) {
      this.showError('callNameNotFound', item, { mixinName: 'UU5_Common_extensionsMixin' });
    } else {
      call = extensions[callName];

      if (!call) {
        this.showError('callNotFound', callName, {
          mixinName: 'UU5_Common_extensionsMixin',
          context: {
            extensions: extensions
          }
        });
      }
    }

    return call;
  },

  // Overriding Functions

  // Component Specific Helpers

  _setExtensions: function (extensions) {
    if (extensions) {
      typeof extensions === 'string' && (extensions = this.stringToObjectType(extensions, 'object', UU5.Common.environment.extensions));

      this.setState({extensions: extensions});
    }
    return this;
  }
};

'use strict';

var UU5 = UU5 || {};
UU5.Common = UU5.Common || {};

UU5.Common.bsScreenSizeMixin = {

  statics: {
    UU5_Common_bsScreenSizeMixin: {
      requiredMixins: ['UU5_Common_baseMixin'],
      defaults: {
        bsScreenSizeEvent: 'UU5_Common_bsScreenSize'
      }
    }
  },

  propTypes: {
    bsScreenSize: React.PropTypes.string
  },

  getDefaultProps: function () {
    return {
      bsScreenSize: null
    };
  },

  getInitialState: function () {
    // initialize
    this.registerMixin('UU5_Common_bsScreenSizeMixin');
    // state
    return {
      bsScreenSize: this.getBsScreenSize()
    };
  },

  componentDidMount: function (nextProps) {
    $(window).on(this.getDefault(null, 'UU5_Common_bsScreenSizeMixin').bsScreenSizeEvent, this._onChangeScreenSize);
  },

  componentWillUnmount: function () {
    $(window).off(this.getDefault(null, 'UU5_Common_bsScreenSizeMixin').bsScreenSizeEvent, this._onChangeScreenSize);
  },

  // Interface

  hasUU5_Common_bsScreenSizeMixinProps: function () {
    return this.hasMixin('UU5_Common_bsScreenSizeMixin');
  },

  getUU5_Common_bsScreenSizeMixinProps: function () {
    return {
      bsScreenSize: this.getBsScreenSize()
    };
  },

  getUU5_Common_bsScreenSizeMixinPropsToPass: function () {
    return this.getUU5_Common_bsScreenSizeMixinProps();
  },

  getBsScreenSizeFromCss: function () {
    return window.getComputedStyle(document.body, ':after').getPropertyValue('content').replace(/['"]/g, '');
  },

  getBsScreenSize: function () {
    return this.state && this.state.bsScreenSize || this.getBsScreenSizeFromCss();
  },

  isXs: function () {
    return this.getBsScreenSize() === 'xs';
  },

  isSm: function () {
    return this.getBsScreenSize() === 'sm';
  },

  isMd: function () {
    return this.getBsScreenSize() === 'md';
  },

  isLg: function () {
    return this.getBsScreenSize() === 'lg';
  },

  // Overriding Functions

  // Component Specific Helpers

  _onChangeScreenSize: function (e, actualBsScreenSize) {
    if (this.getBsScreenSize() !== actualBsScreenSize) {
      if (typeof this.onChangeScreenSize_ === 'function') {
        this.onChangeScreenSize_(actualBsScreenSize, e);
      } else {
        this.setState({ bsScreenSize: actualBsScreenSize });
      }
    }
    return this;
  }
};

UU5.Common.bsScreenSizeMixin.screenSize = '';

$(window).on('resize orientationchange load', function () {
  var bsActualScreenSize = UU5.Common.bsScreenSizeMixin.getBsScreenSizeFromCss();
  if (bsActualScreenSize !== UU5.Common.bsScreenSizeMixin.screenSize) {
    $(window).trigger(UU5.Common.bsScreenSizeMixin.statics.UU5_Common_bsScreenSizeMixin.defaults.bsScreenSizeEvent, bsActualScreenSize);
    UU5.Common.bsScreenSizeMixin.screenSize = bsActualScreenSize;
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Common = UU5.Common || {};

UU5.Common.callsMixin = {
  mixins: [
    UU5.Common.extensionsMixin
  ],

  statics: {
    UU5_Common_callsMixin: {
      defaults: {
        minAutomaticReloadInterval: 10 * 1000, // 10s
        loadOnMountCall: 'loadOnMount',
        automaticReloadCall: 'automaticReload'
      },
      warnings: {
        notImplementedLoadOnMountSuccess: 'Function loadOnMountSuccess_ is not implemented.'
      }
    }
  },

  propTypes: {
    uri: React.PropTypes.string,
    data: React.PropTypes.object,
    automaticReloadInterval: React.PropTypes.number
  },

  getDefaultProps: function () {
    return {
      uri: null,
      data: null,
      automaticReloadInterval: 0
    };
  },

  componentWillMount: function () {
    if (this.props.data && !this.props.uri) {
      if (typeof this.loadOnMountSuccess_ === 'function') {
        this.loadOnMountSuccess_({ data: this.props.data });
      } else {
        this.showWarning('notImplementedLoadOnMountSuccess', null, { mixinName: 'UU5_Common_callsMixin' });
      }
    }
  },

  componentDidMount: function () {
    if (this.props.uri) {
      this._loadOnMount();
    }
  },

  // Interface

  getUri: function () {
    return this.props.uri;
  },

  getData: function () {
    return this.props.data;
  },

  _loadOnMount: function () {
    var loadCall = this.getCall(this.getDefault('loadOnMountCall', 'UU5_Common_callsMixin'));

    if (loadCall) {
      var thisComponent = this;

      var dtoIn = {
        done: function (dtoIn) {
          if (typeof thisComponent.loadOnMountSuccess_ === 'function') {
            thisComponent.loadOnMountSuccess_(dtoIn);
          } else {
            thisComponent.showWarning('notImplementedLoadOnMountSuccess', null, { mixinName: 'UU5_Common_callsMixin' });
          }
        },
        fail: function (dtoIn) {
          console.error('loadOnMountError', dtoIn);
          typeof thisComponent.loadOnMountError_ === 'function' && thisComponent.loadOnMountError_(dtoIn);
        }
      };

      if (this.props.uri) {
        dtoIn.uri = this.props.uri;
      }

      if (typeof this.getLoadOnMountData_ === 'function') {
        dtoIn.data = $.extend({}, dtoIn.data, this.getLoadOnMountData_(dtoIn.data));
      }

      loadCall(dtoIn);

      if (this.props.automaticReloadInterval) {
        loadCall = this.getCall(this.getDefault('automaticReloadCall', 'UU5_Common_callsMixin'));

        if (loadCall) {
          setInterval(function () {
            loadCall(dtoIn);
          }, Math.max(this.props.automaticReloadInterval, this.getDefault('minAutomaticReloadInterval', 'UU5_Common_callsMixin')));
        }
      }
    }

    return this;
  }
};
'use strict';

var UU5 = UU5 || {};
UU5.Common = UU5.Common || {};

UU5.Common.ccrReaderMixin = {

  statics: {
    UU5_Common_ccrReaderMixin: {
      requiredMixins: ['UU5_Common_baseMixin'],
      errors: {
        keyNotRegistered: 'Component with key %s is not registered.'
      }
    }
  },

  getInitialState: function () {
    // initialize
    this.registerMixin('UU5_Common_ccrReaderMixin');
    // state
    return null;
  },

  // Interface

  hasUU5_Common_ccrReaderMixin: function () {
    return this.hasMixin('UU5_Common_ccrReaderMixin');
  },

  getUU5_Common_ccrReaderMixinProps: function () {
    return {};
  },

  getUU5_Common_ccrReaderMixinPropsToPass: function () {
    return this.getUU5_Common_ccrReaderMixinProps();
  },

  getCcrComponentByKey: function (key) {
    var component = this.getCcrByKeyRegister()[key];
    !component && this.showError('keyNotRegistered', key, {
      mixinName: 'UU5_Common_ccrReaderMixin'
    });
    return component;
  },

  isCcrRegisteredByKey: function (key) {
    return !!this.getCcrByKeyRegister()[key];
  },

  getCcrByKeyRegister: function () {
    return UU5.Common.environment.ccr.byKey;
  }

  // Overriding Functions

  // Component Specific Helpers
};

'use strict';

var UU5 = UU5 || {};
UU5.Common = UU5.Common || {};

UU5.Common.ccrWriterMixin = {

  mixins: [UU5.Common.ccrReaderMixin],

  statics: {
    UU5_Common_ccrWriterMixin: {
      requiredMixins: ['UU5_Common_ccrReaderMixin'],
      warnings: {
        keyNotRegistered: 'Component with key %s was not registered.'
      },
      errors: {
        alreadyRegistered: 'Component with ccr key %s is already registered.',
        unregisterNotThis: 'Component with ccr key %s is not this component, cannot be unregistered.'
      }
    }
  },

  propTypes: {
    ccrKey: React.PropTypes.string
  },

  getDefaultProps: function () {
    return {
      ccrKey: null
    };
  },

  getInitialState: function () {
    // initialize
    this.registerMixin('UU5_Common_ccrWriterMixin');
    // state
    return null;
  },

  componentWillMount: function () {
    this.props.ccrKey && this._registerToCcrByKey(this.props.ccrKey);
  },

  componentWillReceiveProps: function (nextProps) {
    if (nextProps.ccrKey && nextProps.ccrKey !== this.getCcrKey()) {
      this.getCcrKey() && this.isCcrRegisteredByKey(this.getCcrKey()) && this._unregisterFromCcrByKey(this.getCcrKey());
      this._registerToCcrByKey(nextProps.ccrKey);
    }
  },

  componentWillUnmount: function () {
    this.getCcrKey() && this.isCcrRegisteredByKey(this.getCcrKey()) && this._unregisterFromCcrByKey(this.getCcrKey());
  },

  // Interface

  hasUU5_Common_ccrWriterMixin: function () {
    return this.hasMixin('UU5_Common_ccrWriterMixin');
  },

  getUU5_Common_ccrWriterMixinProps: function () {
    return {
      ccrKey: this.getCcrKey()
    };
  },

  getUU5_Common_ccrWriterMixinPropsToPass: function () {
    return {};
  },

  getCcrKey: function () {
    return this.props.ccrKey;
  },

  // Overriding Functions

  // Component Specific Helpers
  _registerToCcrByKey: function (key) {
    var component = this.getCcrByKeyRegister()[key];
    if (component && component !== this) {
      this.showError('alreadyRegistered', key, {
        mixinName: 'UU5_Common_ccrWriterMixin',
        context: {
          registeredComponent: {
            tagName: component.getTagName(),
            id: component.getId(),
            component: component
          }
        }
      });
    }
    this.getCcrByKeyRegister()[key] = this;
    return this;
  },

  _unregisterFromCcrByKey: function (key) {
    var component = this.getCcrByKeyRegister()[key];
    if (!component) {
      this.showWarning('keyNotRegistered', key, {
        mixinName: 'UU5_Common_ccrWriterMixin'
      });
    } else if (component !== this) {
      this.showError('unregisterNotThis', key, {
        mixinName: 'UU5_Common_ccrWriterMixin',
        context: {
          registeredComponent: {
            tagName: component.getTagName(),
            id: component.getId(),
            component: component
          }
        }
      });
    } else {
      delete this.getCcrByKeyRegister()[key];
    }
    return this;
  }
};

'use strict';

var UU5 = UU5 || {};
UU5.Common = UU5.Common || {};
UU5.Common.environment = UU5.Common.environment || {};

// Mode
UU5.Common.environment.isDevelopment = function () {
  return true;
};

// Version
UU5.Common.environment.version = 'UU5V03.10 ©Unicorn 2016';
console.log(UU5.Common.environment.version);

// Color schema
UU5.Common.environment.colorSchema = [
  'low', 'primary', 'success', 'info', 'warning', 'danger',
  'success-rich', 'info-rich', 'warning-rich', 'danger-rich',
  'white', 'yellow', 'orange', 'pink', 'red', 'purple', 'cyan', 'blue', 'green', 'brown', 'grey', 'black',
  'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve'
];
// schema-rich -> SchemaRich
UU5.Common.environment.colorSchemaCapitalize = function (colorSchema) {

  var colorSchemaCapitalize = null;
  if (colorSchema) {
    colorSchemaCapitalize = colorSchema.charAt(0).toUpperCase() + colorSchema.slice(1);
    colorSchemaCapitalize = colorSchemaCapitalize.replace(/(\-[a-z])/g, function ($1) {
      return $1.toUpperCase().replace('-', '');
    });
  }
  return colorSchemaCapitalize;
};

// External component calls
UU5.Common.environment.extensions = UU5.Common.environment.extensions || {};

// External component limits
UU5.Common.environment.limits = UU5.Common.environment.limits || {};

// Central component register (ccr)
UU5.Common.environment.ccr = UU5.Common.environment.ccr || {};
UU5.Common.environment.ccr.byKey = UU5.Common.environment.ccr.byKey || {};

// Component language sensitive items
UU5.Common.environment.languages = [
  { language: 'cs', q: 1.0 },
  { language: 'sk', q: 0.7 },
  { language: 'en', q: 0.4 }
];

// Cookies

UU5.Common.environment.setCookie = function (cookieName, cookieValue, expireDays) {
  var d = new Date();
  d.setTime(d.getTime() + (expireDays * 24 * 60 * 60 * 1000));
  var expires = "expires=" + d.toUTCString();
  document.cookie = cookieName + "=" + cookieValue + "; " + expires;
};

UU5.Common.environment.getCookie = function (cookieName) {
  var name = cookieName + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
};
'use strict';

var UU5 = UU5 || {};
UU5.Common.environment.hardSpace = UU5.Tools || {};

UU5.Common.environment.hardSpace = {
  regExp: '  ',
  nbSpace: "\u00a0",
  nbHyphen: "\u2011",
  lsiReplacer: {}
};

UU5.Common.environment.hardSpace.lsiReplacer['cs-cz'] = function (text) {

  var newText = text.replace(
    new RegExp(UU5.Common.environment.hardSpace.regExp, 'g'),
    UU5.Common.environment.hardSpace.nbSpace
  );

  var nbSpace = UU5.Common.environment.hardSpace.nbSpace;

  var wordsWithSpace = {
    after: [
      "s", "k", "v", "z", "a", "i", "o", "u"
    ],
    degreesBefore: [
      "bc.", "bca.", "ing.", "ing.arch.", "mudr.", "mvdr.", "mga.", "mgr.", "judr.", "phdr.", "rndr.", "pharmdr.",
      "thlic.", "thdr.", "prof.", "doc.", "paeddr.", "dr.", "phmr."
    ],
    degreesAfter: ["ph.d.", "th.d.", "csc.", "drsc.", "dis."],
    units: [
      "%", "Kč", "€", "m", "g", "l", "q", "t", "w", "J", "ks", "mm", "cm", "km", "mg", "dkg", "kg", "ml", "cl", "dl", "hl",
      "m3", "km3", "mm2", "cm2", "dm2", "m2", "km2", "ha", "Pa", "hPa", "kPa", "MPa", "bar", "mbar", "nbar", "atm",
      "psi", "kW", "MW", "HP", "m/s", "km/h", "m/min", "MPH", "cal", "Wh", "kWh", "kp·m", "°C", "°F", "kB", "dB",
      "MB", "GB", "kHz", "MHz"
    ]
  };

  var spaceSplitter = newText.split(' ');
  var resultText = '';
  for (var i = 0; i < spaceSplitter.length; i++) {
    var prevPart = spaceSplitter[i];
    var prevPartLowerCase = prevPart.toLowerCase();
    var lastChar = prevPart[prevPart.length - 1];
    var nextPart = spaceSplitter[i + 1];
    var firstChar = nextPart && nextPart[0];

    resultText += prevPart === '' ? ' ' : prevPart;

    if (nextPart) {
      if (
        // it is word with space after
      (wordsWithSpace.after.indexOf(prevPartLowerCase) > -1)

      // dot and next char is not upper - not end of sentence
      || (
        '.' === lastChar
        && firstChar && firstChar !== firstChar.toUpperCase() && !isFinite(firstChar)
        && wordsWithSpace.degreesAfter.indexOf(prevPart.toLowerCase()) === -1
        && (spaceSplitter[i - 3] !== "dr." && spaceSplitter[i - 2] !== "h." && prevPart !== "c.")
      )

      // numbers split by space
      || (isFinite(prevPart) && isFinite(nextPart))

      // degrees before name
      || (wordsWithSpace.degreesBefore.indexOf(prevPartLowerCase) > -1)

      // degrees after name
      || (wordsWithSpace.degreesAfter.indexOf(nextPart.toLowerCase()) > -1)

      // between number and unit
      || (prevPart.match(/^\d+$/g) && wordsWithSpace.units.indexOf(nextPart) > -1)
      ) {
        resultText += nbSpace;

        // degree dr. h. c.
      } else if (
        (nextPart === "dr." && spaceSplitter[i + 2] === "h." && spaceSplitter[i + 3] === "c.")
        || (prevPart === "dr." && nextPart === "h." && spaceSplitter[i + 2] === "c.")
        || (spaceSplitter[i - 1] === "dr." && prevPart === "h." && nextPart === "c.")
      ) {
        resultText += nbSpace;

      } else {
        resultText += " ";
      }
    }
  }

  return resultText;
};

UU5.Common.environment.hardSpace.lsiReplacer['code'] = function (text) {
  var resultText = text;

  resultText = resultText.replace(/ /g, UU5.Common.environment.hardSpace.nbSpace);
  resultText = resultText.replace(/-/g, UU5.Common.environment.hardSpace.nbHyphen);

  return resultText;
};
'use strict';

var UU5 = UU5 || {};
UU5.Common = UU5.Common || {};

UU5.Common.lsiMixin = {

  statics: {
    UU5_Common_lsiMixin: {
      requiredMixins: ['UU5_Common_baseMixin'],
      lsiEvent: 'UU5_Common_eventChangeLanguage'
    }
  },

  propTypes: {
    language: React.PropTypes.string
  },

  getDefaultProps: function () {
    return {
      language: null
    };
  },

  getInitialState: function () {
    // initialize
    this.registerMixin('UU5_Common_lsiMixin');
    // state
    return {
      language: this.props.language || this.getEnvironmentLanguages()[0].language
    };
  },

  componentDidMount: function () {
    $(window).on(this.constructor.UU5_Common_lsiMixin.lsiEvent, this._changeLanguage);
  },

  componentWillUnmount: function () {
    $(window).off(this.constructor.UU5_Common_lsiMixin.lsiEvent, this._changeLanguage);
  },

  // Interfaces

  hasUU5_Common_lsiMixin: function () {
    return this.hasMixin('UU5_Common_lsiMixin');
  },

  getUU5_Common_lsiMixinProps: function () {
    return {
      language: this.props.language
    };
  },

  getUU5_Common_lsiMixinPropsToPass: function () {
    return this.getUU5_Common_lsiMixinProps();
  },

  getLanguages: function () {
    return this.getEnvironmentLanguages();
  },

  getLanguage: function () {
    return this.state.language;
  },

  // component gets languages from global environment
  getLSIItem: function (lsi) {
    return this.getLSIItemByLanguage(lsi, this.getLanguages());
  },

  // Overriding Functions

  // Component Specific Helpers

  // TODO: 'cs-CZ,en;q=0.8...' is set to state and never will be used, necessary is doing setState and render the component
  _setLanguage: function (language, callback) {
    this.setState({ language: language }, callback);
    return this;
  },

  _changeLanguage: function (e, language) {
    if (this.getLanguage() !== language) {
      if (typeof this.onChangeLanguage_ === 'function') {
        this.onChangeLanguage_(language, e);
      } else {
        this._setLanguage(language);
      }
    }
  }
};

'use strict';

UU5.Common.sectionMixin = {

  mixins: [UU5.Common.contentMixin, UU5.Common.levelMixin],

  statics: {
    UU5_Common_sectionMixin: {
      requiredMixins: ['UU5_Common_baseMixin', 'UU5_Common_contentMixin', 'UU5_Common_levelMixin'],
      defaults: {
        headerTag: 'UU5.Bricks.header',
        footerTag: 'UU5.Bricks.footer'
      }
    }
  },

  propTypes: {
    header: React.PropTypes.oneOfType([
    // content body:[{tag:'',props:{}},{tag:'',props:{}},...]
    React.PropTypes.arrayOf(React.PropTypes.shape({
      tag: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.element]),
      props: React.PropTypes.object
    })),
    // content bodyItem:{tag:'',props{}}
    React.PropTypes.shape({
      tag: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.element]),
      props: React.PropTypes.arrayOf(React.PropTypes.object)
    }),
    // content items:{tag:'',propsArray:[{},{},{},...]}
    React.PropTypes.shape({
      tag: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.element]),
      propsArray: React.PropTypes.arrayOf(React.PropTypes.object)
    }),
    // content node
    React.PropTypes.node,
    // element
    React.PropTypes.shape({
      element: React.PropTypes.oneOfType([React.PropTypes.element, React.PropTypes.shape({
        tag: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.element]),
        props: React.PropTypes.object
      })])
    })]),
    footer: React.PropTypes.oneOfType([
    // content body:[{tag:'',props:{}},{tag:'',props:{}},...]
    React.PropTypes.arrayOf(React.PropTypes.shape({
      tag: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.element]),
      props: React.PropTypes.object
    })),
    // content bodyItem:{tag:'',props{}}
    React.PropTypes.shape({
      tag: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.element]),
      props: React.PropTypes.arrayOf(React.PropTypes.object)
    }),
    // content items:{tag:'',propsArray:[{},{},{},...]}
    React.PropTypes.shape({
      tag: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.element]),
      propsArray: React.PropTypes.arrayOf(React.PropTypes.object)
    }),
    // content node
    React.PropTypes.node,
    // element
    React.PropTypes.shape({
      element: React.PropTypes.oneOfType([React.PropTypes.element, React.PropTypes.shape({
        tag: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.element]),
        props: React.PropTypes.object
      })])
    })])
  },

  getDefaultProps: function () {
    return {
      header: null,
      footer: null
    };
  },

  getInitialState: function () {
    // initialize
    this.registerMixin('UU5_Common_sectionMixin');
    this.renderedHeaderChild = null; // renderedChild
    this.renderedFooterChild = null; // renderedChild
    // state
    return null;
  },

  // Interface

  hasUU5_Common_sectionMixin: function () {
    return this.hasMixin('UU5_Common_sectionMixin');
  },

  getHeader: function () {
    return this.props.header;
  },

  getFooter: function () {
    return this.props.footer;
  },

  getHeaderType: function (headerContent) {
    var type = null; // one of ['contentOfStandardHeader','headerElement','headerBodyItem']
    if (headerContent) {
      if (typeof headerContent.element === 'object') {
        type = headerContent.element.tag ? 'headerBodyItem' : 'headerElement';
      } else {
        type = 'contentOfStandardHeader';
      }
    }
    return type;
  },

  getFooterType: function (footerContent) {
    var type = null; // one of ['contentOfStandardFooter','footerElement','footerBodyItem']
    if (footerContent) {
      if (typeof footerContent.element === 'object') {
        type = footerContent.element.tag ? 'footerBodyItem' : 'footerElement';
      } else {
        type = 'contentOfStandardFooter';
      }
    }
    return type;
  },

  getUU5_Common_sectionMixinProps: function () {
    return {
      header: this.getHeader(),
      footer: this.getFooter()
    };
  },

  getUU5_Common_sectionMixinPropsToPass: function () {
    return $.extend(true, {}, this.getUU5_Common_sectionMixinProps(), this.getUU5_Common_contentMixinPropsToPass(), this.getUU5_Common_levelMixinPropsToPass());
  },

  registerRenderedHeaderChild: function (renderedHeaderChild) {
    if (renderedHeaderChild.hasUU5_Common_baseMixin) {
      this.renderedHeaderChild = renderedHeaderChild;
    }
    return this;
  },

  registerRenderedFooterChild: function (renderedFooterChild) {
    if (renderedFooterChild.hasUU5_Common_baseMixin) {
      this.renderedFooterChild = renderedFooterChild;
    }
    return this;
  },

  expandHeaderProps: function (prevHeaderChild) {
    var newHeaderChildProps = prevHeaderChild.props;
    newHeaderChildProps = $.extend(true, {}, newHeaderChildProps);
    newHeaderChildProps.parent = this;
    newHeaderChildProps.id = newHeaderChildProps.id || this.getId() + '-header';

    this.props.bsContainerStyle && (newHeaderChildProps.bsContainerStyle = this.props.bsContainerStyle);
    this.props.bsColWidth && (newHeaderChildProps.bsColWidth = this.props.bsColWidth);

    if (typeof this.expandHeaderProps_ === 'function') {
      var tempHeaderChild = this.cloneChild(prevHeaderChild, newHeaderChildProps);
      newHeaderChildProps = this.expandHeaderProps_(tempHeaderChild);
    }

    newHeaderChildProps.key = newHeaderChildProps.id;

    newHeaderChildProps.ref = function (renderedChild) {
      renderedChild && this.registerRenderedHeaderChild(renderedChild);
    }.bind(this);

    return newHeaderChildProps;
  },

  expandFooterProps: function (prevFooterChild) {
    var newFooterChildProps = prevFooterChild.props;
    newFooterChildProps = $.extend(true, {}, newFooterChildProps);
    newFooterChildProps.parent = this;
    newFooterChildProps.id = newFooterChildProps.id || this.getId() + '-footer';

    this.props.bsContainerStyle && (newFooterChildProps.bsContainerStyle = this.props.bsContainerStyle);
    this.props.bsColWidth && (newFooterChildProps.bsColWidth = this.props.bsColWidth);

    if (typeof this.expandFooterProps_ === 'function') {
      var tempFooterChild = this.cloneChild(prevFooterChild, newFooterChildProps);
      newFooterChildProps = this.expandFooterProps_(tempFooterChild);
    }

    newFooterChildProps.key = newFooterChildProps.id;

    newFooterChildProps.ref = function (renderedChild) {
      renderedChild && this.registerRenderedFooterChild(renderedChild);
    }.bind(this);

    return newFooterChildProps;
  },

  buildHeaderChild: function (header) {
    header = header || this.getHeader();

    var headerChild = null;

    if (typeof this.buildHeaderChild_ === 'function') {
      headerChild = this.buildHeaderChild_(header);
    } else {
      var headerType = this.getHeaderType(header);

      switch (headerType) {
        case 'headerBodyItem':
          headerChild = this.buildChild(header.element.tag, header.element.props);
          headerChild = this.cloneChild(headerChild, this.expandHeaderProps(headerChild));
          break;
        case 'headerElement':
          headerChild = header.element;
          headerChild = this.cloneChild(headerChild, this.expandHeaderProps(headerChild));
          break;
        case 'contentOfStandardHeader':
          headerChild = this.buildChild(this.getDefault('headerTag', 'UU5_Common_sectionMixin'), { content: header });
          headerChild = this.cloneChild(headerChild, this.expandHeaderProps(headerChild));
          break;
        default:
        // there is no header
      }
    }
    return headerChild;
  },

  buildFooterChild: function (footer) {
    footer = footer || this.getFooter();

    var footerChild = null;

    if (typeof this.buildFooterChild_ === 'function') {
      footerChild = this.buildFooterChild_(footer);
    } else {
      var footerType = this.getFooterType(footer);

      switch (footerType) {
        case 'footerBodyItem':
          footerChild = this.buildChild(footer.element.tag, footer.element.props);
          footerChild = this.cloneChild(footerChild, this.expandFooterProps(footerChild));
          break;
        case 'footerElement':
          footerChild = footer.element;
          footerChild = this.cloneChild(footerChild, this.expandFooterProps(footerChild));
          break;
        case 'contentOfStandardFooter':
          footerChild = this.buildChild(this.getDefault('footerTag', 'UU5_Common_sectionMixin'), { content: footer });
          footerChild = this.cloneChild(footerChild, this.expandFooterProps(footerChild));
          break;
        default:
        // there is no footer
      }
    }
    return footerChild;
  },

  getHeaderChild: function () {
    return this.buildHeaderChild();
  },

  getFooterChild: function () {
    return this.buildFooterChild();
  },

  getRenderedHeaderChild: function () {
    return this.renderedHeaderChild;
  },

  getRenderedFooterChild: function () {
    return this.renderedFooterChild;
  }

  // Overriding Functions

  // Component Specific Helpers
};

'use strict';

var UU5 = UU5 || {};
UU5.Common = UU5.Common || {};

UU5.Common.swipeMixin = {

  statics: {
    UU5_Common_swipeMixin: {
      requiredMixins: ['UU5_Common_baseMixin'],
      maxLengthOnShortSwipe: 70,
      minLengthOnSwipe: 10,
      maxSpeedOnSlowSwipe: 0.3,
      maxAngleDivergence: 10
    }
  },

  // React lifecycle
  propTypes: {
    swiped: React.PropTypes.oneOf(['up', 'right', 'down', 'left', 'upRight', 'downRight', 'upLeft', 'downLeft']),
    // null is not false - null is not swiped
    swipedLong: React.PropTypes.bool,
    swipedFast: React.PropTypes.bool
  },

  getDefaultProps: function () {
    return {
      swiped: null, // up|right|down|left|upRight|downRight|upLeft|downLeft
      swipedLong: null, // true|false
      swipedFast: null // true|false
    };
  },

  getInitialState: function () {
    // initialize
    this.registerMixin('UU5_Common_swipeMixin');
    this.lastSwipe = {
      x: 0,
      y: 0,
      dx: 0,
      dy: 0,
      ex: 0,
      ey: 0,
      left: false,
      right: false,
      up: false,
      down: false,
      long: this.props.swipedLong,
      fast: this.props.swipedFast,
      length: 0,
      speed: 0,
      startTime: null,
      endTime: null,
      angle: 0
    };
    // state
    return null;
  },

  componentWillMount: function () {
    this.props.swiped && (
      this.lastSwipe = this._getUpdatedSwipe(this.props)
    );
  },

  componentWillReceiveProps: function (nextProps) {
    nextProps.swiped && (
      this.lastSwipe = this._getUpdatedSwipe(nextProps)
    );
  },

  // Interface

  hasUU5_Common_swipeMixin: function () {
    return this.hasMixin('UU5_Common_swipeMixin');
  },

  getUU5_Common_swipeMixinProps: function () {
    return {
      swiped: this.props.swiped,
      swipedLong: this.props.swipedLong,
      swipedFast: this.props.swipedFast
    };
  },

  getUU5_Common_swipeMixinPropsToPass: function () {
    return this.getUU5_Common_swipeMixinProps();
  },

  getSwipeLength: function () {
    var dx = this.lastSwipe.dx;
    var dy = this.lastSwipe.dy;
    return Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
  },

  getAngle: function () {
    return this.lastSwipe.angle;
  },

  isSwipedHorizontal: function () {
    return Math.abs(this.getAngle())
      <= UU5.Common.swipeMixin.statics.UU5_Common_swipeMixin.maxAngleDivergence
      || Math.abs(this.getAngle())
      >= 180 - UU5.Common.swipeMixin.statics.UU5_Common_swipeMixin.maxAngleDivergence;
  },

  isSwipedVertical: function () {
    return Math.abs(this.getAngle())
      <= 90 + UU5.Common.swipeMixin.statics.UU5_Common_swipeMixin.maxAngleDivergence
      && Math.abs(this.getAngle())
      >= 90 - UU5.Common.swipeMixin.statics.UU5_Common_swipeMixin.maxAngleDivergence;
  },

  isSwiped: function () {
    return this.isSwipedUp() || this.isSwipedRight() || this.isSwipedDown() || this.isSwipedLeft();
  },

  isSwipedShort: function () {
    return this.lastSwipe.long !== null && !this.lastSwipe.long;
  },

  isSwipedLong: function () {
    return this.lastSwipe.long !== null && this.lastSwipe.long;
  },

  isSwipedSlow: function () {
    return this.lastSwipe.fast !== null && !this.lastSwipe.fast;
  },

  isSwipedFast: function () {
    return this.lastSwipe.fast !== null && this.lastSwipe.fast;
  },

  isSwipedUp: function () {
    return this.lastSwipe.up && this.isSwipedVertical();
  },

  isSwipedRight: function () {
    return this.lastSwipe.right && this.isSwipedHorizontal();
  },

  isSwipedDown: function () {
    return this.lastSwipe.down && this.isSwipedVertical();
  },

  isSwipedLeft: function () {
    return this.lastSwipe.left && this.isSwipedHorizontal();
  },

  getSwipeAngle: function () {
    return this.lastSwipe.angle;
  },

  // Two directions
  isSwipedUpRight: function () {
    return this.lastSwipe.up && this.lastSwipe.right;
  },

  isSwipedDownRight: function () {
    return this.lastSwipe.down && this.lastSwipe.right;
  },

  isSwipedUpLeft: function () {
    return this.lastSwipe.up && this.lastSwipe.left;
  },

  isSwipedDownLeft: function () {
    return this.lastSwipe.down && this.lastSwipe.left;
  },

  // Short
  isSwipedUpShort: function () {
    return this.isSwipedUp() && this.isSwipedShort();
  },

  isSwipedRightShort: function () {
    return this.isSwipedRight() && this.isSwipedShort();
  },

  isSwipedDownShort: function () {
    return this.isSwipedDown() && this.isSwipedShort();
  },

  isSwipedLeftShort: function () {
    return this.isSwipedLeft() && this.isSwipedShort();
  },

  isSwipedUpRightShort: function () {
    return this.isSwipedUpRight() && this.isSwipedShort();
  },

  isSwipedDownRightShort: function () {
    return this.isSwipedDownRight() && this.isSwipedShort();
  },

  isSwipedUpLeftShort: function () {
    return this.isSwipedUpLeft() && this.isSwipedShort();
  },

  isSwipedDownLeftShort: function () {
    return this.isSwipedDownLeft() && this.isSwipedShort();
  },

  // Long
  isSwipedUpLong: function () {
    return this.isSwipedUp() && this.isSwipedLong();
  },

  isSwipedRightLong: function () {
    return this.isSwipedRight() && this.isSwipedLong();
  },

  isSwipedDownLong: function () {
    return this.isSwipedDown() && this.isSwipedLong();
  },

  isSwipedLeftLong: function () {
    return this.isSwipedLeft() && this.isSwipedLong();
  },

  isSwipedUpRightLong: function () {
    return this.isSwipedUpRight() && this.isSwipedLong();
  },

  isSwipedDownRightLong: function () {
    return this.isSwipedDownRight() && this.isSwipedLong();
  },

  isSwipedUpLeftLong: function () {
    return this.isSwipedUpLeft() && this.isSwipedLong();
  },

  isSwipedDownLeftLong: function () {
    return this.isSwipedDownLeft() && this.isSwipedLong();
  },

  swipeOnTouchStart: function (handler, e) {
    if (typeof e === 'string') {
      e = handler;
      handler = null;
    }
    e = e || handler;
    this.lastSwipe = $.extend(
      {}, this.lastSwipe, {
        x: e.touches[0].clientX,
        y: e.touches[0].clientY,
        dx: 0,
        dy: 0,
        length: 0,
        speed: null,
        startTime: new Date().getTime()
      }
    );
    typeof handler === 'function' && handler(arguments);
    return this;
  },

  swipeOnTouchMove: function (handler, e) {
    if (typeof e === 'string') {
      e = handler;
      handler = null;
    }
    e = e || handler;
    // e.preventDefault();
    this.lastSwipe = $.extend(
      {}, this.lastSwipe, {
        dx: e.touches[0].clientX - this.lastSwipe.x,
        dy: e.touches[0].clientY - this.lastSwipe.y,
        ex: e.touches[0].clientX,
        ey: e.touches[0].clientY,
        endTime: new Date().getTime()
      }
    );

    (
      this.isSwipedLeft() || this.isSwipedRight()
    ) && e.preventDefault();

    typeof handler === 'function' && handler(e);
    return this;
  },

  swipeOnTouchEnd: function (handler, e) {
    if (typeof e === 'string') {
      e = handler;
      handler = null;
    }
    e = e || handler;
    var length = this.getSwipeLength();

    if (length > UU5.Common.swipeMixin.statics.UU5_Common_swipeMixin.minLengthOnSwipe) {
      var duration = this.lastSwipe.endTime - this.lastSwipe.startTime;
      var speed = length / duration;

      this.lastSwipe = $.extend(
        {}, this.lastSwipe, {
          left: this.lastSwipe.dx < -UU5.Common.swipeMixin.statics.UU5_Common_swipeMixin.minLengthOnSwipe,
          right: this.lastSwipe.dx > UU5.Common.swipeMixin.statics.UU5_Common_swipeMixin.minLengthOnSwipe,
          down: this.lastSwipe.dy > -UU5.Common.swipeMixin.statics.UU5_Common_swipeMixin.minLengthOnSwipe && this.lastSwipe.dy > 0,
          up: this.lastSwipe.dy < UU5.Common.swipeMixin.statics.UU5_Common_swipeMixin.minLengthOnSwipe && this.lastSwipe.dy < 0,
          long: length > UU5.Common.swipeMixin.statics.UU5_Common_swipeMixin.maxLengthOnShortSwipe,
          fast: speed > UU5.Common.swipeMixin.statics.UU5_Common_swipeMixin.maxSpeedOnSlowSwipe,
          length: length,
          speed: length / duration,
          angle: this.angle(this.lastSwipe.x, this.lastSwipe.y, this.lastSwipe.ex, this.lastSwipe.ey)
        }
      );

      typeof handler === 'function' && handler(arguments);
    }
    return this;
  },

  angle: function (x, y, ex, ey) {
    var dy = y - ey;
    var dx = ex - x;
    var theta = Math.atan2(dy, dx);
    theta *= 180 / Math.PI;
    //if (theta < 0) theta = 360 + theta;
    return theta;
  },

  // Overriding Functions

  // Component Specific Helpers
  _getUpdatedSwipe: function (props) {
    return {
      up: props.swiped === 'up' || props.swiped === 'upRight' || props.swiped === 'upLeft',
      right: props.swiped === 'right' || props.swiped === 'upRight' || props.swiped === 'downRight',
      down: props.swiped === 'down' || props.swiped === 'downRight' || props.swiped === 'downLeft',
      left: props.swiped === 'left' || props.swiped === 'upLeft' || props.swiped === 'downLeft',
      long: props.swipedLong,
      fast: props.swipedFast
    };
  }
};
'use strict';

var UU5 = UU5 || {};
UU5.Tools = UU5.Tools || {};

UU5.Tools.checkTag = function (tag) {
  var cTag = tag;
  if (typeof cTag === 'string') {
    var tagArray = cTag.split('.');
    var isSimpleString = tagArray.length === 1;
    var calculatedTag = window;
    while (calculatedTag && tagArray.length > 0) {
      calculatedTag = calculatedTag[tagArray.shift()];
    }
    cTag = calculatedTag ? calculatedTag : isSimpleString ? cTag : null;
  }
  if (typeof cTag !== 'function' && typeof cTag !== 'string') {
    console.error('Wrong tag ' + tag + ' - element was not found.', {
      notFoundObject: cTag,
      notFoundObjectType: typeof cTag
    });
    cTag = null;
  }
  return cTag;
};

UU5.Tools.buildAttributes = function (attrsString) {
  // Regular Expressions for parsing tags and attributes
  // var startTag0 = /^<([-A-Za-z0-9_]+)((?:\s+\w+(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)>/,
  //   endTag0 = /^<\/([-A-Za-z0-9_]+)[^>]*>/,
  //   attr0 = /([-A-Za-z0-9_]+)(?:\s*=\s*(?:(?:"((?:\\.|[^"])*)")|(?:'((?:\\.|[^'])*)')|([^>\s]+)))?/g;

  var attrs = {};

  var attrsRegEx = /([-A-Za-z0-9_]+)(?:\s*=\s*(?:(?:"((?:\\.|[^"])*)")|(?:'((?:\\.|[^'])*)')|([^\s]+)))?/g;
  var matchAttrs = attrsRegEx.exec(attrsString.trim());

  while (matchAttrs) {
    var name = matchAttrs[1];
    var value = true;
    var matchValue = matchAttrs[2] || matchAttrs[3];

    if (matchValue) {
      // react input
      var innerString = /^{(.*)}$/.exec(matchValue);
      // TODO how check backslash before { ?
      if (innerString) {
        if (innerString[1] === "true") {
          value = true;
        } else if (innerString[1] === "false") {
          value = false;
        } else if (isFinite(innerString[1])) {
          value = +innerString[1];
        } else if (/^({.*})|(\[.*\])$/.exec(innerString[1])) {
          value = JSON.stringify(innerString[1]);

        } else if (/^<.*>$/.exec(innerString[1])) {
          value = UU5.Tools.getChildrenFromHTML(innerString[1]);
        } else {
          console.error("Unknown value " + value + " of attribute " + name + ".");
          value = innerString[1];
        }

        // string
      } else {
        value = matchValue;
      }
    }

    attrs[name] = value;

    matchAttrs = attrsRegEx.exec(attrsString);
  }

  return attrs;
};

// opt (Object) - tagsRegExp, ignoreSpaces, ignoreGrammar, language - defaults from UU5.Bricks.textCorrector
UU5.Tools.getChildrenFromHTML = function (html, opt) {
  opt = opt || {};

  var tagsRegExp = opt.tagsRegExp || /.*/;

  // first level
  var reS = /<([-A-Za-z0-9_\.]+)((?:\s+\w+(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|(?:\{.*})|[^>\s]+))?)*)\s*(\/?)>/g;
  var index = 0;

  var children = [];

  var matchS = reS.exec(html);
  while (matchS) {
    var childTag = matchS[1];
    var childAttrs = UU5.Tools.buildAttributes(matchS[2]);
    var isChildSelfClosing = matchS[3] === "/";
    var childChildren = null;

    var beforeTextLength = matchS.index - index;
    if (beforeTextLength) {
      // text before
      var beforeText = html.substr(index, beforeTextLength);
      children.push(beforeText);
      index += beforeTextLength;
    }
    if (isChildSelfClosing) {
      index += matchS[0].length;
    } else {
      var reSE = new RegExp('(<' + childTag + "((?:\s+\w+(?:\s*=\s*(?:(?:\"[^\"]*\")|(?:'[^']*')|(?:\{[^\}]*\})|[^>\s]+))?)*)\s*>|<\/" + childTag + "[^>]*>)", 'g');
      var matchSE = matchS;
      var startIndex = matchS.index;
      var endIndex = null;
      var startLevel = 1;
      var endLevel = 0;
      reSE.lastIndex = reS.lastIndex;

      while (startLevel !== endLevel && matchSE) {
        reSE.lastIndex = matchSE.index + matchSE[0].length;
        matchSE = reSE.exec(html);
        if (matchSE) {
          matchSE[0][1] !== '/' ? ++startLevel : ++endLevel;
        } else {
          console.error("Invalid HTML", { html: html, tag: childTag, position: startIndex });
          return React.createElement(UU5.Bricks.error, null, "Invalid HTML: " + html + ". Tag '" + childTag + "' at position " + startIndex + " is not closed.");
        }
      }

      if (!childChildren) {
        if (startLevel !== endLevel) {
          console.error("Start level " + startLevel + " is not same as end level " + endLevel + ".");
        } else {
          endIndex = reSE.lastIndex;

          index += endIndex - startIndex;
          if (!childTag.match(tagsRegExp)) {
            console.warn("Unexpected HTML tag:", childTag);
            childTag = "span"; // TODO def tag, attrs
            childAttrs = null;
          } else {
            var childHtml = html.substr(startIndex, endIndex - startIndex);
            var childInnerHtml = childHtml.substr(matchS[0].length, endIndex - startIndex - matchS[0].length - matchSE[0].length);

            if (childInnerHtml) {
              childChildren = UU5.Tools.getChildrenFromHTML(childInnerHtml, opt);
            }
          }
        }
      }
    }
    children.push(React.createElement(UU5.Tools.checkTag(childTag), childAttrs, childChildren));
    reS.lastIndex = index;
    matchS = reS.exec(html);
  }

  if (index !== html.length) {
    // text after
    var afterText = html.substr(index, html.length - index);
    children.push(afterText);
  }

  return React.Children.toArray(children);
};

UU5.Tools.pad = function (n, width, z) {
  //return width times leading z for n ... pad(99,5,'-') -> '---99'
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
};

UU5.Tools.serverRequestGet = function (src, parameters, done, fail) {
  // function done (data);
  // function fail ();
  $.get(src, parameters)
    .done(done)
    .fail(fail);
};

'use strict';

var UU5 = UU5 || {};

/*
 * UU5.Layout
 */
UU5.Layout = UU5.Layout || {};

'use strict';

var UU5 = UU5 || {};
UU5.Layout = UU5.Layout || {};

UU5.Layout.columnMixin = {

  statics: {
    layoutType: 'column',
    UU5_Layout_columnMixin: {
      defaults: {
        bsColWidth: { xs: 12, sm: 12, md: 12, lg: 12 }
      },
      errors: {
        rowNotFound: "Row was not found in parent's hierarchy."
      }
    }
  },

  getInitialState: function () {
    // initialize
    this.registerMixin('UU5_Layout_columnMixin');
    // state
    return null;
  },

  // Interface

  hasUU5_Layout_columnMixin: function () {
    return this.hasMixin('UU5_Layout_columnMixin');
  },

  getUU5_Layout_columnMixinProps: function () {
    return {
      bsColWidth: this.props.bsColWidth || this.getDefault('bsColWidth', 'UU5_Layout_columnMixin')
    };
  },

  getUU5_Layout_columnMixinPropsToPass: function () {
    return this.getUU5_Layout_columnMixinProps();
  },

  buildLayoutColumnClassName: function (className) {
    return className + ' ' + this._buildColWidthClassName(this.props.bsColWidth);
  },

  getRow: function () {
    var parent = this.getParentByType('hasUU5_Layout_rowMixin');
    !parent && this.showError('rowNotFound', null, {
      mixinName: 'UU5_Layout_columnMixin'
    });
    return parent || null;
  },

  getRowCollection: function () {
    return this.getParentByType('hasUU5_Layout_rowCollectionMixin');
  },

  getContainer: function () {
    return this.getRow().getContainer();
  },

  getContainerCollection: function () {
    return this.getRow().getContainerCollection();
  },

  getRoot: function () {
    return this.getContainer().getRoot();
  },

  // Overriding Functions

  // Component Specific Helpers

  _buildColWidthClassName: function (bsColWidth) {
    var newBsColWidth = bsColWidth || this.getDefault('bsColWidth', 'UU5_Layout_columnMixin');

    if (typeof newBsColWidth === 'string') {
      var bsColWidthArray = newBsColWidth.split(' ');
      newBsColWidth = {};
      bsColWidthArray.forEach(function (colWidth) {
        var match = colWidth.match(/^(.*)-(\d+)$/);
        newBsColWidth[match[1]] = parseInt(match[2]);
      });
    }

    var sizeClassNames = [];
    var lowerWidth = 12;

    !newBsColWidth.xs && lowerWidth && (newBsColWidth.xs = lowerWidth);
    (lowerWidth = newBsColWidth.xs) && sizeClassNames.push('col-xs-' + newBsColWidth.xs);
    !newBsColWidth.sm && lowerWidth && (newBsColWidth.sm = lowerWidth);
    (lowerWidth = newBsColWidth.sm) && sizeClassNames.push('col-sm-' + newBsColWidth.sm);
    !newBsColWidth.md && lowerWidth && (newBsColWidth.md = lowerWidth);
    (lowerWidth = newBsColWidth.md) && sizeClassNames.push('col-md-' + newBsColWidth.md);
    !newBsColWidth.lg && lowerWidth && (newBsColWidth.lg = lowerWidth);
    newBsColWidth.lg && sizeClassNames.push('col-lg-' + newBsColWidth.lg);

    newBsColWidth['xs-offset'] && sizeClassNames.push('col-xs-offset-' + newBsColWidth['xs-offset']);
    newBsColWidth['sm-offset'] && sizeClassNames.push('col-sm-offset-' + newBsColWidth['sm-offset']);
    newBsColWidth['md-offset'] && sizeClassNames.push('col-md-offset-' + newBsColWidth['md-offset']);
    newBsColWidth['lg-offset'] && sizeClassNames.push('col-lg-offset-' + newBsColWidth['lg-offset']);

    return sizeClassNames.join(' ');
  }
};

'use strict';

UU5.Layout = UU5.Layout || {};

UU5.Layout.containerMixin = {

  statics: {
    layoutType: 'container',
    UU5_Layout_containerMixin: {
      defaults: {
        bsContainerStyle: 'standard',
        standard: 'container',
        fluid: 'container-fluid'
      },
      errors: {
        rootNotFound: "Root was not found in parent's hierarchy."
      }
    }
  },

  getInitialState: function () {
    // initialize
    this.registerMixin('UU5_Layout_containerMixin');
    // state
    return null;
  },

  // Interface

  hasUU5_Layout_containerMixin: function () {
    return this.hasMixin('UU5_Layout_containerMixin');
  },

  getUU5_Layout_containerMixinProps: function () {
    return {};
  },

  getUU5_Layout_containerMixinPropsToPass: function () {
    return this.getUU5_Layout_containerMixinProps();
  },

  buildLayoutContainerClassName: function (className) {
    var bsContainerStyle = this.props.bsContainerStyle || this.getDefault('bsContainerStyle', 'UU5_Layout_containerMixin');
    return className + ' ' + this.getDefault(bsContainerStyle, 'UU5_Layout_containerMixin');
  },

  getRoot: function () {
    var parent = this.getParentByType('hasUU5_Layout_rootMixin');
    !parent && this.showError('rootNotFound', null, {
      mixinName: 'UU5_Layout_containerMixin'
    });
    return parent || null;
  }

  // Overriding Functions

  // Component Specific Helpers
};

'use strict';

var UU5 = UU5 || {};
UU5.Layout = UU5.Layout || {};

UU5.Layout.flcMixin = {

  statics: {
    layoutType: 'flc',
    UU5_Layout_flcMixin: {
      errors: {
        wrapperNotFound: "Wrapper was not found in parent's hierarchy."
      }
    }
  },

  getInitialState: function () {
    // initialize
    this.registerMixin('UU5_Layout_flcMixin');
    // state
    return null;
  },

  // Interface

  hasUU5_Layout_flcMixin: function () {
    return this.hasMixin('UU5_Layout_flcMixin');
  },

  getUU5_Layout_flcMixinProps: function () {
    return {};
  },

  getUU5_Layout_flcMixinPropsToPass: function () {
    return this.getUU5_Layout_flcMixinProps();
  },

  getWrapper: function () {
    var parent = this.getParent();
    (!parent || !parent.hasUU5_Layout_wrapperMixin) && this.showError('wrapperNotFound', null, {
      mixinName: 'UU5_Layout_flcMixin',
      parent: {
        tagName: parent ? parent.getTagName() : null,
        component: parent
      }
    });
    return parent || null;
  },

  getWrapperCollection: function () {
    return this.getParentByType('hasUU5_Layout_wrapperCollectionMixin');
  },

  getWrapperIndex: function () {
    return this.getWrapper().getIndex();
  },

  getColumn: function () {
    return this.getWrapper().getColumn();
  },

  getColumnCollection: function () {
    return this.getWrapper().getColumnCollection();
  },

  getColumnIndex: function () {
    return this.getColumn().getIndex();
  },

  getRow: function () {
    return this.getWrapper().getRow();
  },

  getRowCollection: function () {
    return this.getWrapper().getRowCollection();
  },

  getRowIndex: function () {
    return this.getRow().getIndex();
  },

  getContainer: function () {
    return this.getWrapper().getContainer();
  },

  getContainerCollection: function () {
    return this.getWrapper().getContainerCollection();
  },

  getContainerIndex: function () {
    return this.getContainer().getIndex();
  },

  getRoot: function () {
    return this.getWrapper().getRoot();
  }

};
'use strict';

var UU5 = UU5 || {};
UU5.Layout = UU5.Layout || {};

UU5.Layout.rootMixin = {

  statics: {
    layoutType: 'root',
    UU5_Layout_rootMixin: {}
  },

  getInitialState: function () {
    // initialize
    this.registerMixin('UU5_Layout_rootMixin');
    // state
    return null;
  },

  // Interface

  hasUU5_Layout_rootMixin: function () {
    return this.hasMixin('UU5_Layout_rootMixin');
  },

  getUU5_Layout_rootMixinProps: function () {
    return {};
  },

  getUU5_Layout_rootMixinPropsToPass: function () {
    return this.getUU5_Layout_rootMixinProps();
  }

  // Overriding Functions

  // Component Specific Helpers
};
'use strict';

var UU5 = UU5 || {};
UU5.Layout = UU5.Layout || {};

UU5.Layout.rowMixin = {

  statics: {
    layoutType: 'row',
    UU5_Layout_rowMixin: {
      errors: {
        containerNotFound: "Container was not found in parent's hierarchy."
      }
    }
  },

  getInitialState: function () {
    // initialize
    this.registerMixin('UU5_Layout_rowMixin');
    // state
    return null;
  },

  // Interface

  hasUU5_Layout_rowMixin: function () {
    return this.hasMixin('UU5_Layout_rowMixin');
  },

  getUU5_Layout_rowMixinProps: function () {
    return $.extend(true, {}, {});
  },

  getUU5_Layout_rowMixinPropsToPass: function () {
    return this.getUU5_Layout_rowMixinProps();
  },

  getContainer: function () {
    var parent = this.getParentByType('hasUU5_Layout_containerMixin');
    !parent && this.showError('containerNotFound', null, {
      mixinName: 'UU5_Layout_rowMixin'
    });
    return parent;
  },

  getContainerCollection: function () {
    return this.getParentByType('hasUU5_Layout_containerCollectionMixin');
  },

  getRoot: function () {
    return this.getContainer().getRoot();
  }

  // Overriding Functions

  // Component Specific Helpers
};

'use strict';

var UU5 = UU5 || {};
UU5.Layout = UU5.Layout || {};

UU5.Layout.wrapperMixin = {

  statics: {
    layoutType: 'wrapper',
    UU5_Layout_wrapperMixin: {
      errors: {
        columnNotFound: "Column was not found in parent's hierarchy."
      }
    }
  },

  getInitialState: function () {
    // initialize
    this.registerMixin('UU5_Layout_wrapperMixin');
    // state
    return null;
  },

  // Interface

  hasUU5_Layout_wrapperMixin: function () {
    return this.hasMixin('UU5_Layout_wrapperMixin');
  },

  getUU5_Layout_wrapperMixinProps: function () {
    return {};
  },

  getUU5_Layout_wrapperMixinToPass: function () {
    return this.getUU5_Layout_wrapperMixinProps();
  },

  getColumn: function () {
    var parent = this.getParentByType('hasUU5_Layout_columnMixin');
    !parent && this.showError('columnNotFound', null, {
      mixinName: 'UU5_Layout_wrapperMixin'
    });
    return parent;
  },

  getColumnCollection: function () {
    return this.getParentByType('hasUU5_Layout_columnCollectionMixin');
  },

  getRow: function () {
    return this.getColumn().getRow();
  },

  getRowCollection: function () {
    return this.getColumn().getRowCollection();
  },

  getContainer: function () {
    return this.getRow().getContainer();
  },

  getContainerCollection: function () {
    return this.getRow().getContainerCollection();
  },

  getRoot: function () {
    return this.getContainer().getRoot();
  }

  // Overriding Functions

  // Component Specific Helpers
};

'use strict';

var UU5 = UU5 || {};
UU5.Layout = UU5.Layout || {};

UU5.Layout.root = React.createClass({
  displayName: 'root',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin, UU5.Common.ccrWriterMixin, UU5.Layout.rootMixin],

  statics: {
    tagName: 'UU5.Layout.root',
    classNames: {
      main: 'UU5_Layout_root'
    },
    defaults: {
      childrenLayoutType: ['container', 'containerCollection'],
      parentsLayoutType: ['root']
    },
    errors: {
      childIsWrong: 'Child %s is not container or container collection.'
    }
  },

  // Interface

  // Overriding Functions
  expandChild_: function (child, key) {
    var result = child;

    if (typeof child.type !== 'function' || !child.type.layoutType || this.getDefault().childrenLayoutType.indexOf(child.type.layoutType) === -1) {
      if (this.getDefault().parentsLayoutType.indexOf(child.type.layoutType) > -1) {
        this.showError('childIsWrong', child.type.tagName, {
          context: {
            child: {
              tagName: child.type.tagName,
              component: child
            }
          }
        });
      } else {
        result = React.createElement(
          UU5.Layout.container,
          {
            parent: this,
            id: this.getId() + '-container',
            key: key,
            bsContainerStyle: child.props.bsContainerStyle
          },
          child
        );
      }
    }

    return result;
  },

  // Component Specific Helpers

  // Render
  render: function () {
    return React.createElement(
      'div',
      this.buildMainAttrs(),
      this.getHeaderChild(),
      this.getFilteredSorterChildren(),
      this.getFooterChild(),
      this.getDisabledCover()
    );
  }
});

'use strict';

UU5.Layout = UU5.Layout || {};

UU5.Layout.container = React.createClass({
  displayName: 'container',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin, UU5.Common.ccrWriterMixin, UU5.Layout.containerMixin],

  statics: {
    tagName: 'UU5.Layout.container',
    classNames: {
      main: 'UU5_Layout_container'
    },
    defaults: {
      childrenLayoutType: ['row', 'rowCollection'],
      parentsLayoutType: UU5.Layout.root.defaults.parentsLayoutType.concat(['container', 'containerCollection'])
    },
    errors: {
      childIsWrong: 'Child %s is not row or row collection.'
    }
  },

  // Interface

  // Overriding Functions
  expandChild_: function (child, key) {
    var result = child;

    if (typeof child.type !== 'function' || !child.type.layoutType || this.getDefault().childrenLayoutType.indexOf(child.type.layoutType) === -1) {
      if (this.getDefault().parentsLayoutType.indexOf(child.type.layoutType) > -1) {
        this.showError('childIsWrong', child.type.tagName, {
          context: {
            child: {
              tagName: child.type.tagName,
              component: child
            }
          }
        });
      } else {
        result = React.createElement(
          UU5.Layout.row,
          {
            parent: this,
            id: this.getId() + '-row',
            key: key
          },
          child
        );
      }
    }

    return result;
  },

  // Component Specific Helpers

  // Render
  render: function () {
    var mainAttrs = this.buildMainAttrs();
    mainAttrs.className = this.buildLayoutContainerClassName(mainAttrs.className);

    return React.createElement(
      'div',
      mainAttrs,
      this.getHeaderChild(),
      this.getFilteredSorterChildren(),
      this.getFooterChild(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Layout = UU5.Layout || {};

UU5.Layout.row = React.createClass({
  displayName: 'row',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin, UU5.Common.ccrWriterMixin, UU5.Layout.rowMixin],

  statics: {
    tagName: 'UU5.Layout.row',
    classNames: {
      main: 'UU5_Layout_row row',
      flex: 'UU5_Layout_row-flex',
      flexFull: 'UU5_Layout_row-flex-full'
    },
    defaults: {
      childrenLayoutType: ['column', 'columnCollection'],
      parentsLayoutType: UU5.Layout.container.defaults.parentsLayoutType.concat(['row', 'rowCollection'])
    },
    errors: {
      childIsWrong: 'Child %s is not column or column collection.'
    }
  },

  // Interface

  // Overriding Functions
  expandChild_: function (child, key) {
    var result = child;

    if (typeof child.type !== 'function' || !child.type.layoutType || this.getDefault().childrenLayoutType.indexOf(child.type.layoutType) === -1) {
      if (this.getDefault().parentsLayoutType.indexOf(child.type.layoutType) > -1) {
        this.showError('childIsWrong', child.type.tagName, {
          context: {
            child: {
              tagName: child.type.tagName,
              component: child
            }
          }
        });
      } else {
        result = React.createElement(
          UU5.Layout.column,
          {
            parent: this,
            id: this.getId() + '-column',
            key: key,
            bsColWidth: child.props.bsColWidth
          },
          child
        );
      }
    }

    return result;
  },

  // Component Specific Helpers
  _getMainAttrs: function () {
    var mainAttrs = this.buildMainAttrs();

    switch (this.props.display) {
      case 'flex':
        mainAttrs.className += ' ' + this.getClassName().flex;
        break;
      case 'flex-full':
        mainAttrs.className += ' ' + this.getClassName().flexFull;
        break;
    }

    return mainAttrs;
  },

  // Render
  render: function () {
    return React.createElement(
      'div',
      this._getMainAttrs(),
      this.getHeaderChild(),
      this.getFilteredSorterChildren(),
      this.getFooterChild(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Layout = UU5.Layout || {};

UU5.Layout.column = React.createClass({
  displayName: 'column',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin, UU5.Common.ccrWriterMixin, UU5.Layout.columnMixin],

  statics: {
    tagName: 'UU5.Layout.column',
    classNames: {
      main: 'UU5_Layout_column'
    },
    defaults: {
      childrenLayoutType: ['wrapper', 'wrapperCollection'],
      parentsLayoutType: UU5.Layout.row.defaults.parentsLayoutType.concat(['column', 'columnCollection'])
    },
    errors: {
      childIsWrong: 'Child %s is not wrapper or wrapper collection.'
    }
  },

  // Interface

  // Overriding Functions
  expandChild_: function (child, key) {
    var result = child;

    if (typeof child.type !== 'function' || !child.type.layoutType || this.getDefault().childrenLayoutType.indexOf(child.type.layoutType) === -1) {
      if (this.getDefault().parentsLayoutType.indexOf(child.type.layoutType) > -1) {
        this.showError('childIsWrong', child.type.tagName, {
          context: {
            child: {
              tagName: child.type.tagName,
              component: child
            }
          }
        });
      } else {
        result = React.createElement(
          UU5.Layout.wrapper,
          {
            parent: this,
            id: this.getId() + '-wrapper',
            key: key
          },
          child
        );
      }
    }

    return result;
  },

  // Component Specific Helpers

  // Render
  render: function () {
    var mainAttrs = this.buildMainAttrs();
    mainAttrs.className = this.buildLayoutColumnClassName(mainAttrs.className);

    return React.createElement(
      'div',
      mainAttrs,
      this.getHeaderChild(),
      this.getFilteredSorterChildren(),
      this.getFooterChild(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Layout = UU5.Layout || {};

UU5.Layout.wrapper = React.createClass({
  displayName: 'wrapper',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin, UU5.Common.ccrWriterMixin, UU5.Layout.wrapperMixin],

  statics: {
    tagName: 'UU5.Layout.wrapper',
    classNames: {
      main: 'UU5_Layout_wrapper'
    },
    defaults: {
      childrenLayoutType: ['flc'],
      parentsLayoutType: UU5.Layout.row.defaults.parentsLayoutType.concat(['wrapper', 'wrapperCollection'])
    },
    errors: {
      childIsNotFlc: 'Child %s is not FLC.',
      onlyOneChildIsAllowed: 'Wrapper can contain just one FLC.'
    }
  },

  getInitialState: function () {
    this.flcCounter = 0;
    return {};
  },

  // Interface

  // Overriding Functions
  expandChild_: function (child, key) {
    this.flcCounter++;
    var result = child;

    if (typeof child.type !== 'function' || !child.type.layoutType || this.getDefault().childrenLayoutType.indexOf(child.type.layoutType) === -1) {
      if (this.getDefault().parentsLayoutType.indexOf(child.type.layoutType) > -1) {
        this.showError('childIsNotFlc', child.type.tagName, {
          context: {
            child: {
              tagName: child.type.tagName,
              component: child
            }
          }
        });
      } else {
        result = React.createElement(
          UU5.Layout.flc,
          {
            parent: this,
            id: this.getId() + '-flc',
            key: key
          },
          child
        );
      }
    }

    if (this.flcCounter > 1) {
      this.showError('onlyOneChildIsAllowed', null, {
        context: {
          child: {
            tagName: child.type.tagName,
            component: child
          }
        }
      });
    }

    return result;
  },

  // Component Specific Helpers

  // Render
  render: function () {
    this.flcCounter = 0;
    return React.createElement(
      'div',
      this.buildMainAttrs(),
      this.getHeaderChild(),
      this.getFilteredSorterChildren(),
      this.getFooterChild(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Layout = UU5.Layout || {};

UU5.Layout.flc = React.createClass({
  displayName: 'flc',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin, UU5.Layout.flcMixin],

  statics: {
    tagName: 'UU5.Layout.flc',
    classNames: {
      main: 'UU5_Layout_flc'
    }
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  // Render
  render: function () {
    return React.createElement(
      'div',
      this.buildMainAttrs(),
      this.getHeaderChild(),
      this.getChildren(),
      this.getFooterChild(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Layout = UU5.Layout || {};

UU5.Layout.columnCollectionMixin = {

  statics: {
    layoutType: 'columnCollection',
    UU5_Layout_columnCollectionMixin: {
      errors: {
        rowNotFound: "Row was not found in parent's hierarchy."
      }
    }
  },

  getInitialState: function () {
    // initialize
    this.registerMixin('UU5_Layout_columnCollectionMixin');
    // state
    return null;
  },

  // Interface

  hasUU5_Layout_columnCollectionMixin: function () {
    return this.hasMixin('UU5_Layout_columnCollectionMixin');
  },

  getUU5_Layout_columnCollectionMixinProps: function () {
    return {};
  },

  getUU5_Layout_columnCollectionMixinPropsToPass: function () {
    return this.getUU5_Layout_columnCollectionMixinProps();
  },

  getRow: function () {
    var parent = this.getParentByType('hasUU5_Layout_rowMixin');
    !parent && this.showError('rowNotFound', null, {
      mixinName: 'UU5_Layout_columnCollectionMixin'
    });
    return parent || null;
  },

  getRowCollection: function () {
    return this.getParentByType('hasUU5_Layout_rowCollectionMixin');
  },

  getContainer: function () {
    return this.getRow().getContainer();
  },

  getContainerCollection: function () {
    return this.getRow().getContainerCollection();
  },

  getRoot: function () {
    return this.getContainer().getRoot();
  }
};

'use strict';

var UU5 = UU5 || {};
UU5.Layout = UU5.Layout || {};

UU5.Layout.containerCollectionMixin = {

  statics: {
    layoutType: 'containerCollection',
    UU5_Layout_containerCollectionMixin: {
      errors: {
        rootNotFound: "Root was not found in parent's hierarchy."
      }
    }
  },

  getInitialState: function () {
    // initialize
    this.registerMixin('UU5_Layout_containerCollectionMixin');
    // state
    return null;
  },

  // Interface

  hasUU5_Layout_containerCollectionMixin: function () {
    return this.hasMixin('UU5_Layout_containerCollectionMixin');
  },

  getContainerCollectionMixinProps: function () {
    return {};
  },

  getContainerCollectionMixinPropsToPass: function () {
    return this.getContainerCollectionMixinProps();
  },

  getRoot: function () {
    var parent = this.getParentByType('hasUU5_Layout_rootMixin');
    !parent && this.showError('rootNotFound', null, {
      mixinName: 'UU5_Layout_containerCollectionMixin'
    });
    return parent || null;
  }
};

'use strict';

var UU5 = UU5 || {};
UU5.Layout = UU5.Layout || {};

/**
 * Mixin says that the component is layout row collection.
 */
UU5.Layout.rowCollectionMixin = {

  statics: {
    layoutType: 'rowCollection',
    UU5_Layout_rowCollectionMixin: {
      errors: {
        containerNotFound: "Container was not found in parent's hierarchy."
      }
    }
  },

  getInitialState: function () {
    // initialize
    this.registerMixin('UU5_Layout_rowCollectionMixin');
    // state
    return null;
  },

  // Interface

  hasUU5_Layout_rowCollectionMixin: function () {
    return this.hasMixin('UU5_Layout_rowCollectionMixin');
  },

  getUU5_Layout_rowCollectionMixinProps: function () {
    return {};
  },

  getUU5_Layout_rowCollectionMixinPropsToPass: function () {
    return this.getUU5_Layout_rowCollectionMixinProps();
  },

  getContainer: function () {
    var parent = this.getParentByType('hasUU5_Layout_containerMixin');
    !parent && this.showError('containerNotFound', null, {
      mixinName: 'UU5_Layout_rowCollectionMixin'
    });
    return parent;
  },

  getContainerCollection: function () {
    return this.getParentByType('hasUU5_Layout_containerCollectionMixin');
  },

  getRoot: function () {
    return this.getContainer().getRoot();
  }
};
'use strict';

var UU5 = UU5 || {};
UU5.Layout = UU5.Layout || {};

UU5.Layout.wrapperCollectionMixin = {

  statics: {
    layoutType: 'wrapperCollection',
    UU5_Layout_wrapperCollectionMixin: {
      errors: {
        columnNotFound: "Column was not found in parent's hierarchy."
      }
    }
  },

  getInitialState: function () {
    // initialize
    this.registerMixin('UU5_Layout_wrapperCollectionMixin');
    // state
    return null;
  },

  // Interface

  hasUU5_Layout_wrapperCollectionMixin: function () {
    return this.hasMixin('UU5_Layout_wrapperCollectionMixin');
  },

  getUU5_Layout_wrapperCollectionMixinProps: function () {
    return {};
  },

  getUU5_Layout_wrapperCollectionMixinPropsToPass: function () {
    return this.getUU5_Layout_wrapperCollectionMixinProps();
  },

  getColumn: function () {
    var parent = this.getParentByType('hasUU5_Layout_columnMixin');
    !parent && this.showError('columnNotFound', null, {
      mixinName: 'UU5_Layout_wrapperCollectionMixin'
    });
    return parent;
  },

  getColumnCollection: function () {
    return this.getParentByType('hasUU5_Layout_columnCollectionMixin');
  },

  getRow: function () {
    return this.getColumn().getRow();
  },

  getRowCollection: function () {
    return this.getColumn().getRowCollection();
  },

  getContainer: function () {
    return this.getRow().getContainer();
  },

  getContainerCollection: function () {
    return this.getRow().getContainerCollection();
  },

  getRoot: function () {
    return this.getContainer().getRoot();
  }
};

'use strict';

var UU5 = UU5 || {};
UU5.Layout = UU5.Layout || {};

UU5.Layout.containerCollection = React.createClass({
  displayName: 'containerCollection',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin, UU5.Common.ccrWriterMixin, UU5.Layout.containerCollectionMixin],

  statics: {
    tagName: 'UU5.Layout.containerCollection',
    classNames: {
      main: 'UU5_Layout_containerCollection'
    },
    defaults: {
      childrenLayoutType: ['container', 'containerCollection'],
      parentsLayoutType: UU5.Layout.root.defaults.parentsLayoutType.concat(['container'])
    },
    errors: {
      childIsWrong: 'Child %s is not container or container collection.'
    }
  },

  // Interface

  // Overriding Functions
  expandChild_: function (child, key) {
    var result = child;

    if (typeof child.type !== 'function' || !child.type.layoutType || this.getDefault().childrenLayoutType.indexOf(child.type.layoutType) === -1) {
      if (this.getDefault().parentsLayoutType.indexOf(child.type.layoutType) > -1) {
        this.showError('childIsWrong', child.type.tagName, {
          context: {
            child: {
              tagName: child.type.tagName,
              component: child
            }
          }
        });
      } else {
        result = React.createElement(
          UU5.Layout.container,
          {
            parent: this,
            id: this.getId() + '-container',
            key: key,
            bsContainerStyle: child.props.bsContainerStyle
          },
          child
        );
      }
    }

    return result;
  },

  // Component Specific Helpers

  // Render
  render: function () {
    return React.createElement(
      'div',
      this.buildMainAttrs(),
      this.getHeaderChild(),
      this.getChildren(),
      this.getFooterChild(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Layout = UU5.Layout || {};

UU5.Layout.rowCollection = React.createClass({
  displayName: 'rowCollection',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin, UU5.Common.ccrWriterMixin, UU5.Layout.rowCollectionMixin],

  statics: {
    tagName: 'UU5.Layout.rowCollection',
    classNames: {
      main: 'UU5_Layout_rowCollection'
    },
    defaults: {
      childrenLayoutType: ['row', 'rowCollection'],
      parentsLayoutType: UU5.Layout.containerCollection.defaults.parentsLayoutType.concat(['containerCollection', 'row'])
    },
    errors: {
      childIsWrong: 'Child %s is not row or row collection.'
    }
  },

  // Interface

  // Overriding Functions
  expandChild_: function (child, key) {
    var result = child;

    if (typeof child.type !== 'function' || !child.type.layoutType || this.getDefault().childrenLayoutType.indexOf(child.type.layoutType) === -1) {
      if (this.getDefault().parentsLayoutType.indexOf(child.type.layoutType) > -1) {
        this.showError('childIsWrong', child.type.tagName, {
          context: {
            child: {
              tagName: child.type.tagName,
              component: child
            }
          }
        });
      } else {
        result = React.createElement(
          UU5.Layout.row,
          {
            parent: this,
            id: this.getId() + '-row',
            key: key
          },
          child
        );
      }
    }

    return result;
  },

  // Component Specific Helpers

  // Render
  render: function () {
    return React.createElement(
      'div',
      this.buildMainAttrs(),
      this.getHeaderChild(),
      this.getChildren(),
      this.getFooterChild(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Layout = UU5.Layout || {};

UU5.Layout.columnCollection = React.createClass({
  displayName: 'columnCollection',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin, UU5.Common.ccrWriterMixin, UU5.Layout.columnCollectionMixin],

  statics: {
    tagName: 'UU5.Layout.columnCollection',
    classNames: {
      main: 'UU5_Layout_columnCollection'
    },
    defaults: {
      childrenLayoutType: ['column', 'columnCollection'],
      parentsLayoutType: UU5.Layout.rowCollection.defaults.parentsLayoutType.concat(['rowCollection', 'column'])
    },
    errors: {
      childIsNotColumn: 'Child %s is not column or column collection.'
    }
  },

  // Interface

  // Overriding Functions
  expandChild_: function (child, key) {
    var result = child;

    if (typeof child.type !== 'function' || !child.type.layoutType || this.getDefault().childrenLayoutType.indexOf(child.type.layoutType) === -1) {
      if (this.getDefault().parentsLayoutType.indexOf(child.type.layoutType) > -1) {
        this.showError('childIsWrong', child.type.tagName, {
          context: {
            child: {
              tagName: child.type.tagName,
              component: child
            }
          }
        });
      } else {
        result = React.createElement(
          UU5.Layout.column,
          {
            parent: this,
            id: this.getId() + '-column',
            key: key,
            bsColWidth: child.props.bsColWidth
          },
          child
        );
      }
    }

    return result;
  },

  // Component Specific Helpers

  // Render
  render: function () {
    return React.createElement(
      'div',
      this.buildMainAttrs(),
      this.getHeaderChild(),
      this.getChildren(),
      this.getFooterChild(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Layout = UU5.Layout || {};

UU5.Layout.wrapperCollection = React.createClass({
  displayName: 'wrapperCollection',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin, UU5.Common.ccrWriterMixin, UU5.Layout.wrapperCollectionMixin],

  statics: {
    tagName: 'UU5.Layout.wrapperCollection',
    classNames: {
      main: 'UU5_Layout_wrapperCollection'
    },
    defaults: {
      childrenLayoutType: ['wrapper', 'wrapperCollection'],
      parentsLayoutType: UU5.Layout.columnCollection.defaults.parentsLayoutType.concat(['columnCollection', 'wrapper'])
    },
    errors: {
      childIsNotWrapper: 'Child %s is not wrapper or wrapper collection.'
    }
  },

  // Interface

  // Overriding Functions
  expandChild_: function (child, key) {
    var result = child;

    if (typeof child.type !== 'function' || !child.type.layoutType || this.getDefault().childrenLayoutType.indexOf(child.type.layoutType) === -1) {
      if (this.getDefault().parentsLayoutType.indexOf(child.type.layoutType) > -1) {
        this.showError('childIsWrong', child.type.tagName, {
          context: {
            child: {
              tagName: child.type.tagName,
              component: child
            }
          }
        });
      } else {
        result = React.createElement(
          UU5.Layout.wrapper,
          {
            parent: this,
            id: this.getId() + '-wrapper',
            key: key
          },
          child
        );
      }
    }

    return result;
  },

  // Component Specific Helpers

  // Render
  render: function () {
    return React.createElement(
      'div',
      this.buildMainAttrs(),
      this.getHeaderChild(),
      this.getChildren(),
      this.getFooterChild(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};

/*
 * UU5.Bricks
 */
UU5.Bricks = UU5.Bricks || {};
UU5.Bricks.factory = UU5.Bricks.factory || {};
'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * Accordion component based on bootstrap standard. Accordion consists of panels. With default settings there can
 * always be only one panel expanded (all other panels will automatically collapse upon opening a panel).
 * This can be changed by setting onClickNotCollapseOthers property to false.
 *
 * @see UU-BT:44191585619841331
 * @tutorial https://jsfiddle.net/uu_ui/5q8xuLm9/
 * @class
 */
UU5.Bricks.accordion = React.createClass({
  displayName: 'accordion',

  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.accordion',
    classNames: {
      main: 'UU5_Bricks_accordion panel-group'
    },
    defaults: {
      child: { tag: 'UU5.Bricks.accordion.panel', props: {} }
    },
    warnings: {
      unsupportedType: 'Type %s of parameter %s is not supported. Allowed types are: %s.'
    }
  },

  propTypes: {
    /**
     * Array of panel's props.
     */
    panels: React.PropTypes.arrayOf(React.PropTypes.object),

    /**
     * If set true any number of panels can be opened at the same time and no panels will collapse automatically.
     */
    onClickNotCollapseOthers: React.PropTypes.bool,

    /**
     * Color style of all panels. This property is set for all panels if they do not have another one.
     */
    colorSchema: React.PropTypes.oneOf(['default', 'primary', 'success', 'info', 'warning', 'danger']),

    /**
     * Glyphicon name or UU5.Bricks.glyphicon props for opened panel. Glyphicon is set for all opened panels
     * if they do not have specifically set another one.
     */
    glyphiconExpand: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.object]),

    /**
     * Glyphicon name or UU5.Bricks.glyphicon props for closed panel. Glyphicon is set to all closed panels
     * if they do not have specifically set another one.
     */
    glyphiconCollapse: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.object]),

    /**
     * If user clicks on a panel, that panel is toggled and onClick function is called in the callback - the parameter
     * is an instance of the clicked panel. Function is set to all panels if they do not have specifically
     * set another one.
     * Called: this.props.onClick(panel);
     */
    onClick: React.PropTypes.func
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      panels: null,
      colorSchema: 'default',
      glyphiconExpand: null,
      glyphiconCollapse: null,
      onClickNotCollapseOthers: false,
      onClick: null
    };
  },

  // Interface

  /**
   * Finds panel (child) by ID.
   *
   * @param {string} panelId - Unique panel ID.
   * @returns {object} Instance of rendered panel.
   */
  getPanelById: function (panelId) {
    return this.getRenderedChildById(panelId);
  },

  /**
   * Finds panel (child) by name.
   *
   * @param {string} panelName - Panel name (if set).
   * @returns {object} Instance of rendered panel.
   */
  getPanelByName: function (panelName) {
    return this.getRenderedChildByName(panelName);
  },

  /**
   * Gets all rendered panels.
   *
   * @returns {Array} Array of rendered panels instances.
   */
  getPanels: function () {
    return this.getRenderedChildren();
  },

  /**
   * Function as parameter 'callback' goes through all rendered panels and returns their instance and index.
   * If return value is false, the cycle is stopped.
   *
   * @param {function} callback - Function has parameters: panel instance and index. If returns false, the cycle stops.
   * @returns {UU5.Bricks.accordion} this
   */
  eachPanel: function (callback) {
    var panels = this.getPanels();
    for (var i = 0; i < panels.length; i++) {
      var result = callback(panels[i], i);
      if (result === false) {
        break;
      }
    }
    return this;
  },

  /**
   * Function as parameter 'callback' goes through all rendered panels and returns their instance and index.
   * If return value is false, the cycle is stopped.
   *
   * @param {Array|string} ids - Array of one or more panel's IDs which will be processed by a callback function.
   * @param {function} callback - Function has parameters: panel instance and index. If returns false, the cycle stops.
   * @returns {UU5.Bricks.accordion} this
   */
  eachPanelByIds: function (ids, callback) {
    for (var i = 0; i < ids.length; i++) {
      var result = callback(this.getPanelById(ids[i]), i);
      if (result === false) {
        break;
      }
    }
    return this;
  },

  /**
   * Function as parameter 'callback' goes through all rendered panels and returns their instance and index.
   * If return value is false, the cycle is stopped.
   *
   * @param {Array|string} names - Array of one or more panel's names which will be processed by a callback function.
   * @param {function} callback - Function has parameters: panel instance and index. IIf returns false, the cycle stops.
   * @returns {UU5.Bricks.accordion} this
   */
  eachPanelByNames: function (names, callback) {
    for (var i = 0; i < names.length; i++) {
      var result = callback(this.getPanelByName(names[i]), i);
      if (result === false) {
        break;
      }
    }
    return this;
  },

  /**
   * Each panel specified by ID will be opened.
   *
   * @param {Array|string} panelId - Array of one or more panel's IDs as string, specifying panels which will be opened.
   * @param {function} setStateCallback - Callback will start after all panel's setState is finished.
   * @returns {UU5.Bricks.accordion} this
   */
  expandPanelById: function (panelId, setStateCallback) {
    this._eachPanelByIdWithCallback(panelId, setStateCallback, function (panel, i, newSetStateCallback) {
      panel.expand(newSetStateCallback);
    });
    return this;
  },

  /**
   * Each panel specified by name will be opened.
   *
   * @param {Array|string} panelName - Array of one or more panel's names as string, specifying panels which will be opened.
   * @param {function} setStateCallback - Callback will start after all panel's setState is finished.
   * @returns {UU5.Bricks.accordion} this
   */
  expandPanelByName: function (panelName, setStateCallback) {
    this._eachPanelByNameWithCallback(panelName, setStateCallback, function (panel, i, newSetStateCallback) {
      panel.expand(newSetStateCallback);
    });
    return this;
  },

  /**
   * Each panel specified by ID will be closed.
   *
   * @param {Array|string} panelId - Array of one or more panel's IDs as string, specifying panels which will be closed.
   * @param {function} setStateCallback - Callback will start after all panel's setState is finished.
   * @returns {UU5.Bricks.accordion} this
   */
  collapsePanelById: function (panelId, setStateCallback) {
    this._eachPanelByIdWithCallback(panelId, setStateCallback, function (panel, i, newSetStateCallback) {
      panel.collapse(newSetStateCallback);
    });
    return this;
  },

  /**
   * Each panel specified by name will be closed.
   *
   * @param {Array|string} panelName - Array of one or more panel's names as string, specifying panels which will be closed.
   * @param {function} setStateCallback - Callback will start after all panel's setState is finished.
   * @returns {UU5.Bricks.accordion} this
   */
  collapsePanelByName: function (panelName, setStateCallback) {
    this._eachPanelByNameWithCallback(panelName, setStateCallback, function (panel, i, newSetStateCallback) {
      panel.collapse(newSetStateCallback);
    });
    return this;
  },

  /**
   * Each panel specified by ID will be toggled.
   *
   * @param {Array|string} panelId - Array of one or more panel's IDs as string, specifying which panels will be toggled.
   * @param {function} setStateCallback - Callback will start after all panel's setState is finished.
   * @returns {UU5.Bricks.accordion} this
   */
  togglePanelById: function (panelId, setStateCallback) {
    this._eachPanelByIdWithCallback(panelId, setStateCallback, function (panel, i, newSetStateCallback) {
      panel.toggle(newSetStateCallback);
    });
    return this;
  },

  /**
   * Each panel specified by name will be toggled.
   *
   * @param {Array|string} panelName - Array of one or more panel's names as string, specifying which panels will be toggled.
   * @param {function} setStateCallback - Callback will start after all panel's setState is finished.
   * @returns {UU5.Bricks.accordion} this
   */
  togglePanelByName: function (panelName, setStateCallback) {
    this._eachPanelByNameWithCallback(panelName, setStateCallback, function (panel, i, newSetStateCallback) {
      panel.toggle(newSetStateCallback);
    });
    return this;
  },

  /**
   * Each panel will be opened.
   *
   * @param {function} setStateCallback - Callback will start after all panel's setState is finished.
   * @returns {UU5.Bricks.accordion} this
   */
  expandAll: function (setStateCallback) {
    this._eachPanelWithCallback(setStateCallback, function (panel, i, newSetStateCallback) {
      panel.expand(newSetStateCallback);
    });
    return this;
  },

  /**
   * Each panel will be closed.
   *
   * @param {function} setStateCallback - Callback will start after all panel's setState is finished.
   * @returns {UU5.Bricks.accordion} this
   */
  collapseAll: function (setStateCallback) {
    this._eachPanelWithCallback(setStateCallback, function (panel, i, newSetStateCallback) {
      panel.collapse(newSetStateCallback);
    });
    return this;
  },

  /**
   * Each panel will be toggled.
   *
   * @param {function} setStateCallback - Callback will start after all panel's setState is finished.
   * @returns {UU5.Bricks.accordion} this
   */
  toggleAll: function (setStateCallback) {
    this._eachPanelWithCallback(setStateCallback, function (panel, i, newSetStateCallback) {
      panel.toggle(newSetStateCallback);
    });
    return this;
  },

  /**
   * Returns true if only one panel can be opened by onClick (without others collapsing).
   *
   * @returns {boolean} - True - other panels should close, false - more than one panel can be opened
   */
  shouldCollapseOthers: function () {
    return !this.props.onClickNotCollapseOthers;
  },

  /**
   * Each panel, except the panel specified by panelId, will be closed.
   *
   * @param {string} panelId - Unique ID of panel which will not be closed.
   * @param {function} setStateCallback - Callback will start after all panel's setState is finished.
   * @returns {UU5.Bricks.accordion} this
   */
  collapseOthers: function (panelId, setStateCallback) {
    var panels = this.getPanels();

    var counter = 0;
    panels.forEach(function (panel) {
      panel.getId() !== panelId && panel.isExpandable() && counter++;
    });

    var newSetStateCallback = this.buildCounterCallback(setStateCallback, counter);

    panels.forEach(function (panel) {
      panel.getId() !== panelId && panel.isExpandable() && panel.collapse(newSetStateCallback);
    });

    return this;
  },

  // Overriding Functions

  shouldChildRender_: function (child) {
    return this.getChildTagName(child) === this.getDefault().child.tag;
  },

  expandChildProps_: function (child, i) {
    var newChildProps = $.extend(true, {}, child.props);

    newChildProps.glyphiconExpand = newChildProps.glyphiconExpand || this.props.glyphiconExpand;
    newChildProps.glyphiconCollapse = newChildProps.glyphiconCollapse || this.props.glyphiconCollapse;
    newChildProps.colorSchema = newChildProps.colorSchema || this.props.colorSchema;
    newChildProps.onClick = newChildProps.onClick || this.props.onClick;

    return newChildProps;
  },

  // Component Specific Helpers
  _getValuesAsArray: function (value, name) {
    var values = [];

    if (typeof value === 'string') {
      values = [value];
    } else if (Array.isArray(value)) {
      values = value;
    } else {
      this.showWarning('unsupportedType', [typeof value, name, 'string, array']);
    }

    return values;
  },

  _eachPanelWithCallback: function (setStateCallback, callback) {
    var panels = this.getPanels();
    var newSetStateCallback = this.buildCounterCallback(setStateCallback, panels.length);

    for (var i = 0; i < panels.length; i++) {
      var result = callback(panels[i], i, newSetStateCallback);
      if (result === false) {
        break;
      }
    }

    return this;
  },

  _eachPanelByIdWithCallback: function (panelId, setStateCallback, callback) {
    var ids = this._getValuesAsArray(panelId, 'panelId');
    var newSetStateCallback = this.buildCounterCallback(setStateCallback, ids.length);

    this.eachPanelByIds(ids, function (panel, i) {
      return callback(panel, i, newSetStateCallback);
    });

    return this;
  },

  _eachPanelByNameWithCallback: function (panelName, setStateCallback, callback) {
    var names = this._getValuesAsArray(panelName, 'panelName');
    var newSetStateCallback = this.buildCounterCallback(setStateCallback, names.length);

    this.eachPanelByNames(names, function (panel, i) {
      return callback(panel, i, newSetStateCallback);
    });

    return this;
  },

  // Render
  _buildChildren: function () {
    var childrenProps = {};
    if (this.props.panels) {
      childrenProps.content = { tag: this.getDefault().child.tag, propsArray: this.props.panels };
    } else if (this.props.children) {
      childrenProps.children = this.props.children;
    } else {
      childrenProps.content = React.createElement(this.checkTag(this.getDefault().child.tag), this.getDefault().child.props);
    }

    return this.buildChildren(childrenProps);
  },

  render: function () {
    return React.createElement(
      'div',
      this.buildMainAttrs(),
      this._buildChildren(),
      this.getDisabledCover()
    );
  }
});

/**
 * Panel can only be used in accordion.
 *
 * @class
 */
UU5.Bricks.accordion.panel = React.createClass({
  displayName: 'panel',

  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin],

  statics: {
    tagName: 'UU5.Bricks.accordion.panel',
    classNames: {
      main: 'UU5_Bricks_accordion_panel panel',
      expanded: 'UU5_Bricks_accordion_panel-expanded'
    },
    defaults: {
      headerTagName: 'UU5.Bricks.accordion.panel._header',
      bodyTagName: 'UU5.Bricks.accordion.panel._body',
      parentTagName: 'UU5.Bricks.accordion',
      colorSchema: 'default',
      header: 'noHeader',
      body: 'noBody'
    }
  },

  propTypes: {
    /**
     * True if the panel is opened, false if it is closed.
     */
    expanded: React.PropTypes.bool,

    /**
     * True if the panel is opened and can never be collapsed (either by user's click or by interface).
     */
    alwaysExpanded: React.PropTypes.bool,

    /**
     * Panel color style.
     */
    colorSchema: React.PropTypes.oneOf(['default', 'primary', 'success', 'info', 'warning', 'danger']),

    /**
     * Glyphicon name or UU5.Bricks.glyphicon props for opened panel.
     */
    glyphiconExpand: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.object]),

    /**
     * Glyphicon name or UU5.Bricks.glyphicon props for closed panel.
     */
    glyphiconCollapse: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.object]),

    /**
     * If user clicks on a panel, that panel is toggled and onClick function is called in the callback - the parameter
     * is an instance of the clicked panel. Function is set to all panels if they do not have specifically
     * set another one.
     * Called: this.props.onClick(panel);
     */
    onClick: React.PropTypes.func
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      expanded: false,
      alwaysExpanded: false,
      colorSchema: null, // 'default'
      glyphiconExpand: null,
      glyphiconCollapse: null,
      onClick: null
    };
  },

  getInitialState: function () {
    return {
      expanded: this.props.alwaysExpanded || this.props.expanded
    };
  },

  componentWillMount: function () {
    this.checkParentTagName(this.getDefault().parentTagName);
  },

  componentWillReceiveProps: function (nextProps) {
    if (nextProps.alwaysExpanded) {
      !this.isExpanded() && this.setState({ expanded: true });
    } else if (nextProps.expanded !== this.props.expanded && nextProps.expanded !== this.isExpanded()) {
      // new expanded != this.props.expanded because if hide or disable accordion, default props to panel set again but state in panel can be different!!!
      this.setExpandedValue(nextProps.expanded);
    }
  },

  // Interface

  /**
   * Opens or closes the panel, if the panel is able to expand and collapse.
   *
   * @param {boolean} expanded - True - open the panel, false - close the panel.
   * @param {function} setStateCallback - Function is called after setting state.
   * @returns {UU5.Bricks.accordion.panel} this
   */
  setExpandedValue: function (expanded, setStateCallback) {
    if (this.isExpandable()) {
      if (this.isExpanded() === expanded) {
        typeof setStateCallback === 'function' && setStateCallback();
      } else {
        this.setState({ expanded: expanded }, setStateCallback);
      }
    }
    return this;
  },

  /**
   * Opens the panel if the panel is able to expand.
   *
   * @param {function} setStateCallback - Function is called after setting state.
   * @returns {UU5.Bricks.accordion.panel} this
   */
  expand: function (setStateCallback) {
    this.setExpandedValue(true, setStateCallback);
    return this;
  },

  /**
   * Closes the panel if the panel is able to collapse.
   *
   * @param {function} setStateCallback - Function is called after setting state.
   * @returns {UU5.Bricks.accordion.panel} this
   */
  collapse: function (setStateCallback) {
    this.setExpandedValue(false, setStateCallback);
    return this;
  },

  /**
   * Toggles panel open or close if the panel is able to expand and collapse.
   *
   * @param {function} setStateCallback - Function is called after setting state.
   * @returns {UU5.Bricks.accordion.panel} this
   */
  toggle: function (setStateCallback) {
    if (this.isExpandable()) {
      var panel = this;
      this.setState(function (state) {
        return { expanded: !state.expanded };
      }, setStateCallback);
    }
    return this;
  },

  /**
   * Returns panel expanded state.
   *
   * @returns {boolean} True - panel is opened, false - panel is closed.
   */
  isExpanded: function () {
    return this.state.expanded;
  },

  /**
   * Says if the panel can be expanded and collapsed.
   *
   * @returns {boolean}
   */
  isExpandable: function () {
    return !this.isDisabled() && !this.props.alwaysExpanded;
  },

  // Overriding Functions

  buildHeaderChild_: function (headerTypes) {
    var headerType = this.getHeaderType(headerTypes);

    var headerChild;
    if (headerType === 'contentOfStandardHeader') {
      headerChild = this.buildChild(this.getDefault().headerTagName, { content: headerTypes.header });
      headerChild = this.cloneChild(headerChild, this.expandHeaderProps(headerChild));
    }

    return headerChild;
  },

  expandHeaderProps_: function (headerChild) {
    var headerProps = headerChild.props;
    var newProps = {};

    // default values is used if child is set as react element so null or undefined will not set!!!
    for (var key in headerProps) {
      headerProps[key] !== null && headerProps[key] !== undefined && (newProps[key] = headerProps[key]);
    }

    return $.extend(newProps, {
      key: newProps.id,
      _onClick: this._onClickToggle,
      _glyphicon: this.isExpanded() ? this.props.glyphiconExpand : this.props.glyphiconCollapse
    });
  },

  // Component Specific Helpers
  _findChildByTagName: function (tagName) {
    var children = Array.isArray(this.props.children) ? this.props.children : [this.props.children];

    var result = children.filter(function (child) {
      return child.type && child.type.tagName === tagName;
    });

    return result[0] || null;
  },

  _onClickToggle: function (e) {
    var panel = this;
    this.toggle(function () {
      var accordion = panel.getParent();
      var onClick;

      if (typeof panel.props.onClick === 'function') {
        onClick = function () {
          panel.props.onClick(panel);
        };
      }

      if (accordion.shouldCollapseOthers() && panel.isExpanded()) {
        accordion.collapseOthers(panel.getId(), onClick);
      } else if (onClick) {
        onClick();
      }
    });
    return this;
  },

  _getBodyId: function () {
    return this.getId() + '-body';
  },

  _getBodyChild: function () {
    var bodyContent = this.getContent();
    !this.props.children && !bodyContent && (bodyContent = this.getDefault().body);

    var bodyId = this._getBodyId();

    var bodyProps = {
      id: bodyId,
      key: bodyId,
      content: bodyContent,
      _expanded: this.isExpanded()
    };

    return this.buildChildren({
      children: React.createElement(this.checkTag(this.getDefault().bodyTagName), bodyProps, this.props.children)
    });
  },

  _getBodyHeight: function () {
    return this.getRenderedChildById(this._getBodyId()).getFullHeight();
  },

  // Render
  _buildChildren: function () {
    var headerChild = this.buildHeaderChild({ header: this.getHeader() || this.getDefault().header });
    var bodyChild = this._getBodyChild();

    return [headerChild, bodyChild];
  },

  render: function () {
    var mainProps = this.buildMainAttrs();
    mainProps.className += ' panel-' + (this.props.colorSchema || this.getDefault().colorSchema);
    this.isExpanded() && (mainProps.className += ' ' + this.getClassName().expanded);

    return React.createElement(
      'div',
      mainProps,
      this._buildChildren(),
      this.getDisabledCover()
    );
  }
});

/**
 * Header can only be used in accordion panel.
 *
 * @class
 */
UU5.Bricks.accordion.panel._header = React.createClass({
  displayName: '_header',

  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.accordion.panel._header',
    classNames: {
      main: 'UU5_Bricks_accordion_panel_header panel-heading panel-title'
    },
    defaults: {
      parentTagName: 'UU5.Bricks.accordion.panel'
    }
  },

  propTypes: {
    /**
     * Glyphicon name or UU5.Bricks.glyphicon props for header in current state.
     */
    _glyphicon: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.object]),

    /**
     * Function is called if user clicks on the header.
     * Called: this.props.onClick(event);
     */
    _onClick: React.PropTypes.func
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      _glyphicon: null,
      _onClick: null
    };
  },

  componentWillMount: function () {
    this.checkParentTagName(this.getDefault().parentTagName);
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _onClickHandler: function (e) {
    this.props._onClick(e);
    return this;
  },

  // Render
  _getGlyphicon: function () {
    var glyphicon = null;
    var glyphiconProps = this.props._glyphicon;

    if (typeof glyphiconProps === 'string') {
      glyphicon = React.createElement(UU5.Bricks.glyphicon, { glyphicon: glyphiconProps });
    } else if (glyphiconProps && typeof glyphiconProps === 'object') {
      glyphicon = React.createElement(UU5.Bricks.glyphicon, glyphiconProps);
    }

    return glyphicon;
  },

  render: function () {
    return React.createElement(
      'div',
      _extends({}, this.buildMainAttrs(), { onClick: this._onClickHandler }),
      this.buildChildren(),
      this._getGlyphicon(),
      this.getDisabledCover()
    );
  }
});

/**
 * Body can only be used in accordion panel.
 *
 * @class
 */
UU5.Bricks.accordion.panel._body = React.createClass({
  displayName: '_body',

  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.accordion.panel._body',
    classNames: {
      main: 'UU5_Bricks_accordion_panel_body',
      block: 'UU5_Bricks_accordion_panel_body-block',
      expanding: 'UU5_Bricks_accordion_panel_body-expanding'
    },
    defaults: {
      parentTagName: 'UU5.Bricks.accordion.panel',
      duration: 250
    }
  },

  propTypes: {
    _expanded: React.PropTypes.bool
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      _expanded: false
    };
  },

  getInitialState: function () {
    return {
      height: this._getHeight(this.props)
    };
  },

  componentWillMount: function () {
    this.checkParentTagName(this.getDefault().parentTagName);
  },

  componentWillReceiveProps: function (nextProps) {
    var body = this;
    var height = this._getHeight(nextProps);

    if (height) {
      this.setState({ height: height }, function () {
        body.timer && clearTimeout(body.timer);
        body.timer = setTimeout(function () {
          body.setState({ height: null });
        }, body.getDefault().duration);
      });
    } else {
      this.setState({ height: this._getHeight(this.props) }, function () {
        body.timer && clearTimeout(body.timer);
        body.timer = setTimeout(function () {
          body.setState({ height: height });
        }, 1);
      });
    }
  },

  componentWillUnmount: function () {
    this.timer && clearTimeout(this.timer);
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _getPanelBodyId: function () {
    return this.getId() + '-content';
  },

  _getHeight: function (props) {
    return props._expanded ? $('#' + this._getPanelBodyId()).outerHeight(true) : 0;
  },

  // Render
  _getMainAttrs: function () {
    var mainAttrs = this.buildMainAttrs();

    if (this.state.height === null) {
      mainAttrs.className += ' ' + this.getClassName().block;
    } else {
      this.state.height > 0 && (mainAttrs.className += ' ' + this.getClassName().expanding);
      var duration = this.getDefault().duration / 1000 + 's';
      mainAttrs.style = mainAttrs.style || {};
      mainAttrs.style.height = this.state.height + 'px';
      mainAttrs.style.WebkitTransitionDuration = duration; // Safari
      mainAttrs.style.transitionDuration = duration;
    }

    return mainAttrs;
  },

  _buildChildren: function () {
    return this.buildChildren();
  },

  render: function () {
    return React.createElement(
      'div',
      this._getMainAttrs(),
      React.createElement(
        'div',
        { className: 'panel-body', id: this._getPanelBodyId() },
        this._buildChildren()
      ),
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * Alert used for additional communication with the user and usually displays some important message.
 */
UU5.Bricks.alert = React.createClass({
  displayName: 'alert',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.alert',
    classNames: {
      main: 'UU5_Bricks_alert alert',
      position: 'UU5_Bricks_alert-',
      block: 'UU5_Bricks_alert-block'
    },
    defaults: {
      transitionDuration: 150
    }
  },

  propTypes: {
    /**
     * Important message label which is displayed as bold if set.
     */
    label: React.PropTypes.string,

    /**
     * Message for the user.
     */
    message: React.PropTypes.string,

    /**
     * Importance color style of message box.
     */
    colorSchema: React.PropTypes.oneOf(['success', 'info', 'warning', 'danger']),

    /**
     * Alert position in relative parent.
     */
    position: React.PropTypes.oneOf(['left', 'center', 'right']),

    /**
     * Time in milliseconds specifying for how long the alert is shown. When time expires the alert disappears.
     *
     * TODO: Maybe should be in seconds? If someone wants time shorter than sec, set 0.01 = 10ms
     */
    closeTimer: React.PropTypes.number,

    /**
     * True - it is not possible to close the alert by click on X.
     */
    closeDisabled: React.PropTypes.bool,

    /**
     * True - the alert is displayed with block position, false - absolute position.
     */
    block: React.PropTypes.bool,

    /**
     * Function will be called if the alert is closed.
     * Called: this.props.onClose(this);
     */
    onClose: React.PropTypes.func
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      label: null,
      message: 'noMessage',
      colorSchema: 'info',
      position: 'center',
      closeTimer: null,
      closeDisabled: false,
      block: false,
      onClose: null
    };
  },

  componentWillReceiveProps: function (nextProps) {
    nextProps.message !== this.message && this._clearTimeout();
  },

  componentWillUnmount: function () {
    this._clearTimeout();
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _clearTimeout: function () {
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
    return this;
  },

  _hide: function () {
    var alert = this;
    this._clearTimeout();
    this.hide(typeof this.props.onClose === 'function' && function () {
      setTimeout(function () {
        alert.props.onClose(alert);
      }, this.getDefault().transitionDuration);
    });
    return this;
  },

  // Render
  _getMainAttrs: function () {
    var mainAttrs = this.buildMainAttrs();
    mainAttrs.className += ' alert-' + this.props.colorSchema + ' ' + this.getClassName().position + this.props.position;
    this.props.block && (mainAttrs.className += ' ' + this.getClassName().block);

    mainAttrs.style = mainAttrs.style || {};

    var time = this.getDefault().transitionDuration / 1000;
    ['WebkitTransitionDuration', 'MozTransitionDuration', 'MsTransitionDuration', 'OTransitionDuration', 'transitionDuration'].forEach(function (style) {
      mainAttrs.style[style] = time + 's';
    });

    return mainAttrs;
  },

  _manageTimeout: function () {
    if (this.props.closeTimer) {
      if (this.isHidden()) {
        this._clearTimeout();
      } else if (!this.timeout) {
        this.timeout = setTimeout(this._hide, this.props.closeTimer);
      }
    }
  },

  _buildChildren: function () {
    var children;
    if (this.props.message) {
      children = this.props.message;
      this.props.label && (children = React.createElement(
        'span',
        null,
        React.createElement(
          'strong',
          null,
          this.props.label
        ),
        ' ',
        children
      ));
    } else {
      children = this.getChildren();
    }
    return children;
  },

  render: function () {
    var mainAttrs = this._getMainAttrs();
    this._manageTimeout();

    return React.createElement(
      'div',
      mainAttrs,
      !this.props.closeDisabled && React.createElement(UU5.Bricks.link, { className: 'close', onClick: this._hide, content: '×', parent: this }),
      this._buildChildren(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * Alert bus serves as a container for alerts. It can contain any number of alerts.
 * First alert is shown and the rest of alerts wait and when the first alert is closed (by timer or by user),
 * next alert is shown (with its own timer).
 */
UU5.Bricks.alertBus = React.createClass({
  displayName: 'alertBus',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.ccrWriterMixin],

  statics: {
    tagName: 'UU5.Bricks.alertBus',
    classNames: {
      main: 'UU5_Bricks_alertBus'
    },
    warnings: {
      noMessage: 'Message "%s" is not set.'
    }
  },

  propTypes: {
    /**
     * Important message label for all alerts in alertBus. It is used when added message does not have its own label.
     */
    label: React.PropTypes.string,

    /**
     * Importance color style for all alerts in alertBus.
     * It is used when added message does not have its own colorSchema color style.
     */
    colorSchema: React.PropTypes.string,

    /**
     * Alert position for all messages in alertBus. Cannot be set for each added message separately.
     */
    position: React.PropTypes.string,

    /**
     * Time in milliseconds specifying for how long each alert is shown. When time expires the alert disappears.
     * It is used when added message does not have its own timer.
     *
     * TODO: Maybe should be in seconds? If someone wants time shorter than sec, set 0.01 = 10ms
     */
    closeTimer: React.PropTypes.number,

    /**
     * True - it is not possible to close any alert by click on X.
     * Cannot be set for each added message separately.
     */
    closeDisabled: React.PropTypes.bool,

    /**
     * True - all alerts are displayed with block position, false - absolute position.
     * Cannot be set for each added message separately.
     */
    block: React.PropTypes.bool,

    /**
     * This function will be called when the alert is closed.
     * It is used when added message does not have its own onClose function.
     * Called: this.props.onClose(alert);
     */
    onClose: React.PropTypes.func
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      label: null,
      colorSchema: 'info',
      position: 'center',
      closeTimer: 10000,
      closeDisabled: false,
      block: false,
      onClose: null
    };
  },

  getInitialState: function () {
    return {
      messages: []
    };
  },

  // Interface

  /**
   * Adds message with set options to the queue.
   *
   * @param {string} message - Message which will be shown to the user.
   * @param {object} alertProps - Additional options of alert.
   * @param {string} alertProps.id - Unique message ID.
   * @param {string} alertProps.label - Important message label which is displayed as bold if set.
   * @param {string} alertProps.colorSchema - Importance color style of message box.
   * @param {number} alertProps.closeTimer - Time in milliseconds specifying for how long the alert is shown.
   * @param {function} alertProps.onClose - This function will be called when the alert is closed.
   * @returns {UU5.Bricks.alertBus} this
   */
  addMessage: function (message, alertProps) {
    if (message) {
      var messageProps = this._getMessageProps(message, alertProps);

      this.setState(function (state) {
        var messages = state.messages;
        messages.push(messageProps);
        return { messages: messages };
      });
    } else {
      this.showWarning('noMessage', message, { alertProps: alertProps });
    }

    return this;
  },

  /**
   * Sets just one message with options. The message is shown in the time and other messages are never shown.
   *
   * @param {string} message - Message which will be shown to the user.
   * @param {object} alertProps - Additional options of alert.
   * @param {string} alertProps.id - Unique message ID.
   * @param {string} alertProps.label - Important message label which is displayed as bold if set.
   * @param {string} alertProps.colorSchema - Importance color style of message box.
   * @param {number} alertProps.closeTimer - Time in milliseconds specifying for how long the alert is shown.
   * @param {function} alertProps.onClose - This function will be called when the alert is closed.
   * @returns {UU5.Bricks.alertBus} this
   */
  setMessage: function (message, alertProps) {
    if (message) {
      var messageProps = this._getMessageProps(message, alertProps);
      this.setState({ messages: [messageProps] });
    } else {
      this.showWarning('noMessage', message, { alertProps: alertProps });
    }
    return this;
  },

  /**
   * Removes message by ID.
   *
   * @param {string} messageId - ID of message which is being deleted.
   * @param {function} setStateCallback - Callback is called after setState.
   * @returns {UU5.Bricks.alertBus} this
   */
  removeMessage: function (messageId, setStateCallback) {
    this.setState(function (state) {
      var messages = state.messages;
      var index = null;

      for (var i = 0; i < messages.length; i++) {
        if (messages[i].id === messageId) {
          index = i;
          break;
        }
      }

      messages.splice(index, 1);
      return { messages: messages };
    }, setStateCallback);

    return this;
  },

  /**
   * Clears all messages.
   *
   * @param setStateCallback - Callback is called after setState.
   * @returns {UU5.Bricks.alertBus} this
   */
  close: function (setStateCallback) {
    this.setState({ messages: [] }, setStateCallback);
    return this;
  },

  /**
   * Gets all messages props.
   *
   * @returns {Array} Array of messages props.
   */
  getMessages: function () {
    return this.state.messages;
  },

  // Overriding Functions

  // Component Specific Helpers
  _getMessageProps: function (message, alertProps) {
    alertProps = alertProps || {};
    var alertBus = this;
    var messageId = alertProps.id || this.generateUUID();

    return {
      id: messageId,
      hidden: false,

      label: typeof alertProps.label === 'string' ? alertProps.label : this.props.label,
      message: message,
      colorSchema: typeof alertProps.colorSchema === 'string' ? alertProps.colorSchema : this.props.colorSchema,
      closeTimer: typeof alertProps.closeTimer === 'number' ? alertProps.closeTimer : this.props.closeTimer,
      onClose: function (alert) {
        var onClose;
        if (typeof alertProps.onClose === 'function') {
          onClose = function () {
            alertProps.onClose(alert);
          };
        } else if (typeof alertBus.props.onClose === 'function') {
          onClose = function () {
            alertBus.props.onClose(alert);
          };
        }

        alertBus.removeMessage(messageId, onClose);
      }
    };
  },

  // Render
  _getProps: function () {
    var messages = this.getMessages();

    var alertProps = {};
    var hidden = messages.length === 0;

    if (!hidden) {
      alertProps = messages[0];
    }

    alertProps.id = this.getId() + '-alert';
    alertProps.position = this.props.position;
    alertProps.closeDisabled = this.props.closeDisabled;
    alertProps.block = this.props.block;
    alertProps.parent = this;
    alertProps.hidden = hidden;

    return alertProps;
  },

  render: function () {
    return React.createElement(UU5.Bricks.alert, this._getProps());
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.backdrop = React.createClass({
  displayName: 'backdrop',

  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin],

  statics: {
    tagName: 'UU5.Bricks.backdrop',
    classNames: {
      main: 'UU5_Bricks_backdrop'
    }
  },

  propTypes: {
    onClick: React.PropTypes.func
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      onClick: null
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  // Render
  render: function () {
    var mainAttrs = this.buildMainAttrs();
    mainAttrs.id = this.getId();
    mainAttrs.onClick = this.props.onClick;

    return React.createElement(
      'div',
      mainAttrs,
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * Badge is a number or text indicator, which is associated with other text or link.
 * The component is based on bootstrap but unlike bootstrap, we allow inserting string value.
 *
 * @see UU-BT:44191585704348747
 * @tutorial https://jsfiddle.net/uu_ui/5q8xuLm9/
 * @class
 */
UU5.Bricks.badge = React.createClass({
  displayName: 'badge',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin],

  statics: {
    tagName: 'UU5.Bricks.badge',
    classNames: {
      main: 'UU5_Bricks_badge badge'
    }
  },

  propTypes: {
    /**
     * Badge value. Empty value is rendered as empty span only with padding.
     */
    value: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.number])
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      value: 0
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  // Render
  render: function () {
    return React.createElement(
      'span',
      this.buildMainAttrs(),
      this.props.value,
      this.getDisabledCover()
    );
  }
});

'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.block = React.createClass({
  displayName: 'block',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.block',
    layoutType: 'brick',
    classNames: {
      main: 'UU5_Bricks_block',
      colorSchema: 'bl-',
      bg: 'bg-'
    }
  },

  propTypes: {
    colorSchema: React.PropTypes.oneOf(UU5.Common.environment.colorSchema),
    isBackground: React.PropTypes.bool
  },

  getDefaultProps: function () {
    return {
      colorSchema: null,
      isBackground: false
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _buildMainAttrs: function () {
    var mainAttrs = this.buildMainAttrs();
    mainAttrs.className += ' ' + this.getClassName().colorSchema + this.props.colorSchema;
    this.props.isBackground && (mainAttrs.className += ' ' + this.getClassName().bg + this.props.colorSchema);
    return mainAttrs;
  },

  // Render
  render: function () {
    return React.createElement(
      'div',
      this._buildMainAttrs(),
      this.getChildren(),
      this.getDisabledCover()
    );
  }
});

UU5.Bricks.factory = UU5.Bricks.factory || {};
UU5.Bricks.factory._createBlock = function (colorSchema, isBackground) {
  return React.createClass({

    // Interface

    // Overriding Functions

    // Component Specific Helpers

    // Render
    render: function () {
      return React.createElement(
        UU5.Bricks.block,
        _extends({}, this.props, { colorSchema: colorSchema, isBackground: isBackground }),
        React.Children.toArray(this.props.children)
      );
    }
  });
};

UU5.Common.environment.colorSchema.forEach(function (colorSchema) {
  var colorSchemaCapitalize = UU5.Common.environment.colorSchemaCapitalize(colorSchema);
  UU5.Bricks['block' + colorSchemaCapitalize] = UU5.Bricks.factory._createBlock(colorSchema);
  UU5.Bricks['blockBg' + colorSchemaCapitalize] = UU5.Bricks.factory._createBlock(colorSchema, true);
});

'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.blockquote = React.createClass({
  displayName: 'blockquote',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.blockquote',
    classNames: {
      main: 'UU5_Bricks_blockquote',
      colorSchema: 'bl-',
      bg: 'bg-'
    }
  },

  propTypes: {
    colorSchema: React.PropTypes.oneOf(UU5.Common.environment.colorSchema),
    isBackground: React.PropTypes.bool
  },

  getDefaultProps: function () {
    return {
      colorSchema: null,
      isBackground: false
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _buildMainAttrs: function () {
    var mainAttrs = this.buildMainAttrs();
    mainAttrs.className += ' ' + this.getClassName().colorSchema + this.props.colorSchema;
    this.props.isBackground && (mainAttrs.className += ' ' + this.getClassName().bg + this.props.colorSchema);
    return mainAttrs;
  },

  // Render
  render: function () {
    return React.createElement(
      'blockquote',
      this._buildMainAttrs(),
      this.getChildren(),
      this.getDisabledCover()
    );
  }
});

UU5.Bricks.factory = UU5.Bricks.factory || {};
UU5.Bricks.factory._createBlockquote = function (colorSchema, isBackground) {
  return React.createClass({

    // Interface

    // Overriding Functions

    // Component Specific Helpers

    // Render
    render: function () {
      return React.createElement(
        UU5.Bricks.blockquote,
        _extends({}, this.props, { colorSchema: colorSchema, isBackground: isBackground }),
        React.Children.toArray(this.props.children)
      );
    }
  });
};

UU5.Common.environment.colorSchema.forEach(function (colorSchema) {
  var colorSchemaCapitalize = UU5.Common.environment.colorSchemaCapitalize(colorSchema);
  UU5.Bricks['blockquote' + colorSchemaCapitalize] = UU5.Bricks.factory._createBlockquote(colorSchema);
  UU5.Bricks['blockquoteBg' + colorSchemaCapitalize] = UU5.Bricks.factory._createBlockquote(colorSchema, true);
});

'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};
UU5.Bricks.factory = UU5.Bricks.factory || {};

UU5.Bricks.factory._createBsTag = function (bsTag, uu5Name, bsClass, defaultContent) {
  return React.createClass({

    mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

    statics: {
      tagName: 'UU5.Bricks.' + (uu5Name || bsTag),
      classNames: {
        main: 'UU5_Bricks_' + (uu5Name || bsTag) + (bsClass ? ' ' + bsClass : '')
      },
      defaults: {
        content: defaultContent || null
      }
    },

    // Interface

    // Overriding Functions

    // Component Specific Helpers

    // Render
    render: function () {
      return React.createElement(bsTag, this.buildMainAttrs(), this.getChildren() || this.getDefault().content, this.getDisabledCover());
    }
  });
};

UU5.Bricks.div = UU5.Bricks.factory._createBsTag('div');
UU5.Bricks.p = UU5.Bricks.factory._createBsTag('p');
UU5.Bricks.span = UU5.Bricks.factory._createBsTag('span');

UU5.Bricks.paragraph = UU5.Bricks.factory._createBsTag('p', 'paragraph', null, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Donec ipsum massa, ullamcorper in, auctor et, scelerisque sed, est. Vestibulum fermentum tortor id mi. Etiam commodo dui eget wisi. Integer malesuada. Fusce consectetuer risus a nunc. Nullam eget nisl. In sem justo, commodo ut, suscipit at, pharetra vitae, orci. Aenean placerat. Etiam neque. Fusce suscipit libero eget elit.');

UU5.Bricks.box = UU5.Bricks.factory._createBsTag('span', 'box');

UU5.Bricks.small = UU5.Bricks.factory._createBsTag('small');
UU5.Bricks.lead = UU5.Bricks.factory._createBsTag('span', 'lead', 'lead');
UU5.Bricks.mark = UU5.Bricks.factory._createBsTag('mark');
UU5.Bricks.del = UU5.Bricks.factory._createBsTag('del');
UU5.Bricks.s = UU5.Bricks.factory._createBsTag('s');
UU5.Bricks.ins = UU5.Bricks.factory._createBsTag('ins');
UU5.Bricks.u = UU5.Bricks.factory._createBsTag('u');
UU5.Bricks.strong = UU5.Bricks.factory._createBsTag('strong');
UU5.Bricks.em = UU5.Bricks.factory._createBsTag('em');
UU5.Bricks.abbr = UU5.Bricks.factory._createBsTag('abbr');
UU5.Bricks.address = UU5.Bricks.factory._createBsTag('address');
UU5.Bricks.dd = UU5.Bricks.factory._createBsTag('dd');
UU5.Bricks.dt = UU5.Bricks.factory._createBsTag('dt');

// code
UU5.Bricks.var = UU5.Bricks.factory._createBsTag('var');
UU5.Bricks.kbd = UU5.Bricks.factory._createBsTag('kbd');
UU5.Bricks.pre = UU5.Bricks.factory._createBsTag('pre');
UU5.Bricks.samp = UU5.Bricks.factory._createBsTag('samp');
UU5.Bricks._code = UU5.Bricks.factory._createBsTag('code', '_code');

// box - all schemas
UU5.Bricks.factory = UU5.Bricks.factory || {};
UU5.Bricks.factory._createBox = function (colorSchema) {
  return React.createClass({

    // Interface

    // Overriding Functions

    // Component Specific Helpers

    // Render
    render: function () {
      return React.createElement(
        UU5.Bricks.box,
        _extends({}, this.props, { className: 'bg-' + colorSchema }),
        React.Children.toArray(this.props.children)
      );
    }
  });
};

UU5.Common.environment.colorSchema.forEach(function (colorSchema) {
  var colorSchemaCapitalize = UU5.Common.environment.colorSchemaCapitalize(colorSchema);
  UU5.Bricks['box' + colorSchemaCapitalize] = UU5.Bricks.factory._createBox(colorSchema);
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * Button component based on bootstrap standard.
 *
 * @see UU-BT:44191585665273643
 * @tutorial https://jsfiddle.net/uu_ui/5q8xuLm9/
 * @class
 */
UU5.Bricks.button = React.createClass({
  displayName: 'button',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.button',
    classNames: {
      main: 'UU5_Bricks_button btn'
    },
    defaults: {
      colorSchema: 'default',
      content: 'Button'
    },
    warnings: {
      labelDeprecated: 'Property label (%s) is deprecated since v03.10. Use property content instead of this.'
    }
  },

  propTypes: {
    /**
     * Button text.
     * @deprecated since version 03.10. Replaced by property content.
     */
    label: React.PropTypes.string,

    /**
     * Button type (Mostly use button type, submit and reset are for form buttons.).
     */
    type: React.PropTypes.oneOf(['button', 'submit', 'reset']),

    /**
     * Color style for button.
     */
    colorSchema: React.PropTypes.oneOf(['default', 'primary', 'success', 'warning', 'danger', 'info', 'link']),

    /**
     * Bootstrap button size.
     */
    size: React.PropTypes.oneOf(['lg', 'md', 'sm', 'xs']),

    /**
     * Bootstrap block button. Width of button will be the same as width of parent node.
     */
    displayBlock: React.PropTypes.bool,

    /**
     * Bootstrap activated button. Button looks like it is pressed.
     */
    pressed: React.PropTypes.bool,

    /**
     * Tooltip text, which is shown on hover.
     */
    tooltip: React.PropTypes.string,

    /**
     * Tooltip position (if tooltip text is set).
     */
    tooltipPosition: React.PropTypes.oneOf(['top', 'bottom', 'left', 'right', 'auto top', 'auto bottom', 'auto left', 'auto right']),

    /**
     * Function is called after clicking the button. Parameters: instance of a rendered button and event.
     * Called: this.props.onClick(this, event);
     */
    onClick: React.PropTypes.func
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      label: null,
      type: 'button',
      onClick: null,
      colorSchema: null,
      displayBlock: false,
      pressed: false,
      size: 'md',
      tooltip: null,
      tooltipPosition: 'auto top'
    };
  },

  getInitialState: function () {
    return {
      pressed: this.props.pressed
    };
  },

  componentWillMount: function () {
    if (this.props.label) {
      this.showWarning('labelDeprecated', this.props.label);
    }
  },

  componentDidMount: function () {
    this.props.tooltip && $(this.findDOMNode()).tooltip();
  },

  componentWillReceiveProps: function (nextProps) {
    this.setState({ pressed: nextProps.pressed });
  },

  // Interface

  /**
   * Sets button as pressed or non pressed.
   *
   * @param {boolean} pressed - true - activate, false - deactivate the button.
   * @param {function} setStateCallback - Function is called after setting state.
   * @return {UU5.Bricks.button} this
   */
  setActive: function (pressed, setStateCallback) {
    this.setState({ pressed: pressed }, setStateCallback);
    return this;
  },

  /**
   * Sets button as pressed.
   *
   * @param {function} setStateCallback - Function is called after setting button as pressed.
   * @return {UU5.Bricks.button} this
   */
  press: function (setStateCallback) {
    return this.setActive(true, setStateCallback);
  },

  /**
   * Sets button as non pressed.
   *
   * @param {function} setStateCallback - Function is called after setting button as non pressed.
   * @return {UU5.Bricks.button} this
   */
  pressUp: function (setStateCallback) {
    return this.setActive(false, setStateCallback);
  },

  /**
   * TODO: doc?
   * @param setStateCallback
   * @returns {UU5.Bricks.button}
   */
  togglePress: function (setStateCallback) {
    this.setState(function (state) {
      return { pressed: !state.pressed };
    }, setStateCallback);
    return this;
  },

  /**
   * True if the button is pressed, false if it is non pressed.
   *
   * @return {boolean} true - is pressed, false - is non pressed.
   */
  isPressed: function () {
    return this.state.pressed;
  },

  // Overriding Functions

  // Component Specific Helpers
  _onClickHandler: function (event) {
    this.props.onClick && this.props.onClick(this, event);
    return this;
  },

  // Render
  _buildMainAttrs: function () {
    var mainProps = this.buildMainAttrs();
    var colorSchema = this.props.colorSchema || this.getDefault().colorSchema;

    mainProps.className += ' btn-' + colorSchema + (this.props.displayBlock ? ' btn-block' : '') + (this.isPressed() ? ' active' : '') + (this.isDisabled() ? ' disabled' : '') + ' btn-' + this.props.size;

    mainProps.type = this.props.type;

    this.isDisabled() ? mainProps.disabled = 'disabled' : mainProps.onClick = this.props.onClick && this._onClickHandler;

    // TODO: tooltip by bootstrap is not good for React DOM !!!
    if (this.props.tooltip) {
      mainProps['data-toggle'] = 'tooltip';
      mainProps['data-placement'] = this.props.tooltipPosition;
      mainProps.title = this.props.tooltip;
    }

    return mainProps;
  },

  render: function () {
    return React.createElement(
      'button',
      this._buildMainAttrs(),
      this.getChildren() || this.props.label || this.getDefault().content
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * buttonGroup component based on bootstrap standard.
 *
 * @requires UU5.Common.baseMixin
 * @requires UU5.Common.elementaryMixin
 */
UU5.Bricks.buttonGroup = React.createClass({

  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.buttonGroup',
    classNames: {
      main: 'UU5_Bricks_buttonGroup',
      horizontal: 'btn-group',
      vertical: 'btn-group-vertical'
      //displayBlock: 'btn-group-justified'
    },
    defaults: {
      childTagNames: ['UU5.Bricks.button', 'UU5.Bricks.dropdown', 'UU5.Bricks.buttonSwitch']
    }
  },

  /**
   * @property {(string)} bsSize - buttonGroup size - null|buttonGroup-sm|buttonGroup-lg (normal|small|large)
   * @property {(string)} text - buttonGroup displayed text
   */
  propTypes: {
    colorSchema: React.PropTypes.oneOf(['default', 'primary', 'success', 'warning', 'danger', 'info', 'link']),
    size: React.PropTypes.oneOf(['xs', 'sm', 'md', 'lg']),
    vertical: React.PropTypes.bool

    // TODO: not possible for button, but for <a> element
    //displayBlock: React.PropTypes.bool
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      colorSchema: 'default',
      size: 'md',
      vertical: false
      //displayBlock: false
    };
  },

  // Interface

  // Overriding Functions

  shouldChildRender_: function (child) {
    var childTagName = this.getChildTagName(child);
    return this.getDefault().childTagNames.indexOf(childTagName) > -1;
  },

  expandChildProps_: function (child, i) {
    var newChildProps = child.props;
    newChildProps = $.extend(true, {}, newChildProps);

    newChildProps.colorSchema = newChildProps.colorSchema || this.props.colorSchema;
    newChildProps.size = this.props.size;
    newChildProps.disabled = this.isDisabled();

    var childTagName = this.getChildTagName(child);
    if (childTagName === this.getDefault().childTagNames[1]) {
      var className = newChildProps.className ? newChildProps.className + ' ' : '';
      className += this.getClassName().horizontal;
      newChildProps.className = className;
    }

    return newChildProps;
  },

  // Component Specific Helpers
  _getMainAttrs: function () {
    var mainAttrs = this.buildMainAttrs();

    mainAttrs.className += ' ' + this.getClassName(this.props.vertical ? 'vertical' : 'horizontal');
    //this.props.displayBlock && (mainAttrs.className += ' ' + this.getClassName().displayBlock);

    return mainAttrs;
  },

  // Render
  _getChildren: function () {
    return this.getChildren() || this.buildChildren({ children: React.createElement(UU5.Bricks.button, null) });
  },

  render: function () {
    return React.createElement(
      'div',
      this._getMainAttrs(),
      this._getChildren()
    );
  }
});

'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.buttonSwitch = React.createClass({

  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.buttonSwitch',
    classNames: {
      main: 'UU5_Bricks_buttonSwitch'
    },
    defaults: {
      colorSchema: 'default'
    }
  },

  propTypes: {
    onProps: React.PropTypes.object,
    offProps: React.PropTypes.object,
    switchOn: React.PropTypes.bool,
    props: React.PropTypes.object
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      onProps: null,
      offProps: null,
      switchOn: false,
      props: null
    };
  },

  getInitialState: function () {
    return {
      switchOn: this.props.switchOn
    };
  },

  componentWillReceiveProps: function (nextProps) {
    this.setState({ switchOn: nextProps.switchOn });
  },

  // Interface

  switchOn: function (setStateCallback) {
    return this.setState({ switchOn: true }, setStateCallback);
  },

  switchOff: function (setStateCallback) {
    return this.setState({ switchOn: false }, setStateCallback);
  },

  toggle: function (setStateCallback) {
    this.setState(function (state) {
      return { switchOn: !state.switchOn };
    }, setStateCallback);
    return this;
  },

  isSwitchOn: function () {
    return this.state.switchOn;
  },

  isSwitchOff: function () {
    return !this.isSwitchOn();
  },

  // Overriding Functions

  // Component Specific Helpers

  // Render
  render: function () {
    return React.createElement(
      UU5.Bricks.button,
      _extends({}, this.buildMainAttrs(), this.isSwitchOn() ? this.props.onProps : this.props.offProps, this.props.props),
      this.getChildren()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

// TODO
// quality of picture -> jpg, png, ...
// choice of camera devices if pc has more cameras
UU5.Bricks.camera = React.createClass({
  displayName: 'camera',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin],

  statics: {
    tagName: 'UU5.Bricks.camera',
    classNames: {
      main: 'UU5_Bricks_camera',
      video: 'UU5_Bricks_camera-video',
      canvas: 'UU5_Bricks_camera-canvas'
    }
  },

  propTypes: {
    autoPlay: React.PropTypes.bool
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      autoPlay: true
    };
  },

  componentWillMount: function () {
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;

    if (navigator.getUserMedia) {
      navigator.getUserMedia({ video: true }, this._handleVideo, this._videoError);
    }
  },

  // Interface

  getScreenShoot: function () {
    var img = null;

    if (this.localMediaStream) {
      var canvas = this.canvas;
      canvas.width = this.getWidth();
      canvas.height = this.getHeight();

      var ctx = canvas.getContext('2d');
      ctx.drawImage(this.video, 0, 0, this.getWidth(), this.getHeight());
      img = canvas.toDataURL('template/png');
    }

    return img;
  },

  // Overriding Functions

  // Component Specific Helpers
  _handleVideo: function (stream) {
    var video = this.video;
    video.src = window.URL.createObjectURL(stream);
    this.localMediaStream = stream;
    return this;
  },

  _videoError: function (e) {
    // TODO show some text instead of video
    console.error(e);
    return this;
  },

  _refVideo: function (video) {
    this.video = video;
    return this;
  },

  _refCanvas: function (canvas) {
    this.canvas = canvas;
    return this;
  },

  // Render
  render: function () {
    return React.createElement(
      'div',
      this.buildMainAttrs(),
      React.createElement('video', { autoPlay: this.props.autoPlay, ref: this._refVideo, className: this.getClassName().video }),
      React.createElement('canvas', { ref: this._refCanvas, className: this.getClassName().canvas }),
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.code = React.createClass({
  displayName: 'code',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.code',
    classNames: {
      main: 'UU5_Bricks_code'
    }
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _getMainProps: function () {
    var mainProps = this.getMainPropsToPass(['UU5_Common_baseMixin', 'UU5_Common_elementaryMixin', 'UU5_Common_contentMixin']);

    mainProps.id = this.getId() + '-code';
    mainProps.ignoreInnerHTML = true;
    mainProps.language = 'code';

    return mainProps;
  },

  // Render
  render: function () {
    return React.createElement(
      UU5.Bricks._code,
      this._getMainProps(),
      React.Children.toArray(this.props.children)
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.cookieBar = React.createClass({
  displayName: 'cookieBar',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.cookieBar',
    classNames: {
      main: 'UU5_Bricks_cookieBar',
      top: 'UU5_Bricks_cookieBar-top',
      bottom: 'UU5_Bricks_cookieBar-bottom',
      button: 'UU5_Bricks_cookieBar-button',
      link: 'UU5_Bricks_cookieBar-link',
      bg: 'bg-'
    },
    defaults: {
      content: 'Cookies help us to provide, protect and improve our services. By viewing this site, you agree to their use.'
    }
  },

  propTypes: {
    agreedText: React.PropTypes.string,
    infoText: React.PropTypes.string,
    infoHref: React.PropTypes.string,
    colorSchema: React.PropTypes.oneOf(UU5.Common.environment.colorSchema),
    fixed: React.PropTypes.oneOf(['top', 'bottom']),
    onAgree: React.PropTypes.func
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      agreedText: 'I Understand',
      infoText: null,
      infoHref: null,
      colorSchema: 'white',
      fixed: 'bottom',
      onAgree: null
    };
  },

  componentWillMount: function () {
    if (UU5.Common.environment.getCookie('viewed_cookie_policy') === 'yes') {
      this.setState({ hidden: true }, this.props.onAgree);
    }
  },

  componentWillReceiveProps: function () {
    if (UU5.Common.environment.getCookie('viewed_cookie_policy') === 'yes') {
      this.setState({ hidden: true }, this.props.onAgree);
    }
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  _confirm: function () {
    UU5.Common.environment.setCookie('viewed_cookie_policy', 'yes');
    this.hide(this.props.onAgree);
  },

  _getText: function () {
    var content = this.getContent();

    if (!content && !this.props.children) {
      content = this.getDefault().content;
    }

    return React.createElement(
      UU5.Bricks.span,
      { content: content },
      this.props.children
    );
  },

  _getButton: function () {
    var buttonProps = {
      content: this.props.agreedText,
      colorSchema: this.props.colorSchema,
      onClick: this._confirm,
      className: this.getClassName().button
    };

    return React.createElement(UU5.Bricks.button, buttonProps);
  },

  _getLink: function () {
    var link = null;

    if (this.props.infoText) {
      // href -> linkHref || undefined -> if undefined, default prop of link is set - other way, null is set
      link = React.createElement(UU5.Bricks.link, {
        content: this.props.infoText,
        href: this.props.infoHref || undefined,
        className: this.getClassName().link
      });
    }

    return link;
  },

  _getMainProps: function () {
    var mainProps = this.getMainPropsToPass(['UU5_Common_baseMixin', 'UU5_Common_elementaryMixin']);

    this.props.fixed && (mainProps.className += ' ' + this.getClassName(this.props.fixed));
    this.props.colorSchema && (mainProps.className += ' ' + this.getClassName().bg + this.props.colorSchema);

    return mainProps;
  },

  // Render
  render: function () {
    return React.createElement(
      UU5.Bricks.div,
      this._getMainProps(),
      this._getText(),
      this._getButton(),
      this._getLink()
    );
  }
});

'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * UU5.Bricks.cookiesInfo
 */
UU5.Bricks.cookiesInfo = React.createClass({
  displayName: 'cookiesInfo',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Layout.containerCollectionMixin],

  statics: {
    tagName: 'UU5.Bricks.cookiesInfo',
    classNames: {
      main: 'UU5_Bricks_cookiesInfo'
    }
  },

  propTypes: {
    data: React.PropTypes.shape({
      header: React.PropTypes.node,
      chapters: React.PropTypes.arrayOf(React.PropTypes.shape({
        header: React.PropTypes.string,
        // array of contents
        rows: React.PropTypes.arrayOf(React.PropTypes.any)
      }))
    }),
    headerLevel: React.PropTypes.oneOf(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]),
    chapterLevel: React.PropTypes.oneOf(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      data: {},
      headerLevel: 1,
      chapterLevel: 2
    };
  },

  // Interface

  // Overriding Functions

  // Helpers

  _getChapters: function (chaptersData) {
    var collection = this;
    return chaptersData && chaptersData.map(function (chapter, i) {
      var paragraphs = chapter.rows.map(function (content, j) {
        return React.createElement(UU5.Bricks.paragraph, { content: content, key: j });
      });

      return React.createElement(
        UU5.Layout.rowCollection,
        { header: chapter.header, key: i, level: collection.props.chapterLevel },
        React.createElement(
          UU5.Layout.flc,
          null,
          paragraphs
        )
      );
    });
  },

  // Render
  render: function () {
    var data = this.props.data;
    var chapters = this._getChapters(data.chapters);

    return React.createElement(
      UU5.Layout.container,
      _extends({}, this.getMainPropsToPass(), { header: data.header, level: this.props.headerLevel }),
      chapters
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.dl = React.createClass({
  displayName: 'dl',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin],

  statics: {
    tagName: 'UU5.Bricks.dl',
    classNames: {
      main: 'UU5_Bricks_dl'
    },
    defaults: {
      childTagNames: ['UU5.Bricks.dd', 'UU5.Bricks.dt']
    }
  },

  // Interface

  // Overriding Functions
  shouldChildRender_: function (child) {
    return this.getDefault().childTagNames.indexOf(this.getChildTagName(child)) >= 0;
  },

  // Component Specific Helpers

  // Render
  render: function () {
    return React.createElement(
      'dl',
      this.buildMainAttrs(),
      this.getHeaderChild(),
      this.getChildren(),
      this.getFooterChild(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * Dropdown component based on bootstrap standard.
 *
 * @requires UU5.Common.baseMixin
 * @requires UU5.Common.elementaryMixin
 * @requires UU5.Common.contentMixin
 */
UU5.Bricks.dropdown = React.createClass({
  displayName: 'dropdown',

  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.dropdown',
    classNames: {
      main: 'UU5_Bricks_dropdown',
      dropdown: 'dropdown',
      dropup: 'dropup',
      split: 'btn-group',
      pullRight: 'dropdown-menu-right',
      dropdownBtn: 'dropdown-toggle',
      menu: 'dropdown-menu',
      opened: 'open'
    },
    defaults: {
      childTagName: 'UU5.Bricks.dropdown.item'
    }
  },

  propTypes: {
    // button props
    label: React.PropTypes.node,
    colorSchema: React.PropTypes.oneOf(['default', 'primary', 'success', 'warning', 'danger', 'info', 'link']),
    size: React.PropTypes.oneOf(['lg', 'md', 'sm', 'xs']),
    onClick: React.PropTypes.func,

    // glyphicon props
    glyphiconOpened: React.PropTypes.string,
    glyphiconClosed: React.PropTypes.string,
    glyphiconHidden: React.PropTypes.bool,

    // dropdown props
    items: React.PropTypes.arrayOf(React.PropTypes.object // UU5.Bricks.dropdown.item props
    ),
    pullRight: React.PropTypes.bool,
    dropup: React.PropTypes.bool,
    split: React.PropTypes.bool,

    // link child props
    smoothScroll: React.PropTypes.number,
    offset: React.PropTypes.number
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      label: 'Dropdown',
      colorSchema: 'default',
      size: 'md',
      onClick: null,
      glyphiconOpened: 'caret',
      glyphiconClosed: 'caret',
      glyphiconHidden: false,
      pullRight: false,
      dropup: false,
      split: false,
      smoothScroll: null,
      offset: null
    };
  },

  getInitialState: function () {
    return {
      opened: false
    };
  },

  // Interface

  open: function (callback) {
    this.setState({ opened: true }, callback);
    return this;
  },

  close: function (callback) {
    this.setState({ opened: false }, callback);
    return this;
  },

  toggle: function (callback) {
    this.setState(function (state) {
      return { opened: !state.opened };
    }, callback);
    return this;
  },

  isOpened: function () {
    return this.state.opened;
  },

  // Overriding Functions

  shouldChildRender_: function (child) {
    return this.getChildTagName(child) === this.getDefault().childTagName;
  },

  expandChildProps_: function (child) {
    var newChildProps = $.extend(true, {}, child.props);

    newChildProps.smoothScroll = newChildProps.smoothScroll || this.props.smoothScroll;
    newChildProps.offset = newChildProps.offset || this.props.offset;

    return newChildProps;
  },

  // Component Specific Helpers
  _getGlyphicon: function () {
    var glyphicon = null;
    if (!this.props.glyphiconHidden) {
      var glyphiconName = this.isOpened() ? this.props.glyphiconOpened : this.props.glyphiconClosed;
      glyphicon = React.createElement(UU5.Bricks.glyphicon, { glyphicon: glyphiconName });
    }
    return glyphicon;
  },

  _getChildren: function () {
    var contentProps;

    if (this.props.items) {
      contentProps = {
        content: {
          tag: this.getDefault().childTagName,
          propsArray: this.props.items
        }
      };
    } else if (this.props.children) {
      contentProps = { children: this.props.children };
    }

    return this.buildChildren(contentProps);
  },

  _getButton: function () {
    var dropdown = this;
    var onClick = null;
    if (this.props.split) {
      if (typeof this.props.onClick === 'function') {
        onClick = function (button, event) {
          dropdown.props.onClick(dropdown, event);
        };
      }
    } else {
      onClick = this._onClickHandler;
    }

    var buttonProps = {
      colorSchema: this.props.colorSchema,
      size: this.props.size,
      disabled: this.isDisabled(),
      onClick: onClick
    };

    var glyphicon = null;
    if (!this.props.split) {
      buttonProps.className = this.getClassName().dropdownBtn;
      glyphicon = this._getGlyphicon();
    }

    return React.createElement(
      UU5.Bricks.button,
      buttonProps,
      this.props.label,
      ' ',
      glyphicon
    );
  },

  _getGlyphiconButton: function () {
    var glyphiconButton = null;

    if (this.props.split) {
      glyphiconButton = React.createElement(
        UU5.Bricks.button,
        { colorSchema: this.props.colorSchema,
          size: this.props.size,
          disabled: this.isDisabled(),
          className: this.getClassName().dropdownBtn,
          onClick: this._onClickHandler },
        this._getGlyphicon()
      );
    }

    return glyphiconButton;
  },

  _onClickHandler: function () {
    this.toggle();
    return this;
  },

  _getMainAttrs: function () {
    var mainAttrs = this.buildMainAttrs();

    if (this.isOpened()) {
      mainAttrs.className += ' ' + this.getClassName().opened;
    }

    if (this.props.split) {
      mainAttrs.className += ' ' + this.getClassName().split;
    } else if (this.props.dropup) {
      mainAttrs.className += ' ' + this.getClassName().dropup;
    } else {
      mainAttrs.className += ' ' + this.getClassName().dropdown;
    }

    return mainAttrs;
  },

  _getBackdropProps: function () {
    var backdropId = this.getId() + "backdrop";

    return {
      hidden: !this.isOpened(),
      id: backdropId,
      onClick: function (event) {
        event.target.id === backdropId && this.close();
      }.bind(this)
    };
  },

  // Render
  render: function () {
    var menuClass = this.getClassName().menu;
    this.props.pullRight && (menuClass += ' ' + this.getClassName().pullRight);

    return React.createElement(
      'div',
      this._getMainAttrs(),
      this._getButton(),
      this._getGlyphiconButton(),
      React.createElement(UU5.Bricks.backdrop, this._getBackdropProps()),
      React.createElement(
        'ul',
        { className: menuClass },
        this._getChildren()
      )
    );
  }
});

/**
 * item - dropdown sub-component
 *
 * @requires UU5.Common.baseMixin
 * @requires UU5.Common.elementaryMixin
 */
UU5.Bricks.dropdown.item = React.createClass({
  displayName: 'item',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.dropdown.item',
    classNames: {
      main: 'UU5_Bricks_dropdown_item',
      divider: 'divider',
      header: 'dropdown-header',
      disabledItem: 'disabled'
    }
  },

  /**
   * @property {(boolean)} divider - if true show horizontal rule
   * @property {(string)} text - item displayed text
   * @property {(string)} href - defines a URL to open when this element is clicked
   * @property {(function)} onClick - item onCLick function
   * @property {(number)} smoothScroll - smoothScroll time
   * @property {(number)} offset - scroll offset
   * @property {(boolean)} header
   */
  propTypes: {
    header: React.PropTypes.bool,
    divider: React.PropTypes.bool,
    text: React.PropTypes.string,
    href: React.PropTypes.string,
    onClick: React.PropTypes.func,
    smoothScroll: React.PropTypes.number,
    offset: React.PropTypes.number
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      header: false,
      divider: false,
      text: null,
      href: '#',
      onClick: null,
      smoothScroll: null,
      offset: null
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  _onClickHandler: function () {
    var parent = this.getParent();
    parent && parent.close();
    return this;
  },

  _getStandardItem: function (props) {
    return React.createElement(
      'li',
      _extends({}, props, { onClick: this._onClickHandler }),
      React.createElement(UU5.Bricks.link, { content: this.props.text,
        href: this.props.href,
        onClick: this.props.onClick,
        smoothScroll: this.props.smoothScroll,
        offset: this.props.offset
      })
    );
  },

  _getDividerItem: function (props) {
    props.className += ' ' + this.getClassName().divider;
    return React.createElement('li', props);
  },

  _getHeaderItem: function (props) {
    props.className += ' ' + this.getClassName().header;
    return React.createElement(
      'li',
      props,
      this.props.text
    );
  },

  _getContentItem: function (props) {
    return React.createElement(
      'li',
      props,
      this.getChildren()
    );
  },

  // Render
  render: function () {
    var mainAttrs = this.buildMainAttrs();

    if (this.isDisabled()) {
      mainAttrs.className += ' ' + this.getClassName().disabledItem;
    }

    var result = null;
    if (this.getContent() || this.props.children) {
      result = this._getContentItem(mainAttrs);
    } else if (this.props.divider) {
      result = this._getDividerItem(mainAttrs);
    } else if (this.props.header) {
      result = this._getHeaderItem(mainAttrs);
    } else {
      result = this._getStandardItem(mainAttrs);
    }

    return result;
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * UU5.Bricks.error
 */
UU5.Bricks.error = React.createClass({
  displayName: 'error',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.error',
    classNames: {
      main: 'UU5_Bricks_error bg-danger UU5_Common-padding-5'
    },
    defaults: {
      content: 'Error'
    },
    warnings: {
      textDeprecated: 'Property text (%s) is deprecated since v03.10. Use property content instead of this.'
    }
  },

  propTypes: {
    /**
     * @deprecated since version 03.10. Replaced by property content.
     */
    text: React.PropTypes.string
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      /**
       * @deprecated since version 03.10
       */
      text: null
    };
  },

  componentWillMount: function () {
    if (this.props.text) {
      this.showWarning('textDeprecated', this.props.text);
    }
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  // Render
  render: function () {
    return React.createElement(
      'span',
      this.buildMainAttrs(),
      this.getChildren() || this.props.text || this.getDefault().content,
      this.getDisabledCover()
    );
  }
});

'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.fileViewer = React.createClass({

  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin],

  statics: {
    tagName: 'UU5.Bricks.fileViewer',
    defaults: {
      blockStart: '\@\@viewOn:',
      blockEnd: '\@\@viewOff:',
      displayErrMsg: 'Error during loading file '
    },
    classNames: {
      main: 'UU5_Bricks_fileViewer',
      blockOfLines: "UU5_Bricks_fileViewer-blockOfLines",
      blockOfLineNumbers: "UU5_Bricks_fileViewer-blockOfLineNumbers bg-info UU5_Common-textAlign-right"
    },
    errors: {
      unknownCallFeedback: 'Call feedback %s is not one of %s'
    }
  },

  propTypes: {
    src: React.PropTypes.string,
    parameters: React.PropTypes.string,
    numbered: React.PropTypes.bool,
    blockKey: React.PropTypes.string,
    blockStart: React.PropTypes.string,
    blockEnd: React.PropTypes.string
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      src: null,
      parameters: null,
      numbered: false,
      blockKey: null,
      blockStart: this.defaults.blockStart,
      blockEnd: this.defaults.blockEnd
    };
  },

  getInitialState: function () {
    return {
      data: null,
      blockOfLines: null,
      blockOfLineNumbers: null,
      blockOfLineNumbersWidth: null,
      callFeedback: 'loading',
      message: null
    };
  },

  componentWillMount: function () {
    !this.props.src && this.setState({ callFeedback: 'ready' });
  },

  //@@viewOn:componentDidMount
  componentDidMount: function () {
    if (this.props.src) {
      this._getData(this.props);
    }
  },
  //@@viewOff:componentDidMount

  componentWillReceiveProps: function (nextProps) {
    if (nextProps.src && (nextProps.src !== this.props.src || nextProps.parameters !== this.props.parameters)) {
      this.setState({
        data: null,
        blockOfLines: null,
        blockOfLineNumbers: null,
        blockOfLineNumbersWidth: null,
        callFeedback: 'loading',
        message: null
      }, (function () {
        this._getData(nextProps);
      }).bind(this));
    } else {
      if (nextProps.numbered !== this.props.numbered || nextProps.blockKey !== this.props.blockKey || nextProps.blockStart !== this.blockStart || nextProps.blockEnd !== this.blockEnd) {
        this._buildData(nextProps);
      }
    }
  },

  componentWillUnmount: function () {
    this.serverRequest && this.serverRequest.abort();
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _getData: function (props) {
    var fileViewer = this;
    this.serverRequest && this.serverRequest.abort();
    this.serverRequest = UU5.Tools.serverRequestGet(props.src, props.parameters, function (data) {
      fileViewer.setState({
        data: data,
        callFeedback: 'ready'
      }, function () {
        fileViewer._buildData(props);
      });
    }, function () {
      fileViewer.setState({
        callFeedback: 'error',
        message: fileViewer.getDefault().displayErrMsg + props.src + '(' + props.parameters + ')'
      });
    });
  },

  _buildData: function (props) {
    var fileViewer = this;
    var blockOfLines = '';
    var blockOfLineNumbers = '';
    var lines = this.state.data.split("\n");
    var blockOfLineNumbersWidth = (lines.length + 1 + '').length;
    var blockStart = props.blockStart + props.blockKey;
    var blockEnd = props.blockEnd + props.blockKey;
    var write = props.blockKey ? false : true;

    lines.forEach(function (v, i) {
      if (v.match(blockStart)) {
        write = true;
      } else if (v.match(blockEnd)) {
        write = false;
      } else if (write) {
        if (props.numbered) {
          blockOfLineNumbers += UU5.Tools.pad(i + 1, blockOfLineNumbersWidth) + '\n';
        }
        blockOfLines += v + '\n';
      }
    });
    fileViewer.setState({
      blockOfLines: blockOfLines,
      blockOfLineNumbers: blockOfLineNumbers
    });
  },

  _getMainChild: function () {
    var mainChild;
    var mainProps = this.getMainPropsToPass();

    switch (this.state.callFeedback) {
      case 'loading':
        mainChild = React.createElement(UU5.Bricks.loading, mainProps);
        break;
      case 'ready':
        mainChild = React.createElement(
          UU5.Bricks.table,
          this.buildMainAttrs(),
          React.createElement(
            UU5.Bricks.tbody,
            null,
            React.createElement(
              UU5.Bricks.tr,
              null,
              this.props.numbered && React.createElement(
                UU5.Bricks.td,
                {
                  className: this.getClassName().blockOfLineNumbers,
                  mainAttrs: { style: { width: this.state.blockOfLineNumbersWidth + 1 + "em" } },
                  ignoreGrammar: true,
                  ignoreInnerHTML: true,
                  ignoreSpaces: true
                },
                this.state.blockOfLineNumbers
              ),
              React.createElement(
                UU5.Bricks.td,
                {
                  className: this.getClassName().blockOfLines,
                  ignoreGrammar: true,
                  ignoreInnerHTML: true,
                  ignoreSpaces: true
                },
                this.state.blockOfLines
              )
            )
          )
        );
        break;
      case 'error':
        mainChild = React.createElement(UU5.Bricks.error, _extends({}, mainProps, { text: this.state.message }));
        break;
      default:
        this.showError('unknownCallFeedback', [this.state.callFeedback, 'loading, ready, error']);
        mainChild = React.createElement('span', null);
    }

    return mainChild;
  },

  // Render
  render: function () {
    return this._getMainChild();
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.footer = React.createClass({
  displayName: 'footer',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin, UU5.Common.levelMixin],

  statics: {
    tagName: 'UU5.Bricks.footer',
    classNames: {
      main: 'UU5_Bricks_footer'
    }
  },

  propTypes: {
    colorSchema: React.PropTypes.oneOf(UU5.Common.environment.colorSchema)
  },

  getDefaultProps: function () {
    return {
      colorSchema: 'low'
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  // Render

  render: function () {
    var attrs = this.buildMainAttrs();
    attrs.className += ' text-' + this.props.colorSchema;
    var level = this.getLevel();
    var height = '' + (0.6 - level * 0.05) + 'em';

    return React.createElement(
      'footer',
      attrs,
      React.createElement(UU5.Bricks.lineLow, { mainAttrs: { style: { height: height } } }),
      this.getChildren()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * Glyphicon is based on Bootstrap CSS.
 */
UU5.Bricks.glyphicon = React.createClass({
  displayName: 'glyphicon',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin],

  statics: {
    tagName: 'UU5.Bricks.glyphicon',
    classNames: {
      main: 'UU5_Bricks_glyphicon glyphicon'
    }
  },

  propTypes: {
    /**
     * Glyphicon name.
     */
    glyphicon: React.PropTypes.string
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      glyphicon: 'uu-glyphicon-ok'
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  // Render
  render: function () {
    var mainProps = this.buildMainAttrs();
    mainProps.className += ' ' + this.props.glyphicon;

    return React.createElement(
      'span',
      mainProps,
      this.props.children && React.Children.toArray(this.props.children),
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * UU5.Bricks.googleMap
 */
UU5.Bricks.googleMap = React.createClass({
  displayName: 'googleMap',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin],

  statics: {
    tagName: 'UU5.Bricks.googleMap',
    classNames: {
      main: 'UU5_Bricks_googleMap'
    },
    defaults: {
      loadLibsEvent: 'UU5_Bricks_googleMap-loadLibs',
      apiKeyUrl: 'https://maps.googleapis.com/maps/api/js'
    }
  },

  propTypes: {
    mapType: React.PropTypes.oneOf(['satellite', 'roadmap']),
    latitude: React.PropTypes.number,
    longitude: React.PropTypes.number,
    markers: React.PropTypes.arrayOf(React.PropTypes.shape({
      latitude: React.PropTypes.number,
      longitude: React.PropTypes.number,
      title: React.PropTypes.string,
      label: React.PropTypes.string
    })),
    zoom: React.PropTypes.number,
    zoomable: React.PropTypes.bool,
    draggable: React.PropTypes.bool,
    disabledDefaultUI: React.PropTypes.bool,
    googleApiKey: React.PropTypes.string,
    height: React.PropTypes.string,
    width: React.PropTypes.string,

    // https://developers.google.com/maps/documentation/javascript/styling
    mapStyle: React.PropTypes.arrayOf(React.PropTypes.object)
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      mapType: 'roadmap',
      latitude: 50.107799,
      longitude: 14.453689,
      markers: [],
      zoom: 14,
      zoomable: true,
      draggable: true,
      disabledDefaultUI: false,
      googleApiKey: null,
      height: '400px',
      width: '100%',
      mapStyle: null
    };
  },

  componentDidMount: function () {
    if (typeof google === 'undefined' && !window.loadingGoogleMapApi) {
      this._loadLibraries(this._initMap);
    } else if (loadingGoogleMapApi) {
      $(window).on(this.getDefault().loadLibsEvent, this._initMap);
    } else {
      this._initMap();
    }
  },

  // setting map options through props
  // for additions see https://developers.google.com/maps/documentation/javascript/reference#MapOptions
  componentWillReceiveProps: function (nextProps) {
    var newMapOptions = {};
    nextProps.draggable !== undefined && (newMapOptions.draggable = nextProps.draggable);
    nextProps.zoomable !== undefined && (newMapOptions.scrollwheel = nextProps.zoomable);
    nextProps.disabledDefaultUI !== undefined && (newMapOptions.disableDefaultUI = nextProps.disabledDefaultUI);
    Object.keys(newMapOptions).length && this.setMapOptions(newMapOptions);
  },

  // Interface

  getMap: function () {
    return this.map;
  },

  setMapOptions: function (options) {
    if (typeof options === 'object' && options !== null) {
      this.map.setOptions(options);
    }
    return this;
  },

  // Overriding Functions

  // Component Specific Helpers

  _loadLibraries: function (callback) {
    var googleMap = this;

    window.google = false;
    window.loadingGoogleMapApi = true;

    var script = document.createElement('script');
    document.head.appendChild(script);

    script.onload = function () {
      $(window).trigger(googleMap.getDefault().loadLibsEvent);
      typeof callback === 'function' && callback();
    };

    script.src = this.getDefault().apiKeyUrl + (this.props.googleApiKey ? '?key=' + this.props.googleApiKey : '');
  },

  _initMap: function () {
    var myCenter = new google.maps.LatLng(this.props.latitude, this.props.longitude);

    var mapProps = {
      center: myCenter,
      zoom: this.props.zoom,
      zoomControl: this.props.zoomable,
      scrollwheel: this.props.zoomable,
      disableDoubleClickZoom: !this.props.zoomable,
      draggable: this.props.draggable,
      disableDefaultUI: this.props.disabledDefaultUI,
      mapTypeId: google.maps.MapTypeId[this.props.mapType]
    };

    this.map = new google.maps.Map(ReactDOM.findDOMNode(this.refs.map), mapProps);

    if (this.props.mapStyle) {
      var styledMap = new google.maps.StyledMapType(this.props.mapStyle);
      this.map.mapTypes.set('map_style', styledMap);
      this.map.setMapTypeId('map_style');
    }

    if (this.props.markers !== null) {
      if (!this.props.markers.length) {
        var marker = new google.maps.Marker({
          position: myCenter
        });
        marker.setMap(this.map);
      } else {
        var markers = this.props.markers.map(function (markerProps) {
          var position = new google.maps.LatLng(markerProps.latitude, markerProps.longitude);
          return new google.maps.Marker({
            position: position,
            title: markerProps.title,
            label: markerProps.label
          });
        }.bind(this));
        markers.forEach(function (marker) {
          marker.setMap(this.map);
        }.bind(this));
      }
    }
  },

  // Render
  render: function () {
    var mainAttrs = this.buildMainAttrs();

    var mapAttrs = {
      ref: 'map',
      style: { height: this.props.height, width: this.props.width }
    };

    return React.createElement(
      'div',
      this.buildMainAttrs(),
      React.createElement('div', mapAttrs),
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.header = React.createClass({
  displayName: 'header',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin, UU5.Common.levelMixin],

  statics: {
    tagName: 'UU5.Bricks.header',
    classNames: {
      main: 'UU5_Bricks_header'
    }
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  // Render

  render: function () {
    var level = this.getLevel();
    var headerTag = level > 0 ? 'h' + level : this.checkTag('UU5.Bricks.jumbotron');
    return React.createElement(headerTag, this.buildMainAttrs(), this.buildChildren(), this.getDisabledCover());
  }
});

"use strict";

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * UU5.Bricks.heading
 */
UU5.Bricks.heading = React.createClass({
  displayName: "heading",


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: "UU5.Bricks.heading",
    classNames: {
      main: "UU5_Bricks_heading",
      fixed: "UU5_Bricks_heading-fixed"
    }
  },

  propTypes: {
    fixed: React.PropTypes.bool,
    fixedOnScroll: React.PropTypes.bool,
    onScrollToFixed: React.PropTypes.func, // not called in interface, just on scroll
    onScrollToBlocked: React.PropTypes.func // not called in interface, just on scroll
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      fixed: false,
      fixedOnScroll: false,
      onScrollToFixed: null,
      onScrollToBlocked: null
    };
  },

  getInitialState: function () {
    return {
      fixed: this.props.fixed
    };
  },

  componentWillMount: function () {
    this.scrollPosition = 0;
    this.scrollStart = 0;
  },

  componentDidMount: function () {
    var heading = this;

    if (heading.props.fixedOnScroll || heading.props.onScrollToFixed || heading.props.onScrollToBlocked) {
      $(window).scroll(function () {
        var scrollPosition = $(window).scrollTop();
        var scrollStart = heading.getOffsetTop();

        if (scrollPosition > scrollStart && heading.scrollPosition <= scrollStart) {
          //console.log(true, {oldScrollPosition: heading.scrollPosition, newScrollPosition: scrollPosition, oldScrollStart: heading.scrollOffsetStart, newScrollStart: scrollOffsetStart});
          heading.scrollPosition = scrollPosition;
          heading.scrollStart = scrollStart;

          if (typeof heading.props.onScrollToFixed === 'function') {
            heading.props.onScrollToFixed(heading, scrollPosition, scrollStart);
          } else if (heading.props.fixedOnScroll) {
            heading.setFixed();
          }
        } else if (scrollPosition <= heading.scrollStart && heading.scrollPosition > scrollStart) {
          //console.log(false, {oldScrollPosition: heading.scrollPosition, newScrollPosition: 0, oldScrollStart: heading.scrollOffsetStart, newScrollStart: 0});

          heading.scrollPosition = 0;
          heading.scrollStart = 0;

          if (typeof heading.props.onScrollToBlocked === 'function') {
            heading.props.onScrollToBlocked(heading, scrollPosition, scrollStart);
          } else if (heading.props.fixedOnScroll) {
            heading.setBlocked();
          }
        }
      });
    }
  },

  // Interface
  getOffsetTop: function () {
    var componentJQuery = $("#" + this.getId());
    return componentJQuery ? componentJQuery[0].offsetTop : null;
  },

  setFixedValue: function (fixed, setStateCallback) {
    this.setState({ fixed: fixed }, setStateCallback);
    return this;
  },

  isFixed: function () {
    return this.state.fixed;
  },

  setFixed: function (setStateCallback) {
    this.setFixedValue(true, setStateCallback);
    return this;
  },

  setBlocked: function (setStateCallback) {
    this.setFixedValue(false, setStateCallback);
    return this;
  },

  // Overriding Functions

  // Component Specific Helpers
  _getMainAttrs: function () {
    var mainAttrs = this.buildMainAttrs();

    mainAttrs.id = this.getId();
    this.state.fixed && (mainAttrs.className += " " + this.getClassName().fixed);

    return mainAttrs;
  },

  // Render
  render: function () {
    return React.createElement(
      "div",
      this._getMainAttrs(),
      this.getChildren(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.homeScreen = React.createClass({
  displayName: 'homeScreen',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin],

  statics: {
    tagName: 'UU5.Bricks.homeScreen',
    classNames: {
      main: 'UU5_Bricks_homeScreen'
    },
    defaults: {
      reSmartURL: /\/ath(\/)?$/,
      reQueryString: /([\?&]ath=[^&]*$|&ath=[^&]*(&))/,
      lsi: {
        'cs-cs': {
          iOS: 'Pro přidáni této webové aplikace na úvodní obrazovku: stlačte %icon a pak <strong>Přidat na úvodní obrazovku</strong>.',
          android: 'Pro přidáni této webové aplikace na úvodní obrazovku otevřete menu nastavení prohlížeče a stlačte <strong>Přidat na úvodní obrazovku</strong>. <small>K menu se dostanete stlačením hardwaroveho tlačítka, když ho vaše zařízení má, nebo stlačením pravé horní menu ikony <span class="ath-action-icon">icon</span>.</small>'
        },

        'de-de': {
          iOS: 'Um diese Web-App zum Home-Bildschirm hinzuzufügen, tippen Sie auf %icon und dann <strong>Zum Home-Bildschirm</strong>.',
          android: 'Um diese Web-App zum Home-Bildschirm hinzuzufügen, öffnen Sie das Menü und tippen dann auf <strong>Zum Startbildschirm hinzufügen</strong>. <small>Wenn Ihr Gerät eine Menütaste hat, lässt sich das Browsermenü über diese öffnen. Ansonsten tippen Sie auf %icon.</small>'
        },

        'da-dk': {
          iOS: 'For at tilføje denne web app til hjemmeskærmen: Tryk %icon og derefter <strong>Føj til hjemmeskærm</strong>.',
          android: 'For at tilføje denne web app til hjemmeskærmen, åbn browser egenskaber menuen og tryk på <strong>Føj til hjemmeskærm</strong>. <small>Denne menu kan tilgås ved at trykke på menu knappen, hvis din enhed har en, eller ved at trykke på det øverste højre menu ikon %icon.</small>'
        },

        'el-gr': {
          iOS: 'Για να προσθέσετε την εφαρμογή στην αρχική οθόνη: πατήστε το %icon και μετά <strong>Πρόσθεσε στην αρχική οθόνη</strong>.',
          android: 'Για να προσθέσετε την εφαρμογή στην αρχική οθόνη, ανοίξτε τις επιλογές του browser σας και πατήστε το <strong>Προσθήκη στην αρχική οθόνη</strong>. <small>Μπορείτε να έχετε πρόσβαση στο μενού, πατώντας το κουμπί του μενού του κινητού σας ή το πάνω δεξιά κουμπί του μενού %icon.</small>'
        },

        'en-us': {
          iOS: 'To add this web app to the home screen: tap %icon and then <strong>Add to Home Screen</strong>.',
          android: 'To add this web app to the home screen open the browser option menu and tap on <strong>Add to homescreen</strong>. <small>The menu can be accessed by pressing the menu hardware button if your device has one, or by tapping the top right menu icon %icon.</small>'
        },

        'es-es': {
          iOS: 'Para añadir esta aplicación web a la pantalla de inicio: pulsa %icon y selecciona <strong>Añadir a pantalla de inicio</strong>.',
          android: 'Para añadir esta aplicación web a la pantalla de inicio, abre las opciones y pulsa <strong>Añadir a pantalla inicio</strong>. <small>El menú se puede acceder pulsando el botón táctil en caso de tenerlo, o bien el icono de la parte superior derecha de la pantalla %icon.</small>'
        },

        'fi-fi': {
          iOS: 'Liitä tämä sovellus kotivalikkoon: klikkaa %icon ja tämän jälkeen <strong>Lisää kotivalikkoon</strong>.',
          android: 'Lisätäksesi tämän sovelluksen aloitusnäytölle, avaa selaimen valikko ja klikkaa tähti -ikonia tai <strong>Lisää aloitusnäytölle tekstiä</strong>. <small>Valikkoon pääsee myös painamalla menuvalikkoa, jos laitteessasi on sellainen tai koskettamalla oikealla yläkulmassa menu ikonia %icon.</small>'
        },

        'fr-fr': {
          iOS: 'Pour ajouter cette application web sur l\'écran d\'accueil : Appuyez %icon et sélectionnez <strong>Ajouter sur l\'écran d\'accueil</strong>.',
          android: 'Pour ajouter cette application web sur l\'écran d\'accueil : Appuyez sur le bouton "menu", puis sur <strong>Ajouter sur l\'écran d\'accueil</strong>. <small>Le menu peut-être accessible en appuyant sur le bouton "menu" du téléphone s\'il en possède un <i class="fa fa-bars"></i>. Sinon, il se trouve probablement dans la coin supérieur droit du navigateur %icon.</small>'
        },

        'he-il': {
          iOS: '<span dir="rtl">להוספת האפליקציה למסך הבית: ללחוץ על %icon ואז <strong>הוסף למסך הבית</strong>.</span>',
          android: 'To add this web app to the home screen open the browser option menu and tap on <strong>Add to homescreen</strong>. <small>The menu can be accessed by pressing the menu hardware button if your device has one, or by tapping the top right menu icon %icon.</small>'
        },

        'hu-hu': {
          iOS: 'Ha hozzá szeretné adni ezt az alkalmazást a kezdőképernyőjéhez, érintse meg a következő ikont: %icon , majd a <strong>Hozzáadás a kezdőképernyőhöz</strong> menüpontot.',
          android: 'Ha hozzá szeretné adni ezt az alkalmazást a kezdőképernyőjéhez, a böngésző menüjében kattintson a <strong>Hozzáadás a kezdőképernyőhöz</strong> menüpontra. <small>A böngésző menüjét a következő ikon megérintésével tudja megnyitni: %icon.</small>'
        },

        'it-it': {
          iOS: 'Per aggiungere questa web app alla schermata iniziale: premi %icon e poi <strong>Aggiungi a Home</strong>.',
          android: 'Per aggiungere questa web app alla schermata iniziale, apri il menu opzioni del browser e premi su <strong>Aggiungi alla homescreen</strong>. <small>Puoi accedere al menu premendo il pulsante hardware delle opzioni se la tua device ne ha uno, oppure premendo l\'icona %icon in alto a destra.</small>'
        },

        'ja-jp': {
          iOS: 'このウェプアプリをホーム画面に追加するには、%iconをタップして<strong>ホーム画面に追加</strong>してください。',
          android: 'このウェプアプリをホーム画面に追加するには、ブラウザのオプションメニューから<strong>ホーム画面に追加</strong>をタップしてください。<small>オプションメニューは、一部の機種ではデバイスのメニューボタンから、それ以外では画面右上の%iconからアクセスできます。</small>'
        },

        'ko-kr': {
          iOS: '홈 화면에 바로가기 생성: %icon 을 클릭한 후 <strong>홈 화면에 추가</strong>.',
          android: '브라우저 옵션 메뉴의 <string>홈 화면에 추가</string>를 클릭하여 홈화면에 바로가기를 생성할 수 있습니다. <small>옵션 메뉴는 장치의 메뉴 버튼을 누르거나 오른쪽 상단의 메뉴 아이콘 %icon을 클릭하여 접근할 수 있습니다.</small>'
        },

        'nb-no': {
          iOS: 'For å installere denne appen på hjem-skjermen: trykk på %icon og deretter <strong>Legg til på Hjem-skjerm</strong>.',
          android: 'For å legge til denne webappen på startsiden åpner en nettlesermenyen og velger <strong>Legg til på startsiden</strong>. <small>Menyen åpnes ved å trykke på den fysiske menyknappen hvis enheten har det, eller ved å trykke på menyikonet øverst til høyre %icon.</small>'
        },

        'pt-br': {
          iOS: 'Para adicionar este app à tela de início: clique %icon e então <strong>Tela de início</strong>.',
          android: 'Para adicionar este app à tela de início, abra o menu de opções do navegador e selecione <strong>Adicionar à tela inicial</strong>. <small>O menu pode ser acessado pressionando o "menu" button se o seu dispositivo tiver um, ou selecionando o ícone %icon no canto superior direito.</small>'
        },

        'pt-pt': {
          iOS: 'Para adicionar esta app ao ecrã principal: clique %icon e depois <strong>Ecrã principal</strong>.',
          android: 'Para adicionar esta app web ecrã principal, abra o menu de opções do navegador e selecione <strong>Adicionar à tela inicial</strong>. <small>O menu pode ser acessado pressionando o "menu" button se o seu dispositivo tiver um, ou selecionando o ícone %icon no canto superior direito.</small>'
        },

        'nl-nl': {
          iOS: 'Om deze webapp aan je startscherm toe te voegen, klik op %icon en dan <strong>Zet in startscherm</strong>.',
          android: 'Om deze webapp aan je startscherm toe te voegen, open de browserinstellingen en tik op <strong>Toevoegen aan startscherm</strong>. <small>Gebruik de "menu" knop als je telefoon die heeft, anders het menu-icoon rechtsbovenin %icon.</small>'
        },

        'ru-ru': {
          iOS: 'Чтобы добавить этот сайт на свой домашний экран, нажмите на иконку %icon и затем <strong>На экран "Домой"</strong>.',
          android: 'Чтобы добавить сайт на свой домашний экран, откройте меню браузера и нажмите на <strong>Добавить на главный экран</strong>. <small>Меню можно вызвать, нажав на кнопку меню вашего телефона, если она есть. Или найдите иконку сверху справа %icon[иконка].</small>'
        },

        'sk-sk': {
          iOS: 'Pre pridanie tejto webovej aplikácie na úvodnú obrazovku: stlačte %icon a potom <strong>Pridať na úvodnú obrazovku</strong>.',
          android: 'Pre pridanie tejto webovej aplikácie na úvodnú obrazovku otvorte menu nastavenia prehliadača a stlačte <strong>Pridať na úvodnú obrazovku</strong>. <small>K menu sa dostanete stlačením hardwaroveho tlačidla, ak ho vaše zariadenie má, alebo stlačením pravej hornej menu ikony <span class="ath-action-icon">icon</span>.</small>'
        },

        'sv-se': {
          iOS: 'För att lägga till denna webbapplikation på hemskärmen: tryck på %icon och därefter <strong>Lägg till på hemskärmen</strong>.',
          android: 'För att lägga till den här webbappen på hemskärmen öppnar du webbläsarens alternativ-meny och väljer <strong>Lägg till på startskärmen</strong>. <small>Man hittar menyn genom att trycka på hårdvaruknappen om din enhet har en sådan, eller genom att trycka på menyikonen högst upp till höger %icon.</small>'
        },

        'tr-tr': {
          iOS: 'Uygulamayı ana ekrana eklemek için, %icon ve ardından <strong>ana ekrana ekle</strong> butonunu tıklayın.',
          android: 'Uygulamayı ana ekrana eklemek için, menüye girin ve <strong>ana ekrana ekle</strong> butonunu tıklayın. <small>Cihazınız menü tuşuna sahip ise menüye girmek için menü tuşunu tıklayın. Aksi takdirde %icon butonunu tıklayın.</small>'
        },

        'uk-ua': {
          iOS: 'Щоб додати цей сайт на початковий екран, натисніть %icon, а потім <strong>На початковий екран</strong>.',
          android: 'Щоб додати цей сайт на домашній екран, відкрийте меню браузера та виберіть <strong>Додати на головний екран</strong>. <small>Це можливо зробити, натиснувши кнопку меню на вашому смартфоні, якщо така є. Або ж на іконці зверху справа %icon.</small>'
        },

        'zh-cn': {
          iOS: '如要把应用程序加至主屏幕,请点击%icon, 然后<strong>添加到主屏幕</strong>',
          android: 'To add this web app to the home screen open the browser option menu and tap on <strong>Add to homescreen</strong>. <small>The menu can be accessed by pressing the menu hardware button if your device has one, or by tapping the top right menu icon %icon.</small>'
        },

        'zh-tw': {
          iOS: '如要把應用程式加至主屏幕, 請點擊%icon, 然後<strong>加至主屏幕</strong>.',
          android: 'To add this web app to the home screen open the browser option menu and tap on <strong>Add to homescreen</strong>. <small>The menu can be accessed by pressing the menu hardware button if your device has one, or by tapping the top right menu icon %icon.</small>'
        }
      },
      defaultSession: {
        lastDisplayTime: 0, // last time we displayed the message
        returningVisitor: false, // is this the first time you visit
        displayCount: 0, // number of times the message has been shown
        //optedout: false, // has the user opted out
        added: false // has been actually added to the homescreen
      }
    }
  },

  propTypes: {},

  // Setting defaults
  getDefaultProps: function () {
    return {
      appID: 'uu5.homescreen', // local storage name (no need to change)
      skipFirstVisit: false, // show only to returning visitors (ie: skip the first time you visit)
      startDelay: 1, // display the message after that many seconds from page load
      lifespan: 5, // life of the message in seconds
      displayPace: 1440, // minutes before the message is shown again (0: display every time, default 24 hours)
      maxDisplayCount: 0, // absolute maximum number of times the message will be shown to the user (0: no limit)
      message: '', // the message can be customized
      detectHomescreen: true };
  },

  getInitialState: function () {
    return {
      receiveProps: false
    };
  },

  componentWillMount: function () {
    this.hide();
  },

  componentDidMount: function () {
    var homeScreen = this;
    if (!this.props.hidden) {
      setTimeout(function () {
        homeScreen.activate();
      }, this.props.startDelay < 0 ? 1 : this.props.startDelay * 1000);
    }
  },

  componentWillReceiveProps: function (nextProps) {
    // cannot set visibility by receive props
    this.setState({ hidden: this.isHidden() });
  },

  componentWillUnmount: function () {
    this.hiddenTimeout && clearTimeout(this.hiddenTimeout);
  },

  // Interface
  activate: function (setStateCallback) {
    if (this._shouldRender()) {
      var homeScreen = this;

      this.setHiddenValue(false, function () {
        homeScreen.hiddenTimeout && clearTimeout(homeScreen.hiddenTimeout);
        homeScreen.hiddenTimeout = setTimeout(function () {
          homeScreen.hide(setStateCallback);
        }, homeScreen.props.lifespan * 1000);
      });
    }
    return this;
  },

  // Overriding Functions

  // Component Specific Helpers
  _getMobileOS: function () {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    var os = 'unknown';

    // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
      os = "windowsPhone";
    } else if (/android/i.test(userAgent)) {
      os = "android";

      // iOS detection from: http://stackoverflow.com/a/9039885/177710
    } else if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
      os = "iOS";
    }

    return os;
  },

  _hasToken: function () {
    return document.location.hash === '#ath' || this.getDefault().reSmartURL.test(document.location.href) || this.getDefault().reQueryString.test(document.location.search);
  },

  _isMobileIOS: function () {
    return this._getMobileOS() === 'iOS';
  },

  _isMobileAndroid: function () {
    return this._getMobileOS() === 'android';
  },

  _isSafari: function () {
    var userAgent = window.navigator.userAgent;
    return this._isMobileIOS() && userAgent.indexOf('Safari') > -1 && userAgent.indexOf('CriOS') === -1;
  },

  _isChrome: function () {
    var userAgent = window.navigator.userAgent;
    return (/Chrome\/[.0-9]*/.test(userAgent) && userAgent.indexOf("Version") === -1
    );
  },

  _isAndroidChrome: function () {
    return this._isMobileAndroid() && this._isChrome();
  },

  _isCompatibleEnvironment: function () {
    return this._isSafari() && this._getMobileOSVersion() >= 6 || this._isAndroidChrome();
  },

  _isStandAloneMode: function () {
    return !!window.navigator.standalone;
  },

  _getBrowserLanguage: function () {
    return window.navigator.language ? window.navigator.language.toLowerCase() : 'en';
  },

  _getMobileOSVersion: function () {
    var version = window.navigator.userAgent.match(/(OS|Android|Windows Phone) (\d+[_\.]\d+)/);
    return version && version[2] ? +version[2].replace('_', '.') : 0;
  },

  _isTablet: function () {
    var userAgent = window.navigator.userAgent;
    return this._isSafari() && userAgent.indexOf('iPad') > -1 || this._isAndroidChrome() && userAgent.indexOf('Mobile') === -1;
  },

  _shouldRender: function () {
    var shouldRender = false;

    // load session
    var session = this._getAppSession();

    // user most likely came from a direct link containing our token, we don't need it and we remove it
    if (this._hasToken() && (!this._isCompatibleEnvironment() || !session && !this.props.detectHomescreen)) {
      this._removeToken();
    }

    // the device is supported
    if (this._isCompatibleEnvironment()) {
      session = session || this.getDefault().defaultSession;
      this._setAppSession(session);

      var now = Date.now();
      if (this._checkSession(session) && this._checkShowing(session, now)) {
        this._incrementDisplayCount(session, now);
        shouldRender = true;
      }
    }

    return shouldRender;
  },

  _checkSession: function (session) {
    // if is added at homescreen
    if (session.added) {
      return false;
    }

    // check if the app is in stand alone mode
    if (this._isStandAloneMode()) {
      session.added = true;
      this._setAppSession(session);
      return false;
    }

    // (try to) check if the page has been added to the homescreen
    if (this.props.detectHomescreen) {
      // the URL has the token, we are likely coming from the homescreen
      if (this._hasToken()) {
        this._removeToken(); // we don't actually need the token anymore, we remove it to prevent redistribution

        // this is called the first time the user opens the app from the homescreen
        session.added = true;
        this._setAppSession(session);
        return false;
      }

      // URL doesn't have the token, so add it
      this._addToken();
    }

    // check if this is a returning visitor
    if (!session.returningVisitor) {
      session.returningVisitor = true;
      this._setAppSession(session);

      // we do not show the message if this is your first visit
      if (this.props.skipFirstVisit) {
        return false;
      }
    }

    return true;
  },

  _getSession: function (item) {
    var session;
    if (localStorage) {
      session = localStorage.getItem(item);
      session && (session = JSON.parse(session));
    }
    return session || null;
  },

  _setSession: function (item, session) {
    localStorage && localStorage.setItem(item, JSON.stringify(session));
    return this;
  },

  _getAppSession: function () {
    return this._getSession(this.props.appID);
  },

  _setAppSession: function (session) {
    this._setSession(this.props.appID, session);
    return this;
  },

  _addToken: function () {
    if (this.props.detectHomescreen === true || this.props.detectHomescreen == 'hash') {
      document.location.hash !== '#ath' && history.replaceState('', window.document.title, document.location.href + '#ath');
    } else if (this.props.detectHomescreen == 'smartURL') {
      history.replaceState('', window.document.title, document.location.href.replace(/(\/)?$/, '/ath$1'));
    } else {
      history.replaceState('', window.document.title, document.location.href + (document.location.search ? '&' : '?') + 'ath=');
    }
    return this;
  },

  _removeToken: function () {
    if (document.location.hash == '#ath') {
      history.replaceState('', window.document.title, document.location.href.split('#')[0]);
    }

    if (this.getDefault().reSmartURL.test(document.location.href)) {
      history.replaceState('', window.document.title, document.location.href.replace(this.getDefault().reSmartURL, '$1'));
    }

    if (this.getDefault().reQueryString.test(document.location.search)) {
      history.replaceState('', window.document.title, document.location.href.replace(this.getDefault().reQueryString, '$2'));
    }

    return this;
  },

  _checkShowing: function (session, date) {
    var lastDisplayTime = session.lastDisplayTime;

    // we obey the display pace (prevent the message to popup too often)
    var showing = date - lastDisplayTime > this.props.displayPace * 60000;

    // obey the maximum number of display count
    showing = showing && (this.props.maxDisplayCount === 0 || session.displayCount < this.props.maxDisplayCount);

    return showing;
  },

  _incrementDisplayCount: function (session, date) {
    // increment the display count
    session.lastDisplayTime = date;
    session.displayCount++;
    this._setAppSession(session);
    return this;
  },

  _getMessage: function () {
    var languages = this.sortLanguages(this._getBrowserLanguage());
    var message;
    var messages;

    if (this.props.message) {
      if (typeof this.props.message === 'object') {
        message = this.props.message[this._getMobileOS()];
        if (!message) {
          messages = this.getLSIItemByLanguage(this.props.message, languages);
        }
      } else {
        message = this.props.message;
      }
    } else {
      messages = this.getLSIItemByLanguage(this.getDefault().lsi, languages);
    }

    messages && (message = messages[this._getMobileOS()]);

    message = message && message.replace(/%icon(?:\[([^\]]+)\])?/gi, function (matches, group1) {
      return '<span class="ath-action-icon">' + (!!group1 ? group1 : 'icon') + '</span>';
    });

    return message;
  },

  // Render
  render: function () {
    var message = this._getMessage();

    var visibility = this.isHidden() ? ' UU5_Bricks_homeScreen-hidden' : ' UU5_Bricks_homeScreen-shown';

    var containerProps = {
      className: 'ath-container ath-' + this._getMobileOS() + ' ath-' + this._getMobileOS() + (parseInt(this._getMobileOSVersion()) || '') + ' ath-' + (this._isTablet() ? 'tablet' : 'phone') + visibility
    };

    return React.createElement(
      'div',
      { className: 'ath-viewport' },
      React.createElement(
        'div',
        containerProps,
        React.createElement('p', { dangerouslySetInnerHTML: { __html: message } })
      )
    );
  }
});

'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * UU5.Bricks.iframe
 */
UU5.Bricks.iframe = React.createClass({
  displayName: 'iframe',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin],

  statics: {
    tagName: 'UU5.Bricks.iframe',
    classNames: {
      main: 'UU5_Bricks_iframe',
      disabledWrapper: 'UU5_Bricks_iframe-disabledWrapper UU5_Common-disabledCoverWrapper'
    }
  },

  propTypes: {
    src: React.PropTypes.string
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      src: 'https://plus4u.net'
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  // Render
  render: function () {
    var iframe = React.createElement('iframe', _extends({}, this.buildMainAttrs(), { src: this.props.src }));

    if (this.isDisabled()) {
      iframe = React.createElement(
        'div',
        { className: this.getClassName().disabledWrapper },
        iframe,
        this.getDisabledCover()
      );
    }

    return iframe;
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.ignoreInnerHTML = React.createClass({
  displayName: 'ignoreInnerHTML',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.ignoreInnerHTML',
    classNames: {
      main: 'UU5_Bricks_ignoreInnerHTML'
    }
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _getMainProps: function () {
    var mainProps = this.getMainPropsToPass(['UU5_Common_baseMixin', 'UU5_Common_elementaryMixin', 'UU5_Common_contentMixin']);

    mainProps.id = this.getId() + '-span';
    mainProps.ignoreInnerHTML = true;

    return mainProps;
  },

  // Render
  render: function () {
    return React.createElement(
      U5.Bricks.span,
      this._getMainProps(),
      React.Children.toArray(this.props.children)
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * Image is a component based on bootstrap standard. It allows images to be inserted into a page.
 * Image can have a specific shape.
 */
UU5.Bricks.image = React.createClass({
  displayName: 'image',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin],

  statics: {
    tagName: 'UU5.Bricks.image',
    classNames: {
      main: 'UU5_Bricks_image',
      disabledWrapper: 'UU5_Common-disabledCoverWrapper'
    }
  },

  // TODO: strictCircle -> no ellipse but cut a circle from different template size - e.g. http://sixrevisions.com/css/circular-images-css/
  propTypes: {
    /**
     * Rounded adds rounded corners to an template (IE8 does not support rounded corners).
     * Circle shapes the template into a circle (IE8 does not support circle shape).
     * Thumbnail shapes the template into a thumbnail (border with padding).
     */
    type: React.PropTypes.oneOf(['rounded', 'circle', 'thumbnail']),

    /**
     * Path or URL to template source file.
     */
    src: React.PropTypes.string,

    /**
     * Responsive template automatically adjusts to fit the size of the screen.
     */
    responsive: React.PropTypes.bool
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      type: null,
      src: 'noSrc',
      responsive: true
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _getAlt: function () {
    var alt = this.getName();

    if (!alt && this.props.src) {
      alt = this.getFileName(this.props.src);
    }

    return alt;
  },

  // Render
  render: function () {
    var mainAttrs = this.buildMainAttrs();

    this.props.type && (mainAttrs.className += ' images-' + this.props.type);
    this.props.responsive && (mainAttrs.className += ' images-responsive');
    mainAttrs.src = this.props.src;
    mainAttrs.alt = mainAttrs.alt || this._getAlt();

    var image = React.createElement('img', mainAttrs);

    if (this.isDisabled()) {
      image = React.createElement(
        'div',
        { className: this.getClassName().disabledWrapper },
        image,
        this.getDisabledCover()
      );
    }

    return image;
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * UU5.Bricks.jumbotron
 */
UU5.Bricks.jumbotron = React.createClass({
  displayName: 'jumbotron',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.jumbotron',
    classNames: {
      main: 'UU5_Bricks_jumbotron jumbotron'
    }
  },

  propTypes: {
    colorSchema: React.PropTypes.oneOf(UU5.Common.environment.colorSchema)
  },

  getDefaultProps: function () {
    return {
      colorSchema: null
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _getMainAttrs: function () {
    var attrs = this.buildMainAttrs();
    this.props.colorSchema && (attrs.className += ' bg-' + this.props.colorSchema);
    return attrs;
  },

  // Render
  render: function () {
    return React.createElement(
      'div',
      this._getMainAttrs(),
      this.getChildren(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * Label is used to provide additional information about something.
 */
UU5.Bricks.label = React.createClass({
  displayName: 'label',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin],

  statics: {
    tagName: 'UU5.Bricks.label',
    classNames: {
      main: 'UU5_Bricks_label label'
    }
  },

  propTypes: {
    /**
     * Color style for label.
     */
    colorSchema: React.PropTypes.oneOf(['default', 'primary', 'success', 'info', 'warning', 'danger']),

    /**
     * Label value should be some short text.
     */
    label: React.PropTypes.string
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      colorSchema: 'default',
      label: 'Empty'
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  // Render
  render: function () {
    var mainAttrs = this.buildMainAttrs();
    mainAttrs.className += ' label-' + this.props.colorSchema;

    return React.createElement(
      'span',
      _extends({}, mainAttrs, {
        __self: this
      }),
      this.props.label,
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.li = React.createClass({
  displayName: 'li',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.li',
    classNames: {
      main: 'UU5_Bricks_li'
    },
    defaults: {
      parentTagNames: ['UU5.Bricks.ul', 'UU5.Bricks.ol']
    }
  },

  // Interface
  componentWillMount: function () {
    this.checkParentTagName(this.getDefault().parentTagNames);
  },

  // Overriding Functions

  // Component Specific Helpers

  // Render
  render: function () {
    return React.createElement(
      'li',
      this.buildMainAttrs(),
      this.getChildren(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.line = React.createClass({
  displayName: 'line',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.line',
    classNames: {
      main: 'UU5_Bricks_line',
      bg: 'bg-',
      size: 'UU5_Bricks_line-size-'
    }
  },

  propTypes: {
    colorSchema: React.PropTypes.oneOf(UU5.Common.environment.colorSchema),
    size: React.PropTypes.oneOf(['xs', 'sm', 'md', 'lg'])
  },

  getDefaultProps: function () {
    return {
      colorSchema: null,
      size: 'sm'
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _buildMainAttrs: function () {
    var mainAttrs = this.buildMainAttrs();
    mainAttrs.className += ' ' + this.getClassName().bg + this.props.colorSchema;
    mainAttrs.className += ' ' + this.getClassName().size + this.props.size;
    return mainAttrs;
  },

  // Render
  render: function () {
    return React.createElement(
      'div',
      this._buildMainAttrs(),
      this.getDisabledCover()
    );
  }
});

UU5.Bricks.factory = UU5.Bricks.factory || {};
UU5.Bricks.factory._createLine = function (colorSchema) {
  return React.createClass({

    // Interface

    // Overriding Functions

    // Component Specific Helpers

    // Render
    render: function () {
      return React.createElement(UU5.Bricks.line, _extends({}, this.props, { colorSchema: colorSchema }));
    }
  });
};

UU5.Common.environment.colorSchema.forEach(function (colorSchema) {
  var colorSchemaCapitalize = UU5.Common.environment.colorSchemaCapitalize(colorSchema);
  UU5.Bricks['line' + colorSchemaCapitalize] = UU5.Bricks.factory._createLine(colorSchema);
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * Link can used as button with onClick function or with reference to another page. If href property refers
 * to an ID on the same page, scrolling can be animated by setting smoothScroll (in milliseconds).
 */
UU5.Bricks.link = React.createClass({
  displayName: 'link',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.link',
    classNames: {
      main: 'UU5_Bricks_link'
    },
    defaults: {
      content: 'noText'
    }
  },

  propTypes: {
    /**
     * Reference to another page or an ID on this page.
     */
    href: React.PropTypes.string,

    /**
     * If set, the function is called (instead of using href). Parameters: instance of link and event.
     * Called: this.props.onClick(this, event);
     */
    onClick: React.PropTypes.func,

    /**
     * Time in milliseconds, which it takes to scroll from link to another component on the same page specified by ID.
     *
     * TODO: maybe should be in seconds? If some want to smaller than sec, set 0.01 = 10ms
     */
    smoothScroll: React.PropTypes.number,

    /**
     * If we do not want to scroll to the exact position of a component (e. g. there is a fixed navigation bar
     * at the top of the page and if page scrolls to an exact position of a component, if would be partially hidden
     * behind the navigation bar), we can decrease target's offset.
     */
    offset: React.PropTypes.number,

    target: React.PropTypes.oneOf(['_blank', '_parent', '_top', '_self'])
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      href: null,
      onClick: null,
      smoothScroll: 0,
      offset: 0,
      target: '_self'
    };
  },

  // Interface

  /**
   * Scrolls to a target specified by ID (with animation).
   *
   * @param {string} id - ID of HTML tag which it will be scrolled to.
   * @param {number} smoothScroll - Time in milliseconds specifying how long does the scrolling take.
   * @param {number} offset - How much will be target's offset decreased by (in pixels).
   * @returns {UU5.Bricks.link} this
   */
  scrollToTarget: function (id, smoothScroll, offset) {
    // TODO: maybe should be in baseMixin!!!

    var target = $(id);
    var page = $('html, body');
    if (target.length > 0) {
      var eventNames = 'scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove';

      page.on(eventNames, function () {
        page.stop();
      });

      page.animate({ scrollTop: target.offset().top - offset || 0 }, smoothScroll || 0, function () {
        page.off(eventNames);
      });
    }

    return this;
  },

  // Overriding Functions

  // Component Specific Helpers

  _isSmoothScroll: function () {
    return this.props.href && this.props.href.lastIndexOf('#', 0) === 0 && (this.props.smoothScroll > 0 || this.props.offset > 0);
  },

  _onClickHandler: function (e) {
    if (typeof this.props.onClick === 'function') {
      this.props.onClick(this, e);
    } else if (this._isSmoothScroll()) {
      e.preventDefault();
      this.scrollToTarget(this.props.href, this.props.smoothScroll, this.props.offset);
    }
    return this;
  },

  // Render
  render: function () {
    var mainAttrs = this.buildMainAttrs();

    if (typeof this.props.onClick === 'function' || this._isSmoothScroll()) {
      mainAttrs.onClick = this._onClickHandler;

      // if attribute href in tag a is not set, link cannot reload page
    } else if (!this.isDisabled() && this.props.href) {
      mainAttrs.href = this.props.href;
      mainAttrs.target = this.props.target;
    }

    return React.createElement(
      'a',
      mainAttrs,
      this.getChildren() || this.props.href || this.getDefault().content,
      this.getDisabledCover()
    );
  }
});

UU5.Bricks.a = UU5.Bricks.link;

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};
UU5.Bricks.factory = UU5.Bricks.factory || {};

UU5.Bricks.factory._createLinks = function (uu5Name, href, defaultContent) {
  return React.createClass({

    mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

    statics: {
      tagName: 'UU5.Bricks.link' + uu5Name,
      classNames: {
        main: 'UU5_Bricks_link' + uu5Name
      },
      defaults: {
        content: defaultContent || uu5Name
      }
    },

    // Interface

    // Overriding Functions

    // Component Specific Helpers

    // Render
    render: function () {
      var mainProps = this.getMainPropsToPass();
      mainProps.href = href;

      return React.createElement(
        UU5.Bricks.a,
        mainProps,
        this.props.children || this.getDefault().content
      );
    }
  });
};

UU5.Bricks.linkUnicorn = UU5.Bricks.factory._createLinks("Unicorn", "http://unicorn.eu/", "Unicorn®");
UU5.Bricks.linkUnicornSystems = UU5.Bricks.factory._createLinks("Unicorn", "http://unicornsystems.eu/", "Unicorn\u00a0Systems®");
UU5.Bricks.linkUnicornUniverse = UU5.Bricks.factory._createLinks("Unicorn", "https://unicornuniverse.eu/", "Unicorn\u00a0Universe®");
UU5.Bricks.linkUnicornCollege = UU5.Bricks.factory._createLinks("UnicornCollege", "https://unicorncollege.cz/", "Unicorn\u00a0College®");
UU5.Bricks.linkUAF = UU5.Bricks.factory._createLinks("UAF", "https://uaf.unicorn.eu/", "UAF®");
UU5.Bricks.linkUU5 = UU5.Bricks.factory._createLinks("UU5", "https://uu5.unicorn.eu/", "UU5®");
UU5.Bricks.linkPlus4U = UU5.Bricks.factory._createLinks("Plus4U", "https://plus4u.net/", "Plus4U®");
UU5.Bricks.linkBootstrap = UU5.Bricks.factory._createLinks("Bootstrap", "http://getbootstrap.com/", "Bootstrap®");
UU5.Bricks.linkW3Schools = UU5.Bricks.factory._createLinks("w3schools", "http://www.w3schools.com/", "w3schools®");
UU5.Bricks.linkHTML5 = UU5.Bricks.factory._createLinks("HTML5", "http://www.w3schools.com/html/default.asp", "HTML5®");
UU5.Bricks.linkCSS = UU5.Bricks.factory._createLinks("CSS", "http://www.w3schools.com/css/default.asp", "CSS®");
UU5.Bricks.linkJavaScript = UU5.Bricks.factory._createLinks("JavaScript", "http://www.w3schools.com/js/default.asp", "JavaScript®");
UU5.Bricks.linkJQuery = UU5.Bricks.factory._createLinks("JQuery", "https://jquery.com/", "JQuery®");
UU5.Bricks.linkReact = UU5.Bricks.factory._createLinks("React", "https://facebook.github.io/react/", "React®");

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks._createList = function (tag) {
  return React.createClass({

    mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin],

    statics: {
      tagName: 'UU5.Bricks.' + tag,
      classNames: {
        main: 'UU5_Bricks_' + tag
      },
      defaults: {
        childTagName: 'UU5.Bricks.li'
      }
    },

    // Interface

    // Overriding Functions
    shouldChildRender_: function (child) {
      return this.getChildTagName(child) === this.getDefault().childTagName;
    },

    // Component Specific Helpers

    // Render
    render: function () {
      return React.createElement(tag, this.buildMainAttrs(), this.getHeaderChild(), this.getChildren(), this.getFooterChild(), this.getDisabledCover());
    }
  });
};

['ul', 'ol'].forEach(function (tag) {
  UU5.Bricks[tag] = UU5.Bricks._createList(tag);
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * UU5.Bricks.loading
 */
UU5.Bricks.loading = React.createClass({
  displayName: 'loading',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.loading',
    classNames: {
      main: 'UU5_Bricks_loading UU5_Common-padding-5'
    },
    defaults: {
      content: 'Loading'
    },
    warnings: {
      textDeprecated: 'Property text (%s) is deprecated since v03.10. Use property content instead of this.'
    }
  },

  propTypes: {
    /**
     * @deprecated since version 03.10. Replaced by property content.
     */
    text: React.PropTypes.string
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      /**
       * @deprecated since version 03.10
       */
      text: null
    };
  },

  componentWillMount: function () {
    if (this.props.text) {
      this.showWarning('textDeprecated', this.props.text);
    }
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  // Render
  render: function () {
    return React.createElement(
      'div',
      this.buildMainAttrs(),
      this.getChildren() || this.props.text || this.getDefault().content,
      this.getDisabledCover()
    );
  }
});

'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.lsi = React.createClass({
  displayName: 'lsi',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.lsiMixin],

  statics: {
    tagName: 'UU5.Bricks.lsi',
    classNames: {
      main: 'UU5_Bricks_lsi'
    }
  },

  propTypes: {
    lsi: React.PropTypes.object
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      lsi: null
    };
  },

  // Interface

  // Overriding Functions

  // Render
  render: function () {
    return React.createElement(UU5.Bricks.span, _extends({}, this.getMainPropsToPass(), { content: this.getLSIItem(this.props.lsi) }));
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * UU5.Bricks.modal
 */
UU5.Bricks.modal = React.createClass({
  displayName: 'modal',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin],

  statics: {
    tagName: 'UU5.Bricks.modal',
    classNames: {
      main: 'UU5_Bricks_modal modal',
      dialog: 'UU5_Bricks_modal-dialog modal-dialog modal-',
      content: 'UU5_Bricks_modal-dialog modal-content'
    },
    defaults: {
      tagNames: {
        header: 'UU5.Bricks.modal._header',
        body: 'UU5.Bricks.modal._body',
        footer: 'UU5.Bricks.modal._footer'
      },
      header: 'noHeader',
      body: 'noBody',
      animationDuration: 150 // ms
    }
  },

  propTypes: {
    size: React.PropTypes.oneOf(['sm', 'md', 'lg']),
    shown: React.PropTypes.bool,
    sticky: React.PropTypes.bool,
    scrollableBackground: React.PropTypes.bool,
    onClose: React.PropTypes.func,
    onBlur: React.PropTypes.func
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      size: 'md',
      shown: false,
      sticky: false,
      scrollableBackground: false,
      onClose: null,
      onBlur: null
    };
  },

  componentWillMount: function () {
    !this.props.shown && this.hide();
  },

  componentDidMount: function () {
    if (!this.props.sticky) {
      $(document).on('keyup', function (e) {
        e.which === 27 && !this.isHidden() && this._blur(e);
      }.bind(this));
    }
  },

  componentWillReceiveProps: function (nextProps) {
    if (nextProps.shown !== undefined) {
      this.setState(function (state) {
        var newState = {};

        if (nextProps.shown && state.hidden) {
          newState.hidden = false;
        } else if (!nextProps.shown && !state.hidden) {
          newState.hidden = true;
        }

        return newState;
      });
    }
  },

  // Interface

  open: function (callback) {
    this._stopScroll();
    this.show(callback);
    return this;
  },

  close: function (callback) {
    this._startScroll();
    this.hide(callback);
    return this;
  },

  toggle: function (callback) {
    var modal = this;
    this.setState(function (state) {
      state.hidden ? modal._stopScroll() : modal._startScroll();
      return { hidden: !state.hidden };
    }, callback);
    return this;
  },

  getModalHeader: function () {
    return this.getRenderedChildByTagName(this.getDefault().tagNames.header);
  },

  getModalBody: function () {
    return this.getRenderedChildByTagName(this.getDefault().tagNames.body);
  },

  getModalFooter: function () {
    return this.getRenderedChildByTagName(this.getDefault().tagNames.footer);
  },

  isSticky: function () {
    return this.props.sticky;
  },

  // Overriding Functions
  buildHeaderChild_: function (headerTypes) {
    var headerType = this.getHeaderType(headerTypes);

    var headerChild;
    if (headerType === 'contentOfStandardHeader') {
      headerChild = this.buildChild(this.getDefault().tagNames.header, { content: headerTypes.header });
      headerChild = this.cloneChild(headerChild, this.expandHeaderProps(headerChild));
    }

    return headerChild;
  },

  expandHeaderProps_: function (headerChild) {
    var extendedHeaderProps = this._extendPartProps(headerChild.props, 'header');
    if (extendedHeaderProps) {
      extendedHeaderProps._sticky = this.props.sticky;
      extendedHeaderProps._onClose = this._onCloseHandler;
    }
    return extendedHeaderProps;
  },

  buildFooterChild_: function (footerTypes) {
    var footerType = this.getFooterType(footerTypes);

    var footerChild;
    if (footerType === 'contentOfStandardFooter') {
      footerChild = this.buildChild(this.getDefault().tagNames.footer, { content: footerTypes.footer });
      footerChild = this.cloneChild(footerChild, this.expandFooterProps(footerChild));
    }

    return footerChild;
  },

  expandFooterProps_: function (footerChild) {
    return this._extendPartProps(footerChild.props, 'footer');
  },

  // Component Specific Helpers

  _onBlurHandler: function (event) {
    event.target.id === this.getId() && this._blur(event);
    return this;
  },

  _onCloseHandler: function (e) {
    if (typeof this.props.onClose === 'function') {
      this.props.onClose(this, e);
    } else {
      this.close();
    }
    return this;
  },

  _blur: function (e) {
    if (typeof this.props.onBlur === 'function') {
      this.props.onBlur(this, e);
    } else {
      this.close();
    }
    return this;
  },

  _getScrollbarWidth: function () {
    var width = 0;

    // if scroll bar is visible
    if ($(document).height() > $(window).height()) {
      var div = document.createElement("div");
      div.style.overflow = "scroll";
      div.style.visibility = "hidden";
      div.style.position = 'absolute';
      div.style.width = '100px';
      div.style.height = '100px';

      // temporarily creates a div into DOM
      document.body.appendChild(div);
      try {
        width = div.offsetWidth - div.clientWidth;
      } finally {
        document.body.removeChild(div);
      }
    }

    return width;
  },

  _startScroll: function () {
    var modal = this;
    // TODO: wrong, but not found better solution
    setTimeout(function () {
      !modal.props.scrollableBackground && $('body').removeClass('modal-open');
      $('body').css('padding-right', '');
    }, this.getDefault().animationDuration);
  },

  _stopScroll: function () {
    // TODO: wrong, but not found better solution
    !this.props.scrollableBackground && $('body').addClass('modal-open');
    var paddingRight = this._getScrollbarWidth();
    paddingRight && $('body').css('padding-right', paddingRight);
  },

  _getMainAttrs: function () {
    var mainAttrs = this.buildMainAttrs();

    // id because of checking backdrop on click in _onBlurHandler function
    mainAttrs.id = this.getId();

    !this.props.sticky && (mainAttrs.onClick = this._onBlurHandler);

    var sec = this.getDefault().animationDuration / 1000 + 's';
    mainAttrs.style = mainAttrs.style || {};
    mainAttrs.style.WebkitTransitionDuration = sec;
    mainAttrs.style.MozTransitionDuration = sec;
    mainAttrs.style.OTransitionDuration = sec;
    mainAttrs.style.transitionDuration = sec;

    return mainAttrs;
  },

  _extendPartProps: function (partProps, part) {
    var newProps = {};

    // default values is used if child is set as react element so null or undefined will not set!!!
    for (var key in partProps) {
      partProps[key] !== null && partProps[key] !== undefined && (newProps[key] = partProps[key]);
    }

    newProps.key = newProps.id;

    return newProps;
  },

  _extendBodyProps: function (bodyProps) {
    var id = this.getId() + '-body';

    var newProps = {
      id: id
    };

    // default values is used if child is set as react element so null or undefined will not set!!!
    for (var key in bodyProps) {
      bodyProps[key] !== null && bodyProps[key] !== undefined && (newProps[key] = bodyProps[key]);
    }

    return $.extend(newProps, { key: newProps.id });
  },

  // Render
  _buildChildren: function () {
    var header = this.getHeader();
    var bodyContent = this.getContent();

    var headerChild;
    var footerChild = this.buildFooterChild({ footer: this.getFooter() });

    if (!this.props.children && !bodyContent && (!header && !footerChild || header || footerChild)) {
      header = header || this.getDefault().header;
      bodyContent = bodyContent || this.getDefault().body;
    }

    header && (headerChild = this.buildHeaderChild({ header: header }));

    var bodyProps = this._extendBodyProps({ content: bodyContent });

    var bodyChild = this.buildChildren({
      children: React.createElement(this.checkTag(this.getDefault().tagNames.body), bodyProps, this.props.children)
    });

    return [headerChild, bodyChild, footerChild];
  },

  render: function () {
    return React.createElement(
      'div',
      this._getMainAttrs(),
      React.createElement(
        'div',
        { className: this.getClassName().dialog + this.props.size },
        React.createElement(
          'div',
          { className: this.getClassName().content },
          this._buildChildren()
        )
      ),
      this.getDisabledCover()
    );
  }
});

/**
 * UU5.Bricks.modal.header
 */
UU5.Bricks.modal._header = React.createClass({
  displayName: '_header',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.modal._header',
    classNames: {
      main: 'UU5_Bricks_modal_header modal-header',
      close: 'UU5_Bricks_modal_header-close close'
    },
    defaults: {
      parentTagName: 'UU5.Bricks.modal'
    }
  },

  propTypes: {
    _sticky: React.PropTypes.bool,
    _onClose: React.PropTypes.func
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      _sticky: false
    };
  },

  componentWillMount: function () {
    this.checkParentTagName(this.getDefault().parentTagName);
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  _onCloseHandler: function (e) {
    typeof this.props._onClose === 'function' && this.props._onClose(e);
  },

  _getCloseButton: function () {
    var button;

    if (!this.props._sticky) {
      button = React.createElement(UU5.Bricks.button, { className: this.getClassName().close, onClick: this._onCloseHandler, label: '×' });
    }

    return button;
  },

  _buildChildren: function () {
    var children;

    if (typeof this.getContent() === 'string') {
      children = React.createElement(
        'h4',
        { className: 'modal-title' },
        this.getContent()
      );
    } else {
      children = this.getChildren();
    }

    return children;
  },

  // Render
  render: function () {
    var mainAttrs = this.buildMainAttrs();

    return React.createElement(
      'div',
      mainAttrs,
      this._getCloseButton(),
      this._buildChildren(),
      this.getDisabledCover()
    );
  }
});

/**
 * UU5.Bricks.modal.body
 */
UU5.Bricks.modal._body = React.createClass({
  displayName: '_body',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.modal._body',
    classNames: {
      main: 'UU5_Bricks_modal_body modal-body'
    },
    defaults: {
      parentTagName: 'UU5.Bricks.modal'
    }
  },

  componentWillMount: function () {
    this.checkParentTagName(this.getDefault().parentTagName);
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  // Render
  render: function () {
    return React.createElement(
      'div',
      this.buildMainAttrs(),
      this.getChildren(),
      this.getDisabledCover()
    );
  }
});

/**
 * UU5.Bricks.modal.footer
 */
UU5.Bricks.modal._footer = React.createClass({
  displayName: '_footer',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.modal._footer',
    classNames: {
      main: 'UU5_Bricks_modal_footer modal-footer'
    },
    defaults: {
      parentTagName: 'UU5.Bricks.modal'
    }
  },

  componentWillMount: function () {
    this.checkParentTagName(this.getDefault().parentTagName);
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  // Render
  render: function () {
    return React.createElement(
      'div',
      this.buildMainAttrs(),
      this.getChildren(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * UU5.Bricks.navBar
 */
UU5.Bricks.navBar = React.createClass({
  displayName: 'navBar',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.navBar',
    classNames: {
      main: 'UU5_Bricks_navBar navbar',
      navCover: 'UU5_Bricks_navBar-navCover',
      navContainer: 'UU5_Bricks_navBar-navContainer',
      opened: 'UU5_Bricks_navBar-opened'
    },
    defaults: {
      tagNames: {
        header: 'UU5.Bricks.navBar.header',
        nav: 'UU5.Bricks.navBar.nav'
      }
    },
    warnings: {
      cannotOpenIfAlwaysOpened: 'Cannot open navBar if alwaysOpened is set to true.',
      cannotCloseIfAlwaysOpened: 'Cannot close navBar if alwaysOpened is set to true.',
      cannotToggleIfAlwaysOpened: 'Cannot toggle navBar if alwaysOpened is set to true.'
    }
  },

  propTypes: {
    fixed: React.PropTypes.oneOf(['top', 'bottom']),
    smoothScroll: React.PropTypes.number,
    offset: React.PropTypes.number,
    inverse: React.PropTypes.bool,
    opened: React.PropTypes.bool,
    alwaysOpened: React.PropTypes.bool,
    glyphiconOpened: React.PropTypes.string,
    glyphiconClosed: React.PropTypes.string,
    onOpen: React.PropTypes.func,
    onClose: React.PropTypes.func
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      fixed: null,
      smoothScroll: null,
      offset: null,
      inverse: false,
      opened: false,
      alwaysOpened: false,
      glyphiconOpened: 'uu-glyphicon-hamburger',
      glyphiconClosed: 'uu-glyphicon-hamburger',
      onOpen: null,
      onClose: null
    };
  },

  getInitialState: function () {
    return {
      navHeight: 0,
      offset: this.props.offset
    };
  },

  componentWillReceiveProps: function (nextProps) {
    var newState = {};
    if (nextProps.alwaysOpened || nextProps.opened !== undefined && nextProps.opened !== this.isOpened()) {
      newState.navHeight = nextProps.alwaysOpened || nextProps.opened ? this._countNavHeight() : 0;
    }

    if (nextProps.offset !== undefined && nextProps.offset !== this.getOffset()) {
      newState.offset = nextProps.offset;
    }

    this.setState(newState);
  },

  componentDidMount: function () {
    var newState = {};

    if (this.props.fixed === 'top' && !this.state.offset) {
      newState.offset = this.getOuterHeight();
    }

    if (this.props.alwaysOpened || this.props.opened) {
      newState.navHeight = this._countNavHeight();
      newState.offset && (newState.offset += newState.navHeight);
    }

    Object.keys(newState).length && this.setState(newState);
  },

  // Interface
  isNavBar: function () {
    return true;
  },

  isOpened: function () {
    return !!this._getNavHeight();
  },

  open: function (callback) {
    if (!this.props.alwaysOpened) {
      this.setState({ navHeight: this._countNavHeight() }, callback);
    } else if (typeof callback === 'function') {
      this.showWarning('cannotOpenIfAlwaysOpened', null, { navBarProps: this.props });
    }
    return this;
  },

  close: function (callback) {
    if (!this.props.alwaysOpened) {
      this.setState({ navHeight: 0 }, callback);
    } else if (typeof callback === 'function') {
      this.showWarning('cannotCloseIfAlwaysOpened', null, { navBarProps: this.props });
    }
    return this;
  },

  toggle: function (callback) {
    if (!this.props.alwaysOpened) {
      var navbar = this;
      this.setState(function (state) {
        var newNavHeight = state.navHeight ? 0 : navbar._countNavHeight();
        return { navHeight: newNavHeight };
      }, callback);
    } else if (typeof callback === 'function') {
      this.showWarning('cannotToggleIfAlwaysOpened', null, { navBarProps: this.props });
    }
    return this;
  },

  getOffset: function () {
    return this.state.offset;
  },

  // Overriding Functions

  // Component Specific Helpers

  _getMainAttrs: function () {
    var mainAttrs = this.buildMainAttrs();

    mainAttrs.className += this.props.inverse ? ' navbar-inverse' : ' navbar-default';
    this.props.fixed && (mainAttrs.className += ' navbar-fixed-' + this.props.fixed);
    this.isOpened() && (mainAttrs.className += ' ' + this.getClassName().opened);

    return mainAttrs;
  },

  _prepareHeader: function (headerProps) {
    var headerPropsToPass = {
      id: this.getId() + '-header',
      parent: this
    };

    var newHeaderProps = headerProps ? $.extend(true, headerPropsToPass, headerProps) : headerPropsToPass;
    newHeaderProps._glyphicon = this.isOpened() ? this.props.glyphiconOpened : this.props.glyphiconClosed;
    newHeaderProps._isHamburger = !this.props.alwaysOpened;
    newHeaderProps._onOpen = typeof this.props.onOpen === 'function' ? this.props.onOpen : null;
    newHeaderProps._onClose = typeof this.props.onClose === 'function' ? this.props.onClose : null;

    return React.createElement(UU5.Bricks.navBar.header, newHeaderProps);
  },

  _prepareNavs: function (navsProps) {
    return navsProps.map(function (props, i) {
      props.key = props.key || 'nav-' + i;
      return React.createElement(UU5.Bricks.navBar.nav, props);
    });
  },

  _prepareChildren: function (children) {
    var navBar = this;
    var headerProps = null;

    var newNavProps = { smoothScroll: this.props.smoothScroll, offset: this.getOffset(), parent: this };
    var navsProps = [];

    children && children.map(function (child) {
      var childTagName = navBar.getChildTagName(child);

      switch (childTagName) {
        case navBar.getDefault().tagNames.header:
          headerProps = child.props;
          break;
        case navBar.getDefault().tagNames.nav:
          // TODO: max 3?
          var newProps = {};
          // default values is used if child is set as react element so null or undefined will not set!!!
          for (var key in child.props) {
            child.props[key] !== null && child.props[key] !== undefined && (newProps[key] = child.props[key]);
          }
          navsProps.push($.extend({}, newNavProps, newProps));
          break;
      }
    });

    navsProps.length === 0 && navsProps.push(newNavProps);

    return {
      header: this._prepareHeader(headerProps),
      navs: this._prepareNavs(navsProps)
    };
  },

  _getNavContainerId: function () {
    return this.getId() + '-navContainer';
  },

  _getNavHeight: function () {
    return this.state.navHeight;
  },

  _countNavHeight: function () {
    return $('#' + this._getNavContainerId()).outerHeight(true);
  },

  _getNavCoverProps: function () {
    return {
      className: this.getClassName().navCover,
      style: {
        height: this._getNavHeight()
      }
    };
  },

  _getNavContainerProps: function () {
    return {
      id: this._getNavContainerId(),
      className: this.getClassName().navContainer
    };
  },

  render: function () {
    var childrenObject = this._prepareChildren(this.getChildren());

    return React.createElement(
      'nav',
      this._getMainAttrs(),
      React.createElement(
        'div',
        { className: 'container-fluid' },
        childrenObject.header,
        React.createElement(
          'div',
          this._getNavCoverProps(),
          React.createElement(
            'div',
            this._getNavContainerProps(),
            childrenObject.navs
          )
        )
      ),
      this.getDisabledCover()
    );
  }
});

UU5.Bricks.navBar.header = React.createClass({
  displayName: 'header',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.navBar.header',
    classNames: {
      main: 'UU5_Bricks_navBar_header navbar-header',
      hamburger: 'UU5_Bricks_navBar_header-hamburger navbar-toggle',
      brand: 'UU5_Bricks_navBar_header-brand navbar-brand'
    },
    defaults: {
      parentTagName: 'UU5.Bricks.navBar'
    }
  },

  propTypes: {
    _glyphicon: React.PropTypes.string,
    _isHamburger: React.PropTypes.bool,
    _onOpen: React.PropTypes.func,
    _onClose: React.PropTypes.func
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      _glyphicon: 'uu-glyphicon-hamburger',
      _isHamburger: true,
      _onOpen: null,
      _onClose: null
    };
  },

  componentWillMount: function () {
    this.checkParentTagName(this.getDefault().parentTagName);
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _onClickHamburger: function () {
    var header = this;
    this.getParent() && this.getParent().toggle(function () {
      if (this.isOpened()) {
        header.props._onOpen && header.props._onOpen(this);
      } else {
        header.props._onClose && header.props._onClose(this);
      }
    });
  },

  _getHamburgerProps: function () {
    return {
      className: this.getClassName().hamburger,
      onClick: this._onClickHamburger
    };
  },

  // Render
  render: function () {
    var hamburger = null;
    if (this.props._isHamburger) {
      hamburger = React.createElement(
        UU5.Bricks.button,
        this._getHamburgerProps(),
        React.createElement(UU5.Bricks.glyphicon, { glyphicon: this.props._glyphicon })
      );
    }

    var children = this.getChildren();
    if (children) {
      children = React.createElement(
        'span',
        { className: this.getClassName().brand },
        this.getChildren()
      );
    }

    return React.createElement(
      'div',
      this.buildMainAttrs(),
      hamburger,
      children,
      this.getDisabledCover()
    );
  }
});

UU5.Bricks.navBar.nav = React.createClass({
  displayName: 'nav',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.navBar.nav',
    classNames: {
      main: 'UU5_Bricks_navBar_nav nav navbar-nav',
      align: 'UU5_Bricks_navBar_nav-align'
    },
    defaults: {
      childTagName: 'UU5.Bricks.navBar.nav.item',
      parentTagName: 'UU5.Bricks.navBar'
    }
  },

  propTypes: {
    align: React.PropTypes.oneOf(['left', 'right']),
    smoothScroll: React.PropTypes.number,
    offset: React.PropTypes.number
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      align: 'left',
      smoothScroll: null,
      offset: null
    };
  },

  componentWillMount: function () {
    this.checkParentTagName(this.getDefault().parentTagName);
  },

  // Interface
  isNav: function () {
    return true;
  },

  // Overriding Functions

  shouldChildRender_: function (child) {
    return this.getChildTagName(child) === this.getDefault().childTagName;
  },

  expandChildProps_: function (child, i) {
    var newChildProps = child.props;
    newChildProps = $.extend(true, {}, newChildProps);

    newChildProps.smoothScroll = newChildProps.smoothScroll === undefined || newChildProps.smoothScroll === null ? this.props.smoothScroll : newChildProps.smoothScroll;
    newChildProps.offset = newChildProps.offset === undefined || newChildProps.offset === null ? this.props.offset : newChildProps.offset;

    return newChildProps;
  },

  // Component Specific Helpers

  // Render
  render: function () {
    var mainAttrs = this.buildMainAttrs();
    mainAttrs.className += ' ' + this.getClassName().align + '-' + this.props.align;
    mainAttrs.className += this.props.align === 'right' ? ' navbar-right' : '';

    return React.createElement(
      'ul',
      mainAttrs,
      this.getChildren(),
      this.getDisabledCover()
    );
  }
});

UU5.Bricks.navBar.nav.item = React.createClass({
  displayName: 'item',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.navBar.nav.item',
    classNames: {
      main: 'UU5_Bricks_navBar_nav_item'
    },
    defaults: {
      parentTagName: 'UU5.Bricks.navBar.nav'
    }
  },

  propTypes: {
    href: React.PropTypes.string,
    onClick: React.PropTypes.func,
    smoothScroll: React.PropTypes.number,
    offset: React.PropTypes.number
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      href: '#',
      onClick: null,
      smoothScroll: null,
      offset: null
    };
  },

  componentWillMount: function () {
    this.checkParentTagName(this.getDefault().parentTagName);
  },

  // Interface

  // Overriding functions

  // Component Specific Helpers
  _onClickHandler: function () {
    var navBar = this.getParent().getParent();
    navBar.close();
  },

  // Render
  render: function () {
    var mainAttrs = this.buildMainAttrs();
    mainAttrs.onClick = this._onClickHandler;

    var children = this.getChildren();
    var firstChild = children instanceof Array ? children[0] : children;
    var child;

    if (firstChild.type && firstChild.type.tagName === 'UU5.Bricks.dropdown') {
      console.warn(this.getTagName(), this, 'notImplementedError', 'dropdownItem');
      child = children;
    } else {
      child = React.createElement(
        UU5.Bricks.link,
        {
          href: this.props.href,
          onClick: this.props.onClick,
          smoothScroll: this.props.smoothScroll,
          offset: this.props.offset,
          parent: this
        },
        children
      );
    }

    return React.createElement(
      'li',
      mainAttrs,
      child,
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.newspaper = React.createClass({
  displayName: 'newspaper',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin],

  statics: {
    tagName: 'UU5.Bricks.newspaper',
    classNames: {
      main: 'UU5_Bricks_newspaper',
      columns: 'UU5_Common-newspaper-layout-'
    }
  },

  propTypes: {
    columnsCount: React.PropTypes.oneOf([1, 2, 3, 4, 5, 6])
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      columnsCount: 2
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _getMainProps: function () {
    var mainProps = this.getMainPropsToPass();
    mainProps.className += ' ' + this.getClassName().columns + this.props.columnsCount;
    return mainProps;
  },

  // Render
  render: function () {
    return React.createElement(
      UU5.Bricks.section,
      this._getMainProps(),
      this.props.children
    );
  }
});

'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.outline = React.createClass({
  displayName: 'outline',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin],

  statics: {
    tagName: 'UU5.Bricks.outline',
    classNames: {
      main: 'UU5_Bricks_outline'
    },
    defaults: {
      propsGlyphicon: 'glyphicon-info-sign'
    }
  },

  propTypes: {
    element: React.PropTypes.object
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      element: null
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _openModal: function (tag, props) {
    var propsString = JSON.stringify(props, null, 2);
    this.modal.open(tag, propsString);
    return this;
  },

  _buildOutlineItem: function (tag, props, isContent) {
    var newProps = {};
    var items = null;

    if (props) {
      for (var prop in props) {
        var value = props[prop];
        if (prop === 'parent' && value) {
          value = value.getTagName();
        } else if (typeof value === 'function') {
          value = 'function()';
        }
        newProps[prop] = value;
      }

      if (isContent && typeof props.children !== 'string') {
        items = this._buildOutlineItems(props.children);
        delete newProps.children;
      }
    }

    var link = null;
    if (Object.keys(newProps).length) {
      link = React.createElement(
        UU5.Bricks.link,
        { onClick: this._openModal.bind(this, tag, newProps) },
        React.createElement(UU5.Bricks.glyphicon, { glyphicon: this.getDefault().propsGlyphicon })
      );
    }

    var newItemProps = {
      label: React.createElement(
        UU5.Bricks.span,
        { parent: this },
        tag,
        ' ',
        link
      )
    };

    items && (newItemProps.items = items);

    return newItemProps;
  },

  _buildOutlineItems: function (children) {
    var content = this;
    var items = null;

    children && children.forEach(function (child) {
      items = items || [];
      items.push(content._buildOutlineItem(content.getChildTagName(child), child.props, !!child.type['UU5_Common_contentMixin']));
    });

    return items;
  },

  _refModal: function (modal) {
    this.modal = modal;
    return this;
  },

  // Render

  render: function () {
    var children = this.props.element.getStandardChildren();
    var item = this._buildOutlineItem(this.props.element.getTagName(), $.extend({}, this.props.element.props, { children: children }), true);

    return React.createElement(
      'div',
      this.buildMainAttrs(),
      React.createElement(UU5.Bricks.tree, { items: [item] }),
      React.createElement(UU5.Bricks.outline._modal, { ref: this._refModal }),
      this.getDisabledCover()
    );
  }
});

UU5.Bricks.outline._modal = React.createClass({
  displayName: '_modal',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin],

  statics: {
    tagName: 'UU5.Bricks.outline._modal',
    classNames: {
      main: 'UU5_Bricks_outline_modal'
    }
  },

  getInitialState: function () {
    return {
      tag: null,
      props: null
    };
  },

  // Interface

  open: function (tag, props, setStateCallback) {
    this.setState({ shown: true, tag: tag, props: props }, setStateCallback);
    return this;
  },

  close: function (setStateCallback) {
    this.setState({ shown: false, tag: null, props: null }, setStateCallback);
    return this;
  },

  // Overriding Functions

  // Component Specific Helpers

  // Render

  render: function () {
    return React.createElement(
      UU5.Bricks.modal,
      _extends({}, this.getMainPropsToPass(), { shown: this.state.shown, header: this.state.tag }),
      React.createElement(
        'pre',
        null,
        this.state.props
      )
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.pager = React.createClass({
  displayName: 'pager',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Layout.rowCollectionMixin],

  statics: {
    tagName: 'UU5.Bricks.pager',
    classNames: {
      main: 'UU5_Bricks_pager',
      leftLink: 'UU5_Bricks_pager-left',
      rightLink: 'UU5_Bricks_pager-right',
      upLink: 'UU5_Bricks_pager-up',
      downLink: 'UU5_Bricks_pager-down',
      text: 'UU5_Bricks_pager-text',
      glyphicon: 'UU5_Bricks_pager-glyphicon',
      size: 'UU5_Common-',
      color: 'text-',
      bg: 'bg-'
    },
    defaults: {
      leftLink: 'uu-glyphicon-arrow-left',
      rightLink: 'uu-glyphicon-arrow-right',
      upLink: 'uu-glyphicon-arrow-up',
      downLink: 'uu-glyphicon-arrow-down'
    }
  },

  propTypes: {
    leftLink: React.PropTypes.shape({
      text: React.PropTypes.string,
      href: React.PropTypes.string,
      glyphicon: React.PropTypes.string
    }),
    rightLink: React.PropTypes.shape({
      text: React.PropTypes.string,
      href: React.PropTypes.string,
      glyphicon: React.PropTypes.string
    }),
    upLink: React.PropTypes.shape({
      text: React.PropTypes.string,
      href: React.PropTypes.string,
      glyphicon: React.PropTypes.string
    }),
    downLink: React.PropTypes.shape({
      text: React.PropTypes.string,
      href: React.PropTypes.string,
      glyphicon: React.PropTypes.string
    }),
    colorSchema: React.PropTypes.oneOf(UU5.Common.environment.colorSchema),
    size: React.PropTypes.oneOf(['xs', 'sm', 'md', 'lg']),
    isBackground: React.PropTypes.bool
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      leftLink: null,
      rightLink: null,
      upLink: null,
      downLink: null,
      colorSchema: null,
      size: 'md',
      isBackground: false
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  _getColumn: function (key) {
    var link = null;

    if (this.props[key]) {
      var linkProps = $.extend({}, this.props[key]);
      var text = linkProps.text;
      delete linkProps.text;

      var glyphicon = React.createElement(UU5.Bricks.glyphicon, {
        glyphicon: linkProps.glyphicon || this.getDefault(key),
        className: this.getClassName().glyphicon,
        title: text
      });

      var leftGlyphicon = null;
      var rightGlyphicon = null;

      if (key === 'leftLink' || key === 'upLink') {
        leftGlyphicon = glyphicon;
      } else {
        rightGlyphicon = glyphicon;
      }

      link = React.createElement(
        UU5.Bricks.link,
        linkProps,
        leftGlyphicon,
        ' ',
        React.createElement(
          'span',
          { className: this.getClassName().text },
          text
        ),
        ' ',
        rightGlyphicon
      );
    }

    return React.createElement(
      UU5.Layout.column,
      { className: this.getClassName(key), bsColWidth: 'xs-3' },
      React.createElement(
        UU5.Layout.wrapper,
        null,
        React.createElement(
          UU5.Layout.flc,
          null,
          link
        )
      )
    );
  },

  // Render

  render: function () {
    var mainProps = this.getMainPropsToPass();
    mainProps.className += ' ' + this.getClassName().size + this.props.size;

    var classPrefix = this.props.isBackground ? this.getClassName().bg : this.getClassName().color;
    this.props.colorSchema && (mainProps.className += ' ' + classPrefix + this.props.colorSchema);

    return React.createElement(
      UU5.Layout.row,
      mainProps,
      this._getColumn('leftLink'),
      this._getColumn('upLink'),
      this._getColumn('downLink'),
      this._getColumn('rightLink')
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.pagination = React.createClass({
  displayName: 'pagination',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin],

  statics: {
    tagName: 'UU5.Bricks.pagination',
    classNames: {
      main: 'UU5_Bricks_pagination pagination',
      item: 'UU5_Bricks_pagination-item',
      link: 'UU5_Bricks_pagination-link',
      nav: 'UU5_Bricks_pagination-nav',
      size: 'pagination-',
      active: 'active',
      disabledItem: 'disabled',
      color: 'text-',
      bg: 'bg-'
    }
  },

  propTypes: {
    items: React.PropTypes.array,
    activeIndex: React.PropTypes.number,
    range: React.PropTypes.number,
    prevGlyphicon: React.PropTypes.string,
    prevLabel: React.PropTypes.node,
    nextGlyphicon: React.PropTypes.string,
    nextLabel: React.PropTypes.node,
    firstGlyphicon: React.PropTypes.string,
    firstLabel: React.PropTypes.node,
    lastGlyphicon: React.PropTypes.string,
    lastLabel: React.PropTypes.node,
    colorSchema: React.PropTypes.oneOf(UU5.Common.environment.colorSchema),
    size: React.PropTypes.oneOf(['sm', 'md', 'lg']),
    onChange: React.PropTypes.func,
    onChanged: React.PropTypes.func
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      items: [1, 2, 3, 4, 5],
      activeIndex: 0,
      range: 5,
      prevGlyphicon: 'uu-glyphicon-arrow-left',
      prevLabel: null,
      nextGlyphicon: 'uu-glyphicon-arrow-right',
      nextLabel: null,
      firstGlyphicon: null,
      firstLabel: null,
      lastGlyphicon: null,
      lastLabel: null,
      colorSchema: 'primary',
      size: 'md',
      onChange: null,
      onChanged: null
    };
  },

  getInitialState: function () {
    return {
      activeIndex: this.props.activeIndex
    };
  },

  // Interface
  getItemsLength: function () {
    return this.props.items.length;
  },

  getActiveIndex: function () {
    return this.state.activeIndex;
  },

  setActiveIndex: function (activeIndex, setStateCallback) {
    this.setState({ activeIndex: activeIndex }, setStateCallback);
    return this;
  },

  increaseActiveIndex: function (setStateCallback) {
    var pagination = this;
    this.setState(function (state) {
      var newState = null;
      if (pagination.getItemsLength() - 1 > state.activeIndex) {
        newState = { activeIndex: state.activeIndex + 1 };
      }
      return newState;
    }, setStateCallback);
    return this;
  },

  decreaseActiveIndex: function (setStateCallback) {
    var pagination = this;
    this.setState(function (state) {
      var newState = null;
      if (0 < state.activeIndex) {
        newState = { activeIndex: state.activeIndex - 1 };
      }
      return newState;
    }, setStateCallback);
    return this;
  },

  // Overriding Functions

  // Component Specific Helpers
  _getMainAttrs: function () {
    var mainAttrs = this.buildMainAttrs();

    mainAttrs.className += ' ' + this.getClassName().size + this.props.size;

    return mainAttrs;
  },

  _range: function (start, end, step) {
    step = step || 1;
    var rangeArray = [start];
    while (start + step <= end) {
      rangeArray.push(start += step);
    }
    return rangeArray;
  },

  _getRange: function () {
    var i = this.getActiveIndex();
    var start = 0;
    var end = this.getItemsLength() - 1;
    var range = this.props.range;
    var step = Math.floor(range / 2.0);

    if (i <= start + step) {
      end = range - 1 < end ? range - 1 : end;
    } else if (i >= end - step) {
      start = end - range + 1 > 0 ? end - range + 1 : start;
    } else {
      start = i - step;
      end = i + step;
    }

    return this._range(start, end);
  },

  _onChange: function (newActive, link, event) {
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(newActive, event);
    } else {
      event.preventDefault();
      var onChanged;
      if (typeof this.props.onChanged === 'function') {
        var pagination = this;
        onChanged = function () {
          pagination.props.onChanged(this.getActiveIndex(), newActive, event);
        };
      }

      if (newActive === "prev") {
        this.decreaseActiveIndex(onChanged);
      } else if (newActive === "next") {
        this.increaseActiveIndex(onChanged);
      } else {
        this.setActiveIndex(newActive, onChanged);
      }
    }
    return this;
  },

  _getItemValue: function (value) {
    var newValue = null;
    var label = this.props[value + 'Label'];
    var glyphicon = this.props[value + 'Glyphicon'];

    if (label) {
      // if array of nodes -> set keys
      newValue = Array.isArray(label) ? React.Children.toArray(label) : label;
    } else if (glyphicon) {
      newValue = React.createElement(UU5.Bricks.glyphicon, { glyphicon: glyphicon });
    }

    return newValue;
  },

  _createItem: function (i, value) {
    var liAttrs = { key: i, className: this.getClassName().item };
    var linkAttrs = { className: this.getClassName().link, parent: this };

    if (i === this.getActiveIndex()) {
      liAttrs.className += ' ' + this.getClassName().active;
      this.props.colorSchema && (liAttrs.className += ' ' + this.getClassName().bg + this.props.colorSchema);
      linkAttrs.href = '';
    } else {
      linkAttrs.onClick = this._onChange.bind(null, i);
    }

    return React.createElement(
      'li',
      liAttrs,
      React.createElement(
        UU5.Bricks.link,
        linkAttrs,
        value
      )
    );
  },

  _createNavItem: function (key, disabled, index) {
    var liAttrs = { key: key, className: this.getClassName().item + ' ' + this.getClassName().nav + ' ' + this.getClassName().nav + '-' + key };
    var linkAttrs = { className: this.getClassName().link, parent: this };

    if (disabled) {
      liAttrs.className += ' ' + this.getClassName().disabledItem;
      linkAttrs.href = '';
    } else {
      linkAttrs.onClick = this._onChange.bind(null, index === undefined ? key : index);
    }

    return React.createElement(
      'li',
      liAttrs,
      React.createElement(
        UU5.Bricks.link,
        linkAttrs,
        this._getItemValue(key)
      )
    );
  },

  _getItems: function () {
    var pagination = this;

    var range = this._getRange();

    var items = range.map(function (i) {
      return pagination._createItem(i, pagination.props.items[i]);
    });

    if (this.getItemsLength() > this.props.range) {
      var prevDisabled = this.getActiveIndex() === 0;
      var nextDisabled = this.getActiveIndex() === this.getItemsLength() - 1;

      items.unshift(this._createNavItem('prev', prevDisabled));
      items.push(this._createNavItem('next', nextDisabled));

      (this.props.firstGlyphicon || this.props.firstLabel) && items.unshift(this._createNavItem('first', prevDisabled, 0));
      (this.props.lastGlyphicon || this.props.lastLabel) && items.push(this._createNavItem('last', nextDisabled, this.getItemsLength() - 1));
    }

    return items;
  },

  // Render
  render: function () {
    var mainAttrs = this._getMainAttrs();
    this.props.colorSchema && (mainAttrs.className += ' ' + this.getClassName().color + this.props.colorSchema);

    return React.createElement(
      'ul',
      mainAttrs,
      this._getItems(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * ProgressBar component based on bootstrap standard.
 *
 * @requires UU5.Common.baseMixin
 * @requires UU5.Common.elementaryMixin
 */
UU5.Bricks.progressBar = React.createClass({
  displayName: 'progressBar',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.progressBar',
    classNames: {
      main: 'UU5_Bricks_progressBar'
    },
    defaults: {
      childTagName: 'UU5.Bricks.progressBar.item'
    },
    warnings: {
      increaseImpossible: 'Progress Bar is full. Cannot increase about %d.',
      decreaseImpossible: 'Progress Bar is empty. Cannot decrease about %d.'
    }
  },

  propTypes: {
    colorSchema: React.PropTypes.oneOf(['primary', 'success', 'info', 'warning', 'danger']),
    progress: React.PropTypes.number,
    label: React.PropTypes.string,
    hiddenProgressLabel: React.PropTypes.bool,
    striped: React.PropTypes.bool,
    animated: React.PropTypes.bool
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      colorSchema: 'primary',
      progress: 0,
      label: null,
      hiddenProgressLabel: false,
      striped: false,
      animated: false
    };
  },

  // Interface
  isPossibleChangeProgress: function (progress) {
    var count = 0;
    this.eachRenderedChild(function (progressBar) {
      count += progressBar.getProgress();
    });
    return count + progress >= 0 && count + progress <= 100;
  },

  isPossibleIncrease: function (increasedValue) {
    return this.isPossibleChangeProgress(increasedValue);
  },

  isPossibleDecrease: function (decreasedValue) {
    return this.isPossibleChangeProgress(-decreasedValue);
  },

  getProgress: function (name) {
    return this._getProgressBarItem(name).getProgress();
  },

  setProgress: function (progress, name, callback) {
    return this._getProgressBarItem(name).setProgress(progress, callback);
  },

  increase: function (increasedValue, name, callback) {
    if (this.isPossibleIncrease(increasedValue)) {
      this._getProgressBarItem(name).increase(increasedValue, callback);
    } else {
      this.showWarning('increaseImpossible', increasedValue);
    }
    return this;
  },

  decrease: function (decreasedValue, name, callback) {
    if (this.isPossibleDecrease(decreasedValue)) {
      this._getProgressBarItem(name).decrease(decreasedValue, callback);
    } else {
      this.showWarning('decreaseImpossible', decreasedValue);
    }
    return this;
  },

  getItem: function (name) {
    return this.getRenderedChildByName(name);
  },

  // Overriding Functions

  shouldChildRender_: function (child) {
    return this.getChildTagName(child) === this.getDefault().childTagName;
  },

  // Component Specific Helpers
  _getProgressBarItem: function (name) {
    return this.getItem(name || 'progressBarItem');
  },

  _getMainProps: function () {
    return $.extend(true, {}, this.getUU5_Common_baseMixinPropsToPass(), this.getUU5_Common_elementaryMixinPropsToPass(), this.getUU5_Common_contentMixinPropsToPass());
  },

  _getChildProps: function () {
    return {
      name: 'progressBarItem',
      colorSchema: this.props.colorSchema,
      progress: this.props.progress,
      label: this.props.label,
      hiddenProgressLabel: this.props.hiddenProgressLabel,
      striped: this.props.striped,
      animated: this.props.animated
    };
  },

  _buildChild: function () {
    var child = this.buildChild(this.getDefault().childTagName, this._getChildProps());
    return this.cloneChild(child, this.expandChildProps(child, 0));
  },

  // Render
  render: function () {
    return React.createElement(
      UU5.Bricks.progressBar._cover,
      this._getMainProps(),
      this.getChildren() || this._buildChild()
    );
  }
});

UU5.Bricks.progressBar._cover = React.createClass({
  displayName: '_cover',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin],

  statics: {
    tagName: 'UU5.Bricks.progressBar._cover',
    classNames: {
      main: 'UU5_Bricks_progressBar-cover progress'
    }
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  // Render
  render: function () {
    return React.createElement(
      'div',
      this.buildMainAttrs(),
      React.Children.toArray(this.props.children),
      this.getDisabledCover()
    );
  }
});

UU5.Bricks.progressBar.item = React.createClass({
  displayName: 'item',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin],

  statics: {
    tagName: 'UU5.Bricks.progressBar.item',
    classNames: {
      main: 'UU5_Bricks_progressBar-item progress-bar',
      colorSchema: 'progress-bar-',
      striped: 'progress-bar-striped',
      active: 'active'
    }
  },

  propTypes: {
    colorSchema: React.PropTypes.oneOf(['primary', 'success', 'info', 'warning', 'danger']),
    progress: React.PropTypes.number,
    label: React.PropTypes.string,
    hiddenProgressLabel: React.PropTypes.bool,
    striped: React.PropTypes.bool,
    animated: React.PropTypes.bool
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      colorSchema: 'primary',
      progress: 0,
      label: null,
      hiddenProgressLabel: false,
      striped: false,
      animated: false
    };
  },

  getInitialState: function () {
    return {
      progress: this.props.progress
    };
  },

  // Interface
  getProgress: function () {
    return this.state.progress;
  },

  setProgress: function (progress, callback) {
    var newProgress = this._checkProgress(progress);
    this.setState({ progress: newProgress }, callback);
    return this;
  },

  increase: function (increasedValue, callback) {
    var progressItem = this;
    this.setState(function (state) {
      var newProgress = progressItem._checkProgress(state.progress + increasedValue);
      return { progress: newProgress };
    }, callback);
    return this;
  },

  decrease: function (decreasedValue, callback) {
    this.increase(-decreasedValue, callback);
    return this;
  },

  // Overriding Functions

  // Component Specific Helpers
  _checkProgress: function (newProgress) {
    var result = newProgress;
    if (newProgress > 100) {
      result = 100;
    } else if (newProgress < 0) {
      result = 0;
    }
    return result;
  },

  _getMainAttrs: function () {
    var mainAttrs = this.buildMainAttrs();

    this.props.colorSchema && (mainAttrs.className += ' ' + this.getClassName().colorSchema + this.props.colorSchema);

    if (this.props.animated) {
      mainAttrs.className += ' ' + this.getClassName().striped + ' ' + this.getClassName().active;
    } else if (this.props.striped) {
      mainAttrs.className += ' ' + this.getClassName().striped;
    }

    mainAttrs.style = { width: this.getProgress() + '%' };

    return mainAttrs;
  },

  _getLabel: function () {
    var label = null;

    if (this.getProgress() > 0) {
      label = this.props.label;
      if (!this.props.hiddenProgressLabel) {
        label = this.getProgress() + '%' + (label ? ' ' + label : '');
      }
    }

    return label;
  },

  // Render
  render: function () {
    return React.createElement(
      'div',
      this._getMainAttrs(),
      this._getLabel()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.section = React.createClass({
  displayName: 'section',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin],

  statics: {
    tagName: 'UU5.Bricks.section',
    classNames: {
      main: 'UU5_Bricks_section'
    }
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  // Render
  render: function () {
    return React.createElement(
      'div',
      this.buildMainAttrs(),
      this.getHeaderChild(),
      this.getChildren(),
      this.getFooterChild(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.slider = React.createClass({
  displayName: 'slider',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.slider',
    classNames: {
      main: 'UU5_Bricks_slider',
      input: 'UU5_Bricks_slider-input',
      track: 'UU5_Bricks_slider-track bg-low',
      selection: 'UU5_Bricks_slider-selection',
      pointer: 'UU5_Bricks_slider-pointer',
      active: 'UU5_Bricks_slider-active'
    }
  },

  propTypes: {
    colorSchema: React.PropTypes.oneOf(UU5.Common.environment.colorSchema),
    // TODO
    //position: React.PropTypes.oneOf(['horizontal', 'vertical']),
    min: React.PropTypes.number,
    max: React.PropTypes.number,
    step: React.PropTypes.number,
    value: React.PropTypes.number,
    onChange: React.PropTypes.func,
    onChanged: React.PropTypes.func
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      colorSchema: 'primary',
      //position: 'horizontal',
      min: 0,
      max: 10,
      step: 1,
      value: null, // default: min
      onChange: null,
      onChanged: null
    };
  },

  getInitialState: function () {
    return {
      value: this._checkValue(this.props.value),
      active: false
    };
  },

  componentWillReceiveProps: function (nextProps) {
    this.setValue(nextProps.value);
  },

  // Interface
  getValue: function () {
    return this.state.value;
  },

  setValue: function (value, setStateCallback) {
    this.setState({ value: this._checkValue(value) }, setStateCallback);
    return this;
  },

  // Overriding Functions

  // Component Specific Helpers
  _checkValue: function (value) {
    if (value === null || value < this.props.min) {
      value = this.props.min;
    } else if (value > this.props.max) {
      value = this.props.max;
    }
    return value;
  },

  _changeValue: function (value, e) {
    var onChange;
    var onChanged;
    if (this.getValue() !== value) {
      onChange = this._getOnChange(value, e);
      !onChange && (onChanged = this._getOnChanged(value, e));

      onChange ? onChange() : this.setValue(value, onChanged);
    }

    return this;
  },

  _activate: function (e) {
    var value = this._countValue(e);

    var onChange;
    var onChanged;
    if (this.getValue() !== value) {
      onChange = this._getOnChange(value, e);
      !onChange && (onChanged = this._getOnChanged(value, e));
    }

    var newState = { active: true };
    !onChange && (newState.value = value);

    this.setState(newState, onChange || onChanged);
    return this;
  },

  _deactivate: function (e) {
    this.setState({ active: false });
    return this;
  },

  _isActive: function () {
    return this.state.active;
  },

  _move: function (e) {
    if (this._isActive()) {
      this._changeValue(this._countValue(e), e);
    }
    return this;
  },

  _getOnChange: function (value, e) {
    var onChange;
    if (typeof this.props.onChange === 'function') {
      var slider = this;
      onChange = function () {
        slider.props.onChange({ value: value, component: slider, event: e });
      };
    }
    return onChange;
  },

  _getOnChanged: function (value, e) {
    var onChanged;
    if (typeof this.props.onChanged === 'function') {
      var slider = this;
      onChanged = function () {
        slider.props.onChanged({ value: value, component: slider, event: e });
      };
    }
    return onChanged;
  },

  _getStartPositions: function (el) {
    var xPos = 0;
    var yPos = 0;

    while (el) {
      xPos += el.offsetLeft - el.scrollLeft + el.clientLeft;
      yPos += el.offsetTop - el.scrollTop + el.clientTop;

      el = el.offsetParent;
    }

    return {
      x: xPos,
      y: yPos
    };
  },

  _countValue: function (e) {
    var sliderStart = this._getStartPositions(this.track).x;
    var sliderWidth = this.track.offsetWidth;
    var actualPosition = e.clientX;
    if (e.touches) {
      actualPosition = e.touches.item(0).clientX;
    }

    var absolutePosition = actualPosition - sliderStart;
    var end = sliderWidth;

    var min = this.props.min;
    var max = this.props.max;
    var step = this.props.step;

    var absoluteMax = max - min;

    var realValue = absolutePosition / (end / absoluteMax);
    var value = min + Math.round(realValue / step) * step;
    value > this.props.max && (value = this.props.max);
    value < this.props.min && (value = this.props.min);

    // console.log('move', actualPosition, {
    //   sliderStart: sliderStart,
    //   sliderWidth: sliderWidth,
    //   actualPosition: actualPosition,
    //   absolutePosition: absolutePosition,
    //   end: end,
    //   min: min,
    //   max: max,
    //   step: step,
    //   absoluteMax: absoluteMax,
    //   realValue: realValue,
    //   value: value
    // });

    return value;
  },

  _getMainAttrs: function () {
    var attrs = this.buildMainAttrs();

    if (!this.isDisabled()) {
      this._isActive() && (attrs.className += ' ' + this.getClassName().active);

      attrs.onMouseDown = this._activate;
      attrs.onMouseMove = this._move;
      attrs.onMouseUp = this._deactivate;
      attrs.onTouchStart = this._activate;
      attrs.onTouchMove = this._move;
      attrs.onTouchEnd = this._deactivate;
    }

    return attrs;
  },

  _getInputAttrs: function () {
    var attrs = {
      className: this.getClassName().input,
      type: "range",
      name: this.getName(),
      min: this.props.min,
      max: this.props.max,
      step: this.props.step,
      value: this.getValue(),
      disabled: this.isDisabled()
    };

    if (!this.isDisabled()) {
      var slider = this;
      attrs.onChange = function (e) {
        slider._changeValue(e.target.value, e);
      };
    }

    return attrs;
  },

  _getBackdropProps: function () {
    var backdropId = this.getId() + "-backdrop";
    var slider = this;

    return {
      hidden: !this._isActive(),
      id: backdropId,
      onClick: function (event) {
        event.target.id === backdropId && slider._deactivate();
      },
      mainAttrs: {
        onMouseUp: this._deactivate,
        onTouchEnd: this._deactivate
      }
    };
  },

  _getTrackAttrs: function () {
    var slider = this;
    return {
      className: this.getClassName().track,
      ref: function (div) {
        slider.track = div;
      }
    };
  },

  _getSelectionAttrs: function () {
    return {
      className: this.getClassName().selection,
      style: { width: (this.getValue() - this.props.min) / (this.props.max - this.props.min) * 100 + '%' }
    };
  },

  _getPointerAttrs: function () {
    var attrs = {
      className: this.getClassName().pointer
    };

    this.props.colorSchema && (attrs.className += ' bg-' + this.props.colorSchema);

    return attrs;
  },

  // Render
  render: function () {
    return React.createElement(
      'div',
      this._getMainAttrs(),
      React.createElement('input', this._getInputAttrs()),
      React.createElement(UU5.Bricks.backdrop, this._getBackdropProps()),
      React.createElement(
        'div',
        this._getTrackAttrs(),
        React.createElement(
          'div',
          this._getSelectionAttrs(),
          React.createElement(
            'div',
            this._getPointerAttrs(),
            this.getChildren()
          )
        )
      )
    );
  }
});

'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * UU5.Bricks.swiper
 */
UU5.Bricks.swiper = React.createClass({
  displayName: 'swiper',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin, UU5.Common.swipeMixin],

  statics: {
    tagName: 'UU5.Bricks.swiper',
    classNames: {
      main: 'UU5_Bricks_swiper'
    },
    defaults: {
      bodyTagName: 'UU5.Bricks.swiper.body',
      menuTagName: 'UU5.Bricks.swiper.menu'
    }
  },

  propTypes: {
    leftMenuOpened: React.PropTypes.bool,
    rightMenuOpened: React.PropTypes.bool,
    onSwipeOpenLeftMenu: React.PropTypes.func,
    onSwipeCloseLeftMenu: React.PropTypes.func,
    onSwipeOpenRightMenu: React.PropTypes.func,
    onSwipeCloseRightMenu: React.PropTypes.func
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      leftMenuOpened: false,
      rightMenuOpened: false,
      onSwipeOpenLeftMenu: null,
      onSwipeCloseLeftMenu: null,
      onSwipeOpenRightMenu: null,
      onSwipeCloseRightMenu: null
    };
  },

  getInitialState: function () {
    return {
      leftMenuOpened: this.props.leftMenuOpened,
      rightMenuOpened: this.props.rightMenuOpened
    };
  },

  // Interface
  openLeftMenu: function (callback) {
    this.setState({
      leftMenuOpened: true,
      rightMenuOpened: false
    }, callback);
    return this;
  },

  closeLeftMenu: function (callback) {
    this.setState({
      leftMenuOpened: false
    }, callback);
    return this;
  },

  toggleLeftMenu: function (callback) {
    this.setState(function (state) {
      var newState = { leftMenuOpened: !state.leftMenuOpened };
      !state.leftMenuOpened && (newState.rightMenuOpened = false);
      return newState;
    }, callback);
    return this;
  },

  openRightMenu: function (callback) {
    this.setState({
      leftMenuOpened: false,
      rightMenuOpened: true
    }, callback);
    return this;
  },

  closeRightMenu: function (callback) {
    this.setState({
      rightMenuOpened: false
    }, callback);
    return this;
  },

  toggleRightMenu: function (callback) {
    this.setState(function (state) {
      var newState = { rightMenuOpened: !state.rightMenuOpened };
      !state.rightMenuOpened && (newState.leftMenuOpened = false);
      return newState;
    }, callback);
    return this;
  },

  isLeftMenuOpened: function () {
    return this.state.leftMenuOpened;
  },

  isRightMenuOpened: function () {
    return this.state.rightMenuOpened;
  },

  // Overriding Functions

  shouldChildRender_: function (child) {
    var childTagName = this.getChildTagName(child);
    return childTagName === this.getDefault().menuTagName || childTagName === this.getDefault().bodyTagName;
  },

  expandChildProps_: function (child) {
    var newChildProps = $.extend(true, {}, child.props);
    if (this.getChildTagName(child) === this.getDefault().menuTagName) {
      if (child.props.pullRight) {
        newChildProps._opened = this.isRightMenuOpened();
      } else {
        newChildProps._opened = this.isLeftMenuOpened();
      }
    }
    return newChildProps || child.props;
  },

  // Component Specific Helpers
  _onSwipeEnd: function () {
    var absAngle = Math.abs(this.getSwipeAngle());
    if (this.isSwipedRight()) {
      this.isRightMenuOpened() ? this.closeRightMenu(this.props.onSwipeCloseRightMenu) : this.openLeftMenu(this.props.onSwipeOpenLeftMenu);
    } else if (this.isSwipedLeft()) {
      this.isLeftMenuOpened() ? this.closeLeftMenu(this.props.onSwipeCloseLeftMenu) : this.openRightMenu(this.props.onSwipeOpenRightMenu);
    }
    return this;
  },

  // Render
  _buildChildren: function () {
    var menuLeft;
    var menuRight;
    var body;

    var children = this.getChildren();
    if (children) {
      children.forEach(function (child) {
        if (this.getChildTagName(child) === this.getDefault().bodyTagName) {
          body = child;
        } else if (this.getChildTagName(child) === this.getDefault().menuTagName) {
          if (child.props.pullRight) {
            menuRight = menuRight || child;
          } else {
            menuLeft = menuLeft || child;
          }
        }
      }.bind(this));
    }

    var newChildren = [];
    menuLeft && newChildren.push(menuLeft);
    menuRight && newChildren.push(menuRight);
    body && newChildren.push(body);
    return newChildren;
  },

  render: function () {
    return React.createElement(
      'div',
      _extends({}, this.buildMainAttrs(), {
        onTouchStart: this.swipeOnTouchStart,
        onTouchMove: this.swipeOnTouchMove,
        onTouchEnd: this.swipeOnTouchEnd.bind(this, this._onSwipeEnd)
      }),
      this._buildChildren(),
      this.getDisabledCover()
    );
  }
});

UU5.Bricks.swiper.menu = React.createClass({
  displayName: 'menu',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.swiper.menu',
    classNames: {
      main: 'UU5_Bricks_swiper_menu',
      left: 'UU5_Bricks_swiper_menu-left',
      right: 'UU5_Bricks_swiper_menu-right',
      opened: 'UU5_Bricks_swiper_menu-opened'
    },
    defaults: {
      parentTagName: 'UU5.Bricks.swiper'
    }
  },

  propTypes: {
    pullRight: React.PropTypes.bool,
    _opened: React.PropTypes.bool
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      pullRight: false,
      _opened: false
    };
  },

  componentWillMount: function () {
    this.checkParentTagName(this.getDefault().parentTagName);
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  // Render
  render: function () {
    var mainAttrs = this.buildMainAttrs();

    mainAttrs.className += ' ' + (this.props.pullRight ? this.getClassName().right : this.getClassName().left);
    this.props._opened && (mainAttrs.className += ' ' + this.getClassName().opened);

    return React.createElement(
      'div',
      mainAttrs,
      this.getChildren(),
      this.getDisabledCover()
    );
  }
});

UU5.Bricks.swiper.body = React.createClass({
  displayName: 'body',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.swiper.body',
    classNames: {
      main: 'UU5_Bricks_swiper_body'
    },
    defaults: {
      parentTagName: 'UU5.Bricks.swiper'
    }
  },

  componentWillMount: function () {
    this.checkParentTagName(this.getDefault().parentTagName);
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  // Render
  render: function () {
    return React.createElement(
      'div',
      this.buildMainAttrs(),
      this.getChildren(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.table = React.createClass({
  displayName: 'table',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin],

  statics: {
    tagName: 'UU5.Bricks.table',
    classNames: {
      main: 'UU5_Bricks_table',
      table: 'table table-responsive',
      striped: 'table-striped',
      bordered: 'table-bordered',
      hover: 'table-hover',
      condensed: 'table-condensed'
    },
    defaults: {
      childTagNames: ['UU5.Bricks.tr', 'UU5.Bricks.thead', 'UU5.Bricks.tbody', 'UU5.Bricks.tfoot']
    }
  },

  propTypes: {
    striped: React.PropTypes.bool,
    bordered: React.PropTypes.bool,
    hover: React.PropTypes.bool,
    condensed: React.PropTypes.bool
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      striped: false,
      bordered: false,
      hover: false,
      condensed: false
    };
  },

  // Interface

  // Overriding Functions
  shouldChildRender_: function (child) {
    return this.getDefault().childTagNames.indexOf(this.getChildTagName(child)) > -1;
  },

  // Component Specific Helpers
  _buildTableAttrs: function () {
    var tableAttrs = { className: this.getClassName().table };
    this.props.striped && (tableAttrs.className += ' ' + this.getClassName().striped);
    this.props.bordered && (tableAttrs.className += ' ' + this.getClassName().bordered);
    this.props.hover && (tableAttrs.className += ' ' + this.getClassName().hover);
    this.props.condensed && (tableAttrs.className += ' ' + this.getClassName().condensed);
    return tableAttrs;
  },

  // Render
  render: function () {
    return React.createElement(
      'div',
      this.buildMainAttrs(),
      this.getHeaderChild(),
      React.createElement(
        'table',
        this._buildTableAttrs(),
        this.getChildren()
      ),
      this.getFooterChild(),
      this.getDisabledCover()
    );
  }
});

UU5.Bricks.thead = React.createClass({
  displayName: 'thead',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.thead',
    classNames: {
      main: 'UU5_Bricks_thead'
    },
    defaults: {
      childTagName: 'UU5.Bricks.tr',
      parentTagName: 'UU5.Bricks.table'
    }
  },

  componentWillMount: function () {
    this.checkParentTagName(this.getDefault().parentTagName);
  },

  // Interface

  // Overriding Functions
  shouldChildRender_: function (child) {
    return this.getDefault().childTagName === this.getChildTagName(child);
  },

  // Render
  render: function () {
    return React.createElement(
      'thead',
      this.buildMainAttrs(),
      this.getChildren(),
      this.getDisabledCover()
    );
  }
});

UU5.Bricks.tbody = React.createClass({
  displayName: 'tbody',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.tbody',
    classNames: {
      main: 'UU5_Bricks_tbody'
    },
    defaults: {
      childTagName: 'UU5.Bricks.tr',
      parentTagName: 'UU5.Bricks.table'
    }
  },

  componentWillMount: function () {
    this.checkParentTagName(this.getDefault().parentTagName);
  },

  // Interface

  // Overriding Functions
  shouldChildRender_: function (child) {
    return this.getDefault().childTagName === this.getChildTagName(child);
  },

  // Render
  render: function () {
    return React.createElement(
      'tbody',
      this.buildMainAttrs(),
      this.getFilteredSorterChildren(),
      this.getDisabledCover()
    );
  }
});

UU5.Bricks.tfoot = React.createClass({
  displayName: 'tfoot',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.tfoot',
    classNames: {
      main: 'UU5_Bricks_tfoot'
    },
    defaults: {
      childTagName: 'UU5.Bricks.tr',
      parentTagName: 'UU5.Bricks.table'
    }
  },

  componentWillMount: function () {
    this.checkParentTagName(this.getDefault().parentTagName);
  },

  // Interface

  // Overriding Functions
  shouldChildRender_: function (child) {
    return this.getDefault().childTagName === this.getChildTagName(child);
  },

  // Render
  render: function () {
    return React.createElement(
      'tfoot',
      this.buildMainAttrs(),
      this.getChildren(),
      this.getDisabledCover()
    );
  }
});

UU5.Bricks.tr = React.createClass({
  displayName: 'tr',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.tr',
    classNames: {
      main: 'UU5_Bricks_tr'
    },
    defaults: {
      childTagNames: ['UU5.Bricks.td', 'UU5.Bricks.th'],
      parentTagNames: ['UU5.Bricks.table', 'UU5.Bricks.thead', 'UU5.Bricks.tbody', 'UU5.Bricks.tfoot']
    }
  },

  componentWillMount: function () {
    this.checkParentTagName(this.getDefault().parentTagNames);
  },

  // Interface

  // Overriding Functions
  shouldChildRender_: function (child) {
    return this.getDefault().childTagNames.indexOf(this.getChildTagName(child)) > -1;
  },

  // Render
  render: function () {
    return React.createElement(
      'tr',
      this.buildMainAttrs(),
      this.getChildren(),
      this.getDisabledCover()
    );
  }
});

UU5.Bricks.td = React.createClass({
  displayName: 'td',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.td',
    classNames: {
      main: 'UU5_Bricks_td'
    },
    defaults: {
      parentTagName: 'UU5.Bricks.tr'
    }
  },

  componentWillMount: function () {
    this.checkParentTagName(this.getDefault().parentTagName);
  },

  // Interface

  // Overriding Functions

  // Render
  render: function () {
    return React.createElement(
      'td',
      this.buildMainAttrs(),
      this.getChildren(),
      this.getDisabledCover()
    );
  }
});

UU5.Bricks.th = React.createClass({
  displayName: 'th',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.th',
    classNames: {
      main: 'UU5_Bricks_th'
    },
    defaults: {
      parentTagName: 'UU5.Bricks.tr'
    }
  },

  componentWillMount: function () {
    this.checkParentTagName(this.getDefault().parentTagName);
  },

  // Interface

  // Overriding Functions

  // Render
  render: function () {
    return React.createElement(
      'th',
      this.buildMainAttrs(),
      this.getChildren(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.text = React.createClass({
  displayName: 'text',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.text',
    layoutType: 'brick',
    classNames: {
      main: 'UU5_Bricks_text',
      color: 'text-',
      bg: 'bg-'
    }
  },

  propTypes: {
    colorSchema: React.PropTypes.oneOf(UU5.Common.environment.colorSchema),
    isBackground: React.PropTypes.bool
  },

  getDefaultProps: function () {
    return {
      colorSchema: null,
      isBackground: false
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _buildMainAttrs: function () {
    var mainAttrs = this.buildMainAttrs();
    var colorSchemaClassName = this.props.isBackground ? 'bg' : 'color';
    mainAttrs.className += ' ' + this.getClassName(colorSchemaClassName) + this.props.colorSchema;
    return mainAttrs;
  },

  // Render
  render: function () {
    return React.createElement(
      'span',
      this._buildMainAttrs(),
      this.getChildren(),
      this.getDisabledCover()
    );
  }
});

UU5.Bricks.factory = UU5.Bricks.factory || {};
UU5.Bricks.factory._createText = function (colorSchema, isBackground) {
  return React.createClass({

    // Interface

    // Overriding Functions

    // Component Specific Helpers

    // Render
    render: function () {
      return React.createElement(
        UU5.Bricks.text,
        _extends({}, this.props, { colorSchema: colorSchema, isBackground: isBackground }),
        React.Children.toArray(this.props.children)
      );
    }
  });
};

UU5.Common.environment.colorSchema.forEach(function (colorSchema) {
  var colorSchemaCapitalize = UU5.Common.environment.colorSchemaCapitalize(colorSchema);
  UU5.Bricks['text' + colorSchemaCapitalize] = UU5.Bricks.factory._createText(colorSchema);
  UU5.Bricks['textBg' + colorSchemaCapitalize] = UU5.Bricks.factory._createText(colorSchema, true);
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

UU5.Bricks.textCorrector = React.createClass({

  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin],

  statics: {
    tagName: 'UU5.Bricks.textCorrector',
    classNames: {
      main: 'UU5_Bricks_textCorrector',
      error: 'UU5_Bricks_textCorrector-error'
    }
  },

  propTypes: {
    text: React.PropTypes.string,
    language: React.PropTypes.string,
    ignoreSpaces: React.PropTypes.bool,
    ignoreGrammar: React.PropTypes.bool
  },

  getDefaultProps: function () {
    return {
      text: '',
      language: null,
      ignoreSpaces: false,
      ignoreGrammar: false
    };
  },

  getInitialState: function () {
    return {
      text: this._correctSpaces(this.props.text)
    };
  },

  componentDidMount: function () {
    this._correctGrammar(this.state.text);
  },

  componentWillReceiveProps: function (nextProps) {
    this._correctText(nextProps.text);
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _correctSpaces: function (text) {
    var newText = text;
    if (!this.props.ignoreSpaces) {
      newText = this.replaceByHardSpace(text, this.props.language);
    }
    return newText;
  },

  _isValidWord: function (word) {
    var result = true;

    var text = /[A-Za-z]+/i.exec(word);
    if (text) {
      result = text[0] !== 'vidra';
    }

    return result;
  },

  _correctGrammar: function (text) {
    var newTextChildren = [];
    var tempText = [];
    if (!this.props.ignoreGrammar) {
      var corrector = this;
      var textSplitter = text.split(' ');
      textSplitter.forEach(function (word, i) {
        if (corrector._isValidWord(word)) {
          tempText.push(word);
        } else {
          newTextChildren.push((newTextChildren.length ? ' ' : '') + tempText.join(' ') + ' ');
          tempText = [];
          newTextChildren.push(React.createElement(
            'span',
            { className: corrector.getClassName().error, key: i },
            word
          ));
        }
      });

      tempText.length && newTextChildren.push((newTextChildren.length ? ' ' : '') + tempText.join(' '));
      this.setState({ text: newTextChildren });
    }
    return this;
  },

  _correctText: function (text) {
    var newText = text;
    if (!this.props.ignoreSpaces) {
      newText = this._correctSpaces(text);
    }

    if (!this.props.ignoreGrammar) {
      this._correctGrammar(newText);
    } else {
      this.setState({ text: newText });
    }

    return this;
  },

  // Render
  render: function () {
    return React.createElement(
      'span',
      this.buildMainAttrs(),
      this.state.text,
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * UU5.Bricks.touchIcon
 */
UU5.Bricks.touchIcon = React.createClass({
  displayName: 'touchIcon',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin],

  statics: {
    tagName: 'UU5.Bricks.touchIcon',
    classNames: {
      main: 'UU5_Bricks_touchIcon',
      disabled: 'UU5_Bricks_touchIcon-disabled',
      body: 'UU5_Bricks_touchIcon-body',
      label: 'UU5_Bricks_touchIcon-label',
      glyphicon: 'UU5_Bricks_touchIcon-glyphicon'
    }
  },

  propTypes: {
    glyphicon: React.PropTypes.string,
    label: React.PropTypes.string,
    reference: React.PropTypes.string,
    target: React.PropTypes.oneOf(['_blank', '_parent', '_top', '_self']),
    onClick: React.PropTypes.func,
    bodyClass: React.PropTypes.string,
    bodyClassDisabled: React.PropTypes.string,
    bodyAttrs: React.PropTypes.object,
    labelClass: React.PropTypes.string,
    labelClassDisabled: React.PropTypes.string,
    labelAttrs: React.PropTypes.object
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      glyphicon: 'glyphicon-file',
      label: 'TouchIcon',
      reference: '#',
      target: '_self',
      onClick: null,
      bodyClass: 'UU5_Bricks_touchIcon-bodyDefault',
      bodyClassDisabled: 'UU5_Bricks_touchIcon-bodyDisabled',
      bodyAttrs: null,
      labelClass: 'UU5_Bricks_touchIcon-labelDefault',
      labelClassDisabled: 'UU5_Bricks_touchIcon-labelDisabled',
      labelAttrs: null
    };
  },

  componentDidMount: function () {
    // TODO: uu5 tooltip will replaced bs tooltip
    this.props.title && $(ReactDOM.findDOMNode(this.body)).tooltip();
  },

  componentWillReceiveProps: function (nextProps) {
    nextProps.title && this.props.title !== nextProps.title && $(ReactDOM.findDOMNode(this.body)).tooltip();
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _onClickHandler: function (event) {
    this.props.onClick && this.props.onClick(this, event);
    return this;
  },

  // show tooltip on element with elipsis
  _onMouseOverHandler: function (event) {
    var $this = $(event.target);
    if (event.target.offsetWidth < event.target.scrollWidth) {
      $this.tooltip({
        title: $this.text(),
        placement: 'auto bottom'
      });
      $this.tooltip('show');
    }
    return this;
  },

  _refBody: function (body) {
    this.body = body;
    return this;
  },

  _getMainAttrs: function () {
    var mainAttrs = this.buildMainAttrs();

    if (!this.isDisabled()) {
      if (this.props.onClick) {
        mainAttrs.onClick = this._onClickHandler;
      } else {
        mainAttrs.href = this.props.reference;
        mainAttrs.target = this.props.target;
      }
    }

    return mainAttrs;
  },

  _getLabelAttrs: function () {
    var labelAttrs = $.extend(true, {}, this.props.labelAttrs);
    labelAttrs.className = this.getClassName().label + (labelAttrs.className ? ' ' + labelAttrs.className : '') + ' ' + (this.isDisabled() ? this.props.labelClassDisabled : this.props.labelClass);

    labelAttrs.onMouseOver = this._onMouseOverHandler;

    return labelAttrs;
  },

  _getBodyAttrs: function () {
    var bodyAttrs = $.extend(true, {}, this.props.bodyAttrs);
    bodyAttrs.className = this.getClassName().body + (bodyAttrs.className ? ' ' + bodyAttrs.className : '') + ' ' + (this.isDisabled() ? this.props.bodyClassDisabled : this.props.bodyClass);

    if (this.props.title) {
      bodyAttrs.title = this.props.title;
      bodyAttrs['data-toggle'] = 'tooltip';
      bodyAttrs['data-placement'] = 'auto';
      bodyAttrs.ref = this._ref;
    }

    return bodyAttrs;
  },

  //Render
  render: function () {
    return React.createElement(
      'a',
      this._getMainAttrs(),
      React.createElement(
        'div',
        this._getBodyAttrs(),
        React.createElement(UU5.Bricks.glyphicon, { glyphicon: this.props.glyphicon, className: this.getClassName().glyphicon })
      ),
      React.createElement(
        'div',
        this._getLabelAttrs(),
        this.props.label
      )
    );
  }
});

'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * UU5.Bricks.tree
 */
UU5.Bricks.tree = React.createClass({
  displayName: 'tree',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.tree',
    classNames: {
      main: 'UU5_Bricks_tree'
    },
    defaults: {
      childTagName: 'UU5.Bricks.tree.item'
    }
  },

  propTypes: {
    items: React.PropTypes.array,
    glyphiconExpand: React.PropTypes.string,
    glyphiconCollapse: React.PropTypes.string
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      items: null,
      glyphiconExpand: 'uu-glyphicon-minus',
      glyphiconCollapse: 'uu-glyphicon-plus'
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  // Render
  render: function () {
    return React.createElement(
      UU5.Bricks.tree.list,
      _extends({}, this.getMainPropsToPass(), {
        items: this.props.items,
        glyphiconExpand: this.props.glyphiconExpand,
        glyphiconCollapse: this.props.glyphiconCollapse
      }),
      this.props.children && React.Children.toArray(this.props.children),
      this.getDisabledCover()
    );
  }
});

UU5.Bricks.tree.item = React.createClass({
  displayName: 'item',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin],

  statics: {
    tagName: 'UU5.Bricks.tree.item',
    classNames: {
      main: 'UU5_Bricks_tree_item'
    }
  },

  propTypes: {
    label: React.PropTypes.node,
    items: React.PropTypes.array,
    glyphiconExpand: React.PropTypes.string,
    glyphiconCollapse: React.PropTypes.string,
    expanded: React.PropTypes.bool
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      label: null,
      items: null,
      glyphiconExpand: null,
      glyphiconCollapse: null,
      expanded: false
    };
  },

  getInitialState: function () {
    return {
      expanded: this.props.expanded
    };
  },

  componentWillReceiveProps: function (nextProps) {
    if (nextProps.expanded !== this.props.expanded && nextProps.expanded !== this.isExpanded()) {
      this.setState({ expanded: nextProps.expanded });
    }
  },

  // Interface

  expand: function (callback) {
    this.setState({ expanded: true }, callback);
  },

  collapse: function (callback) {
    this.setState({ expanded: false }, callback);
  },

  toggleExpand: function (callback) {
    this.setState(function (state) {
      return { expanded: !state.expanded };
    }, callback);
  },

  isExpanded: function () {
    return this.state.expanded;
  },

  // Overriding Functions

  // Component Specific Helpers

  _getItems: function () {
    var result = null;

    if (this.props.items) {
      result = React.createElement(UU5.Bricks.tree.list, {
        parent: this,
        items: this.props.items,
        glyphiconExpand: this.props.glyphiconExpand,
        glyphiconCollapse: this.props.glyphiconCollapse,
        hidden: !this.isExpanded()
      });
    }

    return result;
  },

  _onToggle: function () {
    this.toggleExpand();
    return this;
  },

  _getGlyphicon: function () {
    var glyphicon;

    if (this.props.items) {
      if (this.isExpanded()) {
        glyphicon = this.props.glyphiconExpand;
      } else {
        glyphicon = this.props.glyphiconCollapse;
      }
    }

    var link = null;
    if (glyphicon) {
      link = React.createElement(
        UU5.Bricks.link,
        { onClick: this._onToggle, parent: this },
        React.createElement(UU5.Bricks.glyphicon, { glyphicon: glyphicon })
      );
    } else {
      link = React.createElement(UU5.Bricks.glyphicon, { mainAttrs: { style: { visibility: 'hidden' } } });
    }

    return link;
  },

  // Render
  render: function () {
    return React.createElement(
      'li',
      this.buildMainAttrs(),
      this._getGlyphicon(),
      ' ',
      this.props.label,
      this._getItems(),
      this.getDisabledCover()
    );
  }
});

UU5.Bricks.tree.list = React.createClass({
  displayName: 'list',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.tree.list',
    classNames: {
      main: 'UU5_Bricks_tree_list'
    },
    defaults: {
      childTagName: 'UU5.Bricks.tree.item'
    }
  },

  propTypes: {
    items: React.PropTypes.array,
    glyphiconExpand: React.PropTypes.string,
    glyphiconCollapse: React.PropTypes.string
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      items: null,
      glyphiconExpand: null,
      glyphiconCollapse: null
    };
  },

  // Interface

  // Overriding Functions
  expandChildProps_: function (child) {
    var newChildProps = $.extend(true, {}, child.props);
    newChildProps.glyphiconExpand = newChildProps.glyphiconExpand || this.props.glyphiconExpand;
    newChildProps.glyphiconCollapse = newChildProps.glyphiconCollapse || this.props.glyphiconCollapse;
    return newChildProps;
  },

  shouldChildRender_: function (child) {
    return this.getChildTagName(child) === this.getDefault().childTagName;
  },

  // Component Specific Helpers

  _getItems: function () {
    var list = this;
    var content = this.getContent();

    if (this.props.items) {
      content = this.props.items.map(function (itemProps) {
        return { tag: list.getDefault().childTagName, props: itemProps };
      });
    }

    return this.buildChildren({ content: content, children: this.props.children });
  },

  // Render
  render: function () {
    return React.createElement(
      'ul',
      this.buildMainAttrs(),
      this._getItems(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var UU5 = UU5 || {};
UU5.Bricks = UU5.Bricks || {};

/**
 * Well component based on bootstrap standard.
 *
 * @requires UU5.Common.baseMixin
 * @requires UU5.Common.elementaryMixin
 */
UU5.Bricks.well = React.createClass({
  displayName: 'well',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Bricks.well',
    classNames: {
      main: 'UU5_Bricks_well well'
    }
  },

  /**
   *
   * @property {(string)} bsSize - well size - null|well-sm|well-lg (normal|small|large)
   * @property {(string)} text - well displayed text
   */
  propTypes: {
    size: React.PropTypes.oneOf(['sm', 'lg'])
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      size: null
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers

  // Render
  render: function () {
    var mainAttrs = this.buildMainAttrs();
    this.props.size && (mainAttrs.className += ' well-' + this.props.size);

    return React.createElement(
      'div',
      _extends({}, mainAttrs, {
        __self: this
      }),
      this.getChildren(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Layout = UU5.Layout || {};

/**
 * Component should be used as central alert bus. It should be just one in root and it has fixed position.
 */
UU5.Layout.alertBusContainer = React.createClass({
  displayName: 'alertBusContainer',

  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Layout.containerMixin],

  statics: {
    tagName: 'UU5.Layout.alertBusContainer',
    classNames: {
      main: 'UU5_Layout_alertBusContainer'
    }
  },

  // Interface
  /**
   * Adds message with some options to queue.
   *
   * @param {string} message - Message for user to show.
   * @param {object} alertProps - Additional options of alert.
   * @param {string} alertProps.id - Unique ID of message.
   * @param {string} alertProps.label - Important label of message is shown as bold if is set.
   * @param {string} alertProps.colorSchema - Importance color style of message box.
   * @param {number} alertProps.closeTimer - Time in milliseconds how long the alert is shown. If time expires alert disappears.
   * @param {function} alertProps.onClose - Function will be called if the alert is closed.
   * @returns {UU5.Layout.alertBusContainer} this
   */
  addMessage: function (message, alertProps) {
    this.alertBus.addMessage(message, alertProps);
    return this;
  },

  /**
   * Sets just one message with options. The message is shown in the time and other messages are never shown.
   *
   * @param {string} message - Message for user to show.
   * @param {object} alertProps - Additional options of alert.
   * @param {string} alertProps.id - Unique ID of message.
   * @param {string} alertProps.label - Important label of message is shown as bold if is set.
   * @param {string} alertProps.colorSchema - Importance color style of message box.
   * @param {number} alertProps.closeTimer - Time in milliseconds how long the alert is shown. If time expires alert disappears.
   * @param {function} alertProps.onClose - Function will be called if the alert is closed.
   * @returns {UU5.Layout.alertBusContainer} this
   */
  setMessage: function (message, alertProps) {
    this.setMessage(message, alertProps);
    return this;
  },

  /**
   * Removes message by ID.
   *
   * @param {string} messageId - ID of deleted message.
   * @param {function} setStateCallback - Callback is called after setState.
   * @returns {UU5.Layout.alertBusContainer} this
   */
  removeMessage: function (messageId, setStateCallback) {
    this.alertBus.removeMessage(messageId, setStateCallback);
    return this;
  },

  // Overriding Functions

  // Component Specific Helpers
  _registerAlert: function (alertBus) {
    this.alertBus = alertBus;
    return this;
  },

  // Render
  render: function () {
    var mainAttrs = $.extend(true, { ref_: this._registerAlert }, this.getUU5_Common_baseMixinPropsToPass(), this.getUU5_Common_elementaryMixinPropsToPass(), this.getUU5_Layout_containerMixinPropsToPass());
    return React.createElement(UU5.Bricks.alertBus, mainAttrs);
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.DocKit = UU5.DocKit || {};

UU5.DocKit.component = React.createClass({
  displayName: 'component',

  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin],

  statics: {
    tagName: 'UU5.DocKit.component',
    isDummyLevel: true,
    classNames: {
      main: 'UU5_DocKit_component',
      previewBlock: 'UU5_DocKit_demo-previewBlock',
      previewIframe: 'UU5_DocKit_demo-previewIframe',
      sourceBlock: 'UU5_DocKit_demo-sourceBlock',
      sourcePre: 'UU5_DocKit_demo-sourcePre'
    },
    defaults: {
      blockStart: '\@\@viewOn:',
      blockEnd: '\@\@viewOff:'
    },
    lsi: {
      //TODO pointers to UU5.LSI ?!
      header: {
        en: "Component Definition",
        cs: "Definice komponenty",
        sk: "TODO-Translate please!!!"
      },
      intro: {
        en: "TODO-Translate please!!!",
        // cannot be used `` because grunt cannot translate this char for this moment
        cs: 'Komponenta je určena svými vlastnostmi (<UU5.Bricks.code>props</UU5.Bricks.code>) ' + 'a metodami svého rozhraní (<UU5.Bricks.code>interface</UU5.Bricks.code>). ' + 'Jednotlivé vlastnosti a metody rozhraní jsou komponentě buď přímo vestavěny ' + 'nebo je přejímá z mixinů (<UU5.Bricks.code>mixins</UU5.Bricks.code>), které splňuje. Důležitou součástí ' + 'definice komponenty ' + 'jsou i k ní vstažená statická data (<UU5.Bricks.code>statics</UU5.Bricks.code>), která mohou být rovněž ' + 'definována buďto přímo v komponentě a nebo v příslušném mixinu.',
        sk: "TODO-Translate please!!!"
      },
      mixinHeader: {
        en: "Mixins",
        cs: "Mixiny",
        sk: "TODO-Translate please!!!"
      },
      mixinIntro: {
        en: "TODO-Translate please!!!",
        cs: "Komponenta splňuje následující mixiny:",
        sk: "TODO-Translate please!!!"
      },
      mixinPostscript: {
        en: "TODO-Translate please!!!",
        cs: "Komponenta má všechny vlastnosti a metody rozhraní uvedených mixinů.",
        sk: "TODO-Translate please!!!"
      },
      mixinNoMixins: {
        en: "TODO-Translate please!!!",
        cs: "Komponenta nesplňuje žádné mixiny.",
        sk: "TODO-Translate please!!!"
      },
      staticsHeader: {
        en: "Static Data",
        cs: "Statická data",
        sk: "TODO-Translate please!!!"
      },
      staticsIntro: {
        en: "TODO-Translate please!!!",
        // cannot be used `` because grunt cannot translate this char for this moment
        cs: 'Staticka data komponenty ve framevorku <UU5.Bricks.linkUU5/> povinně obsahují klíč ' + '<UU5.Bricks.code>tagName</UU5.Bricks.code> se jménem komponenty, dále obvykle obsahují klíč ' + '<UU5.Bricks.code>layoutType</UU5.Bricks.code>, který určuje typ rozložení komponenty s ohledem na ' + 'responzivní design. (Neni-li uveden, je komponenta typu <UU5.Bricks.code>brick</UU5.Bricks.code>, ' + 'tedy základní stavební kámen vizuálních případů užití.)<br/>Dále statická data obvykle obsahují dle potřeby ' + 'klíče: <UU5.Bricks.code>defaults</UU5.Bricks.code> - pro definici výchozích hodnot ' + '<UU5.Bricks.code>classNames</UU5.Bricks.code> - pro definici potřebných <UU5.Bricks.linkCSS/> tříd komponenty,' + '<UU5.Bricks.code>errors</UU5.Bricks.code> a <UU5.Bricks.code>warnings</UU5.Bricks.code> ' + ' - pro definici potřebných chybových a varovných hlášek, ' + '<UU5.Bricks.code>lsi</UU5.Bricks.code> - pro definici jazykově senzitivního obsahu komponenty, pokud takový komponenta obsahuje. ' + 'Komponenta může obsahovat v případě potřeby další klíče. ' + 'Podrobný popis metod pro práci se statickými daty komponenty je uveden v mixinu <UU5.Bricks.link src={#}>UU5.Common.baseMixin</UU5.Bricks.link>.',
        sk: "TODO-Translate please!!!"
      },
      staticsNoStatics: {
        en: "TODO-Translate please!!!",
        cs: "Komponenta nemá žádná přímo vestavěná statická data.",
        sk: "TODO-Translate please!!!"
      },
      propsHeader: {
        en: "Props",
        cs: "Vlastnosti <small>(props)</small>",
        sk: "TODO-Translate please!!!"
      },
      propsIntro: {
        en: "TODO-Translate please!!!",
        cs: "Komponenta má přímo vestavěny následující vlastnosti:",
        sk: "TODO-Translate please!!!"
      },
      propTypesHeader: {
        en: "Prop Types",
        cs: "Typy vlastností komponenty",
        sk: "TODO-Translate please!!!"
      },
      propDefaultsHeader: {
        en: "Props",
        cs: "Výchozí hodnoty vlastností komponenty",
        sk: "TODO-Translate please!!!"
      },
      propsIntro2: {
        en: "TODO-Translate please!!!",
        cs: 'Vlastnosti komponety jsou předepsány podle standardu <UU5.Bricks.linkReact /> ' + 'jednak svými typy, a to pomocí metody <UU5.Bricks.code>propTypes</UU5.Bricks.code> ' + 'a dále svými výchozími hodnotami, a to pomocí metody <UU5.Bricks.code>getDefaultProps</UU5.Bricks.code>, ' + 'jak následuje.',
        sk: "TODO-Translate please!!!"
      },
      propsTableHeaderName: {
        en: "Name",
        cs: "Název",
        sk: "TODO-Translate please!!!"
      },
      propsTableHeaderDesc: {
        en: "Description",
        cs: "Popis",
        sk: "TODO-Translate please!!!"
      },
      interfaceHeader: {
        en: "Directly build-in interface",
        cs: "Vlastní rozhraní",
        sk: "TODO-Translate please!!!"
      },
      interfaceNoInterface: {
        en: "There is no directly build-in interface for this component.",
        cs: "Komponenta nemá žádné vlastní vestavěné rozhraní.",
        sk: "TODO-Translate please!!!"
      },
      interfaceSourceHeader: {
        en: "TODO-Translate please!!!",
        cs: "Zdrojový kód metod vlastního rozhraní",
        sk: "TODO-Translate please!!!"
      },
      interfaceTableHeaderName: {
        en: "Name",
        cs: "Název",
        sk: "TODO-Translate please!!!"
      },
      interfaceTableHeaderDesc: {
        en: "Description",
        cs: "Popis",
        sk: "TODO-Translate please!!!"
      }
    }
  },

  propTypes: {
    previewColorSchema: React.PropTypes.oneOf(UU5.Common.environment.colorSchema),
    sourceColorSchema: React.PropTypes.oneOf(UU5.Common.environment.colorSchema),
    noteColorSchema: React.PropTypes.oneOf(UU5.Common.environment.colorSchema),
    src: React.PropTypes.string,
    numbered: React.PropTypes.bool,
    blockKey: React.PropTypes.string,
    showSource: React.PropTypes.bool,
    showCompleteSource: React.PropTypes.bool,
    componentData: React.PropTypes.object
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      previewColorSchema: 'success',
      sourceColorSchema: 'info',
      noteColorSchema: 'grey',
      src: null,
      numbered: false,
      blockKey: null,
      showSource: false,
      showCompleteSource: false,
      componentData: null
    };
  },

  getInitialState: function () {
    return {
      sourceHidden: !this.props.showSource,
      showCompleteSource: this.props.showCompleteSource
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _getMainProps: function () {
    var mainProps = this.getMainPropsToPass(['UU5_Common_baseMixin', 'UU5_Common_elementaryMixin', 'UU5_Common_sectionMixin']);
    mainProps.content = null;
    mainProps.header = mainProps.header || this.getLSIComponent('header');
    mainProps.footer = mainProps.footer;
    return mainProps;
  },

  _getIntro: function () {
    return React.createElement(
      UU5.Bricks.p,
      null,
      this.getLSIComponent('intro')
    );
  },

  _getMixinsSection: function () {
    return this.props.componentData.mixins && this.props.componentData.mixins.length ? React.createElement(
      UU5.Bricks.section,
      { header: this.getLSIComponent('mixinHeader') },
      React.createElement(
        UU5.Bricks.p,
        null,
        this.getLSIComponent('mixinIntro')
      ),
      React.createElement(
        UU5.Bricks.ul,
        null,
        React.Children.toArray(this.props.componentData.mixins.map(function (v) {
          return React.createElement(
            UU5.Bricks.li,
            null,
            React.createElement(
              UU5.Bricks.link,
              { src: v.link },
              v.name
            )
          );
        }))
      ),
      this.props.componentData.mixinsDescLSI && React.createElement(UU5.Bricks.lsi, { lsi: this.props.componentData.mixinsDescLSI }),
      React.createElement(
        UU5.Bricks.p,
        null,
        this.getLSIComponent('mixinPostscript')
      )
    ) : React.createElement(
      UU5.Bricks.section,
      { header: this.getLSIComponent('mixinHeader') },
      React.createElement(
        UU5.Bricks.p,
        null,
        this.getLSIComponent('mixinNoMixins')
      )
    );
  },

  _getStaticsSection: function () {
    return React.createElement(
      UU5.Bricks.section,
      { header: this.getLSIComponent('staticsHeader') },
      this.getLSIComponent('staticsIntro'),
      React.createElement(
        UU5.DocKit.sourceCode,
        { header: 'Statics' },
        this.props.componentData.statics
      ),
      this.props.componentData.staticsDescLSI && React.createElement(UU5.Bricks.lsi, { lsi: this.props.componentData.staticsDescLSI })
    );
  },

  _getPropsSection: function () {
    return this.props.componentData.props && this.props.componentData.props.length ? React.createElement(
      UU5.Bricks.section,
      { header: this.getLSIComponent('propsHeader') },
      React.createElement(
        UU5.Bricks.p,
        null,
        this.getLSIComponent('propsIntro')
      ),
      React.createElement(
        UU5.Bricks.table,
        null,
        React.createElement(
          UU5.Bricks.thead,
          null,
          React.createElement(
            UU5.Bricks.tr,
            { className: 'bg-info' },
            React.createElement(
              UU5.Bricks.th,
              null,
              this.getLSIComponent('propsTableHeaderName')
            ),
            React.createElement(
              UU5.Bricks.th,
              null,
              this.getLSIComponent('propsTableHeaderDesc')
            )
          )
        ),
        React.createElement(
          UU5.Bricks.tbody,
          null,
          React.Children.toArray(this.props.componentData.props.map(function (v) {
            return React.createElement(
              UU5.Bricks.tr,
              null,
              React.createElement(
                UU5.Bricks.th,
                null,
                v.name
              ),
              React.createElement(
                UU5.Bricks.td,
                null,
                React.createElement(UU5.Bricks.lsi, { lsi: v.descLSI })
              )
            );
          }))
        )
      ),
      React.createElement(
        UU5.Bricks.p,
        null,
        this.getLSIComponent('propsIntro2')
      ),
      React.createElement(
        UU5.DocKit.sourceCode,
        { header: this.getLSIComponent('propTypesHeader') },
        this.props.componentData.propTypes
      ),
      React.createElement(
        UU5.DocKit.sourceCode,
        { header: this.getLSIComponent('propDefaultsHeader') },
        this.props.componentData.getDefaultProps
      ),
      this.props.componentData.propsDescLSI && React.createElement(UU5.Bricks.lsi, { lsi: this.props.componentData.propsDescLSI })
    ) : React.createElement(
      UU5.Bricks.section,
      { header: this.getLSIComponent('propsHeader') },
      React.createElement(
        UU5.Bricks.p,
        null,
        this.getLSIComponent('propsNoProps')
      )
    );
  },

  _getInterfaceSection: function () {
    return this.props.componentData.interface && this.props.componentData.interface.length ? React.createElement(
      UU5.Bricks.section,
      { header: this.getLSIComponent('interfaceHeader') },
      React.createElement(
        UU5.Bricks.p,
        null,
        this.getLSIComponent('interfaceIntro')
      ),
      React.createElement(
        UU5.Bricks.table,
        null,
        React.createElement(
          UU5.Bricks.thead,
          null,
          React.createElement(
            UU5.Bricks.tr,
            { className: 'bg-info' },
            React.createElement(
              UU5.Bricks.th,
              null,
              this.getLSIComponent('interfaceTableHeaderName')
            ),
            React.createElement(
              UU5.Bricks.th,
              null,
              this.getLSIComponent('interfaceTableHeaderDesc')
            )
          )
        ),
        React.createElement(
          UU5.Bricks.tbody,
          null,
          React.Children.toArray(this.props.componentData.interface.map(function (v) {
            return React.createElement(
              UU5.Bricks.tr,
              null,
              React.createElement(
                UU5.Bricks.td,
                null,
                v.name
              ),
              React.createElement(
                UU5.Bricks.td,
                null,
                React.createElement(UU5.Bricks.lsi, { lsi: v.descLSI })
              )
            );
          }))
        )
      ),
      this.props.componentData.interfaceDescLSI && React.createElement(UU5.Bricks.lsi, { lsi: this.props.componentData.interfaceDescLSI }),
      React.createElement(
        UU5.DocKit.sourceCode,
        { header: this.getLSIComponent('interfaceSourceHeader') },
        this.props.componentData.interfaceSource
      )
    ) : React.createElement(
      UU5.Bricks.section,
      { header: this.getLSIComponent('interfaceHeader') },
      React.createElement(
        UU5.Bricks.p,
        null,
        this.getLSIComponent('interfaceNoInterface')
      )
    );
  },

  // Render
  render: function () {
    return React.createElement(
      UU5.Bricks.section,
      this._getMainProps(),
      this._getIntro(),
      this._getMixinsSection(),
      this._getStaticsSection(),
      this._getPropsSection(),
      this._getInterfaceSection()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.DocKit = UU5.DocKit || {};

UU5.DocKit.demo = React.createClass({
  displayName: 'demo',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin, UU5.Common.lsiMixin],

  statics: {
    tagName: 'UU5.DocKit.demo',
    isDummyLevel: true,
    classNames: {
      main: 'UU5_DocKit_demo',
      previewBlock: 'UU5_DocKit_demo-previewBlock',
      previewIframe: 'UU5_DocKit_demo-previewIframe',
      sourceBlock: 'UU5_DocKit_demo-sourceBlock',
      sourcePre: 'UU5_DocKit_demo-sourcePre'
    },
    defaults: {
      blockStart: '\@\@viewOn:',
      blockEnd: '\@\@viewOff:'
    },
    lsi: {
      //TODO pointers to UU5.LSI ?!
      header: {
        en: "Example",
        cs: "Příklad",
        sk: "TODO-Translate please!!!"
      },
      previewHeader: {
        en: "Preview",
        cs: "Náhled",
        sk: "TODO-Translate please!!!"
      },
      completeSourceHeader: {
        en: "Source",
        cs: "Zdrojový kód <small>(celý)</small>",
        sk: "TODO-Translate please!!!"
      },
      blockSourceHeader: {
        en: "Source",
        cs: "Zdrojový kód <small>(vybraný blok)</small>",
        sk: "TODO-Translate please!!!"
      },
      noteHeader: {
        en: "Note",
        cs: "Poznámka",
        sk: "TODO-Translate please!!!"
      },
      tryMeButton: {
        en: "Try Me..",
        cs: "Zkus mě..",
        sk: "TODO-Translate please!!!"
      },
      tryMeButtonTitle: {
        en: "Translate please!!!",
        cs: "Stisknutím spustíte příklad v samostatném okně.",
        sk: "TODO-Translate please!!!"
      },
      sourceSwitch: {
        en: "TODO-Translate please!!!",
        cs: "Zdrojový kód",
        sk: "TODO-Translate please!!!"
      },
      sourceSwitchTitleOn: {
        en: "TODO-Translate please!!!",
        cs: "Vypnutím schováte zdrojový kód.",
        sk: "TODO-Translate please!!!"
      },
      sourceSwitchTitleOff: {
        en: "TODO-Translate please!!!",
        cs: "Zapnutím ukážete zdrojový kód.",
        sk: "TODO-Translate please!!!"
      },
      completeSourceSwitch: {
        en: "TODO-Translate please!!!",
        cs: "Celý kód",
        sk: "TODO-Translate please!!!"
      },
      completeSourceSwitchTitleOn: {
        en: "TODO-Translate please!!!",
        cs: "Vypnutím získáte pouze vybraný blok zdrojového kódu.",
        sk: "TODO-Translate please!!!"
      },
      completeSourceSwitchTitleOff: {
        en: "TODO-Translate please!!!",
        cs: "Zapnutím získáte celý zdrojový kód.",
        sk: "TODO-Translate please!!!"
      }
    }
  },

  propTypes: {
    previewColorSchema: React.PropTypes.oneOf(UU5.Common.environment.colorSchema),
    sourceColorSchema: React.PropTypes.oneOf(UU5.Common.environment.colorSchema),
    noteColorSchema: React.PropTypes.oneOf(UU5.Common.environment.colorSchema),
    src: React.PropTypes.string,
    numbered: React.PropTypes.bool,
    blockKey: React.PropTypes.string,
    showSource: React.PropTypes.bool,
    showCompleteSource: React.PropTypes.bool
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      previewColorSchema: 'success',
      sourceColorSchema: 'info',
      noteColorSchema: 'grey',
      src: null,
      numbered: false,
      blockKey: null,
      showSource: false,
      showCompleteSource: false
    };
  },

  getInitialState: function () {
    return {
      sourceHidden: !this.props.showSource,
      showCompleteSource: this.props.showCompleteSource
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _getMainProps: function () {
    var mainProps = this.getMainPropsToPass(['UU5_Common_baseMixin', 'UU5_Common_elementaryMixin', 'UU5_Common_sectionMixin']);
    mainProps.content = null;
    mainProps.header = mainProps.header || this.getLSIComponent('header');
    mainProps.footer = mainProps.footer || this._getFooter();
    return mainProps;
  },

  _getFooter: function () {
    return {
      element: React.createElement(
        UU5.Bricks.footer,
        {
          className: 'UU5_Common-textAlign-right'
        },
        this.props.src
      )
    };
  },

  _tryMeButtonHandler: function () {
    window.open(this.props.src, '_blank');
    return this;
  },

  _sourceSwitchHandler: function () {
    this.setState(function (state) {
      return {
        sourceHidden: !state.sourceHidden
      };
    });
    return this;
  },

  _completeSourceSwitchHandler: function () {
    this.setState(function (state) {
      return {
        showCompleteSource: !state.showCompleteSource
      };
    });
    return this;
  },

  // Render
  render: function () {
    return React.createElement(
      UU5.Bricks.section,
      this._getMainProps(),
      React.createElement(
        UU5.Bricks.section,
        {
          header: React.createElement(
            UU5.Bricks.div,
            null,
            React.createElement(UU5.Bricks.line, { colorSchema: this.props.noteColorSchema }),
            this.getLSIComponent('previewHeader')
          )
        },
        React.createElement(
          UU5.Bricks.block,
          {
            colorSchema: this.props.previewColorSchema,
            className: this.getClassName().previewBlock
          },
          React.createElement(UU5.Bricks.iframe, {
            src: this.props.src,
            className: this.getClassName().previewIframe
          })
        ),
        React.createElement('br', null),
        React.createElement(UU5.Bricks.button, {
          colorSchema: this.props.previewColorSchema,
          content: this.getLSIComponent('tryMeButton'),
          title: this.getLSIValue('tryMeButtonTitle'),
          onClick: this._tryMeButtonHandler
        }),
        ' ',
        React.createElement(
          UU5.Bricks.buttonGroup,
          null,
          React.createElement(UU5.Bricks.buttonSwitch, {
            onProps: {
              pressed: true,
              title: this.getLSIValue('sourceSwitchTitleOn')
            },
            offProps: {
              pressed: false,
              title: this.getLSIValue('sourceSwitchTitleOff')
            },
            props: {
              content: this.getLSIComponent('sourceSwitch'),
              colorSchema: this.props.sourceColorSchema,
              onClick: this._sourceSwitchHandler
            },
            switchOn: !this.state.sourceHidden
          }),
          React.createElement(UU5.Bricks.buttonSwitch, {
            onProps: {
              pressed: true,
              title: this.getLSIValue('completeSourceSwitchTitleOn')
            },
            offProps: {
              pressed: false,
              title: this.getLSIValue('completeSourceSwitchTitleOff')
            },
            props: {
              content: this.getLSIComponent('completeSourceSwitch'),
              colorSchema: this.props.sourceColorSchema,
              onClick: this._completeSourceSwitchHandler,
              disabled: this.state.sourceHidden
            },
            switchOn: this.state.showCompleteSource
          })
        )
      ),
      React.createElement(
        UU5.Bricks.section,
        {
          header: React.createElement(
            UU5.Bricks.div,
            null,
            React.createElement(UU5.Bricks.line, { colorSchema: this.props.noteColorSchema }),
            this.state.showCompleteSource ? this.getLSIComponent('completeSourceHeader') : this.getLSIComponent('blockSourceHeader')
          ),
          hidden: this.state.sourceHidden
        },
        React.createElement(
          UU5.Bricks.block,
          {
            colorSchema: this.props.sourceColorSchema,
            className: this.getClassName().sourceBlock
          },
          React.createElement(
            UU5.Bricks.pre,
            { className: this.getClassName().sourcePre },
            React.createElement(UU5.Bricks.fileViewer, {
              src: this.props.src,
              numbered: this.props.numbered,
              blockKey: !this.state.showCompleteSource ? this.props.blockKey : null
            })
          )
        )
      ),
      React.createElement(
        UU5.Bricks.section,
        {
          header: React.createElement(
            UU5.Bricks.div,
            null,
            React.createElement(UU5.Bricks.line, { colorSchema: this.props.noteColorSchema }),
            this.getLSIComponent('noteHeader')
          ),
          hidden: !this.getChildren()
        },
        this.getChildren()
      )
    );
  }
});

'use strict';

var UU5 = UU5 || {};

UU5.DocKit = UU5.DocKit || {};
UU5.DocKit.factory = UU5.DocKit.factory || {};
'use strict';

var UU5 = UU5 || {};
UU5.DocKit = UU5.DocKit || {};

UU5.DocKit.example = React.createClass({
  displayName: 'example',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin],

  statics: {
    tagName: 'UU5.DocKit.example',
    isDummyLevel: true,
    classNames: {
      main: 'UU5_DocKit_example',
      block: 'UU5_DocKit_example-block'
    }
  },

  propTypes: {
    colorSchema: React.PropTypes.oneOf(UU5.Common.environment.colorSchema)
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      colorSchema: 'info'
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _getMainProps: function () {
    var mainProps = this.getMainPropsToPass(['UU5_Common_baseMixin', 'UU5_Common_elementaryMixin', 'UU5_Common_sectionMixin']);

    // because props of contentMixin is returned from selectionMixinPropsToPass, but content is not needed here
    mainProps.content = null;

    mainProps.id = this.getId() + '-section';

    return mainProps;
  },

  _getBlockProps: function () {
    return {
      id: this.getId() + '-body',
      className: this.getClassName().block,
      content: this.getContent(),
      colorSchema: this.props.colorSchema
    };
  },

  // Render
  render: function () {
    return React.createElement(
      UU5.Bricks.section,
      this._getMainProps(),
      React.createElement(
        UU5.Bricks.block,
        this._getBlockProps(),
        React.Children.toArray(this.props.children)
      )
    );
  }
});

'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var UU5 = UU5 || {};
UU5.DocKit = UU5.DocKit || {};

UU5.DocKit.sourceCode = React.createClass({

  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin],

  statics: {
    tagName: 'UU5.DocKit.sourceCode',
    isDummyLevel: true,
    classNames: {
      main: 'UU5_DocKit_sourceCode',
      pre: 'UU5_DocKit_sourceCode-pre'
    }
  },

  propTypes: {
    colorSchema: React.PropTypes.oneOf(UU5.Common.environment.colorSchema),
    src: React.PropTypes.string,
    blockKey: React.PropTypes.string,
    numbered: React.PropTypes.bool
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      colorSchema: 'info',
      src: null,
      blockKey: null,
      numbered: false
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _getMainProps: function () {
    var mainProps = this.getMainPropsToPass(['UU5_Common_baseMixin', 'UU5_Common_elementaryMixin', 'UU5_Common_sectionMixin']);

    // because props of contentMixin is returned from selectionMixinPropsToPass, but content is not needed here
    mainProps.content = null;

    mainProps.id = this.getId() + '-example';

    return mainProps;
  },

  _getPreProps: function () {
    return {
      id: this.getId() + '-pre',
      className: this.getClassName().pre,
      content: this.getContent(),
      ignoreInnerHTML: true
    };
  },

  _getChildren: function () {
    var children;
    if (this.props.src) {
      children = React.createElement(UU5.Bricks.fileViewer, {
        src: this.props.src,
        blockKey: this.props.blockKey,
        numbered: this.props.numbered
      });
    } else {
      children = React.Children.toArray(this.props.children);
    }
    return children;
  },

  // Render
  render: function () {
    return React.createElement(
      UU5.DocKit.example,
      _extends({}, this._getMainProps(), { colorSchema: this.props.colorSchema }),
      React.createElement(
        UU5.Bricks.pre,
        this._getPreProps(),
        this._getChildren()
      )
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Forms = UU5.Forms || {};

UU5.Forms.formMixin = {

  statics: {
    'UU5_Forms_formMixin': {
      warnings: {
        formInForm: 'Form control %s should not be placed in other form control %s.',
        nonRegistered: 'Form control with ID %s cannot be unregistered. Component with the ID is not registered.',
        noName: 'Form control has not any name. It will be used its ID %s.'
      },
      errors: {
        duplicateId: 'Duplicate id \'%s\' of a form control.'
      }
    }
  },

  // React lifecycle
  componentWillMount: function () {
    this.formControls = {};

    var parentForm = this.getParentByType('isForm');
    if (parentForm) {
      this.showWarning('formInForm', [this.getTagName(), parentForm.getTagName()], {
        mixinName: 'UU5_Forms_formMixin'
      });
    }
  },

  // Interface
  eachFormControl: function (func) {
    for (var id in this.formControls) {
      var result = func(this.formControls[id]);
      if (result === false) {
        break;
      }
    }
    return this.formControls;
  },

  isValid: function () {
    var result = true;

    if (typeof this.isValid_ === 'function') {
      result = this.isValid_();
    } else {
      this.eachFormControl(function (formControl) {
        var newResult = typeof formControl.isValid !== 'function' || formControl.isValid();

        if (result) {
          result = newResult;
        }
      });
    }

    return result;
  },

  // for our parent type checking
  isForm: function () {
    return true;
  },

  registerFormControl: function (id, formControl) {
    var registeredControl = this.formControls[id];
    if (registeredControl) {
      this.showError('duplicateId', id, {
        mixinName: 'UU5_Forms_formMixin',
        context: {
          registeredFormControl: {
            tagName: registeredControl.getTagName(),
            props: registeredControl.props,
            component: registeredControl
          },
          newFormControl: {
            tagName: formControl.getTagName(),
            props: formControl.props,
            component: formControl
          }
        }
      });
    } else {
      this.formControls[id] = formControl;
    }
  },

  unregisterFormControl: function (id) {
    if (!this.formControls[id]) {
      this.showWarning('nonRegistered', id, {
        mixinName: 'UU5_Forms_formMixin'
      });
    } else {
      delete this.formControls[id];
    }
  },

  getValues: function () {
    var values = {};
    this._eachFormControlWithName(function (name, input) {
      values[name] = input.getValue();
    });
    return values;
  },

  getInputs: function () {
    var inputs = {};
    this._eachFormControlWithName(function (name, input) {
      inputs[name] = input;
    });
    return inputs;
  },

  getInputByName: function (name) {
    var result = null;

    this._eachFormControlWithName(function (k, input) {
      var compare = k === name;
      compare && (result = input);
      return !compare
    });

    return result;
  },

  reset: function (setStateCallback) {
    var form = this;

    var counter = 0;
    for (var id in this.formControls) {
      typeof this.formControls[id].reset === 'function' && counter++;
    }

    var newSetStateCallback = this.buildCounterCallback(setStateCallback, counter);

    for (var id in this.formControls) {
      typeof form.formControls[id].reset === 'function' && form.formControls[id].reset(newSetStateCallback);
    }

    return this;
  },

  // Overriding Functions

  // Component Specific Helpers

  _eachFormControlWithName: function (func) {
    var form = this;
    this.eachFormControl(function (input) {
      var name = input.getName();
      if (!name) {
        form.showWarning('noName', input.getId(), {
          mixinName: 'UU5_Forms_formMixin',
          context: {
            input: {
              tagName: input.getTagName(),
              props: input.props,
              component: input
            }
          }
        });
        name = input.getId();
      }
      func(name, input);
    });
    return this;
  }
};

/**
 * UU5.Forms.inputMixin
 */

'use strict';

var UU5 = UU5 || {};
UU5.Forms = UU5.Forms || {};

UU5.Forms.inputMixin = {
  statics: {
    UU5_Forms_inputMixin: {
      classNames: {
        main: 'UU5_Forms_input form-group',
        label: 'UU5_Forms_input-label control-label',
        inputWrapper: 'UU5_Forms_input-wrapper',
        input: 'UU5_Forms_input-input form-control',
        message: 'UU5_Forms_input-message',
        glyphicon: "UU5_Forms_input-glyphicon form-control-feedback",
        size: 'UU5_Forms_input-size-',
        hidden: 'UU5_Common-hidden',
        infoMessage: 'info'
      }
    }
  },

  propTypes: {
    label: React.PropTypes.string,
    message: React.PropTypes.string,
    feedback: React.PropTypes.oneOf(['initial', 'error', 'warning', 'success']),

    size: React.PropTypes.oneOf(['lg', 'md', 'sm']),

    onChange: React.PropTypes.func,
    onValidate: React.PropTypes.func,
    onChangeFeedback: React.PropTypes.func,

    successGlyphicon: React.PropTypes.string,
    warningGlyphicon: React.PropTypes.string,
    errorGlyphicon: React.PropTypes.string,

    labelColWidth: React.PropTypes.number,
    inputColWidth: React.PropTypes.number,

    labelAttrs: React.PropTypes.object,
    inputWrapperAttrs: React.PropTypes.object,
    inputAttrs: React.PropTypes.object,
    messageAttrs: React.PropTypes.object
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      label: null,
      message: null,
      feedback: 'initial',
      size: 'md',
      onChange: null,
      onValidate: null,
      successGlyphicon: 'uu-glyphicon-ok-circle',
      warningGlyphicon: 'uu-glyphicon-alert-circle',
      errorGlyphicon: 'uu-glyphicon-error-circle',
      labelColWidth: 3,
      inputColWidth: 9,
      labelAttrs: null,
      inputWrapperAttrs: null,
      inputAttrs: null,
      messageAttrs: null
    };
  },

  getInitialState: function () {
    return {
      message: this.props.message,
      feedback: this.props.feedback,
      value: this.props.value
    };
  },

  componentWillMount: function () {
    var newState = this._validateValue(this.props.value, true);
    if (newState) {
      if (newState.feedback) {
        this._setFeedback(newState.feedback, newState.message, newState.value);
      } else {
        this.setState(newState);
      }
    }
  },

  componentDidMount: function () {
    var parentForm = this._getForm();
    parentForm && parentForm.registerFormControl(this.getId(), this);
  },

  componentWillReceiveProps: function (nextProps) {
    if (nextProps.id && nextProps.id !== this.props.id) {
      var parentForm = this._getForm();
      if (parentForm) {
        parentForm.unregisterFormControl(this.props.id);
        parentForm.registerFormControl(nextProps.id, this);
      }
    }

    var newState = {};
    nextProps.message !== undefined && (newState.message = nextProps.message);
    nextProps.feedback !== undefined && (newState.feedback = nextProps.feedback);

    var value;
    nextProps.value !== undefined && (value = nextProps.value);

    if (value !== undefined) {
      this.setValue(value, null, newState);
    } else if (Object.keys(newState)) {
      this.setState(newState);
    }
  },

  componentWillUnmount: function () {
    var parentForm = this._getForm();
    parentForm && parentForm.unregisterFormControl(this.getId());
  },

  // Interface
  isInput: function () {
    return true;
  },

  getValue: function () {
    var value;
    if (typeof this.getValue_ === 'function') {
      value = this.getValue_();
    } else {
      value = this.state.value;
    }
    return value;
  },

  setValue: function (value, setStateCallback, options) {
    options && options.feedback === '' && (options.feedback = 'initial');

    if (typeof this.setTextValue_ === 'function') {
      this.setTextValue_(value, setStateCallback, options);
    } else if (typeof this.setValue_ === 'function') {
      this.setValue_(value, setStateCallback, options);
    } else {
      options = options || {};
      options.value = value;
      this.setState(options, setStateCallback);
    }
    return this;
  },

  getFeedback: function () {
    return this.state.feedback;
  },

  setFeedback: function (feedback, msg, value, setStateCallback) {
    this.setValue(value === undefined ? this.getValue() : value, setStateCallback, {
      message: msg,
      feedback: feedback
    });

    return this;
  },

  setError: function (msg, value, setStateCallback) {
    if (typeof this.setError_ === 'function') {
      this.setError_(msg, value, setStateCallback);
    } else {
      this._setFeedback('error', msg, value, setStateCallback);
    }
    return this;
  },

  isError: function () {
    return this.getFeedback() === 'error';
  },

  setWarning: function (msg, value, setStateCallback) {
    if (typeof this.setWarning_ === 'function') {
      this.setWarning_(msg, value, setStateCallback);
    } else {
      this._setFeedback('warning', msg, value, setStateCallback);
    }
    return this;
  },

  isWarning: function () {
    return this.getFeedback() === 'warning';
  },

  setSuccess: function (msg, value, setStateCallback) {
    if (typeof this.setSuccess_ === 'function') {
      this.setSuccess_(msg, value, setStateCallback);
    } else {
      this._setFeedback('success', msg, value, setStateCallback);
    }
    return this;
  },

  isSuccess: function () {
    return this.getFeedback() === 'success';
  },

  setInitial: function (msg, value, setStateCallback) {
    if (typeof this.setInitial_ === 'function') {
      this.setInitial_(msg, value, setStateCallback);
    } else {
      this._setFeedback('initial', msg, value, setStateCallback);
    }
    return this;
  },

  isInitial: function () {
    return this.getFeedback() === 'initial';
  },

  getMessage: function () {
    return this.state.message;
  },

  setStateProps: function (options, setStateCallback) {
    var newState = {};

    var value = options.value !== undefined ? options.value : this.state.value;
    options.feedback && (newState.feedback = options.feedback);
    options.message !== undefined && (newState.message = options.message);
    options.disabled !== undefined && (newState.disabled = options.disabled);
    options.hide !== undefined && (newState.hide = options.hide);

    Object.keys(newState).length > 0 && this.setValue(value, setStateCallback, newState);

    return this;
  },

  getResetNewState: function () {
    var newState = this._validateValue(this.props.value, true);

    newState = newState || {
      value: this.props.value,
      message: this.props.message,
      feedback: this.props.feedback,
      disabled: this.props.disabled,
      hidden: this.props.hidden
    };

    return newState;
  },

  reset: function (setStateCallback) {
    if (typeof this.reset_ === 'function') {
      this.reset_(setStateCallback);
    } else {
      this.setState(this.getResetNewState(), setStateCallback);
    }
    return this;
  },

  getInputMainAttrs: function () {
    var mainAttrs = this.buildMainAttrs();

    mainAttrs.className += ' ' + this.getClassName('main', 'UU5_Forms_inputMixin');
    this.props.size && (mainAttrs.className += ' ' + this.getClassName('size', 'UU5_Forms_inputMixin') + this.props.size);

    return mainAttrs;
  },

  getInputTagAttrs: function () {
    var inputAttrs = this.props.inputAttrs ? $.extend(true, {}, this.props.inputAttrs) : {};
    inputAttrs.id = this.getId();
    inputAttrs.name = this.getName();
    this.isDisabled() && (inputAttrs.disabled = 'disabled');

    inputAttrs.onChange = this._onChange;

    inputAttrs.ref = function (input) {
      this.input = input;
    }.bind(this);

    return inputAttrs;
  },

  getMessageAttrs: function () {
    var msgAttrs = this.props.messageAttrs ? $.extend(true, {}, this.props.messageAttrs) : {};
    var msgClassName = this.getClassName('message', 'UU5_Forms_inputMixin');
    msgClassName += ' bg-' + (this.isInitial() ? this.getClassName('infoMessage', 'UU5_Forms_inputMixin') : this.getFeedback());
    // fixing BootStrap inconsistency
    msgClassName = msgClassName.replace('bg-error', 'bg-danger');

    msgAttrs.className = msgAttrs.className ? msgClassName + ' ' + msgAttrs.className : msgClassName;
    return msgAttrs;
  },

  getMessageChild: function (msgAttrs) {
    var result = null;
    var message = this.getMessage();

    if (message) {
      msgAttrs = msgAttrs || this.getMessageAttrs();
      result = React.createElement('div', msgAttrs, message);
    }

    return result;
  },

  getLabelAttrs: function () {
    var labelAttrs = this.props.labelAttrs ? $.extend(true, {}, this.props.labelAttrs) : {};
    var labelClassName = this.getClassName('label', 'UU5_Forms_inputMixin') + ' col-sm-' + this.props.labelColWidth;
    labelAttrs.className = labelAttrs.className ? labelClassName + ' ' + labelAttrs.className : labelClassName;
    labelAttrs.htmlFor = this.getId();

    return labelAttrs;
  },

  getLabelChild: function (labelAttrs) {
    var result = null;

    if (this.props.label !== null) {
      labelAttrs = labelAttrs || this.getLabelAttrs();
      result = React.createElement('label', labelAttrs, this.props.label);
    }

    return result;
  },

  getGlyphiconProps: function () {
    var glyphiconProps = null;

    var glyphicon;
    switch (this.getFeedback()) {
      case 'error':
        glyphicon = this.props.errorGlyphicon;
        break;
      case 'success':
        glyphicon = this.props.successGlyphicon;
        break;
      case 'warning':
        glyphicon = this.props.warningGlyphicon;
        break;
      default:
        this.props.isRequired && (glyphicon = this.props.successGlyphicon);
        break;
    }

    if (glyphicon) {
      glyphiconProps = {
        glyphicon: glyphicon,
        className: this.getClassName('glyphicon', 'UU5_Forms_inputMixin')
      };
    }

    return glyphiconProps;
  },

  getGlyphiconChild: function (glyphiconProps) {
    glyphiconProps === undefined && (glyphiconProps = this.getGlyphiconProps());
    return glyphiconProps && React.createElement(UU5.Bricks.glyphicon, glyphiconProps);
  },

  getInputWrapperAttrs: function () {
    var inputWrapperAttrs = this.props.inputWrapperAttrs ? $.extend(true, {}, this.props.inputWrapperAttrs) : {};
    var inputColWidth = this.props.label === null ? 12 : this.props.inputColWidth;
    var inputWrapperClassName = this.getClassName('inputWrapper', 'UU5_Forms_inputMixin') + ' col-sm-' + inputColWidth;
    inputWrapperAttrs.className = inputWrapperAttrs.className ? inputWrapperClassName + ' ' + inputWrapperAttrs.className : inputWrapperClassName;
    return inputWrapperAttrs;
  },

  // Component Specific Helpers
  _getForm: function () {
    var form = null;
    var parent = this.getParent && this.getParent();
    while (parent) {
      if (typeof parent.isInput === 'function' && parent.isInput()) {
        break;
      } else if (typeof parent.isForm === 'function' && parent.isForm()) {
        form = parent;
        break;
      } else {
        parent = parent.getParent && parent.getParent();
      }
    }
    return form;
  },

  _setFeedback: function (feedback, msg, value, setStateCallback) {
    if (typeof this.props.onChangeFeedback === 'function') {
      this.props.onChangeFeedback({
        feedback: feedback,
        message: msg,
        value: value,
        callback: setStateCallback,
        component: this
      });
    } else {
      this.setFeedback(feedback, msg, value, setStateCallback);
    }
    return this;
  },

  _validateValue: function (value, init) {
    var result = null;
    var validateResult = null;

    if (typeof this.validate_ === 'function') {
      validateResult = this.validate_(value, init);
    }

    if (!validateResult && this.props.onValidate) {
      var params = { value: value, component: this };

      /*
       * @deprecated input since 03.10. Use component instead of this.
       */
      params.input = this;

      validateResult = this.props.onValidate(params);
    }

    if (validateResult && typeof validateResult === 'object') {
      result = {};

      if (validateResult.feedback && (!init || validateResult.feedback !== 'success')) {
        result.feedback = validateResult.feedback;
      }
      if (validateResult.message !== undefined) {
        result.message = validateResult.message;
      }
      if (validateResult.value !== undefined) {
        result.value = validateResult.value;
      } else {
        result.value = value;
      }
    }

    return result;
  }
};

/**
 * UU5.Forms.textInputMixin
 */

'use strict';

var UU5 = UU5 || {};
UU5.Forms = UU5.Forms || {};

UU5.Forms.textInputMixin = {

  mixins: [UU5.Forms.inputMixin],

  statics: {
    UU5_Forms_textInputMixin: {
      classNames: {
        main: 'UU5_Forms_textInput',
        readOnly: 'UU5_Forms_textInput-readOnly',
        hasAutoComplete: 'UU5_Forms_textInput-hasAutoComplete'
      }
    }
  },

  propTypes: {
    placeholder: React.PropTypes.string,
    isReadOnly: React.PropTypes.bool,
    isRequired: React.PropTypes.bool,
    requiredMessage: React.PropTypes.string,
    autoCompleteItems: React.PropTypes.arrayOf(React.PropTypes.shape({
      value: React.PropTypes.string.isRequired,
      params: React.PropTypes.object,
      content: React.PropTypes.any
    })),
    onFocus: React.PropTypes.func,
    onBlur: React.PropTypes.func
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      placeholder: null,
      isRequired: false,
      isReadOnly: false,
      requiredMessage: 'This field is required.',
      autoCompleteItems: null,
      onFocus: null,
      onBlur: null
    };
  },

  getInitialState: function () {
    return {
      isReadOnly: this.props.isReadOnly
    };
  },

  componentWillReceiveProps: function (nextProps) {
    nextProps.isReadOnly !== undefined && this.setState({ isReadOnly: nextProps.isReadOnly });
  },

  // Interface
  isValid: function () {
    var feedback = this.getFeedback();
    var value = this.getValue();
    var result = true;

    if (this.props.isRequired && (value === '' || value === null)) {
      this.setError(this.props.requiredMessage);
      result = false;
    } else if (feedback === 'error') {
      result = false;
    } else if (typeof this.isValid_ === 'function') {
      result = this.isValid_();
    }

    if (result && this.props.onValidate) {
      var validation = this.props.onValidate(value, this);
      if (validation && typeof validation === 'object') {
        if (validation.feedback === 'error') {
          result = false;
        }
      }
    }

    return result;
  },

  readOnly: function (setStateCallback) {
    this.setState({ isReadOnly: true }, setStateCallback);
    return this;
  },

  editable: function (setStateCallback) {
    this.setState({ isReadOnly: false }, setStateCallback);
    return this;
  },

  isReadOnly: function () {
    return this.state.isReadOnly;
  },

  // Overriding Functions

  setTextValue_: function (value, setStateCallback, options) {
    if (typeof this.setValue_ === 'function') {
      this.setValue_(value, setStateCallback, options);
    } else if (options && typeof options === 'object' && Object.keys(options).length) {
      options.value = value;
      this.setState(options, setStateCallback);
    } else {
      var requiredOptions = this._checkRequired(value);

      if (requiredOptions && typeof requiredOptions === 'object' && requiredOptions.feedback === 'error') {
        this.setError(requiredOptions.message, value, setStateCallback);
      } else {
        var newState = this._validateValue(value);

        if (newState) {
          if (newState.feedback) {
            this._setFeedback(newState.feedback, newState.message, newState.value, setStateCallback);
          } else {
            this.setState(newState, setStateCallback);
          }
        } else {
          newState = requiredOptions || { feedback: 'initial', message: '' };
          newState.value = value;
          if (newState.feedback) {
            this._setFeedback(newState.feedback, newState.message, newState.value, setStateCallback);
          } else {
            this.setState(newState, setStateCallback);
          }
        }
      }
    }

    return this;
  },

  getInput: function () {
    return this.input;
  },

  getTextInputMainAttrs: function () {
    var mainAttrs = this.getInputMainAttrs();

    mainAttrs.className += ' ' + this.getClassName('main', 'UU5_Forms_textInputMixin');
    this.props.autoCompleteItems && (mainAttrs.className += ' ' + this.getClassName('hasAutoComplete', 'UU5_Forms_textInputMixin'));

    var feedbackClass = this.isInitial() ? this.props.isRequired ? 'required' : null : this.getFeedback();
    feedbackClass && (mainAttrs.className += ' has-' + feedbackClass + ' has-feedback');
    this.isReadOnly() && (mainAttrs.className += ' ' + this.getClassName('readOnly', 'UU5_Forms_textInputMixin'));

    return mainAttrs;
  },

  getInputAttrs: function () {
    var textInput = this;
    var inputAttrs = this.getInputTagAttrs();
    inputAttrs.type = 'text';
    inputAttrs.placeholder = this.props.placeholder;
    inputAttrs.readOnly = this.isReadOnly();

    var inputClassName = this.getClassName('input', 'UU5_Forms_inputMixin');
    this.props.size && (inputClassName += ' input-' + this.props.size);
    inputAttrs.className = inputClassName + (inputAttrs.className ? ' ' + inputAttrs.className : '');

    inputAttrs.onFocus = this._onFocus;
    inputAttrs.onBlur = this._onBlur;

    // reset text input setting for submit on enter
    var customOnKeyPress = inputAttrs.onKeyPress;
    inputAttrs.onKeyPress = function (e) {
      // stop submit form on enter if input is alone in form
      if (e.keyCode == 13 || e.charCode == 13) {
        e.preventDefault();
        textInput.autoComplete && textInput.autoComplete.confirmSelected();
      }
      if (typeof customOnKeyPress === 'function') {
        customOnKeyPress(e);
      }
    };

    if (textInput.autoComplete) {
      var customOnKeyDown = inputAttrs.onKeyDown;
      inputAttrs.onKeyDown = function (e) {
        switch (e.keyCode) {
          case 27:
            e.preventDefault();
            textInput.autoComplete.close();
            break;
          case 38:
            e.preventDefault();
            textInput.autoComplete.selectUp();
            break;
          case 39:
            textInput.autoComplete.find(e.target.value);
            break;
          case 40:
            e.preventDefault();
            textInput.autoComplete.selectDown();
            break;
        }

        if (typeof customOnKeyDown === 'function') {
          customOnKeyDown(e);
        }
      };
    }

    return inputAttrs;
  },

  getTextInputAttrs: function () {
    var inputAttrs = this.getInputAttrs();
    inputAttrs.value = this.getValue();
    inputAttrs.value === null && (inputAttrs.value = '');
    return inputAttrs;
  },

  getAutoComplete: function () {
    var autoComplete;

    if (this.props.autoCompleteItems) {
      autoComplete = React.createElement(UU5.Forms.autoComplete, {
        ref_: this._refAutoComplete,
        items: this.props.autoCompleteItems,
        onClick: this._onAutoCompleteClick
      });
    }

    return autoComplete;
  },

  // Component Specific Helpers
  _checkRequired: function (value) {
    var options = null;

    if (this.props.isRequired) {
      if (value === '' || value === null) {
        options = { feedback: 'error', message: this.props.requiredMessage, value: value };
      } else if (typeof this.props.onChange !== 'function') {
        options = { feedback: 'success', message: '', value: value };
      }
    }

    return options;
  },

  _onChange: function (e) {
    if (!this.isDisabled()) {
      if (this.autoComplete) {
        this.autoComplete.find(e.target.value);
      }

      if (typeof this.onChange_ === 'function') {
        this.onChange_(e);
      } else {
        var value = e.target.value;

        if (typeof this.props.onChange === 'function') {
          var options = this._checkRequired(value);

          if (options && typeof options === 'object' && options.feedback === 'error') {
            this.setError(options.message, value);
          } else {
            var newState = this._validateValue(value);

            if (newState) {
              if (newState.feedback) {
                this._setFeedback(newState.feedback, newState.message, newState.value);
              } else {
                this.setState(newState);
              }
            } else {
              var params = { value: value, component: this, event: e, options: options };

              /*
               * @deprecated input since 03.10. Use component instead of this.
               */
              params.input = this;

              this.props.onChange(params);
            }
          }
        } else {
          this.setValue(value);
        }
      }
    }

    return this;
  },

  _onFocus: function (e) {
    if (typeof this.onFocus_ === 'function') {
      this.onFocus_(e);
    } else {
      var params = { value: this.getValue(), component: this, event: e };

      /*
       * @deprecated input since 03.10. Use component instead of this.
       */
      params.input = this;

      typeof this.props.onFocus === 'function' && this.props.onFocus(params);
    }
    return this;
  },

  _onBlur: function (e) {
    if (this.onBlur_) {
      this.onBlur_(e);
    } else {
      var params = { value: this.getValue(), component: this, event: e };

      /*
       * @deprecated input since 03.10. Use component instead of this.
       */
      params.input = this;

      typeof this.props.onBlur === 'function' && this.props.onBlur(params);
    }
    return this;
  },

  _refAutoComplete: function (autoComplete) {
    this.autoComplete = autoComplete;
    return this;
  },

  _onAutoCompleteClick: function (opt) {
    if (typeof this.props.onChange === 'function') {
      var params = { value: opt.value, component: this, item: opt.item };

      /*
       * @deprecated input since 03.10. Use component instead of this.
       */
      params.input = this;

      this.props.onChange(params);

      this.getInput().focus();
    } else {
      var textInput = this;
      this.setValue(opt.value, function () {
        textInput.getInput().focus();
      });
    }
    return this;
  }
};

'use strict';

var UU5 = UU5 || {};
UU5.Forms = UU5.Forms || {};

UU5.Forms.autoComplete = React.createClass({
  displayName: 'autoComplete',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Forms.autoComplete',
    classNames: {
      main: 'UU5_Forms_autoComplete',
      open: 'UU5_Forms_autoComplete-opened',
      menu: 'UU5_Forms_autoComplete-menu list-group',
      item: 'UU5_Forms_autoComplete-item list-group-item',
      selected: 'UU5_Forms_autoComplete-selected'
    }
  },

  propTypes: {
    items: React.PropTypes.arrayOf(React.PropTypes.shape({
      value: React.PropTypes.string.isRequired,
      params: React.PropTypes.object,
      content: React.PropTypes.any
    })),
    onClick: React.PropTypes.func
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      items: null,
      onClick: null
    };
  },

  getInitialState: function () {
    return {
      items: null,
      selectedIndex: null
    };
  },

  // Interface
  find: function (foundValue, callback) {
    var values = { first: [], last: [] };
    this.props.items.forEach(function (item) {
      if (foundValue !== '') {
        if (new RegExp('^' + foundValue, 'i').exec(item.value)) {
          values.first.push(item);
        } else if (new RegExp(foundValue, 'gi').exec(item.value)) {
          values.last.push(item);
        }
      }
    });
    var allValues = values.first.concat(values.last);
    this.setState({ items: allValues.length ? allValues : null, selectedIndex: null }, callback);
    return this;
  },

  close: function (callback) {
    this.setState({ items: null, selectedIndex: null }, callback);
    return this;
  },

  isOpened: function () {
    return !!this.state.items;
  },

  selectUp: function (setStateCallback) {
    var autoComplete = this;
    this.setState(function (state) {
      var index = null;
      if (autoComplete.state.items) {
        if (state.selectedIndex == null || state.selectedIndex === 0) {
          index = autoComplete.state.items.length - 1;
        } else {
          index = state.selectedIndex - 1;
        }
      }

      var offset = index > autoComplete.state.items.length - 3 ? $('#' + autoComplete.getId() + '-item-' + (autoComplete.state.items.length - 1))[0].offsetTop : index - 1 > 0 ? $('#' + autoComplete.getId() + '-item-' + (index - 1))[0].offsetTop : 0;
      $('#' + this.getId() + '-menu').animate({ scrollTop: offset }, 0);

      return { selectedIndex: index };
    }, setStateCallback);
    return this;
  },

  selectDown: function (setStateCallback) {
    var autoComplete = this;
    this.setState(function (state) {
      var index = null;
      var newState;
      if (autoComplete.state.items) {
        if (state.selectedIndex == null || state.selectedIndex === autoComplete.state.items.length - 1) {
          index = 0;
        } else {
          index = state.selectedIndex + 1;
        }

        var offset = index > 1 ? $('#' + autoComplete.getId() + '-item-' + (index - 1))[0].offsetTop : 0;
        $('#' + autoComplete.getId() + '-menu').animate({ scrollTop: offset }, 0);

        newState = { selectedIndex: index };
      } else {
        newState = {
          items: autoComplete.props.items.sort(function (a, b) {
            if (a < b) return -1;
            if (a > b) return 1;
            return 0;
          }), selectedIndex: 0
        };
      }

      return newState;
    }, setStateCallback);

    return this;
  },

  confirmSelected: function (setStateCallback) {
    this.state.items && this._confirm(this.state.items[this.state.selectedIndex], this.state.selectedIndex, setStateCallback);
    return this;
  },

  // Overriding Functions

  // Component Specific Helpers
  _getBackdropProps: function () {
    var backdropId = this.getId() + "-backdrop";

    return {
      hidden: !this.isOpened(),
      id: backdropId,
      onClick: function (event) {
        event.target.id === backdropId && this.close();
      }.bind(this)
    };
  },

  _onClick: function (value, i) {
    this._confirm(value, i);
    return this;
  },

  _confirm: function (item, i, setStateCallback) {
    if (typeof this.props.onClick === 'function') {
      this.props.onClick({ value: item.value, item: item, index: i, component: this });
    }
    this.close(setStateCallback);
    return this;
  },

  _getChildren: function () {
    var autoComplete = this;
    return this.state.items && this.state.items.map(function (item, i) {
      var className = autoComplete.getClassName().item;
      autoComplete.state.selectedIndex === i && (className += ' ' + autoComplete.getClassName().selected);
      return React.createElement(UU5.Bricks.link, {
        className: className,
        key: i,
        content: item.content || item.value,
        onClick: autoComplete._onClick.bind(autoComplete, item, i),
        mainAttrs: { id: autoComplete.getId() + '-item-' + i }
      });
    });
  },

  _getMainAttrs: function () {
    var mainAttrs = this.buildMainAttrs();
    this.state.items && (mainAttrs.className += ' ' + this.getClassName().open);
    return mainAttrs;
  },

  // Render
  render: function () {
    return React.createElement(
      'div',
      this._getMainAttrs(),
      React.createElement(UU5.Bricks.backdrop, this._getBackdropProps()),
      React.createElement(
        'div',
        { className: this.getClassName().menu, id: this.getId() + '-menu' },
        this._getChildren()
      )
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Forms = UU5.Forms || {};

UU5.Forms.basicForm = React.createClass({
  displayName: 'basicForm',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.sectionMixin, UU5.Forms.formMixin],

  statics: {
    tagName: 'UU5.Forms.basicForm',
    classNames: {
      main: 'UU5_Forms_basicForm',
      buttons: 'UU5_Forms_basicForm-buttons',
      submit: 'UU5_Forms_basicForm-submitButton',
      reset: 'UU5_Forms_basicForm-resetButton',
      cancel: 'UU5_Forms_basicForm-cancelButton'
    }
  },

  propTypes: {
    // TODO not supported vertical and inline
    style: React.PropTypes.oneOf(['horizontal', 'vertical', 'inline']),
    ignoreValidation: React.PropTypes.bool,
    submitLabel: React.PropTypes.string,
    resetLabel: React.PropTypes.string,
    cancelLabel: React.PropTypes.string,
    onSubmit: React.PropTypes.func,
    onReset: React.PropTypes.func,
    onCancel: React.PropTypes.func,
    submitColorSchema: React.PropTypes.string,
    resetColorSchema: React.PropTypes.string,
    cancelColorSchema: React.PropTypes.string
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      style: 'horizontal',
      ignoreValidation: false,
      submitLabel: null,
      resetLabel: null,
      cancelLabel: null,
      onSubmit: null,
      onReset: null,
      onCancel: null,
      submitColorSchema: null,
      resetColorSchema: null,
      cancelColorSchema: null
    };
  },

  // Interface

  // Component Specific Helpers
  _onSubmit: function (e) {
    if (this.props.ignoreValidation || this.isValid()) {
      typeof this.props.onSubmit === 'function' && this.props.onSubmit(this, e);
    } else {
      // TODO throw some error?
    }
  },

  _onReset: function (e) {
    if (typeof this.props.onReset === 'function') {
      this.props.onReset(this, e);
    } else {
      this.reset();
    }
  },

  _onCancel: function (e) {
    typeof this.props.onCancel === 'function' && this.props.onCancel(this, e);
  },
  _getMainAttrs: function () {
    var mainAttrs = this.buildMainAttrs();
    mainAttrs.className += ' form-' + this.props.style;
    return mainAttrs;
  },

  // Render
  render: function () {
    return React.createElement(
      'form',
      this._getMainAttrs(),
      this.getHeaderChild(),
      this.getChildren(),
      React.createElement(
        'div',
        { className: this.getClassName().buttons },
        this.props.submitLabel && React.createElement(UU5.Bricks.button, { content: this.props.submitLabel,
          className: this.getClassName().submit,
          colorSchema: this.props.submitColorSchema || 'success',
          onClick: this._onSubmit
        }),
        this.props.resetLabel && React.createElement(UU5.Bricks.button, { content: this.props.resetLabel,
          className: this.getClassName().reset,
          colorSchema: this.props.resetColorSchema || 'primary',
          onClick: this._onReset
        }),
        this.props.cancelLabel && React.createElement(UU5.Bricks.button, { content: this.props.cancelLabel,
          className: this.getClassName().cancel,
          colorSchema: this.props.cancelColorSchema || 'default',
          onClick: this._onCancel
        })
      ),
      this.getFooterChild(),
      this.getDisabledCover()
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Forms = UU5.Forms || {};

UU5.Forms.checkbox = React.createClass({
  displayName: 'checkbox',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Forms.inputMixin],

  statics: {
    tagName: 'UU5.Forms.checkbox',
    classNames: {
      main: 'UU5_Forms_checkbox',
      radio: 'UU5_Forms_checkbox-radio',
      input: 'UU5_Forms_checkbox-input',
      labelRight: 'UU5_Forms_input-label-right',
      checked: 'UU5_Forms_checkbox-checked',
      glyphicon: 'UU5_Forms_checkbox-glyphicon'
    }
  },

  propTypes: {
    glyphicon: React.PropTypes.string,
    labelPosition: React.PropTypes.oneOf(['left', 'right']),
    value: React.PropTypes.bool,
    _isRadio: React.PropTypes.bool
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      glyphicon: 'uu-glyphicon-ok',
      labelPosition: 'left',
      value: false,
      _isRadio: false
    };
  },

  // Interface

  // Overriding Functions
  setValue_: function (value, callback, options) {
    var newState = options && typeof options === 'object' ? options : this._validateValue(value) || {};
    newState.value = value;

    this.setState(newState, callback);
    return this;
  },

  // Component Specific Helpers
  _onChange: function (e) {
    if (!this.isDisabled()) {
      var value = !this.getValue();
      var newState = this._validateValue(value);

      if (newState) {
        this.setState(newState);
      } else {
        if (this.props.onChange) {
          var params = { value: value, component: this, event: e };

          /*
           * @deprecated input since 03.10. Use component instead of this.
           */
          params.input = this;

          this.props.onChange(params);
        } else {
          this.setState({ value: value });
        }
      }
    }
    return this;
  },

  _isLabelRight: function () {
    return this.props.labelPosition === 'right';
  },

  _getMainAttrs: function () {
    var mainAttrs = this.getInputMainAttrs();

    this.props._isRadio && (mainAttrs.className += ' ' + this.getClassName().radio);
    this._isLabelRight() && (mainAttrs.className += ' ' + this.getClassName().labelRight);
    !this.isInitial() && (mainAttrs.className += ' has-' + this.getFeedback() + ' has-feedback');
    this.getValue() && (mainAttrs.className += ' ' + this.getClassName().checked);

    return mainAttrs;
  },

  _getInputAttrs: function () {
    var inputAttrs = this.getInputTagAttrs();

    inputAttrs.type = this.props._isRadio ? 'radio' : 'checkbox';
    inputAttrs.checked = this.getValue();
    inputAttrs.className = this.getClassName().input + (inputAttrs.className ? ' ' + inputAttrs.className : '');

    return inputAttrs;
  },

  _getRightLabelAttrs: function () {
    var labelAttrs = this.props.labelAttrs ? $.extend(true, {}, this.props.labelAttrs) : {};
    var labelClassName = this.getClassName('label', 'UU5_Forms_inputMixin');
    labelAttrs.className = labelAttrs.className ? labelClassName + ' ' + labelAttrs.className : labelClassName;
    labelAttrs.htmlFor = this.getId();
    return labelAttrs;
  },

  _getLeftInputWrapperAttrs: function () {
    var inputWrapperAttrs = this.props.inputWrapperAttrs ? $.extend(true, {}, this.props.inputWrapperAttrs) : {};
    var inputWrapperClassName = this.getClassName('inputWrapper', 'UU5_Forms_inputMixin');
    inputWrapperAttrs.className = inputWrapperAttrs.className ? inputWrapperClassName + ' ' + inputWrapperAttrs.className : inputWrapperClassName;
    return inputWrapperAttrs;
  },

  _getButtonChild: function () {
    var buttonProps = {};
    buttonProps.onClick = this._onChange;

    buttonProps.className = this.getClassName('input', 'UU5_Forms_inputMixin');
    buttonProps.disabled = this.isDisabled();

    return React.createElement(
      UU5.Bricks.button,
      buttonProps,
      React.createElement(UU5.Bricks.glyphicon, { glyphicon: this.props.glyphicon, className: this.getClassName().glyphicon })
    );
  },

  // Render
  render: function () {
    var result;

    if (this._isLabelRight()) {
      result = React.createElement(
        'div',
        this._getMainAttrs(),
        React.createElement('input', this._getInputAttrs()),
        React.createElement(
          'div',
          this._getLeftInputWrapperAttrs(),
          this._getButtonChild(),
          this.getLabelChild(this._getRightLabelAttrs())
        ),
        this.getMessageChild()
      );
    } else {
      result = React.createElement(
        'div',
        this._getMainAttrs(),
        React.createElement('input', this._getInputAttrs()),
        this.getLabelChild(),
        React.createElement(
          'div',
          this.getInputWrapperAttrs(),
          this._getButtonChild(),
          this.getMessageChild()
        )
      );
    }

    return result;
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Forms = UU5.Forms || {};

UU5.Forms.checkboxes = React.createClass({
  displayName: 'checkboxes',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Forms.inputMixin],

  statics: {
    tagName: 'UU5.Forms.checkboxes',
    classNames: {
      main: 'UU5_Forms_checkboxes',
      inline: 'UU5_Forms_inputs-inline',
      group: 'UU5_Forms_inputs-group'
    }
  },

  propTypes: {
    glyphicon: React.PropTypes.string,
    isRequired: React.PropTypes.bool,
    inline: React.PropTypes.bool,
    value: React.PropTypes.arrayOf(React.PropTypes.shape({
      label: React.PropTypes.string.isRequired,
      name: React.PropTypes.string,
      value: React.PropTypes.bool,
      disabled: React.PropTypes.bool,
      hidden: React.PropTypes.bool
    })),
    requiredMessage: React.PropTypes.string,
    wrapperAttrs: React.PropTypes.object
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      glyphicon: 'uu-glyphicon-ok',
      isRequired: false,
      inline: false,
      value: [],
      requiredMessage: 'Choose at least one option.',
      wrapperAttrs: null
    };
  },

  // Mount
  componentWillMount: function () {
    this.setState({ value: this._getInitialValue() });
  },

  // Update
  componentWillReceiveProps: function (nextProps) {
    if (nextProps.value !== undefined) {
      var newState = this.state.value;
      nextProps.value.forEach(function (value) {
        newState[value.name] = value.value;
      });
      this.setState({ value: newState });
    }
  },

  // Interface
  isCheckboxes: function () {
    return true;
  },

  setValue_: function (value, setStateCallback, options) {
    if (typeof options === 'object' && options !== null) {
      options.value = value;
      this.setState(options, setStateCallback);
    } else {
      var requiredOptions = this._checkRequired(value);

      if (requiredOptions && typeof requiredOptions === 'object' && requiredOptions.feedback === 'error') {
        this.setError(requiredOptions.message, value, setStateCallback);
      } else {
        var newState = this._validateValue(value);

        if (newState) {
          if (newState.feedback) {
            this._setFeedback(newState.feedback, newState.message, newState.value, setStateCallback);
          } else {
            this.setState(newState, setStateCallback);
          }
        } else {
          newState = requiredOptions || { feedback: 'initial', message: '' };
          newState.value = $.extend({}, this.getValue(), value);
          if (newState.feedback) {
            this._setFeedback(newState.feedback, newState.message, newState.value, setStateCallback);
          } else {
            this.setState(newState, setStateCallback);
          }
        }
      }
    }
    return this;
  },

  isValid: function () {
    var value = this.getValue();
    var result = true;

    var keys = Object.keys(value);
    var newValue = true;
    keys.forEach(function (key) {
      if (newValue) {
        newValue = value[key];
      }
    });

    if (this.props.isRequired && !this._isSelected()) {
      this.setError(this.props.requiredMessage);
      result = false;
    } else if (this.isError()) {
      result = false;
    } else if (typeof this.isValid_ === 'function') {
      result = this.isValid_();
    }

    if (result && this.props.onValidate) {
      var params = { value: this.getValue(), component: this };

      /*
       * @deprecated input since 03.10. Use component instead of this.
       */
      params.input = this;

      var validation = this.props.onValidate(params);
      if (validation && typeof validation === 'object') {
        if (validation.feedback && validation.feedback === 'error') {
          result = false;
        }
      }
    }

    return result;
  },

  // Component Specific Helpers
  _getInitialValue: function (props) {
    props = props || this.props;
    var value = {};

    this.props.value.forEach(function (checkbox, i) {
      value[checkbox.name || i.toString()] = checkbox.value || false;
    });

    return value;
  },

  _isSelected: function (newValue) {
    var value = newValue ? $.extend({}, this.getValue(), newValue) : this.getValue();
    return Object.keys(value).map(function (name) {
      return value[name];
    }).indexOf(true) > -1;
  },

  _checkRequired: function (value) {
    var options = null;
    if (this.props.isRequired) {
      if (value[Object.keys(value)[0]] || this._isSelected(value)) {
        if (typeof this.props.onChange !== 'function') {
          options = { feedback: 'success', message: '', value: value };
        }
      } else {
        options = { feedback: 'error', message: this.props.requiredMessage, value: value };
      }
    }
    return options;
  },

  _onChange: function (opt) {
    var value = opt.value;
    var checkbox = opt.input;

    var newValue = {};
    newValue[checkbox.getName()] = value;

    if (!this.isDisabled()) {
      value = newValue;
      if (this.props.onChange) {
        var options = this._checkRequired(value);

        if (options && typeof options === 'object' && options.feedback === 'error') {
          this.setError(options.message, value);
        } else {
          var newState = this._validateValue(value);

          if (newState) {
            if (newState.feedback) {
              this._setFeedback(newState.feedback, newState.message, newState.value);
            } else {
              this.setState(newState);
            }
          } else {
            var params = { value: value, component: this, event: opt.event, options: options };

            /*
             * @deprecated input since 03.10. Use component instead of this.
             */
            params.input = this;

            this.props.onChange(params);
          }
        }
      } else {
        this.setValue(value);
      }
    }

    return this;
  },

  _getCheckboxChildren: function () {
    return this.props.value.map(function (checkbox, i) {
      var checkboxProps = $.extend({}, checkbox);
      checkboxProps.name = checkboxProps.name || i.toString();
      checkboxProps.value = this.getValue()[checkboxProps.name];
      checkboxProps.key = checkboxProps.name;

      checkboxProps.glyphicon = this.props.glyphicon;
      this.props.disabled && (checkboxProps.disabled = true);
      this.props.label && (checkboxProps.labelPosition = 'right');

      checkboxProps.onChange = this._onChange;

      return React.createElement(UU5.Forms.checkbox, checkboxProps);
    }.bind(this));
  },

  _reset: function (newState, setStateCallback) {
    newState.value = this._getInitialValue();
    this.setState(newState, setStateCallback);
  },

  _getMainAttrs: function () {
    var mainAttrs = this.getInputMainAttrs();

    this.props.inline && (mainAttrs.className += ' ' + this.getClassName().inline);

    return mainAttrs;
  },

  // Render
  render: function () {
    return React.createElement(
      'div',
      this._getMainAttrs(),
      this.getLabelChild(),
      React.createElement(
        'div',
        this.getInputWrapperAttrs(),
        React.createElement(
          'div',
          { className: this.getClassName().group },
          this._getCheckboxChildren()
        ),
        this.getMessageChild()
      )
    );
  }
});

/**
 * UU5.Forms.file
 */

"use strict";

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

UU5.Forms = UU5.Forms || {};

UU5.Forms.file = React.createClass({
  displayName: "file",


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Forms.textInputMixin],

  statics: {
    tagName: "UU5.Forms.file",
    classNames: {
      main: "UU5_Forms_file",
      buttonGroup: 'input-group-btn',
      inputGroup: 'input-group UU5_Forms_text-groupWithFeedback',
      inputFeedback: 'UU5_Forms_text-inputFeedback',
      button: 'UU5_Forms_input-button',
      file: 'UU5_Forms_file-input'
    },
    defaults: {
      fileIcon: 'glyphicon-file'
    }
  },

  propTypes: {
    value: React.PropTypes.string,
    multiple: React.PropTypes.bool
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      value: '',
      multiple: false
    };
  },

  // Interface

  // Overriding Functions

  onChange_: function (e) {
    if (e.target.files[0]) {
      var value = e.target.files[0];
      value.value = e.target.value;

      if (typeof this.props.onChange === 'function') {
        var options = this._checkRequired(value);

        if (options && typeof options === 'object' && options.feedback === 'error') {
          this.setError(options.message, value);
        } else {
          var newState = this._validateValue(value);

          if (newState) {
            if (newState.feedback) {
              this._setFeedback(newState.feedback, newState.message, newState.value);
            } else {
              this.setState(newState);
            }
          } else {
            var params = { value: value, component: this, event: e, options: options };

            /*
             * @deprecated input since 03.10. Use component instead of this.
             */
            params.input = this;

            this.props.onChange(params);
          }
        }
      } else {
        this.setValue(value);
      }
    }

    return this;
  },

  // Component Specific Helpers

  _getTextInputAttrs: function () {
    var attrs = this.getTextInputAttrs();

    attrs.type = 'file';
    attrs.value = this.getValue() ? this.getValue().value : '';
    attrs.className = this.getClassName().file;

    return attrs;
  },

  _getButtonProps: function () {
    return {
      className: this.getClassName().button,
      disabled: this.isDisabled()
    };
  },

  render: function () {
    return React.createElement(
      "div",
      this.getTextInputMainAttrs(),
      this.getLabelChild(),
      React.createElement(
        "div",
        this.getInputWrapperAttrs(),
        React.createElement(
          "div",
          { className: this.getClassName().inputGroup },
          React.createElement(
            "div",
            { className: this.getClassName().inputFeedback },
            React.createElement("input", _extends({}, this.getTextInputAttrs(), { value: this.getValue() ? this.getValue().name : '', readOnly: true })),
            this.getGlyphiconChild()
          ),
          React.createElement(
            "span",
            { className: this.getClassName().buttonGroup },
            React.createElement(
              UU5.Bricks.button,
              this._getButtonProps(),
              React.createElement(UU5.Bricks.glyphicon, { glyphicon: this.getDefault().fileIcon }),
              React.createElement("input", this._getTextInputAttrs())
            )
          )
        ),
        this.getMessageChild()
      )
    );
  }
});

'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var UU5 = UU5 || {};
UU5.Forms = UU5.Forms || {};

UU5.Forms.number = React.createClass({
  displayName: 'number',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Forms.textInputMixin],

  statics: {
    tagName: 'UU5.Forms.number',
    classNames: {
      main: 'UU5_Forms_number',
      buttonGroup: 'input-group-btn',
      inputGroup: 'input-group UU5_Forms_text-groupWithFeedback',
      inputFeedback: 'UU5_Forms_text-inputFeedback'
    },
    defaults: {
      minusIcon: 'uu-glyphicon-minus',
      plusIcon: 'uu-glyphicon-plus'
    },
    errors: {
      notNumber: 'Cannot set value \'%s\' (%s) as number.'
    }
  },

  propTypes: {
    value: React.PropTypes.oneOfType([React.PropTypes.number, React.PropTypes.string]),
    step: React.PropTypes.number,
    min: React.PropTypes.number,
    max: React.PropTypes.number,
    decimals: React.PropTypes.number,
    decimalSeparator: React.PropTypes.string,
    isRounded: React.PropTypes.bool,
    nanMessage: React.PropTypes.string,
    lowerBoundMessage: React.PropTypes.string,
    upperBoundMessage: React.PropTypes.string
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      value: null,
      step: null,
      min: null,
      max: null,
      decimals: 0,
      decimalSeparator: null,
      isRounded: false,
      nanMessage: 'Please insert a number.',
      lowerBoundMessage: 'Please type in bigger number.',
      upperBoundMessage: 'Please type in smaller number.'
    };
  },

  getInitialState: function () {
    var decimals = this._validateNumber(this.props.decimals, 0);
    decimals === null && (decimals = this.getDefault().decimals);

    return {
      // stored in state only for preventing unnecessary repeated validations
      step: this._validateNumber(this.props.step, 0),
      min: this._validateNumber(this.props.min, decimals),
      max: this._validateNumber(this.props.max, decimals),
      decimals: decimals,
      decimalSeparator: this.props.decimalSeparator
    };
  },

  componentWillMount: function () {
    var newValue = '';
    if (this.props.value !== null) {
      newValue = this._checkValue(this.props.value ? this.props.value.toString() : '', this.getDecimals(), this.props.isRounded).toString();
    }
    this.setState({ value: newValue });
  },

  // Update
  componentWillReceiveProps: function (nextProps) {
    var newState = {};

    var decimals;
    if (nextProps.decimals === undefined) {
      decimals = this.getDecimals();
    } else {
      decimals = this._validateNumber(nextProps.decimals, 0);
      newState.decimals = decimals;
    }

    this.setState({
      step: nextProps.step !== null && this._validateNumber(nextProps.step, 0),
      min: nextProps.min !== null && this._validateNumber(nextProps.min, decimals),
      max: nextProps.max !== null && this._validateNumber(nextProps.max, decimals),
      decimals: decimals
    });
  },

  // Interface
  getMin: function () {
    return this.state.min;
  },

  getMax: function () {
    return this.state.max;
  },

  getStep: function () {
    return this.state.step;
  },

  getDecimals: function () {
    return this.state.decimals;
  },

  getDecimalSeparator: function () {
    return this.state && this.state.decimalSeparator;
  },

  // Overriding Functions
  getValue_: function () {
    return this._getValue(this.state.value);
  },

  // primary function, channeling all setState threads
  // we are expecting options object containing message and/or feedback props
  setValue_: function (value, setStateCallback, options) {
    if (options && typeof options === 'object' && Object.keys(options).length) {
      options.value = value === null ? '' : value.toString();
      this.setState(options, setStateCallback);
    } else {
      var requiredOptions = this._checkRequired(value);

      if (requiredOptions && typeof requiredOptions === 'object' && requiredOptions.feedback === 'error') {
        this.setError(requiredOptions.message, value, setStateCallback);
      } else {
        var newState = this._validateValue(value);

        if (newState) {
          if (newState.feedback) {
            this._setFeedback(newState.feedback, newState.message, newState.value, setStateCallback);
          } else {
            this.setState(newState, setStateCallback);
          }
        } else {
          newState = requiredOptions || { feedback: 'initial', message: '' };
          newState.value = value === null ? '' : this._checkValue(value.toString(), this.getDecimals(), this.props.isRounded).toString();
          if (newState.feedback) {
            this._setFeedback(newState.feedback, newState.message, newState.value, setStateCallback);
          } else {
            this.setState(newState, setStateCallback);
          }
        }
      }
    }

    return this;
  },

  onChange_: function (e) {
    var value = e.target.value.trim();

    var options = this._checkRequired(value);

    if (options && typeof options === 'object' && options.feedback === 'error') {
      this.setError(options.message, value);
    } else {
      var newState = this._validateValue(value);

      if (newState) {
        if (newState.feedback) {
          this._setFeedback(newState.feedback, newState.message, newState.value);
        } else {
          this.setState(newState);
        }
      } else if (typeof this.props.onChange === 'function' && !this._isNumberChar(value) && isFinite(value.replace(',', '.'))) {
        var params = { value: this._getValue(value), component: this, event: e, options: options };

        /*
         * @deprecated input since 03.10. Use component instead of this.
         */
        params.input = this;

        this.props.onChange(params);
      } else if (value === '') {
        this.setInitial('', value);
      } else if (value === '-') {
        if (this.getMin() !== null && this.getMin() >= 0) {
          this.setWarning(this.props.nanMessage, '');
        } else {
          this.setInitial('', value);
        }
      } else if (isFinite(value.replace(',', '.'))) {
        var newValue = this._checkValue(value, this.getDecimals(), this.props.isRounded);
        this.setInitial('', newValue.toString());
      } else {
        this.showError('notNumber', [value, typeof value]);
      }
    }

    return this;
  },

  validate_: function (value, init) {
    var result = null;

    value = value === null ? '' : value.toString();
    if (value.length > 0 && !this._isNumberChar(value)) {
      if (isFinite(value.replace(',', '.'))) {
        var newValue = this._validateNumber(+value, this.getDecimals());

        if (this.state.max !== null && newValue > this.state.max) {
          result = {
            value: newValue.toString(),
            message: this.props.upperBoundMessage,
            feedback: 'error'
          };
        } else if (this.state.min !== null && newValue < this.state.min) {
          result = {
            value: newValue.toString(),
            message: this.props.lowerBoundMessage,
            feedback: 'error'
          };
        }
      } else {
        result = {
          value: this.state.value,
          message: this.props.nanMessage,
          feedback: 'warning'
        };
      }
    }

    return result;
  },

  onBlur_: function (e) {
    var value = this.state.value;

    var onBlur;
    if (typeof this.props.onBlur === 'function') {
      onBlur = this.props.onBlur.bind(this, { value: this.getValue(), component: this, event: e });
    }

    if (this._isNumberChar(value)) {
      if (!this.getDecimals() && value.match(/[,.]$/)) {
        this.setState({ value: value.replace(/[,.]$/, '') }, onBlur);
      } else {
        this.setError(this.props.nanMessage, onBlur);
      }
    } else if (this.props.step) {
      var number = Math.round(this.getValue() / this.props.step) * this.props.step;
      var newValue = number.toString();
      this._isComma(this.state.value) && newValue.replace(".", ",");
      this.setState({ value: newValue }, onBlur);
    } else if (onBlur) {
      onBlur();
    }
    return this;
  },

  // Component Specific Helpers
  _getValue: function (value) {
    return value === '' || value === '-' || value === null ? null : +value;
  },

  _isComma: function (value) {
    return value.indexOf(',') > -1;
  },

  // value (number) => number
  _validateNumber: function (value, decimals, round) {
    var result = null;

    if (value !== null) {
      value = value.toString();
      var dotIndex = value.indexOf('.');
      var isFloat = dotIndex > 0;
      var number = value;

      if (isFloat && decimals) {
        if (round) {
          number = Number(value).toFixed(decimals).toString();
        } else {
          number = value.slice(0, dotIndex + 1 + decimals);
        }
      }

      number = number.slice(0, isFloat ? 17 : 16);
      result = Number(number);
    }

    return result;
  },

  _checkSeparator: function (newValue, isComma) {
    if (newValue !== null) {
      if (this.getDecimalSeparator()) {
        newValue = newValue.replace('.', this.getDecimalSeparator());
      } else if (isComma) {
        newValue = newValue.replace('.', ',');
      }
    }

    return newValue;
  },

  // value (String) => number
  _checkValue: function (value, decimals, round) {
    var result = '';

    var isComma = this._isComma(value);
    var newValue = value.replace(',', '.');

    if (newValue.match(/\.$/)) {
      if (decimals) {
        result = newValue;
      } else {
        result = newValue.replace(/\.$/, '');
      }
    } else if (isFinite(newValue)) {
      var number = this._validateNumber(+newValue, decimals, round);
      result = number.toString();
    }

    return this._checkSeparator(result, isComma);
  },

  _isNumberChar: function (value) {
    return !!value.match(/(^-$)|([,.]$)/) && !value.match(/[,.][,.]+/);
  },

  _addStep: function (e) {
    this._changeByStep(this.getStep(), e);
    return this;
  },

  _subtractStep: function (e) {
    this._changeByStep(-this.getStep(), e);
    return this;
  },

  _changeByStep: function (step, e) {
    var newValue = this.getValue() + step;

    if (typeof this.props.onChange === 'function') {
      var params = { value: newValue, component: this, event: e };

      /*
       * @deprecated input since 03.10. Use component instead of this.
       */
      params.input = this;

      this.props.onChange(params);
    } else {
      this.setValue(newValue, this._setInputFocus);
    }
    return this;
  },

  _setInputFocus: function () {
    this.getInput().focus();
    return this;
  },

  _getInputGroupAttrs: function () {
    var inputGroupAttrs = {};
    this.getStep() && (inputGroupAttrs.className = this.getClassName().inputGroup);
    return inputGroupAttrs;
  },

  _getButtonProps: function () {
    return {
      size: this.props.size
    };
  },

  _getPlusButtonProps: function () {
    var buttonProps = this._getButtonProps();

    if (this.getMax() !== null && this.getValue() >= this.getMax() || this.isDisabled()) {
      buttonProps.disabled = true;
    } else {
      buttonProps.onClick = this._addStep;
    }

    return buttonProps;
  },

  _getMinusButtonProps: function () {
    var buttonProps = this._getButtonProps();

    if (this.getMin() !== null && this.getValue() <= this.getMin() || this.isDisabled()) {
      buttonProps.disabled = true;
    } else {
      buttonProps.onClick = this._subtractStep;
    }

    return buttonProps;
  },

  _getButtonGroup: function () {
    var buttonGroup = null;

    if (this.getStep()) {
      buttonGroup = React.createElement(
        'span',
        { className: this.getClassName().buttonGroup },
        React.createElement(
          'span',
          null,
          React.createElement(
            UU5.Bricks.button,
            this._getMinusButtonProps(),
            React.createElement(UU5.Bricks.glyphicon, { glyphicon: this.getDefault().minusIcon })
          )
        ),
        React.createElement(
          'span',
          null,
          React.createElement(
            UU5.Bricks.button,
            this._getPlusButtonProps(),
            React.createElement(UU5.Bricks.glyphicon, { glyphicon: this.getDefault().plusIcon })
          )
        )
      );
    }

    return buttonGroup;
  },

  // Render
  render: function () {
    return React.createElement(
      'div',
      this.getTextInputMainAttrs(),
      this.getLabelChild(),
      React.createElement(
        'div',
        this.getInputWrapperAttrs(),
        React.createElement(
          'div',
          this._getInputGroupAttrs(),
          React.createElement(
            'div',
            { className: this.getClassName().inputFeedback },
            React.createElement('input', _extends({}, this.getTextInputAttrs(), { value: this.state.value })),
            this.getGlyphiconChild()
          ),
          this._getButtonGroup()
        ),
        this.getMessageChild()
      )
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Forms = UU5.Forms || {};

UU5.Forms.radios = React.createClass({
  displayName: 'radios',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Forms.inputMixin],

  statics: {
    tagName: 'UU5.Forms.radios',
    classNames: {
      main: 'UU5_Forms_radios',
      inline: 'UU5_Forms_inputs-inline',
      group: 'UU5_Forms_inputs-group'
    }
  },

  propTypes: {
    glyphicon: React.PropTypes.string,
    isRequired: React.PropTypes.bool,
    inline: React.PropTypes.bool,
    value: React.PropTypes.arrayOf(React.PropTypes.shape({
      label: React.PropTypes.string.isRequired,
      name: React.PropTypes.string,
      value: React.PropTypes.bool,
      disabled: React.PropTypes.bool,
      hidden: React.PropTypes.bool
    })),
    requiredMessage: React.PropTypes.string,
    wrapperAttrs: React.PropTypes.object
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      glyphicon: 'uu-glyphicon-point',
      isRequired: false,
      inline: false,
      requiredMessage: 'Choose one option.',
      wrapperAttrs: null,
      value: []
    };
  },

  // Mount
  componentWillMount: function () {
    this.setState({ value: this._getInitialValue() });
  },

  // Update
  componentWillReceiveProps: function (nextProps) {
    if (nextProps.value !== undefined) {
      var value = this._getInitialValue(nextProps);
      this.setState({ value: value });
    }
  },

  // Interface
  isCheckboxes: function () {
    return true;
  },

  setValue_: function (value, setStateCallback, options) {
    if (typeof options === 'object' && options !== null) {
      options.value = value;
      this.setState(options, setStateCallback);
    } else {
      var requiredOptions = this._checkRequired(value);

      if (requiredOptions && typeof requiredOptions === 'object' && requiredOptions.feedback === 'error') {
        this.setError(requiredOptions.message, value, setStateCallback);
      } else {
        var newState = this._validateValue(value);

        if (newState) {
          if (newState.feedback) {
            this._setFeedback(newState.feedback, newState.message, newState.value, setStateCallback);
          } else {
            this.setState(newState, setStateCallback);
          }
        } else {
          newState = requiredOptions || { feedback: 'initial', message: '' };
          newState.value = value;
          if (newState.feedback) {
            this._setFeedback(newState.feedback, newState.message, newState.value, setStateCallback);
          } else {
            this.setState(newState, setStateCallback);
          }
        }
      }
    }
    return this;
  },

  isValid: function () {
    var value = this.getValue();
    var result = true;

    if (this.props.isRequired && (value === '' || value === null)) {
      this.setError(this.props.requiredMessage);
      result = false;
    } else if (this.isError()) {
      result = false;
    } else if (typeof this.isValid_ === 'function') {
      result = this.isValid_();
    }

    if (result && this.props.onValidate) {
      var params = { value: this.getValue(), component: this };

      /*
       * @deprecated input since 03.10. Use component instead of this.
       */
      params.input = this;

      var validation = this.props.onValidate(params);
      if (validation && typeof validation === 'object') {
        if (validation.feedback && validation.feedback === 'error') {
          result = false;
        }
      }
    }

    return result;
  },

  // Component Specific Helpers
  _getInitialValue: function (props) {
    props = props || this.props;
    var value = null;

    props.value.forEach(function (radio, i) {
      radio.value && (value = radio.name || i.toString());
    });

    return value;
  },

  _checkRequired: function (value) {
    var options = null;
    if (this.props.isRequired) {
      if (value) {
        if (typeof this.props.onChange !== 'function') {
          options = { feedback: 'success', message: '', value: value };
        }
      } else {
        options = { feedback: 'error', message: this.props.requiredMessage, value: value };
      }
    }
    return options;
  },

  _onChange: function (opt) {
    var value = opt.value;
    var radio = opt.input;

    if (!this.isDisabled()) {
      value = radio.getName();
      if (this.props.onChange) {
        var options = this._checkRequired(value);

        if (options && typeof options === 'object' && options.feedback === 'error') {
          this.setError(options.message, value);
        } else {
          var newState = this._validateValue(value);

          if (newState) {
            if (newState.feedback) {
              this._setFeedback(newState.feedback, newState.message, newState.value);
            } else {
              this.setState(newState);
            }
          } else {
            var params = { value: value, component: this, event: opt.event, options: options };

            /*
             * @deprecated input since 03.10. Use component instead of this.
             */
            params.input = this;

            this.props.onChange(params);
          }
        }
      } else {
        this.setValue(value);
      }
    }

    return this;
  },

  _getRadioChildren: function () {
    return this.props.value.map(function (checkbox, i) {
      var checkboxProps = $.extend({}, checkbox);
      checkboxProps.name = checkboxProps.name || i.toString();
      checkboxProps.value = this.getValue() === checkboxProps.name;
      checkboxProps.key = checkboxProps.name;

      checkboxProps.glyphicon = this.props.glyphicon;
      checkboxProps._isRadio = true;
      this.props.disabled && (checkboxProps.disabled = true);
      this.props.label && (checkboxProps.labelPosition = 'right');

      checkboxProps.onChange = this._onChange;

      return React.createElement(UU5.Forms.checkbox, checkboxProps);
    }.bind(this));
  },

  _reset: function (newState, setStateCallback) {
    newState.value = this._getInitialValue();
    this.setState(newState, setStateCallback);
  },

  _getMainAttrs: function () {
    var mainAttrs = this.getInputMainAttrs();
    this.props.inline && (mainAttrs.className += ' ' + this.getClassName().inline);
    return mainAttrs;
  },

  // Render
  render: function () {
    return React.createElement(
      'div',
      this._getMainAttrs(),
      this.getLabelChild(),
      React.createElement(
        'div',
        this.getInputWrapperAttrs(),
        React.createElement(
          'div',
          { className: this.getClassName().group },
          this._getRadioChildren()
        ),
        this.getMessageChild()
      )
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Forms = UU5.Forms || {};

UU5.Forms.select = React.createClass({
  displayName: 'select',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin, UU5.Forms.textInputMixin],

  statics: {
    tagName: 'UU5.Forms.select',
    classNames: {
      main: 'UU5_Forms_select',
      input: 'UU5_Forms_select-input'
    },
    warnings: {
      notMultiple: 'Interface %s can be used only if select is multiple.'
    }
  },

  propTypes: {
    value: React.PropTypes.oneOfType([React.PropTypes.number, React.PropTypes.arrayOf(React.PropTypes.number)]),
    isButtonHidden: React.PropTypes.bool,
    openedGlyphicon: React.PropTypes.string,
    closedGlyphicon: React.PropTypes.string,
    multiple: React.PropTypes.bool
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      value: null,
      isButtonHidden: false,
      openedGlyphicon: 'uu-glyphicon-arrow-up',
      closedGlyphicon: 'uu-glyphicon-arrow-down',
      multiple: false
    };
  },

  // Interface
  getSelectedValue: function () {
    return this.input.getSelectedValue();
  },

  addValue: function (index, setStateCallback) {
    if (this.props.multiple) {
      var indexes = this.getValue() || [];
      var indexPosition = indexes.indexOf(index);
      if (indexPosition === -1) {
        indexes.push(index);
        this.setValue(indexes, setStateCallback);
      } else if (typeof setStateCallback === 'function') {
        setStateCallback();
      }
    } else {
      this.showWarning('notMultiple', 'addValue');
    }
    return this;
  },

  removeValue: function (index, setStateCallback) {
    if (this.props.multiple) {
      var indexes = this.getValue() || [];
      var indexPosition = indexes.indexOf(index);
      if (indexPosition > -1) {
        indexes.splice(indexPosition, 1);
        !indexes.length && (indexes = null);
        this.setValue(indexes, setStateCallback);
      } else if (typeof setStateCallback === 'function') {
        setStateCallback();
      }
    } else {
      this.showWarning('notMultiple', 'removeValue');
    }
    return this;
  },

  // Overriding Functions

  // Component Specific Helpers

  _getInputProps: function () {
    var select = this;
    return {
      className: this.getClassName().input,
      content: this.getContent(),
      value: this.getValue(),
      placeholder: this.props.placeholder,
      openedGlyphicon: this.props.openedGlyphicon,
      closedGlyphicon: this.props.closedGlyphicon,
      isButtonHidden: this.props.isButtonHidden,
      glyphicon: this.getGlyphiconChild(),

      onClick: function (opt) {
        if (typeof select.props.onChange === 'function') {
          opt.component = select;
          select.props.onChange(opt);
        } else {
          select.setValue(opt.value);
        }
      },
      ref_: function (input) {
        select.input = input;
      }
    };
  },

  _getMultipleProps: function () {
    var props = this._getInputProps();

    var select = this;
    props.onClick = null;

    props.onAddValue = function (index) {
      if (typeof select.props.onAddValue === 'function') {
        select.props.onAddValue({ value: index, component: select });
      } else {
        select.addValue(index);
      }
    };

    props.onRemoveValue = function (index) {
      if (typeof select.props.onRemoveValue === 'function') {
        select.props.onRemoveValue({ value: index, component: select });
      } else {
        select.removeValue(index);
      }
    };

    return props;
  },

  // Render
  render: function () {
    var inputTagName;
    var inputProps;

    if (this.props.multiple) {
      inputTagName = '_multipleInput';
      inputProps = this._getMultipleProps();
    } else {
      inputTagName = '_input';
      inputProps = this._getInputProps();
    }

    return React.createElement(
      'div',
      this.getTextInputMainAttrs(),
      this.getLabelChild(),
      React.createElement(
        'div',
        this.getInputWrapperAttrs(),
        React.createElement(UU5.Forms.select[inputTagName], inputProps, this.props.children && React.Children.toArray(this.props.children)),
        this.getMessageChild()
      )
    );
  }
});

UU5.Forms.select._input = React.createClass({
  displayName: '_input',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Forms.select._input',
    classNames: {
      main: 'UU5_Forms_select__input',
      inputGroup: 'input-group UU5_Forms_text-groupWithFeedback',
      inputFeedback: 'UU5_Forms_text-inputFeedback',
      inputBtnGroup: 'UU5_Forms_select__input-btnGroup input-group-btn',
      header: 'UU5_Forms_select__input-header form-control',
      menu: 'UU5_Forms_select__input-menu',
      innerMenu: 'UU5_Forms_select__input-innerMenu list-group',
      button: 'UU5_Forms_select__input-button',
      opened: 'UU5_Forms_select__input-opened',
      selectedItem: 'UU5_Forms_select__input-selectedItem bg-info',
      placeholder: 'UU5_Forms_select__input-placeholder',
      noButton: 'UU5_Forms_select__input-noButton'
    }
  },

  propTypes: {
    isOpened: React.PropTypes.bool,
    value: React.PropTypes.number,
    placeholder: React.PropTypes.string,
    isButtonHidden: React.PropTypes.bool,
    openedGlyphicon: React.PropTypes.string,
    closedGlyphicon: React.PropTypes.string,
    onClick: React.PropTypes.func.isRequired
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      isOpened: false,
      value: null,
      placeholder: null,
      isButtonHidden: false,
      openedGlyphicon: null,
      closedGlyphicon: null,
      onClick: null
    };
  },

  getInitialState: function () {
    return {
      isOpened: this.props.isOpened,
      selectedIndex: this.props.value
    };
  },

  componentWillReceiveProps: function (nextProps) {
    nextProps.value != this.props.value && this.setState({ selectedIndex: nextProps.value });
  },

  // Interface

  isOpened: function () {
    return this.state.isOpened;
  },

  open: function (setStateCallback) {
    this.setState({ isOpened: true }, setStateCallback);
    return this;
  },

  close: function (setStateCallback) {
    this.setState({ isOpened: false }, setStateCallback);
    return this;
  },

  toggle: function (setStateCallback) {
    var input = this;
    this.setState(function (state) {
      return { isOpened: !state.isOpened, selectedIndex: input.props.value };
    }, setStateCallback);
    return this;
  },

  getSelectedIndex: function () {
    return this.state.selectedIndex;
  },

  getSelectedOption: function () {
    var option;
    if (this.getSelectedIndex() === null) {
      option = null;
    } else {
      option = this.getRenderedChildren()[this.getSelectedIndex()];
    }
    return option;
  },

  getSelectedValue: function () {
    var option = this.getSelectedOption();
    return option ? option.getValue() : null;
  },

  // Overriding Functions
  expandChildProps_: function (child, i) {
    var props = $.extend(true, {}, child.props);
    props.selected = i == this.props.value;

    if (this.getSelectedIndex() === i) {
      props.className = props.className ? props.className + ' ' + this.getClassName().selectedItem : this.getClassName().selectedItem;
    }

    props.mainAttrs = props.mainAttrs || {};
    props.mainAttrs.id = this.getId() + '-item-' + i;

    var input = this;
    var childOnClick = props.onClick;
    props.onClick = function (opt) {
      input._onClose(opt, childOnClick);
    };
    return props;
  },

  selectUp: function (setStateCallback) {
    var input = this;
    this.setState(function (state) {
      var index = null;
      if (state.isOpened) {
        var childrenLength;

        if (state.selectedIndex == null || state.selectedIndex === 0) {
          childrenLength = input.getRenderedChildren().length;
          index = childrenLength - 1;
        } else {
          index = state.selectedIndex - 1;
        }

        childrenLength = childrenLength || input.getRenderedChildren().length;
        var offset = index > childrenLength - 3 ? $('#' + input.getId() + '-item-' + (childrenLength - 1))[0].offsetTop : index - 1 > 0 ? $('#' + input.getId() + '-item-' + (index - 1))[0].offsetTop : 0;

        $('#' + this.getId() + '-menu').animate({ scrollTop: offset }, 0);
      }

      return { selectedIndex: index };
    }, setStateCallback);
    return this;
  },

  selectDown: function (setStateCallback) {
    var input = this;
    this.setState(function (state) {
      var index = null;
      var newState;
      if (state.isOpened) {
        var childrenLength = input.getRenderedChildren().length;

        if (state.selectedIndex == null || state.selectedIndex === childrenLength - 1) {
          index = 0;
        } else {
          index = state.selectedIndex + 1;
        }

        var offset = index > 1 ? $('#' + input.getId() + '-item-' + (index - 1))[0].offsetTop : 0;
        $('#' + input.getId() + '-menu').animate({ scrollTop: offset }, 0);

        newState = { selectedIndex: index };
      } else {
        newState = { isOpened: true, selectedIndex: 0 };
      }

      return newState;
    }, setStateCallback);

    return this;
  },

  // Component Specific Helpers
  _onClose: function (opt, onClick) {
    var input = this;
    this.setState({ isOpened: false, selectedIndex: opt.value }, function () {
      var onClick = onClick || input.props.onClick;
      onClick(opt);
    });
    return this;
  },

  _getMainAttrs: function () {
    var attrs = this.getMainAttrs();
    this.props.isButtonHidden && (attrs.className += ' ' + this.getClassName().noButton);

    if (this.isOpened()) {
      attrs.className += ' ' + this.getClassName().opened;

      var input = this;
      var customOnKeyPress = attrs.onKeyPress;
      attrs.onKeyPress = function (e) {
        // stop submit form on enter if input is alone in form
        if (e.keyCode == 13 || e.charCode == 13) {
          e.preventDefault();
          input._onClose({ component: input, event: e, value: input.getSelectedIndex() });
        }
        if (typeof customOnKeyPress === 'function') {
          customOnKeyPress(e);
        }
      };

      var customOnKeyDown = attrs.onKeyDown;
      attrs.onKeyDown = function (e) {
        switch (e.keyCode) {
          case 27:
            e.preventDefault();
            input.close();
            break;
          case 38:
            e.preventDefault();
            input.selectUp();
            break;
          // case 39:
          //   input.open();
          //   break;
          case 40:
            e.preventDefault();
            input.selectDown();
            break;
        }

        if (typeof customOnKeyDown === 'function') {
          customOnKeyDown(e);
        }
      };

      var onMouseOver = attrs.onMouseOver;

      attrs.onMouseOver = function (e) {
        if (input.getSelectedIndex() !== null) {
          var callback;
          typeof onMouseOver === 'function' && (callback = onMouseOver.bind(input, e));
          input.setState({ selectedIndex: null }, callback);
        }
      };
    }

    return attrs;
  },

  _getBackdropProps: function () {
    var backdropId = this.getId() + "-backdrop";

    return {
      hidden: !this.isOpened(),
      id: backdropId,
      onClick: function (event) {
        event.target.id === backdropId && this.close();
      }.bind(this)
    };
  },

  _getChildren: function () {
    return this.getChildren();
  },

  _getButtonProps: function () {
    var input = this;
    return {
      className: this.getClassName().button,
      onClick: function () {
        input.toggle();
      },
      disabled: this.isDisabled(),
      pressed: this.isOpened()
    };
  },

  _getHeader: function (children) {
    var result;
    if (this.props.value === null) {
      if (this.props.placeholder) {
        result = React.createElement(UU5.Bricks.span, { className: this.getClassName().placeholder, content: this.props.placeholder });
      }
    } else {
      var child = children[this.props.value];
      result = child.props.selectedContent || child.props.content || child.props.children || child.props.value;
    }
    return result;
  },

  _getMenuAttrs: function () {
    return {
      className: this.getClassName().menu
    };
  },

  _getHeaderProps: function (children) {
    var input = this;
    return {
      className: this.getClassName().header,
      content: this._getHeader(children),
      mainAttrs: {
        onClick: function () {
          input.toggle();
        }
      }
    };
  },

  // Render
  render: function () {
    var children = this._getChildren();

    return React.createElement(
      'div',
      this._getMainAttrs(),
      React.createElement(
        'div',
        { className: this.getClassName().inputGroup },
        React.createElement(
          'div',
          { className: this.getClassName().inputFeedback },
          React.createElement(UU5.Bricks.div, this._getHeaderProps(children)),
          this.props.glyphicon
        ),
        !this.props.isButtonHidden && React.createElement(
          'span',
          { className: this.getClassName().inputBtnGroup },
          React.createElement(
            UU5.Bricks.button,
            this._getButtonProps(),
            React.createElement(UU5.Bricks.glyphicon, { glyphicon: this.props[this.isOpened() ? 'openedGlyphicon' : 'closedGlyphicon'] })
          )
        )
      ),
      React.createElement(
        'div',
        this._getMenuAttrs(),
        React.createElement(
          'div',
          { className: this.getClassName().innerMenu, id: this.getId() + '-menu' },
          children
        )
      ),
      React.createElement(UU5.Bricks.backdrop, this._getBackdropProps())
    );
  }
});

UU5.Forms.select.option = React.createClass({
  displayName: 'option',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Forms.select.option',
    classNames: {
      main: 'UU5_Forms_select_option list-group-item'
    }
  },

  propTypes: {
    value: React.PropTypes.string.isRequired,
    selectedContent: React.PropTypes.any,
    onClick: React.PropTypes.func
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      value: null,
      selectedContent: null,
      onClick: null
    };
  },

  // Interface
  getValue: function () {
    return this.props.value;
  },

  // Overriding Functions

  // Component Specific Helpers
  _getMainProps: function () {
    var props = this.getMainPropsToPass();

    var option = this;
    props.onClick = function (link, e) {
      option.props.onClick({ component: option, event: e, value: option.getIndex() });
    };

    if (!props.content && !this.props.children) {
      props.content = this.props.value;
    }

    return props;
  },

  // Render
  render: function () {
    return React.createElement(
      UU5.Bricks.link,
      this._getMainProps(),
      this.props.children && React.Children.toArray(this.props.children)
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Forms = UU5.Forms || {};

UU5.Forms.select._multipleInput = React.createClass({
  displayName: '_multipleInput',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin],

  statics: {
    tagName: 'UU5.Forms.select._multipleInput',
    classNames: {
      main: 'UU5_Forms_select__multipleInput',
      inputGroup: 'input-group UU5_Forms_text-groupWithFeedback',
      inputFeedback: 'UU5_Forms_text-inputFeedback',
      inputBtnGroup: 'UU5_Forms_select__input-btnGroup input-group-btn',
      header: 'UU5_Forms_select__input-header form-control',
      headerContent: 'UU5_Forms_select__input-headerContent',
      menu: 'UU5_Forms_select__input-menu',
      innerMenu: 'UU5_Forms_select__input-innerMenu list-group',
      button: 'UU5_Forms_select__input-button',
      opened: 'UU5_Forms_select__input-opened',
      selectedItem: 'UU5_Forms_select__input-selectedItem bg-info',
      hoverItem: 'UU5_Forms_select__input-hoverItem',
      placeholder: 'UU5_Forms_select__input-placeholder',
      noButton: 'UU5_Forms_select__input-noButton',
      headerItem: 'UU5_Forms_select__multipleInput-headerItem bg-info',
      headerItemText: 'UU5_Forms_select__multipleInput-headerItemText',
      headerItemClose: 'UU5_Forms_select__multipleInput-headerItemClose'
    },
    defaults: {
      closeIcon: 'uu-glyphicon-cross'
    }
  },

  propTypes: {
    isOpened: React.PropTypes.bool,
    value: React.PropTypes.arrayOf(React.PropTypes.number),
    placeholder: React.PropTypes.string,
    isButtonHidden: React.PropTypes.bool,
    openedGlyphicon: React.PropTypes.string,
    closedGlyphicon: React.PropTypes.string,
    onAddValue: React.PropTypes.func,
    onRemoveValue: React.PropTypes.func
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      isOpened: false,
      value: null,
      placeholder: null,
      isButtonHidden: false,
      openedGlyphicon: null,
      closedGlyphicon: null,
      onAddValue: null,
      onRemoveValue: null
    };
  },

  getInitialState: function () {
    this.toClose = true;

    return {
      isOpened: this.props.isOpened,
      selectedIndex: null
    };
  },

  // componentWillReceiveProps: function (nextProps) {
  //   nextProps.value != this.props.value && this.setState({ selectedIndexes: nextProps.value });
  // },

  // Interface

  isOpened: function () {
    return this.state.isOpened;
  },

  open: function (setStateCallback) {
    this.setState({ isOpened: true }, setStateCallback);
    return this;
  },

  close: function (setStateCallback) {
    this.setState({ isOpened: false }, setStateCallback);
    return this;
  },

  toggle: function (setStateCallback) {
    var input = this;
    this.setState(function (state) {
      input.toClose = true;
      return { isOpened: !state.isOpened, selectedIndex: null };
    }, setStateCallback);
    return this;
  },

  selectUp: function (setStateCallback) {
    var input = this;
    this.setState(function (state) {
      var index = null;
      if (state.isOpened) {
        var childrenLength;

        if (state.selectedIndex == null || state.selectedIndex === 0) {
          childrenLength = input.getRenderedChildren().length;
          index = childrenLength - 1;
        } else {
          index = state.selectedIndex - 1;
        }

        childrenLength = childrenLength || input.getRenderedChildren().length;
        var offset = index > childrenLength - 3 ? $('#' + input.getId() + '-item-' + (childrenLength - 1))[0].offsetTop : index - 1 > 0 ? $('#' + input.getId() + '-item-' + (index - 1))[0].offsetTop : 0;

        $('#' + this.getId() + '-menu').animate({ scrollTop: offset }, 0);
      }

      return { selectedIndex: index };
    }, setStateCallback);
    return this;
  },

  selectDown: function (setStateCallback) {
    var input = this;
    this.setState(function (state) {
      var index = null;
      var newState;
      if (state.isOpened) {
        var childrenLength = input.getRenderedChildren().length;

        if (state.selectedIndex == null || state.selectedIndex === childrenLength - 1) {
          index = 0;
        } else {
          index = state.selectedIndex + 1;
        }

        var offset = index > 1 ? $('#' + input.getId() + '-item-' + (index - 1))[0].offsetTop : 0;
        $('#' + input.getId() + '-menu').animate({ scrollTop: offset }, 0);

        newState = { selectedIndex: index };
      } else {
        newState = { isOpened: true, selectedIndex: 0 };
      }

      return newState;
    }, setStateCallback);

    return this;
  },

  addValue: function (index) {
    if (typeof this.props.onAddValue === 'function') {
      this.props.onAddValue(index);
    }
    return this;
  },

  removeValue: function (index, e) {
    if (typeof this.props.onRemoveValue === 'function') {
      this.toClose = false;
      this.props.onRemoveValue(index);
    }
    return this;
  },

  getSelectedIndex: function () {
    return this.state.selectedIndex;
  },

  getSelectedOptions: function () {
    var input = this;
    var options;
    if (this.getSelectedIndex() === null) {
      options = null;
    } else {
      options = this.getRenderedChildren().filter(function (child, i) {
        return input.props.value && input.props.value.indexOf(i);
      });
    }
    return options;
  },

  getSelectedValue: function () {
    var options = this.getSelectedOptions();
    return options ? options.map(function (opt) {
      return opt.getValue();
    }) : null;
  },

  // Overriding Functions
  expandChildProps_: function (child, i) {
    var props = $.extend(true, {}, child.props);
    props.selected = i == this.props.value;

    if (this.props.value && this.props.value.indexOf(i) > -1 && this.state.selectedIndex !== i) {
      props.className = props.className ? props.className + ' ' + this.getClassName().selectedItem : this.getClassName().selectedItem;
    }

    if (this.state.selectedIndex === i) {
      props.className = props.className ? props.className + ' ' + this.getClassName().hoverItem : this.getClassName().hoverItem;
    }

    props.mainAttrs = props.mainAttrs || {};
    props.mainAttrs.id = this.getId() + '-item-' + i;

    var input = this;
    var childOnClick = props.onClick;
    props.onClick = function (opt) {
      input.addValue(opt.value, childOnClick);
    };

    return props;
  },

  // Component Specific Helpers

  _getMainAttrs: function () {
    var attrs = this.getMainAttrs();
    this.props.isButtonHidden && (attrs.className += ' ' + this.getClassName().noButton);

    if (this.isOpened()) {
      attrs.className += ' ' + this.getClassName().opened;

      var input = this;
      var customOnKeyPress = attrs.onKeyPress;
      attrs.onKeyPress = function (e) {
        // stop submit form on enter if input is alone in form
        if (e.keyCode == 13 || e.charCode == 13) {
          e.preventDefault();
          input.addValue(input.getSelectedIndex());
        }
        if (typeof customOnKeyPress === 'function') {
          customOnKeyPress(e);
        }
      };

      var customOnKeyDown = attrs.onKeyDown;
      attrs.onKeyDown = function (e) {
        switch (e.keyCode) {
          case 27:
            e.preventDefault();
            input.close();
            break;
          case 38:
            e.preventDefault();
            input.selectUp();
            break;
          // case 39:
          //   input.open();
          //   break;
          case 40:
            e.preventDefault();
            input.selectDown();
            break;
        }

        if (typeof customOnKeyDown === 'function') {
          customOnKeyDown(e);
        }
      };

      var onMouseOver = attrs.onMouseOver;

      attrs.onMouseOver = function (e) {
        if (input.getSelectedIndex() !== null) {
          var callback;
          typeof onMouseOver === 'function' && (callback = onMouseOver.bind(input, e));
          input.setState({ selectedIndex: null }, callback);
        }
      };
    }

    return attrs;
  },

  _getBackdropProps: function () {
    var backdropId = this.getId() + "-backdrop";

    return {
      hidden: !this.isOpened(),
      id: backdropId,
      onClick: function (event) {
        event.target.id === backdropId && this.close();
      }.bind(this)
    };
  },

  _getChildren: function () {
    return this.getChildren();
  },

  _getButtonProps: function () {
    var input = this;
    return {
      className: this.getClassName().button,
      onClick: function () {
        input.toggle();
      },
      disabled: this.isDisabled(),
      pressed: this.isOpened()
    };
  },

  _getHeader: function (children) {
    var result;
    if (this.props.value === null) {
      if (this.props.placeholder) {
        result = React.createElement(UU5.Bricks.span, { className: this.getClassName().placeholder, content: this.props.placeholder });
      }
    } else {
      var input = this;

      result = this.props.value.map(function (i) {
        var child = children[i];
        var value = child.props.selectedContent || child.props.content || child.props.children && React.Children.toArray(child.props.children) || child.props.value;

        return React.createElement(
          UU5.Bricks.span,
          { className: input.getClassName().headerItem, key: i },
          React.createElement(UU5.Bricks.span, { className: input.getClassName().headerItemText, content: value }),
          React.createElement(UU5.Bricks.glyphicon, {
            glyphicon: input.getDefault().closeIcon,
            className: input.getClassName().headerItemClose,
            mainAttrs: {
              onClick: input.removeValue.bind(input, i)
            }
          })
        );
      });
    }
    return result;
  },

  _getMenuAttrs: function () {
    return {
      className: this.getClassName().menu
    };
  },

  _getHeaderProps: function () {
    var input = this;
    return {
      className: this.getClassName().header,
      mainAttrs: {
        onClick: function (e) {
          if (input.toClose) {
            input.toggle();
          } else {
            input.toClose = true;
          }
        }
      }
    };
  },

  _getHeaderContentProps: function (children) {
    return {
      className: this.getClassName().headerContent,
      content: this._getHeader(children)
    };
  },

  // Render
  render: function () {
    var children = this._getChildren();

    return React.createElement(
      'div',
      this._getMainAttrs(),
      React.createElement(
        'div',
        { className: this.getClassName().inputGroup },
        React.createElement(
          'div',
          { className: this.getClassName().inputFeedback },
          React.createElement(
            UU5.Bricks.div,
            this._getHeaderProps(),
            React.createElement(UU5.Bricks.div, this._getHeaderContentProps(children))
          ),
          this.props.glyphicon
        ),
        !this.props.isButtonHidden && React.createElement(
          'span',
          { className: this.getClassName().inputBtnGroup },
          React.createElement(
            UU5.Bricks.button,
            this._getButtonProps(),
            React.createElement(UU5.Bricks.glyphicon, { glyphicon: this.props[this.isOpened() ? 'openedGlyphicon' : 'closedGlyphicon'] })
          )
        )
      ),
      React.createElement(
        'div',
        this._getMenuAttrs(),
        React.createElement(
          'div',
          { className: this.getClassName().innerMenu, id: this.getId() + '-menu' },
          children
        )
      ),
      React.createElement(UU5.Bricks.backdrop, this._getBackdropProps())
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Forms = UU5.Forms || {};

UU5.Forms.slider = React.createClass({
  displayName: 'slider',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Common.contentMixin, UU5.Forms.inputMixin],

  statics: {
    tagName: 'UU5.Forms.slider',
    classNames: {
      main: 'UU5_Forms_slider',
      inputGroup: 'UU5_Forms_slider-inputGroup',
      slider: 'UU5_Forms_slider-slider',
      number: 'UU5_Forms_slider-number'
    }
  },

  propTypes: {
    colorSchema: React.PropTypes.oneOf(UU5.Common.environment.colorSchema),
    // TODO
    //position: React.PropTypes.oneOf(['horizontal', 'vertical']),
    min: React.PropTypes.number,
    max: React.PropTypes.number,
    step: React.PropTypes.number,
    value: React.PropTypes.number,
    onChange: React.PropTypes.func,
    onChanged: React.PropTypes.func
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      colorSchema: 'primary',
      //position: 'horizontal',
      min: 0,
      max: 10,
      step: 1,
      value: null,
      onChanged: null
    };
  },

  componentWillMount: function () {
    this.setState({ value: typeof this.props.value === 'number' ? this.props.value : this.props.min });
  },

  // Interface

  // Overriding Functions
  setValue_: function (value, setStateCallback, options) {
    options = options || {};
    options.feedback = options.feedback || 'initial';
    options.message = options.message || '';
    options.value = value;
    this.setState(options, setStateCallback);
    return this;
  },

  // Component Specific Helpers
  // _onChange: function (e) {
  //   if (!this.isDisabled()) {
  //     var value = !this.getValue();
  //     var newState = this._validateValue(value);
  //
  //     if (newState) {
  //       this.setState(newState);
  //     } else {
  //       if (this.props.onChange) {
  //         this.props.onChange({ value: value, input: this, event: e });
  //       } else {
  //         this.setState({ value: value });
  //       }
  //     }
  //   }
  //   return this;
  // },

  _getMainAttrs: function () {
    return this.getInputMainAttrs();
  },

  _getInputGroupAttrs: function () {
    return {
      className: this.getClassName().inputGroup
    };
  },

  _getSliderProps: function () {
    return {
      name: this.getName(),
      className: this.getClassName().slider,
      colorSchema: this.props.colorSchema,
      //position: 'horizontal',
      min: this.props.min,
      max: this.props.max,
      step: this.props.step,
      value: this.getValue() === null ? this.props.min : this.getValue(),
      content: this.getContent(),
      onChange: this._onChange,
      onChanged: this.props.onChanged,
      disabled: this.isDisabled()
    };
  },

  _getNumberProps: function () {
    var value = this.getValue();
    value = value > this.props.max ? this.props.max : value < this.props.min ? this.props.min : value;

    return {
      className: this.getClassName().number,
      min: this.props.min,
      max: this.props.max,
      value: this.getValue(),
      onChange: this._onChange,
      onBlur: function (opt) {
        if (this.getValue() < this.props.min) {
          opt.component.setValue(this.props.min);
        } else if (this.getValue() > this.props.max) {
          opt.component.setValue(this.props.max);
        }
      }.bind(this),
      disabled: this.isDisabled(),
      onChangeFeedback: this._onChangeNumberFeedback
    };
  },

  _getOnChanged: function (value, e) {
    var onChanged;
    if (typeof this.props.onChanged === 'function') {
      var slider = this;
      onChanged = function () {
        slider.props.onChanged({ value: value, component: slider, event: e });
      };
    }
    return onChanged;
  },

  _onChange: function (opt) {
    if (!this.isDisabled()) {
      if (typeof this.props.onChange == 'function') {
        this.props.onChange($.extend({}, opt, { component: this }));
      } else {
        this.setFeedback('initial', '', opt.value, this._getOnChanged(opt.value, opt.event));
      }
    }
    return this;
  },

  _onChangeNumberFeedback: function (opt) {
    this.setValue(opt.value ? +opt.value : null, opt.callback);
    //this.setFeedback(opt.feedback, opt.message, opt.value ? +opt.value : null, opt.callback);
    return this;
  },

  // Render
  render: function () {
    return React.createElement(
      'div',
      this._getMainAttrs(),
      this.getLabelChild(),
      React.createElement(
        'div',
        this.getInputWrapperAttrs(),
        React.createElement(
          'div',
          this._getInputGroupAttrs(),
          React.createElement(
            UU5.Bricks.slider,
            this._getSliderProps(),
            this.props.children && React.Children.toArray(this.props.children)
          ),
          React.createElement(UU5.Forms.number, this._getNumberProps())
        ),
        this.getMessageChild()
      )
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Forms = UU5.Forms || {};

UU5.Forms.text = React.createClass({
  displayName: 'text',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Forms.textInputMixin],

  statics: {
    tagName: 'UU5.Forms.text',
    classNames: {
      main: 'UU5_Forms_text'
    }
  },

  propTypes: {
    value: React.PropTypes.string,
    isPassword: React.PropTypes.bool
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      value: '',
      isPassword: false
    };
  },

  // Interface

  // Overriding Functions

  // Component Specific Helpers
  _getInputAttrs: function () {
    var inputAttrs = this.getTextInputAttrs();
    this.props.isPassword && (inputAttrs.type = 'password');
    return inputAttrs;
  },

  // Render
  render: function () {
    return React.createElement(
      'div',
      this.getTextInputMainAttrs(),
      this.getLabelChild(),
      React.createElement(
        'div',
        this.getInputWrapperAttrs(),
        this.getAutoComplete(),
        React.createElement('input', this._getInputAttrs()),
        this.getGlyphiconChild(),
        this.getMessageChild()
      )
    );
  }
});

'use strict';

var UU5 = UU5 || {};
UU5.Forms = UU5.Forms || {};

UU5.Forms.textarea = React.createClass({
  displayName: 'textarea',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Forms.textInputMixin],

  statics: {
    tagName: 'UU5.Forms.textarea',
    classNames: {
      main: 'UU5_Forms_textarea'
    }
  },

  propTypes: {
    value: React.PropTypes.string,
    rows: React.PropTypes.number
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      value: '',
      rows: 4
    };
  },

  // Interface

  // Component Specific Helpers
  _getInputAttrs: function () {
    var onKeyPress = this.props.mainAttrs && this.props.mainAttrs.onKeyPress;

    var inputAttrs = this.getTextInputAttrs();
    inputAttrs.rows = this.props.rows;

    // reset text input setting onKeyPress for submit on enter - see textInputMixin getInputAttrs
    inputAttrs.onKeyPress = onKeyPress;

    return inputAttrs;
  },

  // Render
  render: function () {
    return React.createElement(
      'div',
      this.getTextInputMainAttrs(),
      this.getLabelChild(),
      React.createElement(
        'div',
        this.getInputWrapperAttrs(),
        React.createElement('textarea', this._getInputAttrs()),
        this.getGlyphiconChild(),
        this.getMessageChild()
      )
    );
  }
});

'use strict';

UU5.Forms = UU5.Forms || {};

UU5.Forms.textGlyphicon = React.createClass({
  displayName: 'textGlyphicon',


  mixins: [UU5.Common.baseMixin, UU5.Common.elementaryMixin, UU5.Forms.textInputMixin],

  statics: {
    tagName: 'UU5.Forms.textGlyphicon',
    classNames: {
      main: 'UU5_Forms_textGlyphicon',
      position: 'UU5_Forms_textGlyphicon-position-',
      onClick: 'UU5_Forms_textGlyphicon-click'
    }
  },

  propTypes: {
    glyphicon: React.PropTypes.string,
    glyphiconPosition: React.PropTypes.oneOf(['left', 'right']),
    value: React.PropTypes.string,
    isPassword: React.PropTypes.bool,
    onClick: React.PropTypes.func
  },

  // Setting defaults
  getDefaultProps: function () {
    return {
      glyphicon: null,
      glyphiconPosition: 'right',
      value: '',
      isPassword: false,
      onClick: null
    };
  },

  // Interface

  // Component Specific Helpers
  _onClick: function (e) {
    this.props.onClick({ value: this.getValue(), component: this, event: e });
    return this;
  },

  // Render
  _performPropsAndState2Local: function (local) {
    local.mainProps = $.extend(true, {}, this.props.mainAttrs);
    this.buildMainProps(local.mainProps);
    local.mainProps.className += ' ' + this.getDefault().position + this.props.glyphiconPosition;
    this.props.bsSize && (local.mainProps.className += ' ' + this.getDefault().size + this.props.bsSize);

    if (this.props.glyphicon) {
      local.mainProps.className += ' has-feedback';

      local.glyphiconProps = {
        glyphicon: this.props.glyphicon,
        className: this.getDefault('glyphicon')
      };

      if (typeof this.props.onClick === 'function') {
        local.glyphiconProps.onClick = this._onClick;
        local.glyphiconProps.className += ' ' + this.getDefault().onClick;
      }
    }

    // input attributes
    local.inputProps = {};
    local.inputProps.ref = 'input';
    local.inputProps.type = this.props.password ? 'password' : 'text';
    local.inputProps.name = this.props.name;
    local.inputProps.placeholder = this.props.placeholder;
    local.inputProps.className = this.getDefault('input') + (this.props.bsSize ? ' input-' + this.props.bsSize : '');
    local.inputProps.onFocus = this._onFocusHandler;
    local.inputProps.onBlur = this._onBlurHandler;
    local.inputProps.onChange = this._onChange;
    this.isDisabled() && (local.inputProps.disabled = 'disabled');
    local.inputProps.value = this.state.value;

    // render itself
    local.main = React.createElement(
      'div',
      local.mainProps,
      React.createElement('input', local.inputProps),
      local.glyphiconProps && React.createElement(UU5.Bricks.glyphicon, local.glyphiconProps)
    );

    return this;
  },

  _getMainAttrs: function () {
    var mainAttrs = this.getTextInputMainAttrs();
    mainAttrs.className += ' ' + this.getClassName().position + this.props.glyphiconPosition;
    this.props.glyphicon && (mainAttrs.className += ' has-feedback');
    return mainAttrs;
  },

  _getGlyphicon: function () {
    var glyphicon = null;

    if (this.props.glyphicon) {
      var glyphiconProps = {
        glyphicon: this.props.glyphicon,
        className: this.getClassName('glyphicon', 'UU5_Forms_inputMixin')
      };

      if (typeof this.props.onClick === 'function' && !this.isDisabled()) {
        glyphiconProps.mainAttrs = {
          onClick: this._onClick
        };
        glyphiconProps.className += ' ' + this.getClassName().onClick;
      }

      glyphicon = React.createElement(UU5.Bricks.glyphicon, glyphiconProps);
    }

    return glyphicon;
  },

  _getInputAttrs: function () {
    var inputAttrs = this.getTextInputAttrs();
    this.props.isPassword && (inputAttrs.type = 'password');
    return inputAttrs;
  },

  render: function () {
    return React.createElement(
      'div',
      this._getMainAttrs(),
      this.getLabelChild(),
      React.createElement(
        'div',
        this.getInputWrapperAttrs(),
        React.createElement('input', this._getInputAttrs()),
        this._getGlyphicon(),
        this.getMessageChild()
      )
    );
  }
});

'use strict';

// Never use UU5abbr in UU5 project internally !!!

// bricks
var UU5div = UU5.Bricks.div;
var UU5p = UU5.Bricks.paragraph;
var UU5span = UU5.Bricks.span;
var UU5header = UU5.Bricks.header;
var UU5footer = UU5.Bricks.footer;

var UU5textLow = UU5.Bricks.textLow;
var UU5textPrimary = UU5.Bricks.textPrimary;
var UU5textSuccess = UU5.Bricks.textSuccess;
var UU5textInfo = UU5.Bricks.textInfo;
var UU5textWarning = UU5.Bricks.textWarning;
var UU5textDanger = UU5.Bricks.textDanger;

var UU5boxPrimary = UU5.Bricks.boxPrimary;
var UU5boxSuccess = UU5.Bricks.boxSuccess;
var UU5boxInfo = UU5.Bricks.boxInfo;
var UU5boxWarning = UU5.Bricks.boxWarning;
var UU5boxDanger = UU5.Bricks.boxDanger;

// layout
var UU5root = UU5.Layout.root;
var UU5container = UU5.Layout.container;
var UU5containerCollection = UU5.Layout.containerCollection;
var UU5row = UU5.Layout.row;
var UU5rowCollection = UU5.Layout.rowCollection;
var UU5column = UU5.Layout.column;
var UU5columnCollection = UU5.Layout.columnCollection;
var UU5wrapper = UU5.Layout.wrapper;
var UU5wrapperCollection = UU5.Layout.wrapperCollection;
var UU5flc = UU5.Layout.flc;


