/**
 * Created by vladimir on 13. 12. 2016.
 */

var goodymatSchema = {
  id: "01234567890abcdef01234567890abcdef", //generated unique code
  awid: "01234567890abcdef01234567890abcdef", //appWorkspaceId - unique code specified externally
  sys: {
    cts: "...", //creation timestamp
    mts: "...", //modification timestamp
    rev: 0 //revision number
  },
  code: "...", //Plus4UID of Goodymat (uuThing)
  state: "...", //state of order ["active","finished"]
  nameLSI: { //LSI Goodymat name
    en: "...",
    cz: "..."
  },
  descLSI: { //LSI Goodymat description
    en: "...",
    cz: "..."
  },
  operationMap: {
    "O1": {
      customer: "...", //Plus4UID of customer
      product: "...", //Code of product
    },
    "O2": {
      customer: "...", //Plus4UID of customer
      product: "...", //Code of product
    },
    "...": {}
  }
};
