/**
 * Created by vladimir on 13. 12. 2016.
 */


var shopSchema = {
  id: "01234567890abcdef01234567890abcdef", //generated unique code
  awid: "01234567890abcdef01234567890abcdef", //appWorkspaceId - unique code specified externally
  sys: {
    cts: "...", //creation timestamp
    mts: "...", //modification timestamp
    rev: 0, //revision number
  },
  state: "...", //state of order ["open","closed"]
  nameLSI: { //LSI name of the shop
    "en": "Vladimir's Goodymat",
    "cz": "Vladimírův pamlskomat"
  },
  welcomeLSI: { //LSI welcome message of the shop
    "en": "Welcome,  ...",
    "cz": "Ahoj, toto je internetový obchod, který nabízí uzená kolena a králičí uši."
  },
  productMap: { //collection of products
    "PCODE1": {
      nameLSI: { //LSI name of product
        "en": "...",
        "cz": "uzené koleno"
      },
      descLSI: { //LSI description of product
        "en": "...",
        "cz": "Výborná pochoutka z vepřového uzeného kolene, 100 % přírodní."
      },
      image: "...", //image of product
    },
    "PCODE2": {
      nameLSI: { //LSI name of product
        "en": "...",
        "cz": "králičí uši"
      },
      descLSI: { //LSI description of product
        "en": "...",
        "cz": "Výborná pochoutka pro pejsky z králičích oušek, bez konzervantů. Neobsahuje alergeny."
      },
      image: "...", //image of product
    },
    "PCODE3": {
      nameLSI: { //LSI name of product
        "en": "...",
        "cz": "gumoví medvídci"
      },
      descLSI: { //LSI description of product
        "en": "...",
        "cz": "Výborná pochoutka pro děti a lidi. Neobsahuje alergeny."
      },
      image: "...", //image of product
    }
  }
};
