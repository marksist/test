/**
 * Created by vladimir on 13. 12. 2016.
 */


var orderSchema = {
  id: "01234567890abcdef01234567890abcdef", //generated unique code
  awid: "01234567890abcdef01234567890abcdef", //appWorkspaceId - unique code specified externally
  sys: {
    cts: "...", //creation timestamp
    mts: "...", //modification timestamp
    rev: 0 //revision number
  },
  state: "...", //state of order ["ordered","finished"]
  createOrderTs: "...", //creation order timestamp
  finishOrderTs: "...", //finish order timestamp
  customer: "...", //Plus4UId of customer
  product: "...", //product code
  goodymat: "..." //Plus4UId of Goodymat (if not via IoT, then null)
};
