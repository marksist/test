/**
 * Created by vladimir on 13. 12. 2016.
 */

var customerSchema = {
  id: "01234567890abcdef01234567890abcdef", //generated unique code
  awid: "01234567890abcdef01234567890abcdef", //appWorkspaceId - unique code specified externally
  sys: {
    cts: "...", //creation timestamp
    mts: "...", //modification timestamp
    rev: 0 //revision number
  },
  code: "...", //Plus4UID of customer (uuPerson, uuEE)
  state: "...", //state of order ["active","closed"]
  name: "...", //customer name
  type: "...", //customer type [uuPerson|uuEE]
  image: "..." //image
};