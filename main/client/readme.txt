**Instalation**

cd client/
npm install


**Run**

npm start

Index - http://localhost.plus4u.net:1234/

Addresses for calling uuCommands can be configured in tools/webpack-uc-*.config.js (for
development and production modes).


**Create distribution for production**

npm run dist

Performs build into ../server/public/ folder.


**Notes and Troubleshooting**

* Web server must be restarted if new project package is created (to update webpack
configuration).
* Web server must be restarted if a dependency is used (imported) in JS file for
the 1st time in whole project (transitive dependencies must be recomputed and that
happens only once during npm start).
* If a dependency is added (npm install --save <dep>), the build tool will try
to find which browser files need to be present so that the dependency can be
loaded in a separate HTTP request ("externally"). If it doesn't succeed, it's
necessary to copy the browser files of the dependency to src/lib/<dep>-<depVersion>/
and configure mapping in tools/dependencies.config.js to dependency's entry point
(main JS file).