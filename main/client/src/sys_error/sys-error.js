import React from "react";
import * as UU5 from 'uu5';
import * as UclGoodymat from "project/common";
import "./sys-error.css";

const VUC_AFTER_LOGIN_ON_ERROR_PAGE = "/eshop";

const SysError = React.createClass(
  {

    //@@viewOn:mixins
    mixins:[
      UU5.Common.BaseMixin,
      UU5.Common.VucMixin,
      UU5.Common.IdentityMixin
    ],
    //@@viewOff:mixins

    //@@viewOn:statics
    statics:{
      vucName:'sysAbout',
      tagName:'UU5.UU.AppWorkspace.VucSysAbout',
      classNames:{
        main:'uu-appworkspace-vucsyserror',
        wrapper:'uu-appworkspace-vucsyserror-wrapper',
        header:'uu-appworkspace-vucsyserror-header',
        info:'uu-appworkspace-vucsyserror-info',
        infoText:'uu-appworkspace-vucsyserror-info-text',
        linkMore:'uu-appworkspace-vucsyserror-link-more',
        contact:'uu-appworkspace-vucsyserror-contact',
        contactImg:'uu-appworkspace-vucsyserror-contact-img uu5-common-font-size-xxl',
        contactTitle:'uu-appworkspace-vucsyserror-contact-title',
        contactPhone:'uu-appworkspace-vucsyserror-contact-phone',
        hand:'uu-appworkspace-vucsyserror-hand uu5-common-font-size-xxl'
      },
      sysErrorData:{
        headerLSI:{
          "cs":"Obsah není možné zobrazit",
          "en-gb":"The content cannot be displayed",
          "en-us":"We can't show yo' da shit",
          "sk":"Obsah nie je možné zobraziť",
          "ua":"Зміст не може бути зображений."
        },
        infoTextLSI:{
          "cs":"Omlouváme se, ale buďto nejste přihlášen, nebo nemáte oprávnění spustit požadovanou funkčnost.",
          "en-gb":"We are sorry, but you either are not logged in, or, you are not permitted to see the content.",
          "en-us":"Sorry, but you either r'not logged in, or, yo' don't have access ta da requested page.",
          "sk":"Ospravedlňujeme sa, ale buď nie ste prihlásený alebo nemáte oprávnenie spustiť požadovanú funkčnosť.",
          "ua":"Вибачте,ви не ввійшли в інтернет-магазин, або немаєте дозволу запустити потрібну функцію."
        },
        linkMoreLSI:{
          "cs":"Dozvědět se více",
          "en-gb":"Learn more",
          "en-us":"Learn more shit",
          "sk":"Zobraziť viac",
          "ua":"Дізнатися більше"
        },
        contactData:{
          titleLSI:{
            "cs":"Centrální helpdesk",
            "en-gb":"Central Helpdesk",
            "en-us":"Central Helpdesk",
            "sk":"Centrálny helpdesk",
            "ua":"Центральний до служби підтримки"
          },
          phoneLSI:{
            "cs":"+420 221 400 400",
            "en-gb":"+420 221 400 400",
            "en-us":"+420 221 400 400",
            "sk":"+420 221 400 400",
            "ua":"+420 221 400 400"
          },
        },
      },
    },
    //@@viewOff:statics

    //@@viewOn:overridingMethods
    onChangeIdentity_(session) {
      this.changeIdentity();

      // if identity changed after 1st VUC authorization then redirect to default VUC
      if(this.state.authorizedFeedback != "loading" && session.isAuthenticated()){
        this.setRoute(VUC_AFTER_LOGIN_ON_ERROR_PAGE);
      }
    },
    //@@viewOff:overridingMethods

    //@@viewOn:render
    render() {
      return (
        <UU5.Layout.Root
          header={{
            element:<UclGoodymat.Header labelLsi={{en:"About us", cs:"O nás"}}
              menuProfileCodes={this.state.profiles}/>
          }}
          footer={{
            element:<UclGoodymat.Footer/>
          }}
        >

          <UU5.Layout.Container className={this.getClassName().wrapper}>

            <UU5.Bricks.Badge className={this.getClassName().hand}>
              <UU5.Bricks.Glyphicon glyphicon='glyphicon glyphicon-remove'/>
            </UU5.Bricks.Badge>

            <UU5.Bricks.Section header={<UU5.Bricks.Lsi className={this.getClassName().header}
              lsi={this.constructor.sysErrorData.headerLSI}/>}>


              <UU5.Bricks.Div className={this.getClassName().info}>
                <UU5.Bricks.P content={<UU5.Bricks.Lsi className={this.getClassName().infoText}
                  lsi={this.constructor.sysErrorData.infoTextLSI}/>}/>

                <UU5.Bricks.Link content={<UU5.Bricks.Lsi className={this.getClassName().linkMore}
                  lsi={this.constructor.sysErrorData.linkMoreLSI}/>} href="https://unicorn.com/" target='_top'/>
              </UU5.Bricks.Div>


              <UU5.Bricks.Div className={this.getClassName().contact}>

                <UU5.Bricks.Glyphicon glyphicon='glyphicon glyphicon-earphone'
                  className={this.getClassName().contactImg}/>

                <UU5.Bricks.P content={<UU5.Bricks.Lsi className={this.getClassName().contactTitle}
                  lsi={this.constructor.sysErrorData.contactData.titleLSI}/>}
                />

                <UU5.Bricks.P
                  content={<UU5.Bricks.Lsi className={this.getClassName().contactPhone}
                    lsi={this.constructor.sysErrorData.contactData.phoneLSI}/>}
                />

              </UU5.Bricks.Div>


            </UU5.Bricks.Section>
          </UU5.Layout.Container>
        </UU5.Layout.Root>
      );
    }
    //@@viewOff:render

  }
);

export default SysError;
