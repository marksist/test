import React from "react";
import * as UU5 from 'uu5';
//CSS imports
import "./backoffice-orders-row.css";

const BackofficeOrdersRow = React.createClass(
  {

    //@@viewOn:mixins
    mixins:[
      UU5.Common.BaseMixin,
      UU5.Common.CallsMixin,
      UU5.Common.CcrReaderMixin,
      UU5.Common.ElementaryMixin,
      UU5.Layout.RowMixin
    ],
    //@@viewOff:mixins

    //@@viewOn:statics
    statics:{
      tagName:'Ucl.Goodymat.BackofficeOrdersRow',
      classNames:{
        main:'ucl-goodymat-order-row',
        orderRowInfo:'ucl-goodymat-order-row-info',
        orderRowInfoOrderId:'ucl-goodymat-order-row-info-order-id',
        orderRowInfoGoodymat:'ucl-goodymat-order-row-info-goodymat',
        orderRowInfoGoodymatIcon:'ucl-goodymat-order-row-info-goodymat-icon uubml-access-point',
        orderButtonWrapper:'ucl-goodymat-order-row-button-wrapper',
        orderButton:'ucl-goodymat-order-row-button'

      },
      orderRowData:{
        buttonNameLSI:{
          "cs":"Uzavřít objednávku",
          "en-gb":"Close order",
          "en-us":"Close orda",
          "sk":"Uzavrieť objednávku",
          "ua":"акрити зам."
        }
      },
      calls:{
        proceedOrder:'proceedOrder'
      }
    },
    //@@viewOff:statics

    //@@viewOn:propTypes
    //@@viewOff:propTypes
    //@@viewOn:getDefaultProps
    //@@viewOff:getDefaultProps
    //@@viewOn:standardComponentLifeCycle
    //@@viewOff:standardComponentLifeCycle

    //@@viewOn:componentSpecificHelpers
    _renderGoodymatInfo(gm){
      if(gm != null){
        return <UU5.Bricks.Div className={this.getClassName().orderRowInfoGoodymat}>
          <UU5.Bricks.Glyphicon  className={this.getClassName().orderRowInfoGoodymatIcon}/>
          <UU5.Bricks.P content={gm}/>
        </UU5.Bricks.Div>
      }
    },

    _proceedOrder(orderId){

      let orderList = this.getCcrComponentByKey(this.props.orderListCcrKey);
      let call = this.getCall('proceedOrder');

      this.disable(
        () => call(
          {
            data:{id:orderId},
            done:dtoOut => orderList.removeItem(orderId),
            fail:dtoOut =>{

              switch(dtoOut.status){

                case 400:
                  let errorCode = (
                    dtoOut.data || {}
                  ).code;
                  if(errorCode === 'UCL.GOODYMAT.MAIN.ORDER/E008' || 'UCL.GOODYMAT.MAIN.ORDER/E002'){
                    orderList.removeItem(orderId);
                  }else{
                    this.enable();
                  }
                  break;

                case 500:
                  this.enable();
                  break;

              }
            }
          }
        )
      )
    },
    //@@viewOff:componentSpecificHelpers

    //@@viewOn:render
    render() {
      return (

        <UU5.Layout.Row {...this.getMainPropsToPass()}
          display='flex-full'
        >

          <UU5.Layout.Column colWidth='xs-2 md-2 lg-2'>
            <UU5.Layout.Flc className={this.getClassName().orderRowInfo}>
              <UU5.Bricks.P content={this.props.createOrderTs}/>
            </UU5.Layout.Flc>
          </UU5.Layout.Column>

          <UU5.Layout.Column colWidth='xs-4 md-4 lg-4'>
            <UU5.Layout.Flc className={this.getClassName().orderRowInfo}>
              <UU5.Bricks.P content={this.props.id}/>
            </UU5.Layout.Flc>
          </UU5.Layout.Column>


          <UU5.Layout.Column colWidth='xs-2 md-2 lg-2'>
            <UU5.Layout.Flc className={this.getClassName().orderRowInfo}>
              <UU5.Bricks.P content={<UU5.Bricks.Lsi lsi={this.props.productName}/>}/>
            </UU5.Layout.Flc>
          </UU5.Layout.Column>

          <UU5.Layout.Column colWidth='xs-2 md-2 lg-2'>
            <UU5.Layout.Flc className={this.getClassName().orderRowInfo}>
              <UU5.Bricks.P content={this.props.customerName}/>
              {this._renderGoodymatInfo(this.props.goodymatCode)}
            </UU5.Layout.Flc>
          </UU5.Layout.Column>

          <UU5.Layout.Column colWidth='xs-2 md-2 lg-2'>
            <UU5.Layout.Flc className={this.getClassName().orderButtonWrapper}>
              <UU5.Bricks.Button
                className={this.getClassName().orderButton}
                content={<UU5.Bricks.Lsi lsi={this.constructor.orderRowData.buttonNameLSI}/>}
                onClick={this._proceedOrder.bind(null, this.props.id)}
              />

            </UU5.Layout.Flc>
          </UU5.Layout.Column>

        </UU5.Layout.Row >

      );
    }
    //@@viewOff:render

  }
);

export default BackofficeOrdersRow;