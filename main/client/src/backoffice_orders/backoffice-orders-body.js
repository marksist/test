import React from 'react';
import UU5 from 'uu5';
import BackofficeOrdersList from "./backoffice-orders-list.js";
import BackofficeOrdersCounter from "./backoffice-orders-counter.js";
import "./backoffice-orders-body.css";

const BackofficeOrdersBody = React.createClass(
  {
    //@@viewOn:mixins
    mixins:[
      UU5.Common.BaseMixin,
      UU5.Common.ElementaryMixin,
      UU5.Common.LoadMixin,
      UU5.Layout.ContainerMixin
    ],
    //@@viewOff:mixins

    //@@viewOn:statics
    statics:{
      tagName:'Ucl.Goodymat.BackofficeOrdersBody',
      classNames:{
        main:'ucl-goodymat-backoffice-body'
      },
      defaults:{
        counterCcrKey:'ucl-goodymat-order-counters',
        orderListCcrKey:'ucl-goodymat-order-list'
      },
      calls:{
        onLoad:'listActiveOrders'
      },
      backofficeData:{
        backofficeHeaderLSI:{
          "cs":"Přehled objednávek",
          "en-gb":"Order list",
          "en-us":"Orda list",
          "sk":"Správa objednávok",
          "ua":"управління замовленнями"
        },
      }
    },
    //@@viewOff:statics

    //@@viewOn:propTypes
    //@@viewOff:propTypes
    //@@viewOn:getDefaultProps
    //@@viewOff:getDefaultProps
    //@@viewOn:standardComponentLifeCycle
    //@@viewOff:standardComponentLifeCycle
    //@@viewOn:interface
    //@@viewOff:interface
    //@@viewOn:overridingMethods
    //@@viewOff:overridingMethods

    //@@viewOn:render
    render() {
      return (

        <UU5.Layout.Container {...this.getMainPropsToPass()}
          header={[
            <UU5.Bricks.Lsi lsi={this.constructor.backofficeData.backofficeHeaderLSI}/>,
            <BackofficeOrdersCounter ccrKey={this.getDefault().counterCcrKey}/>
          ]}
        >

          {this.getLoadFeedbackChildren(
            dtoOut =>{
              return (
                <BackofficeOrdersList
                  items={dtoOut.itemList}
                  calls={this.getCalls()}
                  counterCcrKey={this.getDefault().counterCcrKey}
                  ccrKey={this.getDefault().orderListCcrKey}
                />
              )
            }
          )}

        </UU5.Layout.Container>

      );
    }
    //@@viewOff:render
  }
);

export default BackofficeOrdersBody;