import React from "react";
import * as UU5 from 'uu5';
import BackofficeOrdersRow from "./backoffice-orders-row.js";
import BackofficeOrdersSwiper from "./backoffice-orders-swiper";
import "./backoffice-orders-list.css";

const BackofficeOrdersList = React.createClass(
  {
    //@@viewOn:mixins
    mixins:[
      UU5.Common.BaseMixin,
      UU5.Common.CcrWriterMixin,
      UU5.Common.ElementaryMixin,
      UU5.Common.ScreenSizeMixin,
      UU5.Layout.RowCollectionMixin
    ],
    //@@viewOff:mixins

    //@@viewOn:statics
    statics:{
      tagName:'UU5.Ucl.Goodymat.BackofficeOrdersList',
      classNames:{
        main:'ucl-goodymat-order-list',
        titleRow:'ucl-goodymat-order-list-title-row',
        orderListFooter:'ucl-goodymat-order-list-footer'
      },
      orderListData:{
        orderListFooterLSI:{
          "cs":"Pro vyřízení objednávky přetáhněte položku doleva.",
          "en-gb":"For processing the order, swipe the item to the left.",
          "en-us":"Fo' processin' tha orda, swipe tha shit ta tha left.",
          "sk":"Pre vybavenie objednávky pretiahnite položku doľava",
          "ua":"При виконанні замовлення перетягніть елемент ліворуч."
        },
        orderListTableHeaderLSI:{
          crtLSI:{
            "cs":"Datum a čas",
            "en-gb":"Date and time",
            "en-us":"Date n' time",
            "sk":"Dátum a čas",
            "ua":"Дата і час"
          },
          orderLSI:{
            "cs":"Číslo objednávky",
            "en-gb":"Order Id",
            "en-us":"Orda Id",
            "sk":"id objednávky",
            "ua":"ід замовлення"
          },
          typeLSI:{
            "cs":"Produkt",
            "en-gb":"Product",
            "en-us":"Shit",
            "sk":"Produkt",
            "ua":"продукт"
          },
          customerLSI:{
            "cs":"Zákazník",
            "en-gb":"Customer",
            "en-us":"Customa",
            "sk":"Zákazník",
            "ua":"клієнт"
          }
        }
      }
    },
    //@@viewOff:statics

    //@@viewOn:propTypes
    //@@viewOff:propTypes
    //@@viewOn:getDefaultProps
    //@@viewOff:getDefaultProps

    //@@viewOn:standardComponentLifeCycle
    getInitialState(){
      return {items:this.props.items}
    },

    componentWillReceiveProps(nextProps){
      this.setState({items:nextProps.items})
      this.getCcrComponentByKey(this.props.counterCcrKey).setCounter(nextProps.items.length);
    },

    componentWillMount(){
      this.getCcrComponentByKey(this.props.counterCcrKey).setCounter(this.props.items.length);
    },
    //@@viewOff:standardComponentLifeCycle

    //@@viewOn:interface
    removeItem(id){

      let items = this.state.items;

      for(let i = 0; i < items.length; i++){
        let item = items[i];
        if(id == item['id']){
          items.splice(i, 1);
        }
      }

      this.setState({items:items}, () => this.getCcrComponentByKey(this.props.counterCcrKey).setCounter(items.length));
    },
    //@@viewOff:interface

    //@@viewOn:componentSpecificHelpers
    _renderOrders(items, calls){

      let result = [];

      for(let i = 0; i < items.length; i++){

        let item = items[i];
        let createOrderTs = this._formateDate(item["createOrderTs"]);
        let customerName = item["customerName"];
        let productName = item["productNameLSI"];
        let goodymatCode = item["goodymatCode"];

        if(this.isXs() || this.isSm()){
          createOrderTs = createOrderTs.replace(' ', '<br />');
          result.push(
            <BackofficeOrdersSwiper
              createOrderTs={createOrderTs}
              customerName={customerName}
              productName={productName}
              goodymatCode={goodymatCode}
              key={'key-' + item.id}
              id={item.id}
              calls={calls}
              orderListCcrKey={this.props.ccrKey}
            />
          )
        }else{
          result.push(
            <BackofficeOrdersRow
              createOrderTs={createOrderTs}
              customerName={customerName}
              productName={productName}
              goodymatCode={goodymatCode}
              key={'key-' + item.id}
              id={item.id}
              calls={calls}
              orderListCcrKey={this.props.ccrKey}
            />
          )
        }
      }

      return (
        this.isXs() || this.isSm()
      ) ? <UU5.Layout.Flc>{result}</UU5.Layout.Flc> : result;
    },

    _renderTitleRow(){
      {/* Render row with the description info, only if screen size is large enough. */}
      if(!(
          this.isXs() || this.isSm()
        )){

        return <UU5.Layout.Row className={this.getClassName().titleRow} display='flex-full'>
          <UU5.Layout.Column colWidth='xs-2 md-2 lg-2'><UU5.Bricks.P><UU5.Bricks.Lsi lsi={this.constructor.orderListData.orderListTableHeaderLSI.crtLSI}/></UU5.Bricks.P></UU5.Layout.Column>
          <UU5.Layout.Column colWidth='xs-4 md-4 lg-4'><UU5.Bricks.P><UU5.Bricks.Lsi lsi={this.constructor.orderListData.orderListTableHeaderLSI.orderLSI}/></UU5.Bricks.P></UU5.Layout.Column>
          <UU5.Layout.Column colWidth='xs-2 md-2 lg-2'><UU5.Bricks.P><UU5.Bricks.Lsi lsi={this.constructor.orderListData.orderListTableHeaderLSI.typeLSI}/></UU5.Bricks.P></UU5.Layout.Column>
          <UU5.Layout.Column colWidth='xs-2 md-2 lg-2'><UU5.Bricks.P><UU5.Bricks.Lsi lsi={this.constructor.orderListData.orderListTableHeaderLSI.customerLSI}/></UU5.Bricks.P></UU5.Layout.Column>
          <UU5.Layout.Column colWidth='xs-2 md-2 lg-2'></UU5.Layout.Column>
        </UU5.Layout.Row >
      }else{
        return  <UU5.Layout.Row></UU5.Layout.Row>
      }
    },

    _renderFooter(){
      {/* Render footer with additional info, if screen size is large enough. */}
      if(this.isXs() || this.isSm()){
        return <UU5.Layout.Row>
          <UU5.Bricks.P className={this.getClassName().orderListFooter}
            content={<UU5.Bricks.Lsi lsi={this.constructor.orderListData.orderListFooterLSI}/>}/>
        </UU5.Layout.Row>
      }else{
        return  <UU5.Layout.Row></UU5.Layout.Row>
      }
    },

    _formateDate(date){
      var format_date = new Date(Date.parse(date));
      format_date = format_date.getDate() + '.' + (
          format_date.getMonth() + 1
        ) + '.' + format_date.getFullYear().toString().substr(2, 2)
        + ' ' + format_date.getHours() + ':' + (
          format_date.getMinutes() < 10 ? '0' : ''
        ) + format_date.getMinutes();
      return format_date;
    },
    //@@viewOff:componentSpecificHelpers

    //@@viewOn:render
    render() {
      return (
        <UU5.Layout.RowCollection
          header={{element:this._renderTitleRow()}}
          footer={{element:this._renderFooter()}}
          {...this.getMainPropsToPass()}>
          {this._renderOrders(this.state.items, this.props.calls)}
        </UU5.Layout.RowCollection >

      );
    }
    //@@viewOff:render
  }
);

export default BackofficeOrdersList;