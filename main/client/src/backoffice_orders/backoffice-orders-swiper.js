import React from "react";
import * as UU5 from 'uu5';
import "./backoffice-orders-swiper.css";

const BackofficeOrdersSwiper = React.createClass(
  {
    //@@viewOn:mixins
    mixins:[
      UU5.Common.BaseMixin,
      UU5.Common.CallsMixin,
      UU5.Common.CcrReaderMixin,
      UU5.Common.ElementaryMixin
    ],
    //@@viewOff:mixins

    //@@viewOn:statics
    statics:{
      tagName:'Ucl.Goodymat.BackofficeOrdersSwiper',
      classNames:{
        main:'ucl-goodymat-order-swiper',
        swiperBody:'ucl-goodymat-order-swiper-body',
        productName:'ucl-goodymat-order-swiper-product-name',
        orderId:'ucl-goodymat-order-swiper-product-order-id',
        customerName:'ucl-goodymat-order-swiper-customer-name',
        creationTime:'ucl-goodymat-order-swiper-creation-time',
        orderButton:'ucl-goodymat-order-swiper-button',
        orderButtonWrapper:'ucl-goodymat-order-swiper-button-wrapper',
        swiperMenu:'ucl-goodymat-order-swiper-menu',
        orderSwiperInfo:'ucl-goodymat-order-swiper-info',
        orderSwiperInfoWrapper:'ucl-goodymat-order-swiper-info-wrapper',
        orderSwiperInfoGoodymat:'ucl-goodymat-order-swiper-info-goodymat',
        orderSwiperInfoGoodymatIcon:'ucl-goodymat-order-swiper-info-goodymat-icon uubml-access-point',
        orderSwiperInfoCustomer:'ucl-goodymat-order-swiper-info-customer'
      },
      orderSwiperData:{
        buttonNameMobileLSI:{
          "cs":"Uzavřít",
          "en-gb":"Close",
          "en-us":"Close",
          "sk":"Uzavrieť",
          "ua":"Закрити",
        }
      },
      calls:{
        proceedOrder:'proceedOrder'
      }
    },
    //@@viewOff:statics

    //@@viewOn:propTypes
    //@@viewOff:propTypes
    //@@viewOn:getDefaultProps
    //@@viewOff:getDefaultProps
    //@@viewOn:standardComponentLifeCycle
    //@@viewOff:standardComponentLifeCycle

    //@@viewOn:componentSpecificHelpers
    _renderGoodymatInfo(gm){
      if(gm != null){
        return <UU5.Bricks.Div className={this.getClassName().orderSwiperInfoGoodymat}>
          <UU5.Bricks.Glyphicon className={this.getClassName().orderSwiperInfoGoodymatIcon}/>
          <UU5.Bricks.P content={gm}/>
        </UU5.Bricks.Div>
      }
    },
    _proceedOrder(orderId){

      let orderList = this.getCcrComponentByKey(this.props.orderListCcrKey);
      let call = this.getCall('proceedOrder');

      this.disable(
        () => call(
          {
            data:{id:orderId},
            done:dtoOut => orderList.removeItem(orderId),
            fail:dtoOut =>{

              switch(dtoOut.status){

                case 400:
                  let errorCode = (
                    dtoOut.data || {}
                  ).code;
                  if(errorCode === 'UCL.GOODYMAT.MAIN.ORDER/E008' || 'UCL.GOODYMAT.MAIN.ORDER/E002'){
                    orderList.removeItem(orderId);
                  }else{
                    this.enable();
                  }
                  break;

                case 500:
                  this.enable();
                  break;

              }
            }
          }
        )
      )
    },
    //@@viewOff:componentSpecificHelpers

    //@@viewOn:render
    render() {
      return (
        <UU5.Bricks.Swiper {...this.getMainPropsToPass()}>
          <UU5.Bricks.Swiper.Menu className={this.getClassName().swiperMenu} pullRight>
            <UU5.Bricks.Div className={this.getClassName().orderButtonWrapper}>
              <UU5.Bricks.Button
                className={this.getClassName().orderButton}
                content={<UU5.Bricks.Lsi lsi={this.constructor.orderSwiperData.buttonNameMobileLSI}/>}
                onClick={this._proceedOrder.bind(null, this.props.id)}
              />
            </UU5.Bricks.Div>
          </UU5.Bricks.Swiper.Menu>

          <UU5.Bricks.Swiper.Body className={this.getClassName().swiperBody}>

            <UU5.Bricks.Div className={this.getClassName().orderSwiperInfoWrapper}>

              <UU5.Bricks.Div className={this.getClassName().orderSwiperInfo}>
                <UU5.Bricks.P className={this.getClassName().creationTime} content={this.props.createOrderTs}/>
              </UU5.Bricks.Div>

              <UU5.Bricks.Div className={this.getClassName().orderSwiperInfo}>
                <UU5.Bricks.P className={this.getClassName().productName}
                  content={<UU5.Bricks.Lsi lsi={this.props.productName}/>}/>
                <UU5.Bricks.P className={this.getClassName().orderId} content={this.props.id}/>

                <UU5.Bricks.Div className={this.getClassName().orderSwiperInfoCustomer}>
                  <UU5.Bricks.P content={this.props.customerName}/>
                  {this._renderGoodymatInfo(this.props.goodymatCode)}
                </UU5.Bricks.Div>
              </UU5.Bricks.Div>

            </UU5.Bricks.Div>

          </UU5.Bricks.Swiper.Body>
        </UU5.Bricks.Swiper>

      );
    }
    //@@viewOff:render

  }
);

export default BackofficeOrdersSwiper;