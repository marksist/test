import React from "react";
import * as UU5 from 'uu5';

import "./backoffice-orders-counter.css";

const BackofficeOrdersCounter = React.createClass(
  {
    //@@viewOn:mixins
    mixins:[
      UU5.Common.BaseMixin,
      UU5.Common.CcrWriterMixin
    ],
    //@@viewOff:mixins

    //@@viewOn:statics
    statics:{
      tagName:'Ucl.Goodymat.BackofficeOrdersCounter',
      classNames:{
        main:'ucl-goodymat-order-counter',
        number:'ucl-goodymat-order-counter-number'
      },
      orderCounterData:{}
    },
    //@@viewOff:statics

    //@@viewOn:propTypes
    //@@viewOff:propTypes
    //@@viewOn:getDefaultProps
    //@@viewOff:getDefaultProps

    //@@viewOn:standardComponentLifeCycle
    getInitialState(){
      return {number:0}
    },
    //@@viewOff:standardComponentLifeCycle

    //@@viewOn:interface
    setCounter(number){
      this.setState({number:number});
    },
    //@@viewOff:interface

    //@@viewOn:componentSpecificHelpers
    _getTotalCount(){
      if(this.state.number == 0){
        return "";
      }else{
        return "(" + this.state.number + ")";
      }
    },
    //@@viewOff:componentSpecificHelpers

    //@@viewOn:render
    render() {
      return (
        <UU5.Bricks.Span className={this.getClassName().number}>{this._getTotalCount()}</UU5.Bricks.Span>
      );
    }
    //@@viewOff:render

  }
);

export default BackofficeOrdersCounter;