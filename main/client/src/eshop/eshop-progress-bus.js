//@@viewOn:import
import React from "react";
import ReactDOM from "react-dom";
import * as UU5 from 'uu5';

import "./eshop-progress-bus.css";
//@@viewOff:import

const EshopProgressBus = React.createClass(
  {
    //@@viewOn:mixins
    mixins:[
      UU5.Common.BaseMixin,
      UU5.Common.ElementaryMixin,
      UU5.Layout.FloatMixin,
      UU5.Common.LsiMixin,
      UU5.Common.CcrWriterMixin,
      UU5.Common.ScreenSizeMixin
    ],
    //@@viewOff:mixins

    //@@viewOn:statics
    statics:{
      tagName:'Ucl.Goodymat.Eshop.ProgressBus',
      classNames:{
        main:'ucl-goodymat-eshop-progressbus'
      },
      lsi:{
        progressLSI:{
          cs:"Průběh vyřízení vašich objednávek",
          "en-gb":"Progress of processing your orders",
          "en-us":"Progress o' processin' yo' goddamn orders",
          "sk":"Priebeh realizácie objednávok",
          "ua":"Хід виконання ваших замовлень"
        }
      }
    },
    //@@viewOff:statics

    //@@viewOn:propTypes
    //@@viewOff:propTypes

    //@@viewOn:getDefaultProps
    //@@viewOff:getDefaultProps

    //@@viewOn:standardComponentLifeCycle
    componentDidUpdate()
    {
      this._reposition();
    },

    componentDidMount()
    {
      window.addEventListener("scroll", this._reposition, false);
    },

    componentWillUnmount()
    {
      window.removeEventListener("scroll", this._reposition, false);
    },
    //@@viewOff:standardComponentLifeCycle

    //@@viewOn:interface
    addItem: function (item) {
      let result = this.bus.addItem.apply(this.bus, arguments);

      // force repositioning
      this.setState(
        (state) => (
          {
            forceReposition:(
              state.forceReposition || 0
            ) + 1
          }
        )
      );
      return result;
    },
    updateItem: function (id, item) {
      return this.bus.updateItem.apply(this.bus, arguments);
    },
    setItem: function (id, item) {
      return this.bus.setItem.apply(this.bus, arguments);
    },
    showAlert: function (id) {
      return this.bus.showAlert.apply(this.bus, arguments);
    },
    //@@viewOff:interface

    //@@viewOn:overridingMethods
    //@@viewOff:overridingMethods

    //@@viewOn:componentSpecificHelpers
    _getMainProps:function(){
      let mainProps = this.getMainPropsToPass();
      mainProps.id = this.getId() + '-Progress';
      return mainProps;
    },

    _refProgressBus:function(bus){
      this.bus = bus;
    },

    _reposition()
    {
      // update top position depending on document's scroll position
      if(this.bus){
        let domNode = ReactDOM.findDOMNode(this.bus);
        if(domNode){
          let scrollTop = -document.body.getBoundingClientRect().top;
          if(scrollTop > 73){
            domNode.style.position = "fixed";
            domNode.style.top = 0;
          }else{
            domNode.style.position = "";
            domNode.style.top = "";
          }
        }
      }
    },
    //@@viewOff:componentSpecificHelpers

    //@@viewOn:render
    render()
    {
      return (
        <UU5.Bricks.ProgressBus {...this._getMainProps()}
          ref_={this._refProgressBus}
          verticalPosition='top'
          alertPosition='top'
        >
          {this.getLSIComponent("progressLSI")}
        </UU5.Bricks.ProgressBus>
      );
    }
    //@@viewOff:render

  }
);

export default EshopProgressBus;
