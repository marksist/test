//@@viewOn:import
import React from "react";
import * as UU5 from "uu5";
import * as UclGoodymat from "../common/common.js";
import Calls from "../calls.js";
import EshopBody from "./eshop-body.js";
//@@viewOff:import

const Eshop = React.createClass(
  {

    //@@viewOn:mixins
    mixins:[
      UU5.Common.BaseMixin,
      UU5.Common.ElementaryMixin,
      UU5.Common.VucMixin,
      UU5.Common.IdentityMixin
    ],
    //@@viewOff:mixins

    //@@viewOn:statics
    statics:{
      tagName:"Ucl.Goodymat.Eshop",
      vucName:"eshop",
      vucTitleLSI:{
        "en-gb":"Vladimir's Goodymat",
        "en-us":"Vlado's Goodymat",
        "cs":"Vladimírův Pamlskomat",
        "sk":"Vladimírov pamlskomat",
        "ua":"Володимирові смакоти"
      },
      classNames:{
        main:"ucl-goodymat-eshop"
      }
    },
    //@@viewOff:statics

    //@@viewOn:propTypes
    //@@viewOff:propTypes

    //@@viewOn:getDefaultProps
    //@@viewOff:getDefaultProps

    //@@viewOn:standardComponentLifeCycle
    //@@viewOff:standardComponentLifeCycle

    //@@viewOn:interface
    //@@viewOff:interface

    //@@viewOn:overridingMethods
    onChangeIdentity_(session) {
      this.changeIdentity();

      // re-authorize the VUC when identity changes (but don't do it
      // if it happens prior to 1st VUC authorization)
      if(this.state.authorizedFeedback != "loading"){
        this.checkAuthorizing();
      }
    },
    //@@viewOff:overridingMethods

    //@@viewOn:render
    render() {
      let content = this.getVucChildren(
        (profiles) =>{
          return (
            <EshopBody calls={Calls} profiles={profiles}/>
          );
        }
      );

      return (
        <UU5.Layout.Root
          header={{
            element:<UclGoodymat.Header labelLsi={this.constructor.vucTitleLSI} menuProfileCodes={this.state.profiles}/>
          }}
          footer={{
            element:<UclGoodymat.Footer/>
          }}>
          <UU5.Bricks.HomeScreen appID="ucl.goodymat"/>
          {content}
        </UU5.Layout.Root>
      );
    }
    //@@viewOff:render

  }
);

export default Eshop;