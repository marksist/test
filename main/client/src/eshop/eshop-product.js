//@@viewOn:import
import React from "react";
import * as UU5 from 'uu5';

import "./eshop-product.css";
//@@viewOff:import

const EshopProduct = React.createClass(
  {
    //@@viewOn:mixins
    mixins:[
      UU5.Common.BaseMixin,
      UU5.Common.ElementaryMixin,
      UU5.Layout.ColumnMixin,
      UU5.Common.CcrWriterMixin,
      UU5.Common.CallsMixin
    ],
    //@@viewOff:mixins

    //@@viewOn:statics
    statics:{
      tagName:'Ucl.Goodymat.Eshop.Product',
      classNames:{
        main:'ucl-goodymat-eshop-product',
        header:'ucl-goodymat-eshop-product-header',
        button:'ucl-goodymat-eshop-product-button',
        contentFlc:'ucl-goodymat-eshop-product-contentFlc'
      },
      defaults:{
        buttonOrderIcon:'ucl-goodymat-icon-shopping-cart'
      },
      calls:{
        buy:'buy'
      },
      lsi:{
        buyLSI:{
          "en-gb":"Buy",
          "en-us":"Buy",
          "cs":"Objednat",
          "sk":"Objednať",
          "ua":"Замовити"
        },
        buySuccessLSI:{
          "cs":"Vaše zboží \"${name}\" bylo úspěšně objednáno, dobrou chuť!",
          "en-gb":"Your product \"${name}\" was ordered, bon apetite!",
          "en-us":"Yo' goddamn product ${name} was ordered, enjoy yo' shit!",
          "sk":"Váš tovar \"${name}\" bol úspešne objednaný, dobrú chuť!",
          "ua":"Ваше замовлення \"${name}\" успішно зроблено. Смачного!"
        },
        buyErrorGenericLSI:{
          "cs":"Objednání zboží \"${name}\" selhalo.",
          "en-gb":"Order of the product \"${name}\" has failed.",
          "en-us":"Order o' da goddamn product \"${name}\" has failed.",
          "sk":"Objednanie tovaru \"${name}\" zlyhalo.",
          "ua":"Замовлення товару \"${name}\" не відбулося."
        },
        buyErrorShopClosedLSI:{
          "cs":"Objednávku nelze provést. Obchod je zavřený.",
          "en":"Order cannot be done. Shop is closed.",
          "sk":"Objednávku nie je možné vykonať. Obchod je zatvorený.",
          "ua":"Замовлення товару \"${name}\" не відбулося."
        },
        buyErrorCustomerLSI:{
          "cs":"Objednávku nelze provést. Nejste registrovaný zákazník.",
          "en":"Order cannot be done. You are not registered customer."
        }
      }
    },
    //@@viewOff:statics

    //@@viewOn:propTypes
    propTypes:{
      productName:React.PropTypes.object, // lsi
      image:React.PropTypes.string,
      profiles:React.PropTypes.array
    },
    //@@viewOff:propTypes

    //@@viewOn:getDefaultProps
    //@@viewOff:getDefaultProps

    //@@viewOn:standardComponentLifeCycle
    //@@viewOff:standardComponentLifeCycle

    //@@viewOn:interface
    //@@viewOff:interface

    //@@viewOn:overridingMethods
    //@@viewOff:overridingMethods

    //@@viewOn:componentSpecificHelpers
    _getMainProps:function(){
      let mainProps = this.getMainPropsToPass();
      mainProps.id = this.getId() + '-productColumn';
      mainProps.colWidth = "sm-12 md-4";
      return mainProps;
    },

    _isCustomerProfile:function(){
      return this.props.profiles.some(profile => (profile == "Customers"));
    },

    _productOrderClick:function(productCode, productNameLsi){
      let bus = this.getCcrComponentByKey('Ucl.Goodymat.Eshop.ProgressBus');

      let itemProps = {
        code:productCode,
        colorSchema:'grey',
        pending:true
      };
      let id = bus.addItem(itemProps);

      this.getCall('buy')(
        {
          data:{product:productCode, itemProps:itemProps},
          done:(dtoOut) =>{
            let messageLsi = Object.assign({}, this.getLSI("buySuccessLSI"));
            for(let lang in messageLsi){
              messageLsi[lang] = this._replaceStringParams(
                messageLsi[lang],
                {name:this.getLSIItemByLanguage(productNameLsi, [{language:lang}])}
              );
            }
            bus.updateItem(
              id,
              {
                message:<UU5.Bricks.Lsi lsi={messageLsi}/>,
                colorSchema:'success',
                pending:false,
                timeout:2000
              }
            );
            bus.showAlert(id);
          },
          fail:(dtoOut) =>{
            let errorCode = (
              dtoOut.data || {}
            ).code;

            let messageLsiCode;
            switch(errorCode) {
              case "UCL.GOODYMAT.MAIN.SHOP/E002":
                messageLsiCode = "buyErrorShopClosedLSI";
                break;
              case "UCL.GOODYMAT.MAIN.CUSTOMER/E001":
                messageLsiCode = "buyErrorCustomerLSI";
                break;
              default:
                messageLsiCode = "buyErrorGenericLSI"
            }

            let messageLsi = Object.assign({}, this.getLSI(messageLsiCode));
            for(let lang in messageLsi){
              messageLsi[lang] = this._replaceStringParams(
                messageLsi[lang],
                {name:this.getLSIItemByLanguage(productNameLsi, [{language:lang}])}
              );
            }
            bus.setItem(
              id,
              {
                code:productCode,
                message:<UU5.Bricks.Lsi lsi={messageLsi}/>,
                colorSchema:'danger'
              }
            );
            bus.showAlert(id);
          }
        }
      );
    },

    _replaceStringParams(txt, params){
      if(!txt || typeof txt != "string"){
        return txt;
      }
      // replace ${xyz} values in `txt` string by `params[xyz]`
      return txt.replace(
        /\$\{([^}]*)\}/g, (match, g1) =>{
          let param = params[g1];
          return (
            param != null ? param + "" : ""
          );
        }
      );
    },
    //@@viewOff:componentSpecificHelpers

    //@@viewOn:render
    render() {
      let id = this.getId();
      return (
        <UU5.Layout.Column {...this._getMainProps()}>
          <UU5.Layout.Flc className={this.getClassName().contentFlc} level="2" header={<UU5.Bricks.Lsi lsi={this.props.productName}/>}>
            <UU5.Bricks.Image src={this.props.image}/>
            <UU5.Bricks.Button className={this.getClassName().button}
              colorSchema="success"
              onClick={this._productOrderClick.bind(null, id, this.props.productName)}
              disabled={!this._isCustomerProfile()}>
              <UU5.Bricks.Glyphicon glyphicon={this.getDefault().buttonOrderIcon}/>
              {this.getLSIComponent("buyLSI")}
            </UU5.Bricks.Button>
          </UU5.Layout.Flc>
        </UU5.Layout.Column>
      );
    }
    //@@viewOff:render

  }
);

export default EshopProduct;
