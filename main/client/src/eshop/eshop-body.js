//@@viewOn:import
import React from 'react';
import UU5 from 'uu5';
import {Uri} from "uu_app";

import EshopProduct from "./eshop-product.js";
import EshopProgressBus from "./eshop-progress-bus.js";

import "./eshop-body.css";
//@@viewOff:import

const EshopBody = React.createClass(
  {

    //@@viewOn:mixins
    mixins:[
      UU5.Common.BaseMixin,
      UU5.Common.ElementaryMixin,
      UU5.Common.LoadMixin,
      UU5.Layout.ContainerCollectionMixin
    ],
    //@@viewOff:mixins

    //@@viewOn:statics
    statics:{
      tagName:'Ucl.Goodymat.Eshop.Body',
      classNames:{
        main:'ucl-goodymat-eshop-body',
        welcomeFlc:'ucl-goodymat-eshop-body-welcome-flc',
        guestAlert:'ucl-goodymat-eshop-body-guest-alert',
        closedShop:'ucl-goodymat-eshop-body-closed-component',
        closedShopBlock:'ucl-goodymat-eshop-body-closed-block'
      },
      calls:{
        onLoad:'getShopWindow'
      },
      defaults:{
        closedShopIcon:'ucl-goodymat-icon-maintenance'
      },
      lsi:{
        noRightsToBuyLSI:{
          "en-gb":"Sorry, you do not have sufficient rights to buy any product.",
          "en-us":"Sorry, yo' don't have da rights ta do f*ckin' shoppin'.",
          "cs":"Dobrý den, nemáte potřebná práva k nákupu.",
          "sk":"Dobrý deň, nemáte oprávnenie k nákupu.",
          "ua":"Добрий день, немаєте дозволу на покупку."
        }
      }
    },
    //@@viewOff:statics

    //@@viewOn:propTypes
    //@@viewOff:propTypes

    //@@viewOn:getDefaultProps
    getDefaultProps:function(){
      return {
        profiles:[]
      }
    },
    //@@viewOff:getDefaultProps

    //@@viewOn:standardComponentLifeCycle
    //@@viewOff:standardComponentLifeCycle

    //@@viewOn:interface
    //@@viewOff:interface

    //@@viewOn:overridingMethods
    //@@viewOff:overridingMethods

    //@@viewOn:componentSpecificHelpers
    _getShopData() {
      return this.getDtoOut();
    },

    _isCustomerProfile(){
      return (this.props.profiles || []).some(profile => (profile == "Customers"));
    },

    _getNoBuyRightsAlert() {
      if (!this._isCustomerProfile()) {
        return (
          <UU5.Bricks.Alert className={this.getClassName().guestAlert} closeTimer={8000} key="alert">
            {this.getLSIComponent("noRightsToBuyLSI")}
          </UU5.Bricks.Alert>
        );
      }
    },

    _getProgressBus:function(){
      return (
        <EshopProgressBus ccrKey="Ucl.Goodymat.Eshop.ProgressBus"
          key="progressBus"/>
      );
    },

    _getProducts:function(){
      let shopData = this._getShopData();
      let objects = Object.keys(shopData.productMap).map(
        (productKey) =>{
            let uriBuilder = Uri.UriBuilder.parse(location.protocol + "//" + location.host + location.pathname).setQualifier("getBinaryData").setClassifier("uuBinary");

            return {
            id:productKey,
            nameLSI:shopData.productMap[productKey].nameLSI,
            descLSI:shopData.productMap[productKey].descLSI,
            image: uriBuilder.setParameter("dataKey",productKey).toUri().toString()
          }
        }
      );

      return (
        <UU5.Layout.Row display="flex-full" key="productRow">
          {
            objects.map(
              (product) =>{
                return (
                  <EshopProduct profiles={this.props.profiles}
                    key={product.id}
                    id={product.id}
                    image={product.image}
                    productName={product.nameLSI}
                    calls={this.props.calls}
                  />
                );
              }
            )
          }
        </UU5.Layout.Row>
      )
    },

    _getContent:function(){
      let children;
      let shopData = this._getShopData();
      switch(shopData.state){
        case 'open':
          children = this._getProducts();
          break;
        case 'closed':
          children = (
            <UU5.Layout.Container className={this.getClassName().closedShop} key="shopStateClosed">
              <UU5.Bricks.Div className={this.getClassName().closedShopBlock}>
                <UU5.Bricks.Glyphicon glyphicon={this.getDefault().closedShopIcon}/>
                <UU5.Bricks.P content={<UU5.Bricks.Lsi lsi={shopData.closeWelcomeLSI}/>}>
                </UU5.Bricks.P>
              </UU5.Bricks.Div>
            </UU5.Layout.Container>
          );
          break;
        default:
          console.error(this.getTagName(), this, 'Unknown shop state: ' + shopData.state);
          children = <div key="shopStateError"/>;
      }
      return children;
    },
    //@@viewOff:componentSpecificHelpers

    //@@viewOn:render
    render() {
      return (
        <UU5.Layout.ContainerCollection {...this.getMainPropsToPass()}>
          {this.getLoadFeedbackChildren(
            dtoOut =>{
              return (
                [
                  this._getNoBuyRightsAlert(),
                  this._getProgressBus(),
                  this._getContent()
                ]
              );
            }
          )
          }
        </UU5.Layout.ContainerCollection>
      );
    }
    //@@viewOff:render

  }
);

export default EshopBody;