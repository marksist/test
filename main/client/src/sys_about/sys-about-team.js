import React from "react";
import * as UU5 from "uu5";

import "./sys-about-team.css"

const SysAboutTeam = React.createClass(
  {

    //@@viewOn:mixins
    mixins:[
      UU5.Common.BaseMixin,
      UU5.Common.ElementaryMixin,
      UU5.Common.ScreenSizeMixin,
      UU5.Layout.ContainerMixin
    ],
    //@@viewOff:mixins

    //@@viewOn:statics
    statics:{
      tagName:'Plus4u.Dockit.SysAbout.Team',
      classNames:{
        main:'plus4u-dockit-sysabout-team center',
        header:"plus4u-dockit-sysabout-team-header",
        leadingCreator:'plus4u-dockit-sysabout-team-leading-creator',
        leadingName:'plus4u-dockit-sysabout-team-leading-name',
        leadingRole:'plus4u-dockit-sysabout-team-leading-role',
        otherCreator:'plus4u-dockit-sysabout-team-other-creator',
        otherName:'plus4u-dockit-sysabout-team-other-name',
        otherRole:'plus4u-dockit-sysabout-team-other-role'
      },
      defaults:{
        leadingColWidth:'xs-12 sm-6 md-4 lg-3',
        otherColWidth:'xs-6 sm-4 md-3 lg-2'
      }
    },
    //@@viewOff:statics

    //@@viewOn:propTypes
    propTypes:{
      leadingCreators:React.PropTypes.arrayOf(
        React.PropTypes.shape(
          {
            imgSrc:React.PropTypes.string,
            name:React.PropTypes.string,
            role:React.PropTypes.string
          }
        )
      ),
      otherCreators:React.PropTypes.arrayOf(
        React.PropTypes.shape(
          {
            imgSrc:React.PropTypes.string,
            name:React.PropTypes.string,
            role:React.PropTypes.string
          }
        )
      )
    },
    //@@viewOff:propTypes

    //@@viewOn:getDefaultProps
    getDefaultProps(){
      return {
        leadingCreators:null,
        otherCreators:null,
        screenSizeProps:{
          "lg":{
            "leading":4, //max number of columns in row
            "other":6
          },
          "md":{
            "leading":3, //max number of columns in row
            "other":4
          },
          "sm":{
            "leading":2, //max number of columns in row
            "other":3
          },
          "xs":{
            "leading":1, //max number of columns in row
            "other":2
          }
        }
      }
    },
    //@@viewOff:getDefaultProps

    //@@viewOn:standardComponentLifeCycle
    //@@viewOff:standardComponentLifeCycle

    //@@viewOn:interface
    //@@viewOff:interface

    //@@viewOn:overridingMethods
    //@@viewOff:overridingMethods

    //@@viewOn:componentSpecificHelpers

    //@length(number) - length of leading creators array or other creators array.
    //@index(number) - actual index of column in map iteration.
    //@param(string) - parameter ("leading" or "other"), type of creators.
    //@screenSize(string) - actual size of display, return lg, md, sm or xs.
    //@excessNumber - number of columns at last row (division modulo N).
    //@setIndex(number) - index of first column at last row.
    //@result(string) - return className.
    _calculateOffset(length, index, param)
    {
      let screenSize = this.getScreenSize();
      let result;
      let excessNumber = length % this.props.screenSizeProps[screenSize][param].key;
      let setIndex = length - excessNumber;
      if(index == setIndex){
        result = this.props.screenSizeProps[screenSize][param].values[excessNumber - 1];
      }
      return result;
    },

    _setLeadingColumnOffset(length, index)
    {
      return this._calculateOffset(length, index, "leading");
    },

    _getLeadingCreatorsRow()
    {
      let ncr = this.props.screenSizeProps[this.getScreenSize()].leading;
      let colSize = 12 / ncr;
      let colWidth = this.getScreenSize() + "-" + colSize;
      let length = this.props.leadingCreators.length;
      let mod = length % ncr;
      let sci = length - mod;
      let margin = "" + 50 * (ncr - mod) / ncr + "%";

      return (
        this.props.leadingCreators &&
        <UU5.Layout.Row display="flex-full" className="center">
          {
            this.props.leadingCreators.map(
              (creator, i) =>{
                return (
                  <UU5.Layout.Column key={i}
                    colWidth={colWidth}
                    mainAttrs={{style:{marginLeft: i === sci && i !== 0 ? margin : null}}}
                  >
                    <UU5.Layout.Flc className={this.getClassName("leadingCreator")}>
                      <UU5.Bricks.Image src={creator.imgSrc} type='circle'/>
                      <UU5.Bricks.P className={this.getClassName("leadingName")}>{creator.name}</UU5.Bricks.P>
                      <UU5.Bricks.P className={this.getClassName("leadingRole")}>{creator.role}</UU5.Bricks.P>
                    </UU5.Layout.Flc>
                  </UU5.Layout.Column>
                )
              }
            )
          }
        </UU5.Layout.Row>
      )
    },

    _setOtherColumnOffset(length, index)
    {
      return this._calculateOffset(length, index, "other");
    },

    _getOtherCreatorsRow()
    {
      let ncr = this.props.screenSizeProps[this.getScreenSize()].other;
      let colSize = 12 / ncr;
      let colWidth = this.getScreenSize() + "-" + colSize;
      let length = this.props.otherCreators.length;
      let mod = length % ncr;
      let sci = length - mod;
      let margin = "" + 50 * (ncr - mod) / ncr + "%";

      return (
        this.props.otherCreators &&
        <UU5.Layout.Row display="flex-full">
          {
            this.props.otherCreators.map(
              (creator, i) =>{
                return (
                  <UU5.Layout.Column key={i}
                    colWidth={colWidth}
                    mainAttrs={{style:{marginLeft: i === sci && i !== 0 ? margin : null}}}
                  >
                    <UU5.Layout.Flc className={this.getClassName("otherCreator")}>
                      <UU5.Bricks.Image src={creator.imgSrc} type='circle'/>
                      <UU5.Bricks.P className={this.getClassName("otherName")}>{creator.name}</UU5.Bricks.P>
                      <UU5.Bricks.P className={this.getClassName("otherRole")}>{creator.role}</UU5.Bricks.P>
                    </UU5.Layout.Flc>
                  </UU5.Layout.Column>
                )
              }
            )
          }
        </UU5.Layout.Row>
      )
    },
    //@@viewOff:componentSpecificHelpers

    //@@viewOn:render
    render()
    {
      return (
        <UU5.Layout.Container {...this.getMainPropsToPass()}
          header={
            this.props.header
          }
          level={1}
        >
          <UU5.Layout.RowCollection>
            {this.props.leadingCreators && this._getLeadingCreatorsRow()}
            {this.props.otherCreators && this._getOtherCreatorsRow()}
          </UU5.Layout.RowCollection>
        </UU5.Layout.Container>
      )
    }
  }
  )
  ;
export default SysAboutTeam;