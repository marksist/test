import React from "react";
import * as UU5 from "uu5";
import * as UclGoodymat from "../common/common.js";
import Calls from "../calls.js";
import SysAboutData from "../data/sys-about-data.js";
import SysAboutTeam from "./sys-about-team.js"
import SysAboutLicenceBody from "./sys-about-license-body.js";

import "./sys-about.css";

const SysAbout = React.createClass(
  {

    //@@viewOn:mixins
    mixins:[
      UU5.Common.BaseMixin,
      UU5.Common.VucMixin,
      UU5.Common.IdentityMixin
    ],
    //@@viewOff:mixins

    //@@viewOn:statics
    statics:{
      tagName:"UU.AppWorkspace.VucSysAbout",
      vucName:"sysAbout",
      vucTitleLSI:{"en-gb":"About Application", "en-us":"'bout app", cs:"O aplikaci", sk:"O aplikácii", ua:"Про аплікацію"},
      classNames:{
        main:"uu-appworkspace-vucsysabout",
        about:"uu-appworkspace-vucsysabout-about",
        header:"uu-appworkspace-vucsysabout-header",
        text:"uu-appworkspace-vucsysabout-text",
        license:"uu-appworkspace-about-license",
        technologies:"uu-appworkspace-about-technologies",
        references:"uu-appworkspace-vucsysabout-references",
        reference:'uu-appworkspace-references-reference',
        referenceLink:'uu-appworkspace-references-link',
        referenceText:'uu-appworkspace-references-text',
        creators:"uu-appworkspace-vucsysabout-creators",
        leadingCreator:'uu-appworkspace-creators-leading_creator',
        leadingImage:'uu-appworkspace-creators-leading_img',
        leadingName:'uu-appworkspace-creators-leading_name',
        leadingRole:'uu-appworkspace-creators-leading_role',
        otherCreator:'uu-appworkspace-creators-other_creator',
        otherImage:'uu-appworkspace-creators-other_img',
        otherName:'uu-appworkspace-creators-other_name',
        otherRole:'uu-appworkspace-creators-other_role'
      }
    },
    //@@viewOff:statics

    //@@viewOn:overridingMethods
    onChangeIdentity_(session) {
      this.changeIdentity();

      // re-authorize the VUC when identity changes (but don't do it
      // if it happens prior to 1st VUC authorization)
      if(this.state.authorizedFeedback != "loading"){
        this.checkAuthorizing();
      }
    },
    //@@viewOff:overridingMethods

    //@@viewOn:render
    render() {
      let content = this.getVucChildren(
        (profiles) =>{
          return (
            <UU5.Layout.ContainerCollection>
              {
                SysAboutData.header &&

                <UU5.Layout.Container
                  className={this.getClassName("about")}
                  header={SysAboutData.header}>

                  {
                    SysAboutData.text &&

                    <UU5.Bricks.P className={this.getClassName("text")}>
                      {/*<UU5.Bricks.Lsi lsi={SysAboutData.textLsi}/>*/}
                      {SysAboutData.text}
                    </UU5.Bricks.P>
                  }
                </UU5.Layout.Container>
              }

              {
                SysAboutData.licenseOwnerHeader &&

                <UU5.Layout.Container
                  className={this.getClassName("license")}
                  header={
                    SysAboutData.licenseOwnerHeader
                  }>

                  <SysAboutLicenceBody
                    calls={Calls}
                    licenceHeader={SysAboutData.licenseOrganizationHeader}
                    userHeader={SysAboutData.licenseUserHeader} />

                </UU5.Layout.Container>
              }

              {
                SysAboutData.technologiesHeader && SysAboutData.technologies &&

                <UU5.Layout.Container
                  className={this.getClassName("technologies")}
                  header={
                    /*<UU5.Bricks.Lsi lsi={SysAboutData.technologiesHeaderLsi} className={this.getClassName("header")}/>*/
                    SysAboutData.technologiesHeader
                  }>

                  <UU5.Bricks.P content={
                    /*<UU5.Bricks.Lsi lsi={SysAboutData.technologiesLsi}  />*/
                    SysAboutData.technologies
                  } className={this.getClassName("references")}/>
                </UU5.Layout.Container>
              }

              {
                SysAboutData.creatorsHeader &&

                <SysAboutTeam
                  header={SysAboutData.creatorsHeader}
                  leadingCreators={SysAboutData.creators.leading}
                  otherCreators={SysAboutData.creators.others}
                />
              }

            </UU5.Layout.ContainerCollection>
          );
        }
      );

      return (
      this.getVucChildren( profiles =>
        <UU5.Layout.Root
          header={{
            element:<UclGoodymat.Header labelLsi={this.constructor.vucTitleLSI} menuProfileCodes={this.state.profiles}
                                        colorSchema="primary"/>
          }}
          footer={{
            element:<UclGoodymat.Footer/>
          }}
        >
          <UU5.Bricks.HomeScreen appID="ucl.goodymat" />
          {content}
        </UU5.Layout.Root>
      )
      );
    }
    //@@viewOff:render

  }
);

export default SysAbout;