import React from "react";
import * as UU5 from 'uu5';

import "./sys-about-license-body.css";

let SysAboutLicenceBody = React.createClass(
  {
    //@@viewOn:mixins
    mixins:[
      UU5.Common.BaseMixin,
      UU5.Common.ElementaryMixin,
      UU5.Common.LoadMixin,
      UU5.Common.LsiMixin,
      UU5.Layout.RowCollectionMixin
    ],
    //@@viewOff:mixins

    //@@viewOn:statics
    statics:{
      tagName:'UU.AppWorkspace.SysAboutLicenceBody',
      classNames:{
        main:'uu-appworkspace-licence',
        competentPerson:'uu-appworkspace-licence-competent-person',
        columnHeader:'uu-appworkspace-licence-competent-column-header',
        organization:'uu-appworkspace-licence-organization',
        userName:'uu-appworkspace-licence-user-name',
        organizationBody:'uu-appworkspace-licence-organization-body',
        organizationLogo:'uu-appworkspace-licence-organization-logo',
        organizationName:'uu-appworkspace-licence-organization-name'
      },
      calls:{
        onLoad:'getLicenseOwner'
      }
    },
    //@@viewOff:statics

    //@@viewOn:propTypes
    //@@viewOff:propTypes

    //@@viewOn:getDefaultProps
    //@@viewOff:getDefaultProps

    //@@viewOn:standardComponentLifeCycle
    //@@viewOff:standardComponentLifeCycle

    //@@viewOn:interface
    //@@viewOff:interface

    //@@viewOn:overridingMethods
    //@@viewOff:overridingMethods

    //@@viewOn:componentSpecificHelpers
    _getOrganization(organization){
      return (

        <UU5.Bricks.Table.Tr className={this.getClassName('organization')} key="organizations">
          <UU5.Bricks.Table.Th
            className={this.getClassName('columnHeader')}
            content={this.props.licenceHeader}/>
          <UU5.Bricks.Table.Td className={this.getClassName('organizationName')}
            content={ <UU5.Bricks.Link href={organization.web} target="_blank">{organization.name}</UU5.Bricks.Link>}/>
        </UU5.Bricks.Table.Tr>
      )
    },
    _renderUser(users){
      let result = [];
      for(let i = 0; i < users.length; i++){
        let user = users[i];
        result.push(<UU5.Bricks.P>{user.name}</UU5.Bricks.P>)
      }
      return <UU5.Bricks.Table.Td className={this.getClassName('userName')} content={result}/>;
    },

    _getUsers(users){
      return (
        <UU5.Bricks.Table.Tr key='users' className={this.getClassName('competentPerson')}>
          <UU5.Bricks.Table.Th
            className={this.getClassName('columnHeader')}
            content={this.props.userHeader}/>
          {this._renderUser(users)}
        </UU5.Bricks.Table.Tr>
      )
    },
    //@@viewOff:componentSpecificHelpers

    //@@viewOn:render
    render() {
      return (

        <UU5.Bricks.Table {...this.getMainPropsToPass()}>
          <UU5.Bricks.Table.THead></UU5.Bricks.Table.THead>
          <UU5.Bricks.Table.TBody>
            {this.getLoadFeedbackChildren(
              dtoOut =>{
                if(dtoOut != null){
                  let body = [];
                  !this.props.licenceHeader || body.push(this._getOrganization(dtoOut.organization));
                  !this.props.userHeader || body.push(this._getUsers(dtoOut.userList));
                  return body;
                }
              }
            )}
          </UU5.Bricks.Table.TBody>
          <UU5.Bricks.Table.TFoot></UU5.Bricks.Table.TFoot>
        </UU5.Bricks.Table>
      );
    }
    //@@viewOff:render
  }
);

export default SysAboutLicenceBody;