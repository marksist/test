import React from "react";

import * as UU5 from 'uu5';

import './backoffice-shop-locker-body.css';


const BackofficeShopLockerBody = React.createClass(
  {
    //@@viewOn:mixins
    mixins:[
      UU5.Common.BaseMixin,
      UU5.Common.ElementaryMixin,
      UU5.Common.LoadMixin
    ],
    //@@viewOff:mixins

    //@@viewOn:statics
    statics:{
      vucName:'Ucl.Goodymat.Main.BackofficeShopLockerBody',
      classNames:{
        main:'ucl-goodymat-shop-locker-body'
      },
      shopLockerData:{
        shopStateOpenLSI:{
          "cs":" otevřen",
          "en-gb":" open",
          "en-us":" open",
          "sk":" otvorený",
          "ua":" відкритий"
        },
        shopStateClosedLSI:{
          "cs":' uzavřen',
          "en-gb":' closed',
          "en-us":' closed',
          "sk":' uzavretý',
          "ua":' закритий'
        },
        buttonLabelOpenLSI:{
          "cs":'Zavřít obchod',
          "en-gb":'Close the shop',
          "en-us":'Close da sex shop',
          "sk":'Zavrieť obchod',
          "ua":'закрити магазин'
        },
        buttonLabelClosedLSI:{
          "cs":'Otevřít obchod',
          "en-gb":'Open the shop',
          "en-us":'Open da sex shop',
          "sk":'Otvoriť obchod',
          "ua":'підприємство',
        },
        shopAvailabilityLSI:{
          "cs":'Dostupnost obchodu',
          "en-gb":'Shop availability',
          "en-us":'Sex shop availability',
          "sk":'Dostupnosť obchodu',
          "ua":'наявність торгівлі',
        },
        shopInfoLSI:{
          "cs":'Momentálně je obchod',
          "en-gb":'The shop is',
          "en-us":'Da sex shop iz',
          "sk":'Momentálne je obchod',
          "ua":'В даний час магазин',
        }
      },
      calls:{
        onLoad:'getShopInfo',
        setShopState:'setShopState',
      }
    },
    //@@viewOff:statics

    //@@viewOn:propTypes
    //@@viewOff:propTypes
    //@@viewOn:getDefaultProps
    //@@viewOff:getDefaultProps

    //@@viewOn:standardComponentLifeCycle
    getInitialState(){
      return {callFeedback:'loading', buttonDisabled:false};
    },
    //@@viewOff:standardComponentLifeCycle

    //@@viewOn:interface
    //@@viewOff:interface

    //@@viewOn:overridingMethods
    onLoadSuccess_(dtoOut){
      if(dtoOut.state == 'open'){
        this.setState(
          {
            shopState:this.constructor.shopLockerData.shopStateOpenLSI,
            shopOpen:true,
            callFeedback:'ready'
          }
        );
      }else{
        this.setState(
          {
            shopState:this.constructor.shopLockerData.shopStateClosedLSI,
            shopOpen:false,
            callFeedback:'ready'
          }
        );
      }
    },
    //@@viewOff:overridingMethods

    //@@viewOn:componentSpecificHelpers
    _changeShopState(){
      let call = this.getCall('setShopState');
      const nextServerShopState = this.state.shopOpen ? 'closed' : 'open';
      const nextShopOpen = !this.state.shopOpen;
      const nextShopState = this.state.shopOpen
        ? this.constructor.shopLockerData.shopStateClosedLSI
        : this.constructor.shopLockerData.shopStateOpenLSI;

      this.setState(
        {
          buttonDisabled:true,

        }, () => call(
          {
            data:{state:nextServerShopState},
            done:dtoOut => this.setState(
              {
                buttonDisabled:false,
                shopOpen:nextShopOpen,
                shopState:nextShopState,
              }
            ),
            fail:dtoOut => this.setState(
              {
                buttonDisabled:false
              }
            )
          }
        )
      );
    },

    _getShopLockerBody(){
      switch(this.state.callFeedback){
        case 'loading':
          return <UU5.Bricks.Loading/>;
        case 'ready':
          return (
            <UU5.Bricks.Div>
              <UU5.Bricks.Paragraph className="shop-locker-text">
                <UU5.Bricks.Lsi lsi={this.constructor.shopLockerData.shopInfoLSI}/>
                <UU5.Bricks.Span className={this.state.shopOpen
                  ? "shop-locker-open-label"
                  : "shop-locker-closed-label"}>
                  <UU5.Bricks.Lsi lsi={this.state.shopState}/>
                </UU5.Bricks.Span>
              </UU5.Bricks.Paragraph>
              <UU5.Bricks.ButtonSwitch
                switchedOn={this.state.shopOpen}
                onProps={{
                  shopOpen:true,
                  className:"shop-locker-button-open",
                  content:<UU5.Bricks.Lsi lsi={this.constructor.shopLockerData.buttonLabelOpenLSI}/>
                }}
                offProps={{
                  shopOpen:false,
                  className:"shop-locker-button-closed",
                  content:<UU5.Bricks.Lsi lsi={this.constructor.shopLockerData.buttonLabelClosedLSI}/>
                }}
                props={{disabled:this.state.buttonDisabled, onClick:() => this._changeShopState()}}
              />
            </UU5.Bricks.Div>
          );
      }
    },
    //@@viewOff:componentSpecificHelpers

    //@@viewOn:render
    render(){
      return (
        <UU5.Bricks.Section {...this.getMainPropsToPass()}
          header={{
            element:<UU5.Bricks.Lsi lsi={this.constructor.shopLockerData.shopAvailabilityLSI}
              className="shop-locker-header"/>
          }}>
          {
            this._getShopLockerBody()
          }
        </UU5.Bricks.Section>
      );
    }
    //@@viewOff:render

  }
);

export default BackofficeShopLockerBody;