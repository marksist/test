import React from "react";
import * as UU5 from "uu5";

const SysAboutData = {
    header:<UU5.Bricks.Lsi
      lsi={{
        'en-gb':'About Goodymat Application',
        'en-us':'\'bout Goodymat app ',
        'cs':'O aplikaci Goodymat',
        'sk':'O aplikácii Goodymat',
        'ua':'Про аплікацію Goodymat'
      }}/>,
    text:<UU5.Bricks.Lsi
      lsi={{
        "en-gb":`Goodymat is the initial demo application for the <UU5.Bricks.LinkUAF/> that shows the Unicorn mobile-first IoT-ready cloud architecture. It is a simple e-shop selling three products - SM (Smoked knee), RE (Rabbit Ears) and GB (Gummi Bears).`,
        "en-us":`Goodymat be tha initial demo application fo' tha <UU5.Bricks.LinkUAF/> dat shows da unicorn mobile-first iot-ready cloud architecture. Dat shit be a simple e-shop sellin' three products - sm (Smoked knee), re (Rabbit Earz) and gb (Gummi Bearz).`,
        "cs":`Aplikace Goodymat je úvodní demo aplikací <UU5.Bricks.LinkUAF/>. Představuje Unicorn mobile-first IoT-ready cloud architecture. ‍‍‍‍‍Aplikace je jednoduchým internetovým obchodem se třemi produkty: UK (uzené koleno), KU (králičí uši) a GM (gumoví medvídci).‍‍‍‍‍`,
        "sk":"Aplikácia Goodymat je úvodné demo aplikácií <UU5.Bricks.LinkUAF/>. Predstavuje Unicorn mobile-first IoT-ready cloud architecture.",
        "ua":"Додаток Goodymat є демонстрацією <UU5.Bricks.LinkUAF/> і показує приклад архітектури “Unicorn mobile-first IoT-ready cloud architecture”. Додаток є простою інтернет-крамницею з трьома продуктами: UK (копчене коліно), KU (вуха кролика) та GM (гумові медведики)."
      }}/>,
    technologiesHeader:<UU5.Bricks.Lsi
      lsi={{
        "cs":"Použité technologie",
        "en-gb":"Used Technologies",
        "en-us":"Used technologiez",
        "sk":"Použité technológie",
        "ua":"Використані технології"
      }}/>,
    technologies:<UU5.Bricks.Lsi
      lsi={{
        "cs":`V aplikaci byly použity následující technologie společnosti <UU5.Bricks.LinkUnicorn/>:<br/><UU5.Bricks.LinkUAF/>, <UU5.Bricks.LinkUU5/>, <UU5.Bricks.LinkUUIoT/>, <UU5.Bricks.LinkUUAppServer/>, <UU5.Bricks.LinkUUCloud/> a <UU5.Bricks.LinkUUOIDC/>.<br/><br/>Smartbox Goodymat byl vyvinut ve výzkumném centru <UU5.Bricks.LinkUnicornCollege/>.<br/><br/>Dále byly použity technologie: <UU5.Bricks.LinkHTML5/>, <UU5.Bricks.LinkCSS/>, <UU5.Bricks.LinkJavaScript/>, <UU5.Bricks.LinkBootstrap/>, <UU5.Bricks.LinkReact/>, <UU5.Bricks.LinkRuby/>, <UU5.Bricks.LinkPuma/> a <UU5.Bricks.LinkDocker/>.<br/><br/>Aplikace je provozována v rámci internetové služby <UU5.Bricks.LinkPlus4U/> s využitím cloudu <UU5.Bricks.LinkMSAzure/>.`,
        "en-gb":`Goodymat uses following <UU5.Bricks.LinkUnicorn/> technologies: <br/><UU5.Bricks.LinkUAF/>, <UU5.Bricks.LinkUU5/>, <UU5.Bricks.LinkUUIoT/>, <UU5.Bricks.LinkUUAppServer/>, <UU5.Bricks.LinkUUCloud/> and <UU5.Bricks.LinkUUOIDC/>.<br/><br/>The Goodymat smartbox is the product of the <UU5.Bricks.LinkUnicornCollege/> research centre.<br/><br/>This application uses: <UU5.Bricks.LinkHTML5/>, <UU5.Bricks.LinkCSS/>, <UU5.Bricks.LinkJavaScript/>,<UU5.Bricks.LinkBootstrap/>, <UU5.Bricks.LinkReact/>, <UU5.Bricks.LinkRuby/>, <UU5.Bricks.LinkPuma/> and <UU5.Bricks.LinkDocker/> as well.<br/><br/>This application is hosted in the <UU5.Bricks.LinkPlus4U/> internet service using <UU5.Bricks.LinkMSAzure/> cloud.`,
        "en-us":`Goodymat uses following <UU5.Bricks.LinkUnicorn/> technology shit: <br/><UU5.Bricks.LinkUAF/>, <UU5.Bricks.LinkUU5/>, <UU5.Bricks.LinkUUIoT/>, <UU5.Bricks.LinkUUAppServer/>, <UU5.Bricks.LinkUUCloud/> and <UU5.Bricks.LinkUUOIDC/>.<br/><br/>The Goodymat smartbox iz da product o'da <UU5.Bricks.LinkUnicornCollege/> research center.<br/><br/>Da shit use: <UU5.Bricks.LinkHTML5/>, <UU5.Bricks.LinkCSS/>, <UU5.Bricks.LinkJavaScript/>, <UU5.Bricks.LinkBootstrap/>, <UU5.Bricks.LinkReact/>, <UU5.Bricks.LinkRuby/>, <UU5.Bricks.LinkPuma/> and <UU5.Bricks.LinkDocker/> as well.<br/><br/>Da shit iz hosted in da <UU5.Bricks.LinkPlus4U/> internizzle service using <UU5.Bricks.LinkMSAzure/> cloud shit.`,
        "sk":`V aplikácii boli použité následujúce technológie spoločnosti <UU5.Bricks.LinkUnicorn/>:<br/><UU5.Bricks.LinkUAF/>, <UU5.Bricks.LinkUU5/>, <UU5.Bricks.LinkUUIoT/>, <UU5.Bricks.LinkUUAppServer/>, <UU5.Bricks.LinkUUCloud/> a <UU5.Bricks.LinkUUOIDC/>.<br/><br/>Smartbox Goodymat bol vyvinutý vo výskumnom centre <UU5.Bricks.LinkUnicornCollege/>.<br/><br/>Ďalej boli použité technológie: <UU5.Bricks.LinkHTML5/>, <UU5.Bricks.LinkCSS/>, <UU5.Bricks.LinkJavaScript/>, <UU5.Bricks.LinkBootstrap/>, <UU5.Bricks.LinkReact/>, <UU5.Bricks.LinkRuby/>, <UU5.Bricks.LinkPuma/> a <UU5.Bricks.LinkDocker/>.<br/><br/>Aplikácia je prevádzkovaná v rámci internetovej služby <UU5.Bricks.LinkPlus4U/> s využitím cloudu <UU5.Bricks.LinkMSAzure/>.`,
        "ua":`У додатку були використані наступні технології компанії <UU5.Bricks.LinkUnicorn/>:<br/><UU5.Bricks.LinkUAF/>, <UU5.Bricks.LinkUU5/>, <UU5.Bricks.LinkUUIoT/>, <UU5.Bricks.LinkUUAppServer/>, <UU5.Bricks.LinkUUCloud/> a <UU5.Bricks.LinkUUOIDC/>.<br/><br/>Smartbox Goodymat був розроблений у дослідному центрі <UU5.Bricks.LinkUnicornCollege/>.<br/><br/>Далі також були використані технології: <UU5.Bricks.LinkHTML5/>, <UU5.Bricks.LinkCSS/>, <UU5.Bricks.LinkJavaScript/>, <UU5.Bricks.LinkBootstrap/>, <UU5.Bricks.LinkReact/>, <UU5.Bricks.LinkRuby/>, <UU5.Bricks.LinkPuma/> a <UU5.Bricks.LinkDocker/>.<br/><br/>Додаток експлуатується в рамках інтернет-послуги <UU5.Bricks.LinkPlus4U/> з використанням <UU5.Bricks.LinkMSAzure/> cloud.`
      }}/>,
    creatorsHeader:<UU5.Bricks.Lsi lsi={{
      "en-gb":"Authors",
      "en-us":"Makers o'dis shit",
      "cs":"Tvůrci aplikace",
      "sk":"Autori aplikácie",
      "ua":"Автори додатку"
    }}/>,
    licenseOwnerHeader:<UU5.Bricks.Lsi lsi={{
      "en-gb":"License owner",
      "en-us":"Licence owna",
      "cs":"Vlastník licence",
      "sk":"Vlastník licencie",
      "ua":"Власник ліцензії"
    }}/>,
    licenseOrganizationHeader:<UU5.Bricks.Lsi lsi={{
      "en-gb":"Organization",
      "en-us":"Organization",
      "cs":"Organizace",
      "sk":"Organizácia",
      "ua":"Організація"
    }}/>,
    licenseUserHeader:<UU5.Bricks.Lsi lsi={{
      "en-gb":"Competent person",
      "en-us":"Competent person",
      "cs":"Kompetentní osoba",
      "sk":"Kompetentná osoba",
      "ua":"Компетентна особа"
    }}/>,
    creators:{
      leading:[
        {
          imgSrc:"./data/img/19_Vladimir-Kovar.png",
          name:"Vladimír Kovář",
          role:"Chief Business Architect & Stakeholder"
        },
        {
          imgSrc:"./data/img/01_Marek-Beranek.png",
          name:"Marek Beránek",
          role:"Goodymat IoT Master"
        },
        {
          imgSrc:"./data/img/14_Milos-Vodrazka.png",
          name:"Miloš Vodrážka",
          role:"Chief Developer"
        },
        {
          imgSrc:"./data/img/24_LPF.png",
          name:"Lovecký pes Frey",
          role:"Customer Representative"
        },
        {
          imgSrc:"./data/img/26_Goodymat.png",
          name:"Goodymat",
          role:"IoT Representative"
        }
      ],
      others:[
        {
          imgSrc:"./data/img/21_Zuzana-Liznerova.png",
          name:"Zuzana Líznerová",
          role:"Project Coordinator"
        },
        {
          imgSrc:"./data/img/06_Lubos-Fictum.png",
          name:"Luboš Fictum",
          role:"Designer"
        },
        {
          imgSrc:"./data/img/13_Jan-Dosedel.png",
          name:"Jan Doseděl",
          role:"Developer"
        },
        {
          imgSrc:"./data/img/11_Vaclav-Purner.png",
          name:"Václav Pruner",
          role:"Developer"
        },
        {
          imgSrc:"./data/img/15_Milan-Satka.png",
          name:"Milan Šatka",
          role:"Developer & Evangelist"
        },
        {
          imgSrc:"./data/img/22_Vladimir-Kovar-ml.png",
          name:"Vladimír Kovář ml.",
          role:"UX Designer"
        },
        {
          imgSrc:"./data/img/23_Adam-Biciste.png",
          name:"Adam Bičiště",
          role:"UX Designer"
        },
        {
          imgSrc:"./data/img/07_Petr-Ciochon.png",
          name:"Petr Ciochoň",
          role:"Goodymat Python Master"
        },
        {
          imgSrc:"./data/img/25_Aslan.png",
          name:"Pes Aslan",
          role:"Goodymat Tester"
        },
        {
          imgSrc:"./data/img/20_Radka-Vlacilova.png",
          name:"Radka Vláčilová",
          role:"Tester"
        }
      ]
    }
  }
  ;

export default SysAboutData;