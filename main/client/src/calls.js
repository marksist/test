/**
 * Extensions
 */
import {Uri} from "uu_app"
import {Client} from "uu_os9-app"
import {Session} from "uu_oidc"

let Calls = {

  call:function(method, url, params, dtoIn){
    Client[method](url, params).then(
      function(response){
        dtoIn.done(response.data);
      }, function(response){
        dtoIn.fail(response);
      }
    );
  },

  authorizeVuc:function(dtoIn){
    Session.initPromise.then(
      function(){
        let uri = location.protocol + "//" + location.host + location.pathname;
        Client.get(uri).then(
          function(response){
            response.data.profiles = response.data.profileList;
            dtoIn.done(response);
          }, function(response){
            var errorCode = ((response.error||{}).detail||{}).code;
            if (response.status == 401 || errorCode == "UU.APPWORKSPACE.MAIN/SYSPERMISSION/E001") { // user is not authorized => use standard error route
              dtoIn.done({
                data: {
                  status: "error",
                  profiles: []
                }
              });
            } else { // unknown error
              dtoIn.fail(response);
            }
          }
        )
      }
    )
  },

  getLicenseOwner:function(dtoIn){
    let commandUri = Calls.getCommandUri("sys/getLicenseOwner");
    Calls.call("get", commandUri.toString(), null, dtoIn);
  },

  getShopInfo:function(dtoIn){
    let commandUri = Calls.getCommandUri("getShopInfo");
    Calls.call("get", commandUri.toString(), null, dtoIn);
  },

  setShopState:function(dtoIn){
    let commandUri = Calls.getCommandUri("setShopState");
    Calls.call("post", commandUri.toString(), dtoIn.data, dtoIn);
  },

  getShopWindow:function(dtoIn){
    let commandUri = Calls.getCommandUri("getShopWindow");
    Calls.call("get", commandUri.toString(), null, dtoIn);
  },

  proceedOrder:function(dtoIn){
    let commandUri = Calls.getCommandUri("proceedOrder");
    Calls.call("post", commandUri.toString(), dtoIn.data, dtoIn);
  },

  listActiveOrders:function(dtoIn){
    let commandUri = Calls.getCommandUri("listActiveOrders");
    Calls.call("get", commandUri.toString(), null, dtoIn);
  },

  buy:function(dtoIn) {
    let commandUri = Calls.getCommandUri("buy");
    Calls.call("post", commandUri.toString(), dtoIn.data, dtoIn);
  },

  getCommandUri:function(useCase) { // useCase <=> "getSomething" or "sys/getSomething"
    let parts = useCase.split("/");
    if (parts.length > 2) throw new Error("Command use case can have at most 2 levels, but it has " + parts.length + ". Use case: " + useCase);
    let baseUri = location.protocol + "//" + location.host + location.pathname;
    let uriBuilder = Uri.UriBuilder.parse(baseUri)
      .setClassifier(null)
      .setQualifier(null)
      .setOpaque(null);
    if (parts.length == 2) uriBuilder.setClassifier(parts[0]).setQualifier(parts[1]);
    else uriBuilder.setQualifier(parts[0]);
    return uriBuilder.toUri();
  }
}

export default Calls;
