import React from "react";
import ReactDOM from "react-dom";
import UU5 from "uu5";

import Eshop from "./eshop/eshop.js"
import BackofficeShopLocker from "./backoffice_shop_locker/backoffice-shop-locker.js"
import BackofficeOrders from "./backoffice_orders/backoffice-orders.js"
import SysAbout from "./sys_about/sys-about.js"
import SysError from "./sys_error/sys-error.js"
import Calls from "./calls.js";

import "./index.css";

// export UU5 to global space because some code still expects it there
window.UU5 = UU5;

UU5.Environment.App.callConfig.authorizeVuc = Calls.authorizeVuc;
UU5.Environment.App.vucConfig.errorRoute = "/sysError";

let routerBasePath = location.pathname.replace(/^(.*)\/.*/, "$1");
let route = location.pathname.substr(routerBasePath.length);
let routes = {
  "/eshop": { component: <Eshop /> },
  "/backofficeOrders": { component:   <BackofficeOrders /> },
  "/backofficeShopLocker": { component:   <BackofficeShopLocker/> },
  "/sysAbout": { component:   <SysAbout /> },
  "/sysError": { component:   <SysError /> }
};

// NOTE To be able to detect which modules are already loaded in SystemJS loader,
// we have to export a rendering method instead of immediately perform the rendering.
export function render(targetElementId) {
  ReactDOM.render(
    <UU5.Common.Router basePath={routerBasePath} routes={routes} route={route} />,
    document.getElementById(targetElementId)
  );
}
