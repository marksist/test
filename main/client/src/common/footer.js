//@@viewOn:import
import React from "react";
import * as UU5 from "uu5";

import "./footer.css";
//@@viewOff:import

const Footer = React.createClass(
  {

    //@@viewOn:mixins
    mixins:[
      UU5.Common.BaseMixin,
      UU5.Common.ElementaryMixin
    ],
    //@@viewOff:mixins

    //@@viewOn:statics
    statics:{
      tagName:"UclGoodymat.Footer",
      classNames:{
        main:"ucl-goodymat-footer"
      }
    },
    //@@viewOff:statics

    //@@viewOn:propTypes
    //@@viewOff:propTypes

    //@@viewOn:getDefaultProps
    //@@viewOff:getDefaultProps

    //@@viewOn:standardComponentLifeCycle
    //@@viewOff:standardComponentLifeCycle

    //@@viewOn:interface
    //@@viewOff:interface

    //@@viewOn:overridingMethods
    //@@viewOff:overridingMethods

    //@@viewOn:componentSpecificHelpers
    //@@viewOff:componentSpecificHelpers

    //@@viewOn:render
    render() {
      return (
        <footer {...this.buildMainAttrs()}>
          Copyright © 2017 Plus4U
        </footer>
      );
    }
    //@@viewOff:render

  }
);

export default Footer;