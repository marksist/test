//@@viewOn:import
import React from "react";
import * as UU5 from "uu5";
import UserInfo from "./user-info.js";

import "./menu.css";
//@@viewOff:import

const menuItems = [
  {
    nameLSI:{
      "cs":"Obchod",
      "en-gb":"Eshop",
      "en-us":"Sex shop",
      "sk":"Obchod",
      "ua":"інтернет магазин"
    },
    iconClass:"ucl-goodymat-icon-shopping-cart",
    profileCodes:null,
    route:"/eshop"
  }, {
    nameLSI:{
      "cs":"Správa objednávek",
      "en-gb":"Order Management",
      "en-us":"Orda Management",
      "sk":"Správa objednávok",
      "ua":"управління замовленнями"
    },
    iconClass:"ucl-goodymat-icon-orders",
    profileCodes:["Executives"],
    route:"/backofficeOrders"
  }, {
    nameLSI:{
      "cs":"Správa obchodu",
      "en-gb":"Eshop management",
      "en-us":"Sex shop management",
      "sk":"Správa obchodu",
      "ua":"управління бізнесом"
    },
    iconClass:"ucl-goodymat-icon-settings",
    profileCodes:["Executives"],
    route:"/backofficeShopLocker"
  }, {
    nameLSI:{
      "cs":"O aplikaci",
      "en-gb":"About",
      "en-us":"'bout da shit",
      "sk":"O aplikácii",
      "ua":"Про аплікацію"
    },
    iconClass:"ucl-goodymat-icon-info",
    profileCodes:null,
    route:"/sysAbout"
  }
];

const Menu = React.createClass(
  {

    //@@viewOn:mixins
    mixins:[
      UU5.Common.BaseMixin,
      UU5.Common.ElementaryMixin,
      UU5.Common.LsiMixin,
      UU5.Common.ScreenSizeMixin,
      UU5.Common.IdentityMixin
    ],
    //@@viewOff:mixins

    //@@viewOn:statics
    statics:{
      tagName:"UclGoodymat.Menu",
      classNames:{
        main:"ucl-goodymat-menu",
        body:"ucl-goodymat-menu-content",
        close:"ucl-goodymat-menu-close",
        login:"ucl-goodymat-menu-login",
        logout:"ucl-goodymat-menu-logout",
        wide:"ucl-goodymat-menu-wide",
        opened:"ucl-goodymat-menu-opened"
      },
      lsi:{
        menuLSI:{"cs":"Menu", "en":"Menu", "sk":"Menu", "ua":"меню"},
        logoutLSI:{"cs":"Odhlásit", "en-gb":"Log out", "en-us":"Log f*ckin' out", "sk":"Odhlásiť", "ua":"вийти"},
        loginLSI:{
          "cs":"Přihlásit",
          "en-gb":"Log in",
          "en-us":"Log f*ckin' in",
          "sk":"Prihlásiť",
          "ua":"зареєструватися"
        }
      },
      defaults:{
        glyphiconMenu:"ucl-goodymat-icon-menu",
        glyphiconClose:"ucl-goodymat-icon-close"
      }
    },
    //@@viewOff:statics

    //@@viewOn:propTypes
    propTypes:{
      profileCodes:React.PropTypes.arrayOf(React.PropTypes.string)
    },
    //@@viewOff:propTypes

    //@@viewOn:getDefaultProps
    //@@viewOff:getDefaultProps

    //@@viewOn:standardComponentLifeCycle
    getInitialState() {
      return {
        opened:false
      };
    },
    //@@viewOff:standardComponentLifeCycle

    //@@viewOn:interface
    //@@viewOff:interface

    //@@viewOn:overridingMethods
    //@@viewOff:overridingMethods

    //@@viewOn:componentSpecificHelpers
    _onMenuItemClick(item) {
      this.setState({opened:false});
      this.setRoute(item.route);
    },
    _onMenuToggle() {
      this.setState({opened:!this.state.opened});
    },
    _onCloseClick() {
      if(this.state.opened === true){
        this.setState({opened:false});
      }
    },
    _onLoginClick() {
      this.login();
    },
    _onLogoutClick() {
      this.logout();
    },
    //@@viewOff:componentSpecificHelpers

    //@@viewOn:render
    render() {
      let mainAttrs = this.buildMainAttrs();
      if(this.state.opened){
        mainAttrs.className += " " + this.getClassName("opened");
      }
      if(!this.isXs()){
        mainAttrs.className += " " + this.getClassName("wide");
      }

      let toVCItem = (
        item =>{
          let hide = item.profileCodes && !item.profileCodes.some(
              itemProfileCode => (
                this.props.profileCodes || []
              ).indexOf(itemProfileCode) != -1
            );
          if(hide){
            return null;
          }
          return (
            <UU5.Bricks.Li key={item.route}>
              <UU5.Bricks.Link onClick={this._onMenuItemClick.bind(this, item)}>
                <UU5.Bricks.Glyphicon glyphicon={item.iconClass}/>
                <UU5.Bricks.Lsi lsi={item.nameLSI}/>
              </UU5.Bricks.Link>
            </UU5.Bricks.Li>
          );
        }
      );

      return (
        <UU5.Bricks.Div {...mainAttrs}>
          <UU5.Bricks.Link onClick={this._onMenuToggle}>
            <UU5.Bricks.Glyphicon glyphicon={this.getDefault("glyphiconMenu")}/>
          </UU5.Bricks.Link>
          { !this.isXs()
            ? <UU5.Bricks.Text content={this.getLSIValue("menu")} className={this.getClassName("menuTitle")}/>
            : null}

          <UU5.Bricks.Div className={this.getClassName("body")}>
            <UU5.Bricks.Link onClick={this._onCloseClick} className={this.getClassName("close")}>
              <UU5.Bricks.Glyphicon glyphicon={this.getDefault("glyphiconClose")}/>
            </UU5.Bricks.Link>
            <UserInfo />
            <UU5.Bricks.Ul>
              {menuItems.map(it => toVCItem(it))}
            </UU5.Bricks.Ul>
            { this.isAuthenticated()
              ?
              <UU5.Bricks.Link onClick={this._onLogoutClick} className={this.getClassName("logout")} key="logout">
                {this.getLSIComponent("logoutLSI")}
              </UU5.Bricks.Link>
              :
              <UU5.Bricks.Link onClick={this._onLoginClick} className={this.getClassName("login")} key="login">
                {this.getLSIComponent("loginLSI")}
              </UU5.Bricks.Link>
            }
          </UU5.Bricks.Div>
        </UU5.Bricks.Div>
      );
    }
    //@@viewOff:render

  }
);

export default Menu;