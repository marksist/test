//@@viewOn:import
import React from "react";
import * as UU5 from "uu5";

import Menu from "./menu.js";

import "./header.css";
//@@viewOff:import

// UU5 flag paths are relative to current HTML page, not relative to UU5 library base => change.
const languages = ["cs", "en-gb", "en-us", "sk", "ua"];
languages.forEach(lang => {
  let setting = UU5.Environment.languagesList[lang];
  setting.flag = setting.flag.replace(/^\.\/assets/, "./lib/uu5/assets");
  if (lang == "en-us") setting.language = englishUsName;
});

// change English US name
const englishUsName = "English Trump";
UU5.Environment.languagesList["en-us"].language = englishUsName;


// header component
const Header = React.createClass({

  //@@viewOn:mixins
  mixins: [
    UU5.Common.BaseMixin,
    UU5.Common.ElementaryMixin,
    UU5.Common.ScreenSizeMixin
  ],
  //@@viewOff:mixins

  //@@viewOn:statics
  statics: {
    tagName: "UclGoodymat.Header",
    classNames: {
      main: "ucl-goodymat-header",
      menuTitle: "ucl-goodymat-header-menu-title",
      body: "ucl-goodymat-header-body",
      languageSelector: "ucl-goodymat-header-ls"
    }
  },
  //@@viewOff:statics

  //@@viewOn:propTypes
  propTypes: {
    labelLsi: React.PropTypes.any,
    menuProfileCodes: React.PropTypes.arrayOf(React.PropTypes.string)
  },
  //@@viewOff:propTypes

  //@@viewOn:getDefaultProps
  //@@viewOff:getDefaultProps

  //@@viewOn:standardComponentLifeCycle
  //@@viewOff:standardComponentLifeCycle

  //@@viewOn:interface
  //@@viewOff:interface

  //@@viewOn:overridingMethods
  //@@viewOff:overridingMethods

  //@@viewOn:componentSpecificHelpers
  //@@viewOff:componentSpecificHelpers

  //@@viewOn:render
  render() {
    var mainAttrs = this.buildMainAttrs();

    return (
      <header {...mainAttrs}>
        <Menu profileCodes={this.props.menuProfileCodes} />

        <UU5.Bricks.Div className={this.getClassName("body")}>
          <UU5.Bricks.Lsi lsi={this.props.labelLsi} />
        </UU5.Bricks.Div>

        <UU5.Bricks.LanguageSelector
          className={this.getClassName("languageSelector")}
          displayedLanguages={languages}
          headerMode="flag" />
      </header>
    );
  }
  //@@viewOff:render

});

export default Header;