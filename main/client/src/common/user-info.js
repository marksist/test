//@@viewOn:import
import React from "react";
import * as UU5 from "uu5";
import "uu_oidc";

import "./user-info.css";
//@@viewOff:import

const UserInfo = React.createClass(
  {

    //@@viewOn:mixins
    mixins:[
      UU5.Common.BaseMixin,
      UU5.Common.ElementaryMixin,
      UU5.Common.IdentityMixin
    ],
    //@@viewOff:mixins

    //@@viewOn:statics
    statics:{
      tagName:"UclGoodymat.UserInfo",
      classNames:{
        main:"ucl-goodymat-userinfo",
        image:"ucl-goodymat-userinfo-image",
        name:"ucl-goodymat-userinfo-name"
      },
      lsi:{
        anonymousNameLSI:{
          "en-gb":"Anonymous",
          "en-us":"Unknown loser",
          "cs":"Nepřihlášen",
          "sk":"Neprihlásený",
          "ua":"анонімний"
        }
      }
    },
    //@@viewOff:statics

    //@@viewOn:propTypes
    //@@viewOff:propTypes

    //@@viewOn:getDefaultProps
    //@@viewOff:getDefaultProps

    //@@viewOn:standardComponentLifeCycle
    //@@viewOff:standardComponentLifeCycle

    //@@viewOn:interface
    //@@viewOff:interface

    //@@viewOn:overridingMethods
    //@@viewOff:overridingMethods

    //@@viewOn:componentSpecificHelpers
    //@@viewOff:componentSpecificHelpers

    //@@viewOn:render
    render() {
      // check whether we're authenticated andwhether we're able to get the picture
      const image = (
        this.isAuthenticated() && (
        this.getCurrentSession().getClaims() || {}
        )["sub"]
          ? this.getCurrentSession().serverUri + "/getIdentityPicture?id=" + encodeURIComponent(
          this.getCurrentSession()
            .getClaims()["sub"]
        )
          : "./common/img/anonymous.png"
      );

      return (
        <UU5.Bricks.Div {...this.buildMainAttrs()}>
          <UU5.Bricks.Image
            className={this.getClassName("image")}
            src={image}
            type="circle"/>
          <UU5.Bricks.Span className={this.getClassName("name")}>
            { this.isAuthenticated()
              ? this.getIdentity().name.toUpperCase()
              : this.getLSIComponent("anonymousNameLSI")}
          </UU5.Bricks.Span>
        </UU5.Bricks.Div>
      );
    }
    //@@viewOff:render

  }
);

export default UserInfo;