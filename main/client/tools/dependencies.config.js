var pkg = require("../package.json");
var fs = require("fs-extra");
var buildHelpers = require("./helpers.js");
var isProd = (process.env.NODE_ENV == "production");
var infix = (isProd ? ".min" : "");

// if build tool is set to use externals (i.e. the dependencies aren't in the final bundle and must be loaded
// from other sources), these are the URLs to load them from 
var depConfig = {
  "LIB": {
    externalUri: "lib/",
  },
  "bootstrap": { // bootstrap uses non-standard path to its main file (nested in js/ folder)
    externalUri: "lib/bootstrap-3.3.7/js/bootstrap" + infix + ".js"
  },
  "uu5": { // needs extra configuration while it's not npm module
    externalUri: "lib/uu5/uu5-03.min.js"
  }
  // "some-module": {
  //   externalUri: false // don't load via separate HTTP request (i.e. bundle in); note that this might slow down rebuilds
  //   externalUri: "lib/some-module-1.2.3/mainFile.js"
  // }
};

// mark all remaining dependencies from package.json as loaded from lib/<name>-<version>/ (because we copy them there during build)
if (pkg.dependencies) for (var depName in pkg.dependencies) {
  if (depName in depConfig) continue; // already configured above => skip
  var depDirName = getDependencyDirName(depName);
  if (!depDirName) continue;
  depConfig[depName] = {
    externalUri: "lib/" + depDirName + "/" + depName + infix + ".js"
  };
}

// add project packages to be loaded from corresponding files
var packages = buildHelpers.getProjectPackageList();
packages.forEach(pack => {
  depConfig[pack.moduleName] = {
    externalUri: pack.directory + "/" + pack.name + infix + ".js"
  };
});

module.exports = depConfig;

function getDependencyDirName(depName) {
  // if the dependency is in the src/lib/ (with any version suffix) then use that,
  // otherwise use <depName>-<depVersion> (where <depVersion> is taken from package.json
  // of that dependency)
  var dirs = fs.readdirSync("src/lib") || [];
  var depDirInSrcLib = dirs.filter(it => it === depName || (it.substr(0, depName.length + 1) == depName + "-" && it.substr(depName.length + 1).match(/\d+(\.\d+)*/)))[0];
  if (depDirInSrcLib) return depDirInSrcLib;
  var version = getDependencyVersion(depName);
  return version ? depName + "-" + version : null;
}
function getDependencyVersion(depName) {
  var depPkgJsonPath = "node_modules/" + depName + "/package.json";
  if (!fs.existsSync(depPkgJsonPath)) return null;
  var depPkg = require("../" + depPkgJsonPath);
  return depPkg.version;
}