var fs = require("fs-extra");
var pkg = require("../package.json");
var buildHelpers = require("./helpers.js");

var env = "uu-environment.json"; // URL to load environment from when running in browser
var packages = buildHelpers.getProjectPackageList();
var outputPath = fs.existsSync("../server") ? "../server/public/v01-05-build01" : "public";

var webpackConfig = [].concat(buildHelpers.getWebpackConfig({
  minify: true,
  useExternals: true,
  useSourceMaps: false,
  entryPoints: ["src/index.js"],
  outputPath: outputPath,
  uuConfig: {
    environment: env
  }
}));
webpackConfig = packages.reduce((r, pack) => r.concat(buildHelpers.getWebpackConfig({
  minify: true,
  useExternals: true,
  useSourceMaps: true,
  libraryName: pkg.name + "-" + pack.name,
  libraryFileName: pack.name + "/" + pack.name + ".min.js",
  entryPoints: [pack.entryPoint],
  outputPath: outputPath,
  uuConfig: {
    environment: env
  }
})), webpackConfig);

module.exports = webpackConfig;