var fs = require("fs-extra");
var pkg = require("../package.json");
var buildHelpers = require("./helpers.js");

var env = {
  "gatewayUri": "http://localhost.plus4u.net:6221", // gateway address for uuCommands, e.g. https://uuos9.plusu4.net
  "uuoidc.redirectUri": "callbacks/login-callback.html"
  // ,"uuoidc.sessionCheckInterval": 60 // how often in seconds to check session for detecting logout in another tab
};
var packages = buildHelpers.getProjectPackageList();
var outputPath = fs.existsSync("../server") ? "../server/public/v01-05-build01" : "public";

var webpackConfig = [].concat(buildHelpers.getWebpackConfig({
  minify: false,
  useExternals: true,
  useSourceMaps: true,
  entryPoints: ["src/index.js"],
  outputPath: outputPath,
  uuConfig: {
    environment: env
  }
}));
webpackConfig = packages.reduce((r, pack) => r.concat(buildHelpers.getWebpackConfig({
  minify: false,
  useExternals: true,
  useSourceMaps: true,
  libraryName: pkg.name + "-" + pack.name,
  libraryFileName: pack.name + "/" + pack.name + ".js",
  entryPoints: [pack.entryPoint],
  outputPath: outputPath,
  uuConfig: {
    environment: env
  }
})), webpackConfig);

module.exports = webpackConfig;