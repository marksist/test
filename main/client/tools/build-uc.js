var webpack = require("webpack");
var rimraf = require("rimraf");
var fs = require("fs-extra");
var path = require("path");
var MemoryFS = require("memory-fs");
var buildHelpers = require("./helpers.js");
var pkg = require("../package.json");

rimraf.sync(".tmp");

var isProd = (process.env.NODE_ENV == "production");
var webpackConfigFile = `./webpack-uc-${isProd ? 'prod' : 'dev'}.config.js`;
var webpackConfig = require(webpackConfigFile);
delete require.cache[require.resolve(webpackConfigFile)];
var loaderForBrowser = webpackConfig.filter(it => it.uuUsedOptions.isLoaderForBrowser)[0];
var needsLoaderConfiguration = (loaderForBrowser && loaderForBrowser.uuUsedOptions.useExternals);

// copy dependencies listed in package.json to <destination>/lib/<depName>-<devVersion> folders
// (unless such dependency is already there because of being in src/lib/)
var dest = (loaderForBrowser ? loaderForBrowser.uuMainConfig : webpackConfig[0]).uuUsedOptions.outputPath || "dist";
fs.emptyDirSync(dest);
if (pkg.dependencies) {
  var srcLibs = fs.readdirSync("src/lib") || [];
  var srcLibsMap = {};
  srcLibs.forEach(lib => srcLibsMap[lib.replace(/-(\d+)(\.\d+)*$/, "")] = true);
  for (var depName in pkg.dependencies) {
    if (srcLibsMap[depName]) continue; // it's in src/lib/ => skip it
    var depRootPath = "node_modules/" + depName + "/";
    var depPkgJsonPath =  depRootPath + "package.json";
    if (!fs.existsSync(depPkgJsonPath)) {
      console.error("ERROR Unable to find package.json for dependency " + depName + " (was looking at " + depPkgJsonPath + "). Do 'npm install' or copy browser files of the dependency to src/lib/" + depName + "-<dependencyVersion>/ or remove the dependency from package.json.");
      process.exit(1);
    }
    var depPkgJson = require("../" + depPkgJsonPath);
    var depVersion = depPkgJson.version;
    var depTargetFldName = depName + "-" + depVersion;
    var folders = ["dist-browser", "dist"];
    if (typeof depPkgJson.browser == "string") folders.unshift(depPkgJson.browser.replace(/[\\/].*/, "")); // preserve only 1st folder
    var depBrowserFolder = folders.map(fld => depRootPath + fld).filter(fld => fs.existsSync(fld))[0];
    if (!depBrowserFolder) {
      console.error("ERROR Unable to find browser files of dependency '" + depName + "'. Please, copy browser files of the dependency to src/lib/" + depTargetFldName + "/");
      process.exit(1);
    }
    var depEntryPoints = [depBrowserFolder + "/" + depName + ".js", depBrowserFolder + "/" + depName + ".min.js"];
    if (depEntryPoints.some(depEntryPoint => !fs.existsSync(depEntryPoint))) {
      console.error("ERROR Unable to find entry point(s) of dependency '" + depName + "' or its minified version (was looking at " + depEntryPoints.join(", ") + "). To use this dependency, copy it to src/lib/" + depTargetFldName + "/ and either rename main entry points or configure paths in tools/dependencies.config.js.");
      process.exit(1);
    }

    var targetDir = dest + "/lib/" + depTargetFldName;
    fs.copySync(depBrowserFolder, targetDir);
  }
}

if (needsLoaderConfiguration) {
  console.log("Computing external dependencies...");
  var externalsMain = loaderForBrowser.uuMainConfig.uuUsedOptions.uuConfig.externalDependencies || {};
  // use the same webpack configuration, just override "useExternals" to "false" so that we can
  // figure out transitive dependencies in webpack's stats result
  var webpackNoExternalsConfig = webpackConfig.filter(it => !it.uuMainConfig).reduce((r, conf) => {
    return r.concat(buildHelpers.getWebpackConfig(Object.assign({}, conf.uuUsedOptions, {
      useExternals: false,

      // remaining options are just to speed it up a bit
      generateLoaderForBrowser: false,
      minify: false,
      useSourceMaps: false
    })));
  }, []);
  getExternalDependencies(webpackNoExternalsConfig, externalsMain, continueWithExternals);
} else {
  continueWithExternals({});
}

function continueWithExternals(depMap, errors) {
  if (!errors) {
    // // NOTE Uncomment for more detailed logging.
    // var OUTPUT_INCLUDED_FROM_LIMIT = 5;
    // console.log("External dependencies:");
    // console.log(Object.keys(depMap || {}).map(it => depMap[it]).map(it => {
    //   return `${it.name}@${it.version}\n` +
    //       it.includedFrom.filter((it, idx) => idx < OUTPUT_INCLUDED_FROM_LIMIT).map((inc,idx,s) => (idx == s.length-1 && s.length < it.includedFrom.length
    //         ? `  ... (${it.includedFrom.length} total)` 
    //         : `  from ${inc.fromModule} as ${inc.includedModule}`
    //       )).join("\n");
    // }).join("\n"));
  }

  // pass information about external modules to webpack configuration (via temporary file)
  console.log(`Running webpack with ${isProd ? "production" : "development" } settings...`);
  var depList = Object.keys(depMap || {}).map(it => depMap[it]); // [{ "name":"dep1", "version":"1.2.3", "packageJson": {...}, "includedFrom": [] }]
  fs.mkdirsSync(".tmp");
  fs.writeFileSync(".tmp/computedDeepExternalDependencies.json", JSON.stringify(depList), "utf-8");
  var webpackProdConfig = require(webpackConfigFile);
  webpack(webpackProdConfig, function (err, statsObj) {
    if (err) return console.error(err);

    // show standard output of webpack (if there're errors)
    var stats = statsObj.toJson();
    if (stats.errors.length > 0) {
      console.log(statsObj.toString(true));
      return;
    }
    if (errors) return console.error(errors); // shouldn't happen (should have returned in previous statement)

    // show build summary
    var externalDepsInfo = (loaderForBrowser.uuMainConfig.uuUsedOptions.useExternals ? depList.map(it => {
      return `${it.name}@${it.version} (from ${it.includedFrom[0].fromModule}, ...)`;
    }).join("\n") : "<not using externals>");
    var inBrowserEnv = loaderForBrowser.uuMainConfig.uuUsedOptions.uuConfig.environment;
    var inBrowserEnvInfo = (!inBrowserEnv ? `<empty> - defaults will be used` :
      typeof inBrowserEnv == "string"
        ? `auto-loaded from URL: ${inBrowserEnv}`
        : `hard-coded: ${JSON.stringify(inBrowserEnv)}`);
    console.log(`
BUILD INFO
External dependencies:
${externalDepsInfo.replace(/(^|\n)/g, "$1  ")}
In-browser environment:
${inBrowserEnvInfo.replace(/(^|\n)/g, "$1  ")}
`);
  });
}




/**
 * Runs webpack with provided configuration and extracts list of used dependencies (deep).
 * Webpack is run in-memory with no changes in file system. Webpack configuration must NOT
 * use externals (otherwise webpack would skip processing of dependencies of those externals).
 * Dependencies marked as non-external in uuConfig won't be in the result (unless required
 * from another dependency).
 * 
 * callbackFn(data, errors) where data is object: {
 *   "/path/to/node_modules/depX": {
 *     "name: "depX",
 *     "version": "1.2.3",
 *     "packageJson": {...},  // contents of package.json,
 *     "path": "/path/to/node_modules/depX",
 *     "includedFrom": [{
 *       "includedModule": "depX/some/file.js",
 *       "fromModule": "some/other/module.js"
 *     }]
 *   }, ...
 * } 
 */
function getExternalDependencies(webpackConfig, uuConfExternals, callbackFn) {
  // use webpack configuration which bundles everything without any external dependencies
  // and obtain profiling stats with information about which modules were really included in the build 
  console.log("Computing bundled file list...");

  var memoryFs = new MemoryFS();
  var compiler = webpack(webpackConfig);
  compiler.outputFileSystem = memoryFs;
  compiler.run(function(err, statsObj) {
    if (err) return callbackFn(null, err);
    var stats = statsObj.toJson(); // stats structure example: https://raw.githubusercontent.com/webpack/analyse/master/app/pages/upload/example.json
    if (stats.errors.length > 0) return callbackFn(null, stats.errors);
    
    console.log("Extracting list of used node modules (transitive)...");
    var curDir = path.resolve(".");
    var deps = [];
    var depsMap = {};
    // var uuConfExternals = webpackConfig.uuUsedOptions.uuConfig.externalDependencies || {};
    [stats].concat(stats.children).forEach((child) => {
      (child.modules || [])
      .filter(mod => !mod.name.match(/^external /)) // exclude special externals (UU_ENVIRONMENT)
      .filter(mod => mod.name != "(webpack)/buildin/module.js") // exclude special externals (module)
      .filter(mod => !mod.name.match(/\(ignored\)$/)) // exclude ignored modules ("browser" field with object in some package.json-s, e.g. in es6-promise)
      .forEach((mod) => {
        function stripLoaders(request) {
          return (request||"").replace(/^.*!/, ""); // "css-loader!x/y/z.css"  =>  "x/y/z.css"
        }
        function isDependency(request) {
          return !stripLoaders(request).match(/^([.\/\\]|[A-Z]:|__)/); // not relative, i.e. not starting with "." nor absolute such as "/xyz" or "C:/", and not special (starting with __)
        }
        // function isFromProject(filePath) {
        //   if (filePath.substr(0, curDir.length + path.sep.length) != curDir + path.sep) return false;
        //   var relPath = filePath.substr(curDir.length + path.sep.length);
        //   return !relPath.match(/^node_modules/);
        // }
        var includedAsDependencyFrom = (mod.reasons||[]).filter(reason => isDependency(reason.userRequest));
        if (includedAsDependencyFrom.length == 0) return;

        var filePath = stripLoaders(mod.identifier);
        var includedAs = stripLoaders(includedAsDependencyFrom[0].userRequest); //   e.g. bootstrap/js/bootstrap.js
        var rootNodeModuleName = includedAs.replace(/\/.*/, ""); // e.g. bootstrap (stripped everything after "/")
        if (rootNodeModuleName == "project") return; // don't consider "project/..." as npm-like dependencies

        // filter out based on configuration of externals in uuConfig
        if ((uuConfExternals[includedAs]||{}).externalUri === false || (uuConfExternals[rootNodeModuleName]||{}).externalUri === false) {
          return; // configured to be bundled in
          // includedAsDependencyFrom = includedAsDependencyFrom.filter(reason => !isFromProject(stripLoaders(reason.moduleIdentifier)));
          // if (includedAsDependencyFrom.length == 0) return; // it's marked to be included into bundle and it was included only from local project files (not from another external dependency) => omit
        }

        var rootNodeModuleDir = path.resolve(filePath, includedAs.split(/\//).slice(1).map((/*_*/) => "..").join("/") || "./");
        if (rootNodeModuleName !== "LIB") { // find root directory for the module (if it's npm module at all)
          while (rootNodeModuleDir.length > curDir.length && !fs.existsSync(path.join(rootNodeModuleDir, "package.json"))) rootNodeModuleDir = path.resolve(rootNodeModuleDir, "..");
          if (!(rootNodeModuleDir.length > curDir.length) && rootNodeModuleName != "uu5") { // TODO Remove condition for uu5 after its normal npm module.
            console.warn("Unable to find package.json for root module. Module included as '%s' from '%s'. Module is at %s so root module (%s) should be at %s but there's no package.json there.",
              includedAs, includedAsDependencyFrom[0].moduleIdentifier, filePath, rootNodeModuleName, rootNodeModuleDir);
            return;
          }
        }

        deps.push({ module: mod, includedAsDependencyFrom: includedAsDependencyFrom, rootNodeModuleName });
        if (!depsMap[rootNodeModuleDir]) {
          depsMap[rootNodeModuleDir] = {
            name: rootNodeModuleName,
            path: rootNodeModuleDir,
            includedFrom: [],
            version: "0.0.0",
            packageJson: {}
          };
          var pkgJsonPath = path.resolve(rootNodeModuleDir, "package.json");
          if (fs.existsSync(pkgJsonPath)) {
            var pkgJson = JSON.parse(fs.readFileSync(pkgJsonPath, "utf-8"));
            depsMap[rootNodeModuleDir].version = pkgJson.version;
            depsMap[rootNodeModuleDir].packageJson = pkgJson;
          }
        }
        includedAsDependencyFrom.forEach(it => {
          depsMap[rootNodeModuleDir].includedFrom.push({
            includedModule: it.userRequest,
            fromModule: it.moduleName
          });
        });

        // NOTE "react" has dependencies on "fbjs", "object-assign" and "process" but we don't really need
        // them as they're bundled into react as internals. Problem is we have no way of detecting that
        // (might only hard-code it specifically for react)...
        // The result is that we'll configure paths for our SystemJS loader for those dependencies but they
        // won't really be used so it's not such a problem.
      });
    });

    callbackFn(depsMap);
  });

}