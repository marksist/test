var path = require("path");
var fs = require("fs-extra");
var walk = require("walk");
var CopyWebpackPlugin = require('copy-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var WriteFilePlugin = require('write-file-webpack-plugin');
var webpack = require("webpack");

var UU5_PATH = "lib/uu5/uu5-03.min.js"; // special configuration needed while UU5 is not real npm module
var src = "src";
var pkg = require("../package.json");
var srcAbsPath = path.resolve(src);
var srcLibAbsPath = path.resolve(src, "lib");

function fillDefaults(opts) {
  if (opts.minify == null) opts.minify = false;
  if (opts.useExternals == null) opts.useExternals = true; // false <=> all dependencies will be bundled (nothing will load externally) in final distribution package
  if (opts.useSourceMaps == null) opts.useSourceMaps = true;
  if (opts.libraryName == null) opts.libraryName = "";
  if (opts.libraryFileName == null) opts.libraryFileName = (opts.libraryName ? opts.libraryName + (opts.minify ? ".min" : "") + ".js" : "");
  if (opts.targetEnvironment == null) opts.targetEnvironment = "browser";
  if (opts.separateCss == null) opts.separateCss = false;
  if (opts.generateLoaderForBrowser == null) opts.generateLoaderForBrowser = (opts.targetEnvironment == "browser" && !opts.libraryName);
  if (opts.outputPath == null) opts.outputPath = (opts.targetEnvironment == "browser" ? "dist" : "dist-" + opts.targetEnvironment);
  if (opts.entryPoints == null) {
    if (opts.libraryName) opts.entryPoints = [path.join(src, "index.js")];
    else opts.entryPoints = getHtmlFileList(src).map(htmlFile => htmlFile.replace(/\.html$/, ".js"));
  }
  if (opts.uuConfig == null) opts.uuConfig = {};
  return opts;
}

function getWebpackConfig(options) {
  var opts = Object.assign({}, options);
  opts = fillDefaults(opts);

  // CONFIG webpack loaders
  var extractCss = (opts.separateCss ? new ExtractTextPlugin((opts.libraryFileName.replace(/\.js$/,"") || "[name]") + ".css") : null);
  var loaders = [
    { test: /\.jsx?$/, loader: "babel", include: [srcAbsPath], exclude: [srcLibAbsPath] },
    (extractCss
      ? { test: /\.css$/, loader: extractCss.extract(["css"]) }
      : { test: /\.css$/, loader: "style!css" }
    ),
    { exclude: [/\.jsx?$/, /\.css$/], loader: "file?name=[path][name].[ext]"} // if import-ing anything else just copy it
  ];

  // CONFIG webpack plugins
  var plugins = [
    // NOTE CommonsChunkPlugin does not work with externals - do not use it (https://github.com/webpack/webpack/issues/439 & https://github.com/webpack/webpack/issues/622).

    // write files to output folder even when using webpack-dev-server (because by default it builds & serves them from memory)
    new WriteFilePlugin({ log: false })
  ];
  // copy HTML files explicitly ("from" path is relative to webpack's context, i.e. srcAbsPath)
  if (!opts.libraryName) {
    plugins.push(new CopyWebpackPlugin([{ from: "**/*" }], { ignore: ["*.js", "*.jsx", "*.css", "*.less", "lib/**"] }));
    plugins.push(new CopyWebpackPlugin([{ from: "lib/**" }]));
    plugins.push(new CopyWebpackPlugin([{ context: "../node_modules/uu_oidc/dist", from: "callbacks/**" }]));
  } // https://github.com/kevlened/copy-webpack-plugin/tree/v3.0.1
  if (extractCss) plugins.push(extractCss);
  if (opts.minify) {
    plugins = [
      new webpack.optimize.UglifyJsPlugin(),
      new webpack.optimize.OccurrenceOrderPlugin()
    ].concat(plugins);
  }

  // CONFIG configuration which dependencies are external (loaded from CDN) and shimming
  var externalsConfig = Object.assign({}, require("./dependencies.config.js"));
  opts.uuConfig.externalDependencies = externalsConfig;
  if (opts.useExternals) {
    // all dependencies in package.json are by default external
    var pkgDeps = Object.keys(pkg.dependencies || {}).reduce((r,k) => (r[k] = { externalUri: true }, r), {});
    externalsConfig = Object.assign({}, pkgDeps, externalsConfig);
  } else {
    externalsConfig = {};
  }
  externalsConfig["UU_ENVIRONMENT"] = { externalUri: true }; // force external
  externalsConfig["module"] = { externalUri: true, format: "global", exports: "undefined" }; // force external (shimmed to be undefined if loading directly via <script> tag)

  // aliases for resolving modules
  var aliases = {
    "__project__": srcAbsPath,
    "LIB": path.join(srcAbsPath, "lib"), // for non-npm modules, which we want to handle as if they were standalone modules
    "uu5": path.join(srcAbsPath, UU5_PATH) 
  };
  getProjectPackageList().forEach(pack => {
    aliases[pack.moduleName] = path.resolve(pack.entryPoint);
  });


  if (opts.libraryName && opts.entryPoints.length != 1) {
    throw new Error("Library must have exactly 1 entry point, but these were configured: " + JSON.stringify(opts.entryPoints));
  }
  if (opts.entryPoints.length == 0) {
    throw new Error("At least 1 entry point must be specified when preparing webpack configuration.");
  }
  fs.mkdirsSync(opts.outputPath);

  // prepare webpack configuration
  var webpackConfig = [];

  // convert entry files from entryPoints to webpack configuration (we'll need to generate different entry file
  // for each of these as a workaround because we need to set publicPath during runtime, not during compile time
  // which is not as straightforward with webpack - https://github.com/webpack/webpack/issues/2776)
  var chunksAsModules = opts.entryPoints.map(it => "./" + path.relative(srcAbsPath, path.resolve(it)).replace(/\\/g, "/"));
  var chunkNames = chunksAsModules.map(it => it.substr(2).replace(/\.js$/, ""));
  var chunkNamesMap = chunkNames.reduce((r, it, i) => { r[it] = createEntryPointFile(chunksAsModules[i], opts); return r; }, {}); // e.g. "index" => "./.tmp/index.js"
  var usedLibraryName = (opts.libraryName ? opts.libraryName.replace(/(^|_)(.)/g, (m,g,g2) => g2.toUpperCase()).replace(/-(.)/g, (m,g) => "." + g.toUpperCase()) : ""); // // abc_def-ghi-jkl => AbcDef.Ghi.Jkl
  webpackConfig.push({
    context: srcAbsPath,
    entry: chunkNamesMap,
    output: {
      filename: (opts.libraryName ? opts.libraryFileName : null) || "[name].js",
      chunkFilename: "chunks/[name]-[hash].js",
      path: path.resolve(opts.outputPath),
      // publicPath: undefined, // publicPath is configured during runtime (in browser)
      pathinfo: !opts.minify,
      jsonpFunction: "__webpack_jsonp_" + pkg.name.replace(/[^a-zA-Z0-9]/g, "_"), // to prevent collisions between libraries if they load chunks themselves
      libraryTarget: "umd",
      library: (opts.libraryName ? usedLibraryName.split(/[.]/) : "[name]"),
      umdNamedDefine: true,
      devtoolModuleFilenameTemplate: (opts.useSourceMaps ? "webpack:///" + pkg.name + "@" + pkg.version + "/" + src.replace(/\\/g, "/") + "[resource-path]" : undefined)
    },
    resolve: {
      alias: aliases
    },
    module: {
      loaders: loaders
    },
    plugins: plugins,
    externals: function (context, request, callback) {
      var rootModule = request.replace(/\/.*/, ""); // "module/that/is/nested" => "module"
      var conf = externalsConfig[request] || externalsConfig[rootModule];
      if (!conf || conf.externalUri === false) return callback(); // configured as not external
      var loadAs = {
        amd: request,
        commonjs: request,
        commonjs2: request,
        root: (conf && conf.format == "globals" ? conf.exports : request)
      };
      return callback(null, loadAs);
    },
    devtool: (opts.useSourceMaps ? "source-map" : false),
    devServer: {
      outputPath: path.resolve(opts.outputPath)
    },
    uuUsedOptions: opts
  });

  if (opts.generateLoaderForBrowser) {
    // NOTE "main" configuration needs to be first because that's what webpack-dev-server uses (it reads "devServer" field only from 1st one).
    webpackConfig = webpackConfig.concat(getWebpackConfigForLoader(opts, webpackConfig[0]));
  }

  return webpackConfig;
  

  function createEntryPointFile(name, opts) {
    if (opts.targetEnvironment != "browser") return name; // support for runtime publicPath needs to be done only for browser environment

    fs.mkdirsSync(".tmp/" + name.replace(/^(.*\/).*/, "$1").replace(/^\.\//, ""));
    var noRelativeName = name.replace(/^\.\//, "");
    var entryFilePath = path.resolve(".tmp/" + noRelativeName);
    var depth = noRelativeName.split(/\//).length;
    var entryFileContents = 
  `var mod=require("module");
  var uri = ((mod ? mod.uri : (document.currentScript || Array.prototype.slice.call(document.getElementsByTagName("script"), -1)[0] || {}).src) || "").toString();
  __webpack_public_path__=uri.split(/\\//).slice(0, -${depth}).join("/") + "/"; // runtime publicPath configuration required for proper linking of styles, background images, ...
  module.exports = require("__project__/${noRelativeName}");`;
    fs.writeFileSync(entryFilePath, entryFileContents, "utf-8");
    return entryFilePath;
  }
}

function getWebpackConfigForLoader(options, mainWebpackConfig) {
  var opts = Object.assign({}, options);
  opts = fillDefaults(opts);
  opts.isLoaderForBrowser = true;

  var loaderTpl = `
(function() {
  // use SystemJS as AMD-compatible loader; include JSON plugin
  window.define = System.amdDefine;
  window.require = window.requirejs = System.amdRequire;
  var config = __CONFIG__;
  System.config(config);
  var jsonPlugin = require("systemjs-plugin-json");
  System.amdDefine("json", [], function () { return jsonPlugin; });
  var normJsonModule = System.normalizeSync("json");

  var appPublicBase = System.scriptSrc.replace(/^(.*)\\/.*/, "$1/"); // assume SystemJS loader is in the root of the application
  if (__HAS_UU_ENVIRONMENT__) System.amdDefine('UU_ENVIRONMENT', [], function () {return __UU_ENVIRONMENT__; });
  else {
    var envUri = __UU_ENVIRONMENT_URI__;
    if (!envUri.match(/^[a-zA-Z0-9\\-_]+:/) && !envUri.match(/^\\//)) envUri = appPublicBase + envUri; // it's relative => let it be relative to app base, not to folder of current HTML page
    System.config({ paths: { "UU_ENVIRONMENT": envUri }, meta: { "UU_ENVIRONMENT": { loader: "json" } } });
  }
  var DEFAULT_CDN_URI = "https://cdn.plus4u.net";

  // always load UU_ENVIRONMENT first (with the exception of JSON plugin) as it contains CDN configuration
  var origSystemImport = System.import;
  System.import = function (/*...*/) {
    var args = Array.prototype.slice.call(arguments, 0);
    var scope = this;
    if (args[0] == normJsonModule) return origSystemImport.apply(scope, args);
    return origSystemImport.call(this, "UU_ENVIRONMENT").then(function (env) {
      applyUuEnvironment(env);
      System.import = origSystemImport;
      return origSystemImport.apply(scope, args);
    })
  };
  function applyUuEnvironment(env) {
    function fixCdn(value) {
      return value.replace(/^%%CDN%%/, function () { return (env.cdnUri || DEFAULT_CDN_URI || "").replace(/\\/*$/,""); });
    }
    function fixBase(value) {
      return value.replace(/^%%APP_PUBLIC_BASE%%/, function () { return appPublicBase.replace(/\\/*$/,""); });
    }
    // inject CDN path to SystemJS loader configuration
    var paths = (config||{}).paths||{};
    for (var k in paths) paths[k] = fixBase(fixCdn(paths[k]));
    System.config({paths: paths});
  }
})();`;
  var loaderSettings = getLoaderSettings(opts);
  var systemJsSettings = loaderSettings.config;
  var uuConfig = opts.uuConfig;
  var browserUuEnvironment = uuConfig.environment;
  var hasDirectUuEnvironment = typeof browserUuEnvironment != "string";
  var loader = loaderTpl.replace(/__CONFIG__/g, function () { return JSON.stringify(systemJsSettings||{}); });
  loader = loader.replace(/__HAS_UU_ENVIRONMENT__/g, function () { return hasDirectUuEnvironment+""; });
  loader = loader.replace(/__UU_ENVIRONMENT__/g, function () { return JSON.stringify((hasDirectUuEnvironment ? browserUuEnvironment : null) || {}); });
  loader = loader.replace(/__UU_ENVIRONMENT_URI__/g, function () { return JSON.stringify(hasDirectUuEnvironment ? null : browserUuEnvironment); });
  fs.mkdirsSync(".tmp");
  fs.writeFileSync(path.resolve(".tmp", "__loader.js"), loader, "utf-8");

  var uuEnvProdFile = (typeof uuConfig.environment == "string" ? uuConfig.environment.replace(/^.*\//, "") : null);
  if (uuEnvProdFile) uuEnvProdFile = (fs.existsSync(path.join(src, uuEnvProdFile)) ? path.resolve(src, uuEnvProdFile) : null);

  var cfg = [{
    context: path.resolve("."),
    entry: path.resolve(".tmp/__loader.js"),
    output: {
      filename: "vendors.js",
      path: path.resolve(opts.outputPath)
    },
    plugins: [
      new CopyWebpackPlugin([{ context: "node_modules/systemjs/dist", from: "system" + (opts.minify ? "" : ".src") + ".js", to: "system.js" }]),
      new CopyWebpackPlugin([{ context: "node_modules/systemjs/dist", from: "system-polyfills*" }]),
      new CopyWebpackPlugin([{ context: "node_modules/systemjs/dist", from: "system-csp*" }]),
      // write files to output folder even when using webpack-dev-server (because by default it builds & serves them from memory)
      new WriteFilePlugin({ log: false })
    ].concat(uuEnvProdFile ? [new CopyWebpackPlugin([{ context: path.dirname(uuEnvProdFile), from: uuEnvProdFile }])] : [])
    .concat(opts.minify? [
      new webpack.optimize.UglifyJsPlugin(),
      new webpack.optimize.OccurrenceOrderPlugin()
    ] : []),
    devServer: {
      outputPath: path.resolve(opts.outputPath)
    }
  }];
  cfg[0].uuUsedOptions = opts;
  cfg[0].uuMainConfig = mainWebpackConfig;
  return cfg;
}

function getLoaderSettings(opts) {
  var settings = {
    map: {},
    paths: {},
    meta: {}
  };
  var depsConfig = Object.assign({}, opts.uuConfig.externalDependencies);

  // create mappings, such as "react" => "react@15.0.3" => "%%CDN%%/react/15.0.3/react.min.js"
  var deps = [];
  if (fs.existsSync(".tmp/computedDeepExternalDependencies.json")) deps = JSON.parse(fs.readFileSync(".tmp/computedDeepExternalDependencies.json", "utf-8")) || [];
  deps.forEach(dep => {
    var canonicDepName = dep.name + "@" + dep.version;
    settings.map[dep.name] = canonicDepName;

    var depConfig = depsConfig[dep.name] || {};
    delete depsConfig[dep.name];
    var usedDepUrl = depConfig.externalUri;
    if (!usedDepUrl || usedDepUrl === true) {
      // figure out path from which to load the dependency on our own as it's not given in dependencies.config.js
      var depPackageJson = dep.packageJson;
      var depMain = ((typeof depPackageJson.browser == "string" ? depPackageJson.browser : null) ||
        ((depPackageJson.main || "").replace(/[^\/]*$/, "") + dep.name)); // NOTE Cannot use "depPackageJson.main" directly, only take folder from it (because e.g. react-dom has it set to "dist/index.js" which is wrong for CDN). 
      depMain = depMain.replace(/^dist[^\/]*\//, ""); // guess that CDN will contain only contents of dist*/ folder (i.e. strip "dist/" and such)
      if (depMain.indexOf(".") == -1) depMain += ".js";
      if (opts.minify) depMain = depMain.replace(/(\.js|\.css)$/, ".min$1");
      var depUrlPath = canonicDepName.replace(/@/g, "/") + "/" + depMain;

      usedDepUrl = "%%CDN%%/" + depUrlPath;
    } else if (typeof usedDepUrl == "string") {
      if (!usedDepUrl.match(/^[a-zA-Z0-9\-_]+:/) && !usedDepUrl.match(/^\//)) usedDepUrl = "%%APP_PUBLIC_BASE%%/" + usedDepUrl;
    }
    settings.paths[canonicDepName] = usedDepUrl + "";
  });

  // add also settings for all remaining configuration in depConfig
  for (var key in depsConfig) {
    var usedDepUrl = depsConfig[key].externalUri;
    if (typeof usedDepUrl != "string") continue;
    if (!usedDepUrl.match(/^[a-zA-Z0-9\-_]+:/) && !usedDepUrl.match(/^\//)) usedDepUrl = "%%APP_PUBLIC_BASE%%/" + usedDepUrl;
    settings.paths[key] = usedDepUrl;
  }

  return { config: settings };
}

// get list of HTML files in specified directory
function getHtmlFileList(dir) {
  var htmlPaths = [];
  walk.walkSync(dir, {
    listeners: {
      file: function (dir, fileStats/*, next*/) {
        if (fileStats.name.match(/\.html$/)) htmlPaths.push(path.join(dir, fileStats.name));
      }
    }
  });
  return htmlPaths;
}

/**
 * Returns packages in project. Package is a folder directly in src/, which contain
 * an entry point of the same name as the package. E.g. package "abc" must be in
 * src/abc/abc.js.
 * 
 * @return List of project packakges.
 */
function getProjectPackageList() {
  var packages = fs.readdirSync(src);
  packages = packages.map(dir => dir.replace(/\\/g, "/")).map(dir => {
    var name = dir; // currently only 1 level is supported (folders directly in src/)
    return {
      directory: dir, // relative path (without "./" prefix)
      name: name,
      moduleName: "project/" + name,
      entryPoint: src + "/" + dir + "/" + name + ".js"
    };
  });
  var existingPackages = packages.filter(pack => fs.existsSync(pack.entryPoint));
  return existingPackages;
}

module.exports = {
  getWebpackConfig,
  getHtmlFileList,
  getProjectPackageList
};