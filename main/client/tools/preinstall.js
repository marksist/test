/**
 * Rename project directory based on package.json name property and set git remote directory
 */

var exec = require('child_process').exec;
var fs = require('fs');
var path = require('path');

var pkg = require('../package.json');
var gitBase = 'ssh://git@codebase.plus4u.net:9422/';
var templateRepository = gitBase + 'uuapp.workspace-UU5-Ruby.git';
var projectRepository = gitBase + pkg.name + '.git';

//check if package.json is set up
if (pkg.name == 'uu_project_template_uc') {
  console.log('ERR: Change project name in package.json and run npm install again.');
  process.exit(1);
}

//rename root directory
var targetFolderName = pkg.name;
var crd = path.resolve(__dirname,'..','..','..');
var nrd = path.resolve(crd, '..', targetFolderName);
//check if name is a valid filename
if (!/^[a-zA-Z_][a-zA-Z0-9.\-_]*$/.test(targetFolderName)) {
  console.log('ERR: ' + targetFolderName + ' is not a valid filename. Allowed chars: a-z A-Z 0-9 . - _');
  process.exit(1);
}
//check if target project exists
if (crd != nrd) {
  try {
    fs.renameSync(crd, nrd);
  } catch (err) {
    console.log('ERR: Unable to rename project to ' + targetFolderName + '. Please rename ' + crd + ' to ' + targetFolderName + ' manually.\nError detail:', err);
    //process.exit(1);
  }
} else {
  console.log("WARN: Root directory already renamed.");
}

exec("git remote show origin", (error, stdout) => {
  var regex = /^\s*Fetch URL:\s*(.*)$/m;
  var origin = error ? false : regex.exec(stdout)[1];
  var remoteExist = false;
  if (origin){
    if(templateRepository == origin){
      exec(
        'git ls-remote -h "' + projectRepository + '"', (error, stdout) =>{
          console.log(stdout);
          remoteExist = error ? false : true;

          setGit(projectRepository, origin, remoteExist);
        }
      );

    }else{
      console.log('WARN: Current remote repo ' + origin + ' is not a template repository, origin not changed.')
    }
  } else {
    setGit(projectRepository, origin, remoteExist);
  }
});

function setGit(projectRepository, origin, remoteExist){
  if(remoteExist){
    if(origin){
      exec(
        'git remote set-url origin ' + projectRepository, (error) =>{
          if(error) console.log(error)
          else console.log('Git remote repo set to ' + projectRepository);
          //todo initial push?
        }
      );
    }else{
      exec(
        'git remote add origin "' + projectRepository + '"', (error) =>{
          if(error) console.log(error)
          else console.log('Git remote repo set to ' + projectRepository);
          //todo initial push?
        }
      );
    }
  } else {
    if (origin){
      exec('git remote remove origin', (error) =>{
        if(error) console.log(error)
        else console.log('Git set locally, remote repo does not exist.');
      });
    }
  }
}
