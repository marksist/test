module UclGoodymatMain
  module Dao
    class CustomerMongoDB < UuAppObjectStore::MongoDB::UuObjectDao

      def create_schema
        create_index({ awid: 1, _id: 1 }, { unique: true })
        create_index({ awid: 1, code: 1 })
      end

      def create(customer)
        insert_one(customer)
      end

      def get(awid, id)
        find_one({ awid: awid, _id: id })
      end

      def get_by_code(awid, code)
        find_one({ awid: awid, code: code })
      end

      def update(customer)
        find_one_and_update({ id: customer[:id], awid: customer[:awid] }, customer, 'NONE')
      end

      def delete(customer)
        delete_one({ id: customer[:id], awid: customer[:awid] })
      end

    end
  end
end

