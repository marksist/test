module UclGoodymatMain
  module Dao
    class GoodymatMongoDB < UuAppObjectStore::MongoDB::UuObjectDao

      def create_schema
        create_index({ awid: 1, _id: 1 }, { unique: true })
        create_index({ awid: 1, code: 1 })
      end

      def create(goodymat)
        insert_one(goodymat)
      end

      def get(awid, id)
        find_one({ awid: awid, _id: id })
      end

      def get_by_code(awid, code)
        find_one({ awid: awid, code: code })
      end

      def update(goodymat)
        find_one_and_update({ id: goodymat[:id], awid: goodymat[:awid] }, goodymat, 'NONE')
      end

      def delete(goodymat)
        delete_one(id: goodymat[:id], awid: goodymat[:awid])
      end

    end
  end
end