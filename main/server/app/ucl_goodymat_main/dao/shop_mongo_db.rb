module UclGoodymatMain
  module Dao
    class ShopMongoDB < UuAppObjectStore::MongoDB::UuObjectDao

      def create_schema
        create_index({ :id => 1, :awid => 1 }, { unique: true })
      end

      def create(shop)
        insert_one(shop)
      end

      def get(awid, id)
        find_one({ _id: id, awid: awid })
      end

      def get_by_awid(awid)
        find_one({ awid: awid })
      end

      def update_by_awid(awid, shop)
        find_one_and_update({ awid: awid }, shop, 'NONE')
      end

      def delete(shop)
        delete_one({ id: shop[:id], awid: shop[:awid] })
      end

    end
  end
end