module UclGoodymatMain
  module Dao
    class OrderMongoDB < UuAppObjectStore::MongoDB::UuObjectDao

      def create_schema
        create_index({ awid: 1, _id: 1 }, { unique: true })
        create_index({ awid: 1, state: 1 })
        create_index({ awid: 1, customer: 1 })
        create_index({ awid: 1, product: 1 })
        create_index({ awid: 1, goodymat: 1 })
      end

      def create(order_entity)
        insert_one(order_entity)
      end

      def get(awid, id)
        find_one({ awid: awid, _id: id })
      end

      def update(order_entity)
        find_one_and_update({ id: order_entity[:id], awid: order_entity[:awid] }, order_entity, 'NONE')
      end

      def delete(order_entity)
        delete_one({ id: order_entity[:id], awid: order_entity[:awid] })
      end

      def delete_by_awid(awid)
        delete_many({ awid: awid })
      end

      def list(awid, page_info = {})
        find({ awid: awid }, page_info, { 'sys.cts': -1 })
      end

      def list_by_state(awid, state, page_info = {})
        find({ awid: awid, state: state }, page_info, { 'sys.cts': 1 })
      end

      def list_by_customer(awid, customer, page_info = {})
        find({ awid: awid, customer: customer }, page_info, { 'sys.cts': -1 })
      end

      def list_by_product(awid, product, page_info = {})
        find({ awid: awid, product: product }, page_info, { 'sys.cts': -1 })
      end

      def list_by_goodymat(awid, goodymat, page_info = {})
        find({ awid: awid, goodymat: goodymat }, page_info, { 'sys.cts': -1 })
      end

    end
  end
end

