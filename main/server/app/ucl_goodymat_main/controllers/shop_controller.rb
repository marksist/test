require_relative '../models/shop'

module UclGoodymatMain
  module Controllers
    class ShopController

      def initialize_shop(uc_env)
        uc_env.result = UclGoodymatMain::Models::Shop.instance.initialize_shop(uc_env.uri.awid, uc_env.uri.tid, uc_env.uri.gateway, uc_env.parameters, uc_env.session)
      end

      def set_shop_state(uc_env)
        uc_env.result = UclGoodymatMain::Models::Shop.instance.set_shop_state(uc_env.uri.awid, uc_env.parameters)
      end

      def get_shop_info(uc_env)
        uc_env.result = UclGoodymatMain::Models::Shop.instance.get_shop_info(uc_env.uri.awid)
      end

      def get_shop_window(uc_env)
        uc_env.result = UclGoodymatMain::Models::Shop.instance.get_shop_window(uc_env.uri.awid)
      end

    end
  end
end