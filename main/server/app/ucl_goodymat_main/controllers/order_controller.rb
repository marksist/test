require_relative '../models/order'

module UclGoodymatMain
  module Controllers
    class OrderController

      def list_active_orders(uc_env)
        uc_env.result = UclGoodymatMain::Models::Order.instance.list_active_orders(uc_env.uri.awid, uc_env.parameters)
      end

      def proceed_order(uc_env)
        uc_env.result = UclGoodymatMain::Models::Order.instance.proceed(uc_env.uri.awid, uc_env.parameters)
      end

      def delete_all_orders(uc_env)
        uc_env.result = UclGoodymatMain::Models::Order.instance.delete_all_orders(uc_env.uri.awid)
      end

    end
  end
end