require_relative '../models/goodymat'

module UclGoodymatMain
  module Controllers
    class GoodymatController

      def buy_via_iot(uc_env)
        uc_env.result = UclGoodymatMain::Models::Goodymat.instance.buy_via_iot(uc_env.uri.awid, uc_env.session.identity[:universeId], uc_env.parameters)
      end

    end
  end
end