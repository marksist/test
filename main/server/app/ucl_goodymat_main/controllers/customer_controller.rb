require_relative '../models/customer'

module UclGoodymatMain
  module Controllers

    class CustomerController

      def buy(uc_env)
        uc_env.result = UclGoodymatMain::Models::Customer.instance.buy(uc_env.uri.awid, uc_env.session.identity[:universeId], uc_env.parameters)
      end

    end
  end
end
