const buyDtoInType = shape({
  product: string(/[a-z,A-Z,0-9,_]{1,64}/).isRequired()
})

const setShopStateDtoInType = shape({
  state: oneOf(["open","closed"])
})

const initDtoInType = shape({
  authoritiesUri: uri().isRequired()
});