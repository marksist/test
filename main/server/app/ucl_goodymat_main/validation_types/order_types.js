const listActiveOrdersDtoInType = shape({
  pageInfo: shape({
    pageIndex: integer(),
    pageSize: integer(1000)
  })
})

const proceedOrderDtoInType = shape({
  id: mongoId()
})