const buyViaIotDtoInType = shape({
  operation: string(/[a-z,A-Z,0-9,_]{1,64}/).isRequired()
})