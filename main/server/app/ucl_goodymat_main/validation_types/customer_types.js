const buyDtoInType = shape({
  product: string(/[a-z,A-Z,0-9,_]{1,64}/).isRequired()
})