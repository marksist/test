require 'singleton'
require 'uu_app_objectstore-mongo/mongo_db/store_factory'
require 'uu_app/validation/validator'
require 'uu_app/validation/validation_error_helper'
require 'uu_app_workspace/helper/warning_helper'
require_relative '../models/shop'
require_relative '../models/order'
require_relative '../models/customer'

module UclGoodymatMain
  module Models
    class Goodymat
      include Singleton

      SCHEMA = 'goodymat'
      VALIDATION_TYPES_RELATIVE_PATH = '../validation_types/goodymat_types.js'
      WARNINGS = {
        buyViaIoTInvalidDtoIn: {
          code: 'ucl-goodymat-main/buyViaIot/invalidDtoIn',
          message: 'DtoIn is not valid.'
        }
      }

      def initialize
        @goodymat_dao = UuAppObjectStore::MongoDB::StoreFactory.get_dao(SCHEMA)
        validation_type_path = File.expand_path(VALIDATION_TYPES_RELATIVE_PATH, File.dirname(__FILE__))
        UuApp::Validation::Validator.load(validation_type_path)
      end

      ##
      # Create DB schema and indexes.
      #
      def create_schema
        @goodymat_dao.create_schema
      end

      ##
      # Creates new order of given product by goodymat.
      #
      def buy_via_iot(awid, uu_thing, dto_in, uu_app_error_map = {})

        # hds 1
        shop = UclGoodymatMain::Models::Shop.instance.get_shop(awid)
        raise UclGoodymatMain::Errors::BuyViaIoT::ShopDaoGetByAwidFailedError.new(uu_app_error_map, { awid: awid }) unless shop # A1

        # shop is closed
        unless shop[:state].eql?(UclGoodymatMain::Models::Shop::OPEN_SHOP_STATE)
          raise UclGoodymatMain::Errors::BuyViaIoT::ShopIsClosedError.new(uu_app_error_map, {}) # A2
        end

        # hds 2
        # validation hds 2.1
        dto_in_validation_error_map = UuApp::Validation::Validator.buyViaIotDtoInType.validate(dto_in)

        unless dto_in_validation_error_map.empty? # A3
          uu_app_error_map = UuAppWorkspace::Helper::WarningHelper.get_warning(WARNINGS[:buyViaIoTInvalidDtoIn][:code],
            WARNINGS[:buyViaIoTInvalidDtoIn][:message], dto_in_validation_error_map)
        end

        # hds 2.2
        if dto_in_validation_error_map[:operation]
          raise UclGoodymatMain::Errors::BuyViaIoT::InvalidKeyError.new(uu_app_error_map, { operation: 'invalid' }) # A4
        end

        # hds 2.3 - delete invalid keys
        UuApp::Validation::ValidationErrorHelper.delete_invalid_hash_keys(dto_in, dto_in_validation_error_map)

        # hds 2.4 - set default values
        dto_in[:state] = UclGoodymatMain::Models::Order::ORDERED_STATE
        dto_in[:createOrderTs] = Time.now
        dto_in[:goodymat] = uu_thing

        # hds 3 - get goodymat by uu_id of logged uu thing
        goodymat = get_by_code(awid, uu_thing)
        raise UclGoodymatMain::Errors::BuyViaIoT::GoodymatDaoGetByCodeFailedError.new(uu_app_error_map, { code: uu_thing }) unless goodymat # A5

        # hds 4
        operation = goodymat[:operationMap][dto_in[:operation]]
        raise UclGoodymatMain::Errors::BuyViaIoT::OperationIsNotValidError.new(uu_app_error_map, {}) unless operation # A6

        # hds 5
        uu_object = dto_in.clone
        uu_object[:awid] = awid
        uu_object[:product] = operation[:product]
        uu_object[:customer] = operation[:customer]

        # hds 6 - product is not specified in the shop
        unless shop[:productMap].has_key?(uu_object[:product])
          raise UclGoodymatMain::Errors::BuyViaIoT::ProductDoesNotExistError.new(uu_app_error_map, { product: uu_object[:product] }) # A7
        end

        # hds 7 - get customer and verify whether is active state
        customer = UclGoodymatMain::Models::Customer.instance.get_by_code(awid, uu_object[:customer])
        raise UclGoodymatMain::Errors::BuyViaIoT::CustomerDaoGetByCodeFailedError.new(uu_app_error_map, { code: uu_object[:customer] }) unless customer # A8

        unless customer[:state].eql?(UclGoodymatMain::Models::Customer::CUSTOMER_ACTIVE_STATE)
          raise UclGoodymatMain::Errors::BuyViaIoT::CustomerIsClosedError.new(uu_app_error_map, {}) # A9
        end

        #hds 8
        begin
          dto_out = UclGoodymatMain::Models::Order.instance.create_order(uu_object)
        rescue UuApp::Persistence::Error::PersistenceError => e # A10
          raise UclGoodymatMain::Errors::BuyViaIoT::OrderDaoCreateFailedError.new(uu_app_error_map, { cause: e })
        end

        # hds 9
        dto_out[:uuAppErrorMap] = uu_app_error_map
        dto_out
      end


      # === CRUD METHODS ====================================================================

      ##
      # Create new goodymat from given parameters.
      # Note.: Its simple creation without any business rules.
      #
      def create_goodymat(goodymat)
        @goodymat_dao.create(goodymat)
      end

      ##
      # Update existing goodymat.
      # Note.: Its simple creation without any business rules.
      #
      def update_goodymat(goodymat)
        @goodymat_dao.update(goodymat)
      end

      ##
      # Delete given goodymat.
      # Note.: Its simple creation without any business rules.
      #
      def delete_goodymat(goodymat)
        @goodymat_dao.delete(goodymat)
      end

      # === GET METHODS ====================================================================

      ##
      # Returns goodymat by code, in if is not found in the storage.
      #
      def get_by_code(awid, code)
        @goodymat_dao.get_by_code(awid, code)
      end

    end
  end
end