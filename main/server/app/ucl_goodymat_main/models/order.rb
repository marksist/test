require 'singleton'
require 'uu_app_objectstore-mongo/mongo_db/store_factory'
require 'uu_app/validation/validator'
require 'uu_app/validation/validation_error_helper'
require 'uu_app_workspace/helper/warning_helper'
require_relative '../models/shop'
require_relative '../models/customer'
require_relative '../models/goodymat'

module UclGoodymatMain
  module Models
    class Order
      include Singleton


      @@logger = UuApp::Logging::LoggerFactory.get(self.class.name)
      SCHEMA = 'order'
      VALIDATION_TYPES_RELATIVE_PATH = '../validation_types/order_types.js'
      ORDERED_STATE = 'ordered'
      FINISHED_STATE = 'finished'
      WARNINGS = {
        listActiveOrdersInvalidDtoIn: {
          code: 'ucl-goodymat-main/listActiveOrders/invalidDtoIn',
          message: 'DtoIn is not valid.'
        },
        proceedOrderInvalidDtoIn: {
          code: 'ucl-goodymat-main/proceedOrder/invalidDtoIn',
          message: 'DtoIn is not valid.'
        }
      }

      def initialize
        @order_dao = UuAppObjectStore::MongoDB::StoreFactory.get_dao(SCHEMA)
        validation_type_path = File.expand_path(VALIDATION_TYPES_RELATIVE_PATH, File.dirname(__FILE__))
        UuApp::Validation::Validator.load(validation_type_path)
      end

      ##
      # Create DB schema and indexes.
      #
      def create_schema
        @order_dao.create_schema
      end

      ##
      # Returns paged list of active orders.
      #
      def list_active_orders(awid, dto_in, uu_app_error_map = {})

        # hds 1
        # validation hds 1.1
        dto_in_validation_error_map = UuApp::Validation::Validator.listActiveOrdersDtoInType.validate(dto_in)

        unless dto_in_validation_error_map.empty? # A1
          uu_app_error_map = UuAppWorkspace::Helper::WarningHelper.get_warning(WARNINGS[:listActiveOrdersInvalidDtoIn][:code],
            WARNINGS[:listActiveOrdersInvalidDtoIn][:message], dto_in_validation_error_map)
        end

        # hds 1.2 - delete invalid keys
        UuApp::Validation::ValidationErrorHelper.delete_invalid_hash_keys(dto_in, dto_in_validation_error_map)

        # hds 2
        begin
          orderList = list_by_state(awid, ORDERED_STATE, dto_in[:pageInfo])
        rescue UuApp::Persistence::Error::PersistenceError => e # A2
          raise UuAppWorkspace::Errors::ListActiveOrder::OrderDaoListByStateFailedError.new(uu_app_error_map, { cause: e })
        end

        customers = Hash.new
        products = Hash.new
        goodymats = Hash.new
        items = Array.new

        # hds 3
        shop = UclGoodymatMain::Models::Shop.instance.get_shop(awid)
        raise UuAppWorkspace::Errors::ListActiveOrder::ShopDaoGetByAwidFailedError.new(uu_app_error_map, { cause: e }) unless shop

        # hds 4
        orderList[:itemList].each do |item|
          order = Hash.new
          customer_code = item[:customer]
          product_code = item[:product]
          goodymat_code = item[:goodymat]

          # hds 4.1
          if customers.has_key?(customer_code)
            customer = customers[customer_code]
          else
            customer = UclGoodymatMain::Models::Customer.instance.get_by_code(awid, customer_code)

            unless customer
              e = UuAppWorkspace::Errors::ListActiveOrder::CustomerDaoGetByCodeFailedError.new(uu_app_error_map, { code: customer_code })
              @@logger.warn(UuAppWorkspace::Errors::ListActiveOrder::CustomerDaoGetByCodeFailedError::CODE, e) # A3
              next
            end
            customers[customer_code] = customer
          end

          # hds 4.2
          if products.has_key?(product_code)
            product = products[product_code]
          else
            product = shop[:productMap][product_code]
            unless product
              e = UuAppWorkspace::Errors::ListActiveOrder::ProductDoesNotExistError.new(uu_app_error_map, { product: product_code })
              @@logger.warn(UuAppWorkspace::Errors::ListActiveOrder::ProductDoesNotExistError::CODE, e) # A5
              next
            end
            products[product_code] = product
          end

          # hds 3.3
          if goodymat_code
            if goodymats.has_key?(goodymat_code)
              goodymat = goodymats[goodymat_code]
            else
              goodymat = UclGoodymatMain::Models::Goodymat.instance.get_by_code(awid, goodymat_code)
              unless goodymat
                e = UuAppWorkspace::Errors::ListActiveOrder::GoodymatDaoGetByCodeFailedError.new(uu_app_error_map, { code: goodymat_code })
                @@logger.warn(UuAppWorkspace::Errors::ListActiveOrder::GoodymatDaoGetByCodeFailedError::CODE, e) # A6
                next
              end
              goodymats[goodymat_code] = goodymat
            end
          end

          # hds 3.4
          order[:id] = item[:id]
          order[:createOrderTs] = item[:createOrderTs]
          order[:customerCode] = customer[:code]
          order[:customerName] = customer[:name]
          order[:customerType] = customer[:type]
          order[:customerImage] = customer[:image]
          # add info about product
          order[:productCode] = product_code
          order[:productNameLSI] = product[:nameLSI]
          order[:productImage] = product[:image]
          # add info about goodymat
          order[:goodymatCode] = goodymat_code
          order[:goodymatNameLSI] = goodymat ? goodymat[:nameLSI] : nil
          order[:goodymatImage] = goodymat ? goodymat[:image] : nil

          items << order
        end

        dto_out = Hash.new
        dto_out[:itemList] = items
        dto_out[:pageInfo] = orderList[:pageInfo]
        dto_out[:uuAppErrorMap] = uu_app_error_map
        dto_out
      end

      ##
      # Proceed active order.
      #
      def proceed(awid, dto_in, uu_app_error_map = {})

        # hds 1
        # validation hds 1.1
        dto_in_validation_error_map = UuApp::Validation::Validator.proceedOrderDtoInType.validate(dto_in)

        unless dto_in_validation_error_map.empty? # A1
          uu_app_error_map = UuAppWorkspace::Helper::WarningHelper.get_warning(WARNINGS[:proceedOrderInvalidDtoIn][:code],
            WARNINGS[:proceedOrderInvalidDtoIn][:message], dto_in_validation_error_map)
        end

        # hds 1.2
        if dto_in_validation_error_map[:id]
          raise UclGoodymatMain::Errors::ProceedOrder::InvalidKeyError.new(uu_app_error_map, { id: 'invalid' }) # A2
        end

        # hds 2
        order = get(awid, dto_in[:id])
        raise UclGoodymatMain::Errors::ProceedOrder::OrderDaoGetFailedError.new(uu_app_error_map, { id: dto_in[:id] }) unless order # A3

        # hds 3
        unless order[:state].eql?(ORDERED_STATE)
          raise UclGoodymatMain::Errors::ProceedOrder::OrderIsNotInProperStateError.new(uu_app_error_map, { state: order[:state] }) # A4
        end

        order[:state] = FINISHED_STATE
        order[:finishOrderTs] = Time.now
        begin
          @order_dao.update(order)
        rescue UuApp::Persistence::Error::PersistenceError => e # A5
          raise UclGoodymatMain::Errors::ProceedOrder::OrderDaoUpdateFailedError.new(uu_app_error_map, { cause: e })
        end

        dto_out = order.clone
        dto_out[:uuAppErrorMap] = uu_app_error_map
        dto_out
      end

      ##
      # Deletes all existing orders.
      #
      def delete_all_orders(awid, uu_app_error_map = {})

        # hds 1
        begin
          @order_dao.delete_by_awid(awid)
        rescue UuApp::Persistence::Error::PersistenceError => e # A1
          raise UclGoodymatMain::Errors::DeleteAllOrders::OrderDaoDeleteByAwidFailedError.new(uu_app_error_map, { cause: e })
        end

        # hds 2
        dto_out = Hash.new
        dto_out[:uuAppErrorMap] = uu_app_error_map
        dto_out
      end


      # === CRUD METHODS ====================================================================

      ##
      # Creates new order.
      #
      def create_order(order)
        @order_dao.create(order)
      end

      ##
      # Update existing order.
      # Note.: Its simple creation without any business rules.
      #
      def update_order(order)
        @order_dao.update(order)
      end

      ##
      # Delete given order.
      # Note.: Its simple creation without any business rules.
      #
      def delete_order(order)
        @order_dao.delete(order)
      end

      # === GET METHODS ====================================================================

      ##
      # Get order entity by id.
      #
      def get(awid, id)
        @order_dao.get(awid, id)
      end

      ##
      # Get paged list of orders by state.
      #
      def list_by_state(awid, state, opt)
        @order_dao.list_by_state(awid, state, opt)
      end

      ##
      # Get paged list of orders by customer.
      #
      def list_by_customer(awid, customer, opt)
        @order_dao.list_by_customer(awid, customer, opt)
      end

      ##
      # Get paged list of orders by product.
      #
      def list_by_product(awid, product, opt)
        @order_dao.list_by_product(awid, product, opt)
      end

      ##
      # Get paged list of orders by goodymat.
      #
      def list_by_goodymat(awid, goodymat, opt)
        @order_dao.list_by_goodymat(awid, goodymat, opt)
      end

    end
  end
end