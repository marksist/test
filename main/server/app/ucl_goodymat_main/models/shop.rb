require 'uu_app/client'
require 'singleton'
require 'uu_app_objectstore-mongo/mongo_db/store_factory'
require 'uu_app/validation/validator'
require 'uu_app/validation/validation_error_helper'
require 'uu_app_workspace/helper/warning_helper'

module UclGoodymatMain
  module Models
    class Shop
      include Singleton

      SCHEMA = 'shop'
      VALIDATION_TYPES_RELATIVE_PATH = '../validation_types/shop_types.js'
      OPEN_SHOP_STATE = 'open'
      CLOSED_SHOP_STATE = 'closed'
      CUSTOMERS_PROFILE_CODE = 'Customers'
      GOODYMATS_PROFILE_CODE = 'Goodymats'
      AUTHORITIES_PROFILE_CODE = 'Authorities'
      WARNINGS = {
        setShopStateInvalidDtoIn: {
          code: 'ucl-goodymat-main/setShopState/invalidDtoIn',
          message: 'DtoIn is not valid.'
        },
        initInvalidDtoIn: {
          code: 'ucl-goodymat-main/init/invalidDtoIn',
          message: 'DtoIn is not valid.'
        }
      }

      def initialize
        @shop_dao = UuAppObjectStore::MongoDB::StoreFactory.get_dao(SCHEMA)
        validation_type_path = File.expand_path(VALIDATION_TYPES_RELATIVE_PATH, File.dirname(__FILE__))
        UuApp::Validation::Validator.load(validation_type_path)
      end

      ##
      # Get information about shop.
      #
      def get_shop_info(awid, uu_app_error_map = {})

        # hds 1
        shop = @shop_dao.get_by_awid(awid)
        raise UclGoodymatMain::Errors::GetShopInfo::ShopDaoGetByAwidFailedError.new(uu_app_error_map, { awid: awid }) unless shop # A1

        # hds 2, 3
        dto_out = Hash.new
        dto_out[:state] = shop[:state]
        dto_out[:nameLSI] = shop[:nameLSI]
        dto_out[:uuAppErrorMap] = uu_app_error_map
        dto_out
      end

      ##
      # Sets shop in the given state.
      #
      def set_shop_state(awid, dto_in, uu_app_error_map = {})

        # hds 1
        # validation hds 1.1
        dto_in_validation_error_map = UuApp::Validation::Validator.setShopStateDtoInType.validate(dto_in)

        unless dto_in_validation_error_map.empty? # A1
          uu_app_error_map = UuAppWorkspace::Helper::WarningHelper.get_warning(WARNINGS[:setShopStateInvalidDtoIn][:code],
            WARNINGS[:setShopStateInvalidDtoIn][:message], dto_in_validation_error_map)
        end

        # hds 1.2 - delete invalid keys
        UuApp::Validation::ValidationErrorHelper.delete_invalid_hash_keys(dto_in, dto_in_validation_error_map)

        # hds 1.3 - set default values
        dto_in[:state] = CLOSED_SHOP_STATE unless dto_in[:state]

        begin
          uu_object = dto_in.clone
          dto_out = @shop_dao.update_by_awid(awid, uu_object)
        rescue UuApp::Persistence::Error::NotUnique => e
          raise UclGoodymatMain::Errors::SetShopState::ShopDaoUpdateByAwidFailedError.new(uu_app_error_map, { cause: e }) # A2
        end

        dto_out[:uuAppErrorMap] = uu_app_error_map
        dto_out
      end

      ##
      # Gets information about shop based on its state.
      #
      def get_shop_window(awid, uu_app_error_map = {})

        # hds 1
        shop = @shop_dao.get_by_awid(awid)
        raise UclGoodymatMain::Errors::GetShopWindow::ShopDaoGetByAwidFailedError.new(uu_app_error_map, { awid: awid }) unless shop # A1


        if shop[:state].eql?(OPEN_SHOP_STATE)
          dto_out = shop.clone
          dto_out.delete('closeWelcomeLSI')
        else
          dto_out = {}
          dto_out[:state]= shop[:state]
          dto_out[:nameLSI]= shop[:nameLSI]
          dto_out[:closeWelcomeLSI]= shop[:closeWelcomeLSI]
        end

        dto_out[:uuAppErrorMap] = uu_app_error_map
        dto_out
      end

      ##
      # Initialize shop, creates schema, permissions, init and fill database by init. data.
      #
      def initialize_shop(awid, tid, gateway, dto_in, session, uu_app_error_map = {})

        # hds 1
        # validation hds 1.1
        dto_in_validation_error_map = UuApp::Validation::Validator.initDtoInType.validate(dto_in)

        unless dto_in_validation_error_map.empty? # A1
          uu_app_error_map = UuAppWorkspace::Helper::WarningHelper.get_warning(WARNINGS[:initInvalidDtoIn][:code],
            WARNINGS[:initInvalidDtoIn][:message], dto_in_validation_error_map)
        end

        # hds 1.2
        if dto_in_validation_error_map[:authoritiesUri]
          raise UclGoodymatMain::Errors::Init::InvalidKeyError.new(uu_app_error_map, { authoritiesUri: 'invalid' }) # A2
        end

        # hds 2 - create schemas
        begin
          @shop_dao.create_schema
          UclGoodymatMain::Models::Customer.instance.create_schema
          UclGoodymatMain::Models::Goodymat.instance.create_schema
          UclGoodymatMain::Models::Order.instance.create_schema
        rescue UuApp::Persistence::Error::PersistenceError => e # A3
          raise UclGoodymatMain::Errors::Init::UuObjectDaoCreateSchemaFailedError.new(uu_app_error_map, { cause: e })
        end

        # hds 3
        bds_path = File.expand_path('../bds', File.dirname(__FILE__))
        shop_bds_path = File.join("#{bds_path}", '/shop.json')

        # hds 3.1
        shop_bds = JSON.parse(File.read(shop_bds_path, :external_encoding => 'utf-8'), :symbolize_names => true)
        shop_bds[:state] = CLOSED_SHOP_STATE
        shop_bds[:awid] = awid
        begin
          shop = @shop_dao.create(shop_bds)
        rescue UuApp::Persistence::Error::PersistenceError => e # A4
          raise UclGoodymatMain::Errors::Init::ShopDaoCreateFailedError.new(uu_app_error_map, { cause: e })
        end

        # hds 3.2
        token = session.get_call_token('https://api.plus4u.net/ues/wcp')
        shop[:productMap].each do |key, data|

          File.open("../client/src/#{data[:image][2..-1]}") do |f|
            begin
              request_body = { :parameters => { data: f, contentType: "image/jpeg", dataKey: key }, headers: { :content_type => 'multipart/form-data', :Authorization => "Bearer #{token}" }, :session => session }
              UuApp::Client.post("#{gateway}/#{UclGoodymatMain::PRODUCT}/#{tid}-#{awid}/uuBinary/createBinary", request_body)
            rescue StandardError => e # A5
              raise UclGoodymatMain::Errors::Init::CreateBinaryFailedError.new(uu_app_error_map, { cause: e })
            end
          end

        end

        # hds 3.3
        customers_bds_path = File.join("#{bds_path}", '/customers.json')
        customers_bds = JSON.parse(File.read(customers_bds_path, :external_encoding => 'utf-8'), :symbolize_names => true)
        customer_identities = Array.new
        customers_bds.each do |customer|
          begin
            customer_identities << customer[:code]
            customer[:awid] = awid
            UclGoodymatMain::Models::Customer.instance.create_customer(customer)
          rescue UuApp::Persistence::Error::PersistenceError => e # A6
            raise UclGoodymatMain::Errors::Init::CustomerDaoCreateFailedError.new(uu_app_error_map, { cause: e })
          end
        end

        # hds 3.4
        goodymats_bds_path = File.join("#{bds_path}", '/goodymats.json')
        goodymats_bds = JSON.parse(File.read(goodymats_bds_path, :external_encoding => 'utf-8'), :symbolize_names => true)
        goodymats_identities = Array.new
        goodymats_bds.each do |goodymat|
          begin
            goodymats_identities << goodymat[:code]
            goodymat[:awid] = awid
            UclGoodymatMain::Models::Goodymat.instance.create_goodymat(goodymat)
          rescue UuApp::Persistence::Error::PersistenceError => e # A7
            raise UclGoodymatMain::Errors::Init::GoodymatDaoCreateFailedError.new(uu_app_error_map, { cause: e })
          end
        end

        # hds 3.5 - create permissions for customers
        begin
          dto_in_create_permission = {
            profileCode: CUSTOMERS_PROFILE_CODE,
            uuIdentityList: customer_identities
          }
          UuAppWorkspace::Models::SysPermission.instance.create(awid, dto_in_create_permission)
        rescue UuApp::Persistence::Error::PersistenceError => e # A7
          raise UclGoodymatMain::Errors::Init::CreatePermissionFailedError.new(uu_app_error_map, { cause: e })
        end

        # hds 3.6 - create permissions for goodymats
        begin
          dto_in_create_permission = {
            profileCode: GOODYMATS_PROFILE_CODE,
            uuIdentityList: goodymats_identities
          }
          UuAppWorkspace::Models::SysPermission.instance.create(awid, dto_in_create_permission)
        rescue UuApp::Persistence::Error::PersistenceError => e # A8
          raise UclGoodymatMain::Errors::Init::CreatePermissionFailedError.new(uu_app_error_map, { cause: e })
        end

        # hds 4
        begin
          dto_in_set_profile = {
            code: AUTHORITIES_PROFILE_CODE,
            roleUri: dto_in[:authoritiesUri]
          }
          UuAppWorkspace::Models::SysProfile.instance.set_profile(awid, dto_in_set_profile)
        rescue UuApp::Persistence::Error::PersistenceError => e # A9
          raise UclGoodymatMain::Errors::Init::SetProfileFailedError.new(uu_app_error_map, { cause: e })
        end

        dto_out = Hash.new
        dto_out[:uuAppErrorMap] = uu_app_error_map
        dto_out
      end

      # === CRUD METHODS ====================================================================

      ##
      # Delete given shop.
      # Note.: Its simple creation without any business rules.
      #
      def delete_shop(shop)
        @shop_dao.delete(shop)
      end

      # === GET METHODS ====================================================================

      ##
      # Returns shop by awid. Nil is returned if shop is not found.
      #
      def get_shop(awid)
        @shop_dao.get_by_awid(awid)
      end

    end
  end
end