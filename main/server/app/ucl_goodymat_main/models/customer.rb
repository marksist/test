require 'singleton'
require 'uu_app_objectstore-mongo/mongo_db/store_factory'
require 'uu_app/validation/validator'
require 'uu_app/validation/validation_error_helper'
require 'uu_app_workspace/helper/warning_helper'
require_relative '../models/shop'
require_relative '../models/order'

module UclGoodymatMain
  module Models
    class Customer
      include Singleton

      SCHEMA = 'customer'
      VALIDATION_TYPES_RELATIVE_PATH = '../validation_types/customer_types.js'
      CUSTOMER_TYPE_PERSON = 'uuPerson'
      CUSTOMER_TYPE_UUEE = 'uuEE'
      CUSTOMER_ACTIVE_STATE = 'active'
      WARNINGS = {
        buyInvalidDtoIn: {
          code: 'ucl-goodymat-main/buy/invalidDtoIn',
          message: 'DtoIn is not valid.'
        }
      }

      def initialize
        @customer_dao = UuAppObjectStore::MongoDB::StoreFactory.get_dao(SCHEMA)
        validation_type_path = File.expand_path(VALIDATION_TYPES_RELATIVE_PATH, File.dirname(__FILE__))
        UuApp::Validation::Validator.load(validation_type_path)
      end

      ##
      # Create DB schema and indexes.
      #
      def create_schema
        @customer_dao.create_schema
      end

      ##
      # Creates new order of given product by customer.
      #
      def buy(awid, customer, dto_in, uu_app_error_map = {})

        # hds 1
        shop = UclGoodymatMain::Models::Shop.instance.get_shop(awid)
        raise UclGoodymatMain::Errors::Buy::ShopDaoGetByAwidFailedError.new(uu_app_error_map, { awid: awid }) unless shop # A1

        # shop is closed
        unless shop[:state].eql?(UclGoodymatMain::Models::Shop::OPEN_SHOP_STATE)
          raise UclGoodymatMain::Errors::Buy::ShopIsClosedError.new(uu_app_error_map, {}) # A2
        end

        # hds 2
        # validation hds 2.1
        dto_in_validation_error_map = UuApp::Validation::Validator.buyDtoInType.validate(dto_in)

        unless dto_in_validation_error_map.empty? # A3
          uu_app_error_map = UuAppWorkspace::Helper::WarningHelper.get_warning(WARNINGS[:buyInvalidDtoIn][:code],
            WARNINGS[:buyInvalidDtoIn][:message], dto_in_validation_error_map)
        end

        # hds 2.2
        if dto_in_validation_error_map[:product]
          raise UclGoodymatMain::Errors::Buy::InvalidKeyError.new(uu_app_error_map, { product: 'invalid' }) # A4
        end

        # hds 2.3 - delete invalid keys
        UuApp::Validation::ValidationErrorHelper.delete_invalid_hash_keys(dto_in, dto_in_validation_error_map)

        # hds 2.4 - set default values
        dto_in[:state] = UclGoodymatMain::Models::Order::ORDERED_STATE
        dto_in[:createOrderTs] = Time.now
        dto_in[:customer] = customer

        # hds 3 - product is not specified in the shop
        unless shop[:productMap].has_key?(dto_in[:product])
          raise UclGoodymatMain::Errors::Buy::ProductDoesNotExistError.new(uu_app_error_map, { product: dto_in[:product] }) # A5
        end

        # hds 4 - get customer and verify whether is in active state
        customer = get_by_code(awid, customer)
        raise UclGoodymatMain::Errors::Buy::CustomerDaoGetByCodeFailedError.new(uu_app_error_map, { uuId: customer }) unless customer # A6

        unless customer[:state].eql?(CUSTOMER_ACTIVE_STATE)
          raise UclGoodymatMain::Errors::Buy::CustomerIsClosedError.new(uu_app_error_map, {}) # A7
        end

        #hds 5 - create new order from checked parameters
        begin
          uu_object = dto_in.clone
          uu_object[:awid] = awid
          dto_out = UclGoodymatMain::Models::Order.instance.create_order(uu_object)
        rescue UuApp::Persistence::Error::PersistenceError => e # A8
          raise UclGoodymatMain::Errors::Buy::OrderDaoCreateFailed.new(uu_app_error_map, { cause: e })
        end

        # hds 6 - prepare output DTO and return it
        dto_out[:uuAppErrorMap] = uu_app_error_map
        dto_out
      end

      # === CRUD METHODS ====================================================================

      ##
      # Create new customer from given parameters.
      # Note.: Its simple creation without any business rules.
      #
      def create_customer(customer)
        @customer_dao.create(customer)
      end

      ##
      # Update existing customer.
      # Note.: Its simple creation without any business rules.
      #
      def update_customer(customer)
        @customer_dao.update(customer)
      end

      ##
      # Delete given customer.
      # Note.: Its simple creation without any business rules.
      #
      def delete_customer(customer)
        @customer_dao.delete(customer)
      end

      # === GET METHODS ====================================================================

      ##
      # Returns customer by code, in if is not found in the storage.
      #
      def get_by_code(awid, code)
        @customer_dao.get_by_code(awid, code)
      end

    end
  end
end