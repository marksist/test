module UclGoodymatMain
  module Helper
    class WarningHelper

      WARNING_TYPE = "warning"
      DTO_IN_KEY = "dtoIn"

      def self.get_warning(code, message, dto_in_validation_error_map)
        {
          code => {
            type: WARNING_TYPE,
            message: message,
            paramMap: {
              validationErrorMap: UuApp::Validation::ValidationErrorHelper.transform_error_map(dto_in_validation_error_map, DTO_IN_KEY)
            }
          }
        }
      end


    end
  end
end