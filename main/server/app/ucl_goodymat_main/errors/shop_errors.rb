require_relative 'goodymat_general_error'

module UclGoodymatMain
  module Errors

    module GetShopInfo

      UC_CODE = 'getShopInfo'

      class ShopDaoGetByAwidFailedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Get Shop by Shop Dao getByAwid failed.'
        CODE = "#{UC_CODE}/shopDaoGetByAwidFailed"
      end

    end

    module SetShopState

      UC_CODE = 'setShopState'

      class ShopDaoUpdateByAwidFailedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Update Shop by Shop Dao updateByAwid failed.'
        CODE = "#{UC_CODE}/shopDaoUpdateByAwidFailed"
      end

    end

    module GetShopWindow

      UC_CODE = 'getShopWindow'

      class ShopDaoGetByAwidFailedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Get Shop by Shop Dao getByAwid failed.'
        CODE = "#{UC_CODE}/shopDaoGetByAwidFailed"
      end

    end

    module Init

      UC_CODE = 'init'

      class InvalidKeyError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Key in dtoIn is not valid.'
        CODE = "#{UC_CODE}/invalidKey"
      end

      class UuObjectDaoCreateSchemaFailedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Creation of uuObject schema by uuObject Dao createSchema failed.'
        CODE = "#{UC_CODE}/uuObjectDaoCreateSchemaFailed"
      end

      class ShopDaoCreateFailedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Creation of Shop by Shop Dao create failed.'
        CODE = "#{UC_CODE}/shopDaoCreateFailed"
      end

      class CustomerDaoCreateFailedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Creation Customer by Customer Dao create failed.'
        CODE = "#{UC_CODE}/customerDaoCreateFailed"
      end

      class GoodymatDaoCreateFailedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Create Goodymat by Goodymat Dao create failed.'
        CODE = "#{UC_CODE}/goodymatDaoCreateFailed"
      end

      class CreatePermissionFailedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Create permission failed.'
        CODE = "#{UC_CODE}/sys/createPermissionFailed"
      end

      class SetProfileFailedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Create profile failed.'
        CODE = "#{UC_CODE}/sys/setProfileFailed"
      end

      class CreateBinaryFailedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Create uuBinary failed.'
        CODE = "#{UC_CODE}/uuBinary/createBinaryFailed"
      end

    end

  end
end
