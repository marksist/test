require_relative 'goodymat_general_error'

module UclGoodymatMain
  module Errors

    module BuyViaIoT

      UC_CODE = 'buyViaIot'

      class InvalidKeyError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Key in dtoIn is not valid.'
        CODE = "#{UC_CODE}/invalidKey"
      end

      class GoodymatDaoGetByCodeFailedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Get Goodymat by Goodymat Dao getByCode failed.'
        CODE = "#{UC_CODE}/goodymatDaoGetByCodeFailed"
      end

      class OperationIsNotValidError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Called operation is not registered for Goodymat.'
        CODE = "#{UC_CODE}/operationIsNotValid"
      end

      class ShopDaoGetByAwidFailedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Get Shop by Shop Dao getByAwid failed.'
        CODE = "#{UC_CODE}/shopDaoGetByAwidFailed"
      end

      class ShopIsClosedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Shop is closed.'
        CODE = "#{UC_CODE}/shopIsClosed"
      end

      class ProductDoesNotExistError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Product does not exist.'
        CODE = "#{UC_CODE}/productDoesNotExist"
      end

      class CustomerDaoGetByCodeFailedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Get Customer by Customer Dao getByCode failed.'
        CODE = "#{UC_CODE}/customerDaoGetByCodeFailed"
      end

      class CustomerIsClosedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Logged user is not active customer.'
        CODE = "#{UC_CODE}/customerIsClosed"
      end

      class OrderDaoCreateFailedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = '	Creation of Order by Order Dao create failed.'
        CODE = "#{UC_CODE}/orderDaoCreateFailed"
      end

    end
  end
end
