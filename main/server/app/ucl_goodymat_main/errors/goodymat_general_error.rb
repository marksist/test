module UclGoodymatMain
  module Errors
    class GoodymatGeneralError < UuApp::AppServer::Error::UseCaseError

      ERROR_PREFIX = 'ucl-goodymat-main/'

      def initialize(uu_app_error_map, param_map = nil)
        super(self.class::MESSAGE)
        self.code = "#{ERROR_PREFIX}#{self.class::CODE}"
        self.status = 400
        self.param_map.merge!(param_map) if param_map
        self.uu_app_error_map = uu_app_error_map
      end

    end
  end
end