require_relative 'goodymat_general_error'

module UclGoodymatMain
  module Errors

    module ListActiveOrder

      UC_CODE = 'listActiveOrder'

      class OrderDaoListByStateFailedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'List Orders by Order Dao listByState failed.'
        CODE = "#{UC_CODE}/orderDaoListByStateFailed"
      end

      class CustomerDaoGetByCodeFailedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Get Customer by Customer Dao getByCode failed.'
        CODE = "#{UC_CODE}/customerDaoGetByCodeFailed"
      end

      class ShopDaoGetByAwidFailedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Get Shop by Shop Dao getByAwid failed.'
        CODE = "#{UC_CODE}/shopDaoGetByAwidFailed"
      end

      class ProductDoesNotExistError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Product does not exist.'
        CODE = "#{UC_CODE}/productDoesNotExist"
      end

      class GoodymatDaoGetByCodeFailedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Get Goodymat by Goodymat Dao getByCode failed.'
        CODE = "#{UC_CODE}/goodymatDaoGetByCodeFailed"
      end

    end


    module ProceedOrder

      UC_CODE = 'proceedOrder'

      class InvalidKeyError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Key in dtoIn is not valid.'
        CODE = "#{UC_CODE}/invalidKey"
      end

      class OrderDaoGetFailedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Get order by Order Dao get failed.'
        CODE = "#{UC_CODE}/orderDaoGetFailed"
      end

      class OrderIsNotInProperStateError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Order is not in "Ordered" state.'
        CODE = "#{UC_CODE}/orderIsNotInProperState"
      end

      class OrderDaoUpdateFailedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Update order by order Dao update failed.'
        CODE = "#{UC_CODE}/orderDaoUpdateFailed"
      end

    end

    module DeleteAllOrders

      UC_CODE = 'deleteAllOrders'

      class OrderDaoDeleteByAwidFailedError < UclGoodymatMain::Errors::GoodymatGeneralError
        MESSAGE = 'Delete Order by Order Dao deleteByAwid failed.'
        CODE = "#{UC_CODE}/orderDaoDeleteByAwidFailed"
      end

    end




  end
end
