require 'uu_app_objectstore-mongo'

module UclGoodymatMain
  PRODUCT = 'ucl-goodymat-main'
end

require_module 'ucl_goodymat_main/controllers'
require_module 'ucl_goodymat_main/errors'
require_module 'ucl_goodymat_main'
require_module 'ucl_goodymat_main/dao'
require_module 'ucl_goodymat_main/models'