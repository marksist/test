require_relative 'abstract_shop_test'

class SetShopStateTest < AbstractShopTest

  ##
  # Simple happy day scenario. Set state of the shop - close and then open it.
  #
  def test_happy_day

    initialize_shop
    shop = get_shop(@awid)
    assert_equal(CLOSED_STATE, shop[:state])
    open_shop
    shop = get_shop(@awid)
    assert_equal(OPEN_STATE, shop[:state])
  end

  ##
  # Set shop state if missing defined state in dto_in. It will be set to default (closed) state.
  #
  def test_missing_state

    initialize_shop
    execute_post_command(SET_SHOP_STATE, {})
    shop = get_shop(@awid)
    assert_equal(CLOSED_STATE, shop[:state])
  end

  ##
  # Set state of the shop with using unsupported state. It will be set to default (closed) state.
  #
  def test_unsupported_state

    initialize_shop
    request_body = {
      state: 'FAKE_STATE'
    }

    execute_post_command(SET_SHOP_STATE, request_body)
    shop = get_shop(@awid)
    assert_equal(CLOSED_STATE, shop[:state])
  end

end