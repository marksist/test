require_relative 'abstract_shop_test'

class GetShopInfoTest < AbstractShopTest

  USE_CASE = 'getShopInfo'

  ##
  # Simple happy day scenario.
  #
  def test_happy_day

    initialize_shop
    open_shop
    response = execute_get_command(USE_CASE)
    assert_equal('200', response.code)
    assert_equal('open', response.body[:state])
    assert response.body[:nameLSI]
  end

  ##
  # Tries get information about shop that is not exist.
  #
  def test_get_non_existent_shop

    initialize_shop
    open_shop
    shop = get_shop(AWID)
    delete_shop(shop)
    response = execute_get_command(USE_CASE)
    assert_response_error(response, UclGoodymatMain::Errors::GetShopInfo::ShopDaoGetByAwidFailedError)
  end

end