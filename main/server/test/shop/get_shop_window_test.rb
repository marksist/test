require_relative 'abstract_shop_test'

class GetShopWindowTest < AbstractShopTest

  ##
  # Simple happy day scenario.
  #
  def test_happy_day

    initialize_shop
    response = execute_get_command(GET_SHOP_WINDOW)
    assert_equal('200', response.code)
  end

  ##
  # Test for changing state of shop (closing)
  #
  def test_closed_shop

    initialize_shop
    open_shop
    response = execute_post_command(SET_SHOP_STATE, { state: CLOSED_STATE })
    assert_equal('200', response.code)
    response = execute_get_command(GET_SHOP_WINDOW)
    assert_equal('200', response.code)
    assert_equal(CLOSED_STATE, response.body[:state])
    assert response.body[:nameLSI]
    assert response.body[:closeWelcomeLSI]
  end

  ##
  # Tries get information about shop that is not exist.
  #
  def test_info_about_non_existent_shop

    initialize_shop
    shop = get_shop(AWID)
    delete_shop(shop)
    response = execute_get_command(GET_SHOP_WINDOW)
    assert_response_error(response, UclGoodymatMain::Errors::GetShopWindow::ShopDaoGetByAwidFailedError)
  end

end