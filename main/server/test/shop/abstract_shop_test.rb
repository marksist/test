require_relative '../abstract_ucl_goodymat_main_test'

class AbstractShopTest < AbstractUclGoodymatMainTest

  ##
  # Assert attributes of shop object.
  #
  def assert_shop(shop, ignore_awid = false)
    assert_equal @awid, shop[:awid] unless ignore_awid
    assert_equal(CLOSED_STATE, shop[:state])
    assert shop[:id]
    assert shop[:nameLSI]
    assert shop[:openWelcomeLSI]
    assert shop[:closeWelcomeLSI]
    assert shop[:productMap]
    products = %w(SMOKED_KNEE RABBIT_EARS GUMMY_BEARS)
    products.each do |p|
      product = shop[:productMap][p.to_sym]
      assert product
      assert_product(product)
    end
    assert shop[:sys][:cts]
    assert shop[:sys][:mts]
    assert shop[:sys][:rev]
  end

  def assert_product(product)
    assert product[:nameLSI]
    assert product[:descLSI]
    assert product[:image]
  end

  ##
  # Returns shop by awid. Nil is returned if shop is not found.
  #
  def get_shop(awid)
    UclGoodymatMain::Models::Shop.instance.get_shop(awid)
  end

  def assert_configuration(response, expected_configuration)
    if response.kind_of?(Hash)
      response_body = response
    else
      response_body = response.body
    end

    assert response_body[:id]
    assert response_body[:sys][:cts]
    assert response_body[:sys][:mts]

    expected_configuration.each do |k, v|
      assert_equal(response_body[k], v)
    end
  end

  def assert_license(response, expected_users, expected_org = nil, from_init = true)
    if response.kind_of?(Hash)
      response_body = response
    else
      response_body = response.body
    end

    users = from_init ? response_body[:licenseOwner][:userList] : response_body[:userList]
    org = from_init ? response_body[:licenseOwner][:organization] : response_body[:organization]

    expected_users.each do |user|
      assert_includes(users, user)
    end

    if expected_org
      assert_equal(expected_org[:name], org[:name])
      assert_equal(expected_org[:oId], org[:oId])
      assert_equal(expected_org[:web], org[:web]) if expected_org[:web]
    end

  end

  ##
  # Deletes given shop
  #
  def delete_shop(shop)
    UclGoodymatMain::Models::Shop.instance.delete_shop(shop)
  end

end