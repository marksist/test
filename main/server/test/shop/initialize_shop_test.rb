require_relative 'abstract_shop_test'
class InitializeShopTest < AbstractShopTest

  ##
  # Init shop and SysAppWorkspace with default parameters (profiles, sys owner license, configuration)
  #
  def test_happy_day

    initialize_shop
    shop = get_shop(@awid)
    assert_shop(shop)
    response = get_license
    assert_license(response, HOLLY_HUDSON_LICENSE[:userList], HOLLY_HUDSON_LICENSE[:organization], false)
    response = get_configuration
    assert_configuration(response, DEFAULT_CONFIGURATION)
  end

  ##
  # Initializing shop with SysAppWorkspace configuration and its following re-setting.
  #
  def test_reset_configuration

    initialize_shop
    response = get_configuration
    assert_configuration(response, DEFAULT_CONFIGURATION)
    config = Hash.new
    config[:color] = 'green'
    config[:newParam] = 'Some new config parameter'
    set_configuration(config)
    response = get_configuration
    assert_configuration(response, config)
  end

  ##
  # Initializing shop with SysAppWorkspace configuration and its following deleting.
  #
  def test_clear_configuration

    initialize_shop
    response = get_configuration
    assert_configuration(response, DEFAULT_CONFIGURATION)
    clear_configuration
    response = get_configuration
    assert_configuration(response, {})
  end

end