require_relative 'abstract_order_test'

class DeleteAllOrdersTest < AbstractOrderTest


  ##
  # Simple happy day scenario.
  #
  def test_happy_day

    initialize_shop
    open_shop
    customer = get_customer(VLADIMIR)
    # create 5 orders
    5.times do |i|
      create_order(@awid, customer, SMOKED_KNEE)
    end

    result = execute_get_command(LIST_ACTIVE_ORDERS, Hash.new)
    assert_order_list(result, 5)

    execute_post_command(DELETE_ALL_ORDERS, Hash.new)
    result = execute_get_command(LIST_ACTIVE_ORDERS, Hash.new)
    assert_order_list(result, 0)
  end

end