require_relative 'abstract_order_test'

class CreateOrderTest < AbstractOrderTest

  ##
  # Simple happy day scenario.
  #
  def test_happy_day

    initialize_shop
    open_shop
    customer = get_customer(VLADIMIR)
    order = create_order(@awid, customer, SMOKED_KNEE)
    assert_order(order, VLADIMIR, PERSON, ORDERED_STATE, SMOKED_KNEE, nil)
  end

end