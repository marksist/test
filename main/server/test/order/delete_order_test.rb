require_relative 'abstract_order_test'

class DeleteOrderTest < AbstractOrderTest

  ##
  # Simple happy day scenario.
  #
  def test_happy_day

    initialize_shop
    open_shop
    customer = get_customer(VLADIMIR)
    order = create_order(@awid, customer, SMOKED_KNEE)
    UclGoodymatMain::Models::Order.instance.delete_order(order)
    order = UclGoodymatMain::Models::Order.instance.get(@awid, order[:id])
    assert_nil(order)
  end

end