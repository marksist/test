require_relative '../abstract_ucl_goodymat_main_test'

class AbstractOrderTest < AbstractUclGoodymatMainTest

  ##
  # Create new order from the parameters.
  #
  def create_order(awid, customer, product)

    params = Hash.new
    params[:awid] = awid
    params[:customer] = customer[:code]
    params[:product] = product
    params[:state] = ORDERED_STATE
    UclGoodymatMain::Models::Order.instance.create_order(params)
  end

  ##
  # Changes state of given order.
  #
  def change_state(order, state)

    params = Hash.new
    params[:awid] = order[:awid]
    params[:id] = order[:id]
    params[:state] = state
    UclGoodymatMain::Models::Order.instance.update_order(params)
  end

  ##
  # Assert expected number of orders in the list.
  #
  def assert_order_list(response, expected_count)
    if response.kind_of?(Hash)
      response_body = response
    else
      response_body = response.body
    end
    assert_equal(response_body[:itemList].length, expected_count)
  end

  ##
  # Assert attributes of give order.
  #
  def assert_order(order, expected_customer, expected_customer_type, expected_state, expected_product, expected_goodymat, from_list = false, is_finished = false)

    if from_list
      assert_equal(order[:goodymatCode], expected_goodymat)
      assert_equal(order[:productCode], expected_product)
      assert_equal(order[:customerCode], expected_customer)
      assert_equal(order[:customerType], expected_customer_type)
    else
      assert_equal(order[:product], expected_product)
      assert_equal(order[:customer], expected_customer)
      assert_equal(order[:state], expected_state)
      assert order[:sys][:cts]
      assert order[:sys][:mts]
    end

    if is_finished && order[:finishOrderTs].nil?
      flunk('Attribute finishOrderTs cannot be nil.')

    end
  end

  ##
  # Creates given number of order of product for specific customer.
  #
  def create_orders(awid, number, customer, product)
    $i = 0
    while $i < number do
      create_order(awid, customer, product)
      $i +=1
    end
  end

  ##
  # Prepares query with default values needed for paging.
  #
  def prepare_query_for_paging(page_index, page_size)
    query = { pageInfo: { pageIndex: page_index, pageSize: page_size } }
    query
  end

  ##
  # Prepares query with default values needed for paging.
  #
  def get_order(awid, id)
    UclGoodymatMain::Models::Order.instance.get(awid, id)
  end

end