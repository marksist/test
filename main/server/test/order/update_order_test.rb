require_relative 'abstract_order_test'

class UpdateOrderTest < AbstractOrderTest

  ##
  # Simple happy day scenario.
  #
  def test_happy_day

    initialize_shop
    open_shop
    customer = get_customer(VLADIMIR)
    order = create_order(@awid, customer, SMOKED_KNEE)

    params = {
      awid: @awid,
      id: order[:id],
      state: FINISHED_STATE,
      sponsor: VLADIMIR
    }

    UclGoodymatMain::Models::Order.instance.update_order(params)
    order = UclGoodymatMain::Models::Order.instance.get(@awid, order[:id])
    assert_order(order, VLADIMIR, PERSON, FINISHED_STATE, SMOKED_KNEE, nil)
  end

  ##
  # Update non-existing order.
  #
  def test_non_existing_order

    initialize_shop
    open_shop
    dto_in = {
      awid: @awid,
      id: '0',
      state: FINISHED_STATE,
      sponsor: VLADIMIR,
    }

    begin
      UclGoodymatMain::Models::Order.instance.update_order(dto_in)
    rescue Exception => e
      assert_equal(UU_OBJECT_NOT_FOUND_CODE, e.code)
    end
  end

end