require_relative 'abstract_order_test'

class ListActiveOrdersTest < AbstractOrderTest

  ##
  # Simple happy day scenario.
  #
  def test_happy_day

    initialize_shop
    open_shop
    customer = get_customer(VLADIMIR)
    # create three (in state ordered) orders
    create_order(@awid, customer, SMOKED_KNEE)
    create_order(@awid, customer, RABBIT_EARS)
    # change state to finished
    order = create_order(@awid, customer, RABBIT_EARS)
    change_state(order, FINISHED_STATE)
    query = Hash.new
    result = execute_get_command(LIST_ACTIVE_ORDERS, query)
    assert_order_list(result, 2)
  end

  ##
  # Get paged list of active orders.
  #
  def test_list_active_orders_paged

    initialize_shop
    open_shop
    customer = get_customer(VLADIMIR)
    # create 10 new orders
    create_orders(@awid, 10, customer, SMOKED_KNEE)
    # wait and create next 10 orders with different product
    sleep(2)
    create_orders(@awid, 10, customer, RABBIT_EARS)
    # get first 10 orders sorted by creation timestamp, ie. last created orders
    result = execute_get_command(LIST_ACTIVE_ORDERS, prepare_query_for_paging(0, 10))
    assert_order_list(result, 10)
    result.body[:itemList].each do |order|
      assert_order(order, VLADIMIR, PERSON, ORDERED_STATE, SMOKED_KNEE, nil, true)
    end

    result = execute_get_command(LIST_ACTIVE_ORDERS, prepare_query_for_paging(1, 10))
    assert_order_list(result, 10)
    result.body[:itemList].each do |order|
      assert_order(order, VLADIMIR, PERSON, ORDERED_STATE, RABBIT_EARS, nil, true)
    end

  end
end