require_relative 'abstract_order_test'

class ProceedOrderTest < AbstractOrderTest

  ##
  # Simple happy day scenario.
  #
  def test_happy_day

    initialize_shop
    open_shop
    customer = get_customer(VLADIMIR)
    order = create_order(@awid, customer, SMOKED_KNEE)
    assert_order(order, VLADIMIR, PERSON, ORDERED_STATE, SMOKED_KNEE, nil)

    request_body = Hash.new
    request_body[:id] = order[:id]
    execute_post_command(PROCEED_ORDER, request_body)

    processed_order = get_order(@awid, order[:id])
    assert_order(processed_order, VLADIMIR, PERSON, FINISHED_STATE, SMOKED_KNEE, nil, false, true)
  end

  ##
  # Proceed already finished order.
  #
  def test_proceed_already_finished

    initialize_shop
    open_shop
    customer = get_customer(VLADIMIR)
    order = create_order(@awid, customer, SMOKED_KNEE)
    request_body = Hash.new
    request_body[:id] = order[:id]
    execute_post_command(PROCEED_ORDER, request_body)
    response = execute_post_command(PROCEED_ORDER, request_body)
    assert_response_error(response, UclGoodymatMain::Errors::ProceedOrder::OrderIsNotInProperStateError)
  end

  ##
  # Proceed non-existing order.
  #
  def test_proceed_non_existing_order

    initialize_shop
    open_shop
    request_body = Hash.new
    request_body[:awid] = @awid
    request_body[:id] = INVALID_MONGO_ID
    response = execute_post_command(PROCEED_ORDER, request_body)
    assert_response_error(response, UclGoodymatMain::Errors::ProceedOrder::OrderDaoGetFailedError)
  end

end