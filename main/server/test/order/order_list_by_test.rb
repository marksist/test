require_relative 'abstract_order_test'

class OrderListByTest < AbstractOrderTest

  ##
  # Test trying get list of orders by states.
  #
  def test_list_by_state

    initialize_shop
    open_shop
    customer = get_customer(VLADIMIR)
    # create three new (in state ordered) orders
    create_order(@awid, customer, SMOKED_KNEE)
    create_order(@awid, customer, RABBIT_EARS)
    # change state to finished
    order = create_order(@awid, customer, RABBIT_EARS)
    change_state(order, FINISHED_STATE)

    result = UclGoodymatMain::Models::Order.instance.list_by_state(@awid, ORDERED_STATE, {})
    assert_order_list(result, 2)

    result = UclGoodymatMain::Models::Order.instance.list_by_state(@awid, FINISHED_STATE, {})
    assert_order_list(result, 1)
  end

  ##
  # Test trying get list of orders by customers.
  #
  def test_list_by_customer

    initialize_shop
    open_shop
    customer = get_customer(VLADIMIR)
    create_order(@awid, customer, SMOKED_KNEE)
    create_order(@awid, customer, RABBIT_EARS)
    customer = get_customer(PES)
    create_order(@awid, customer, RABBIT_EARS)

    result = UclGoodymatMain::Models::Order.instance.list_by_customer(@awid, VLADIMIR, {})
    assert_order_list(result, 2)

    result = UclGoodymatMain::Models::Order.instance.list_by_customer(@awid, PES, {})
    assert_order_list(result, 1)
  end

  ##
  # Test trying get list of orders by products.
  #
  def test_list_by_product

    initialize_shop
    open_shop
    customer = get_customer(VLADIMIR)
    create_order(@awid, customer, SMOKED_KNEE)
    create_order(@awid, customer, SMOKED_KNEE)
    create_order(@awid, customer, RABBIT_EARS)

    result = UclGoodymatMain::Models::Order.instance.list_by_product(@awid, SMOKED_KNEE, {})
    assert_order_list(result, 2)
    result = UclGoodymatMain::Models::Order.instance.list_by_product(@awid, RABBIT_EARS, {})
    assert_order_list(result, 1)
  end

end