require_relative 'abstract_goodymat_test'

class DeleteGoodymatTest < AbstractGoodymatTest

  ##
  # Simple happy day scenario.
  #
  def test_happy_day

    name_LSI = Hash.new
    name_LSI[:en] = 'goodymat #1'
    name_LSI[:cz] = 'goodymat 1'
    desc_LSI = Hash.new
    desc_LSI[:en] = 'it is goodymat number 1'
    desc_LSI[:cz] = 'to je goodymat číslo 1'
    operations = Hash.new
    operations[:O1] = { customer: VLADIMIR, product: SMOKED_KNEE }
    operations[:O2] = { customer: PES, product: RABBIT_EARS }
    create_goodymat(@awid, GM1, name_LSI, desc_LSI, operations)
    goodymat = UclGoodymatMain::Models::Goodymat.instance.get_by_code(@awid, GM1)
    assert(goodymat)
    UclGoodymatMain::Models::Goodymat.instance.delete_goodymat(goodymat)
    goodymat = UclGoodymatMain::Models::Goodymat.instance.get_by_code(@awid, GM1)
    assert_nil(goodymat)
  end

end