require_relative 'abstract_goodymat_test'

class BuyViaIOTTest < AbstractGoodymatTest

  ##
  # Simple happy day scenario. Buy via goodymat (internet of things).
  #
  def test_happy_day

    initialize_shop
    open_shop

    dto_in = {
      goodymat: HOLLY_HUDSON_UUID,
      operation: 'O1'
    }

    response = execute_post_command(BUY_VIA_IOT, dto_in)
    assert_equal('200', response.code)
    assert_order(response, VLADIMIR, ORDERED_STATE, SMOKED_KNEE, HOLLY_HUDSON_UUID)
  end

  ##
  # Buy via IoT, when goodymat is not registered in the shop.
  #
  def test_non_registered_goodymat

    initialize_shop
    open_shop

    goodymat = UclGoodymatMain::Models::Goodymat.instance.get_by_code(@awid, HOLLY_HUDSON_UUID)
    UclGoodymatMain::Models::Goodymat.instance.delete_goodymat(goodymat)

    dto_in = {
      goodymat: HOLLY_HUDSON_UUID,
      operation: 'O1'
    }

    response = execute_post_command(BUY_VIA_IOT, dto_in)
    assert_response_error(response, UclGoodymatMain::Errors::BuyViaIoT::GoodymatDaoGetByCodeFailedError)
  end

  ##
  # Buy via IoT if operation is not defined in the goodymat.
  #
  def test_not_defined_operation

    initialize_shop
    open_shop

    dto_in = {
      goodymat: HOLLY_HUDSON_UUID,
      operation: 'FAKE_OPERATION'
    }

    response = execute_post_command(BUY_VIA_IOT, dto_in)
    assert_response_error(response, UclGoodymatMain::Errors::BuyViaIoT::OperationIsNotValidError)
  end

end