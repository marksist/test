require_relative '../abstract_ucl_goodymat_main_test'

class AbstractGoodymatTest < AbstractUclGoodymatMainTest

  ##
  # Create goodymat from parameters.
  #
  def create_goodymat(awid, code, nameLSI, descLSI, operations, state = 'active')

    dto_in = {}
    dto_in[:awid] = awid
    dto_in[:code] = code
    dto_in[:state] = state
    dto_in[:nameLSI] = nameLSI
    dto_in[:descLSI] = descLSI
    dto_in[:operationMap] = operations
    UclGoodymatMain::Models::Goodymat.instance.create_goodymat(dto_in)
  end

  ##
  # Assert attributes of goodymat object.
  #
  def assert_goodymat(goodymat, expected_code, expected_state, expected_nameLSI, expected_descLSI, exptected_operations)

    assert_equal(goodymat[:code], expected_code)
    assert_equal(goodymat[:state], expected_state)
    assert_equal(goodymat[:nameLSI][:en], expected_nameLSI[:en])
    assert_equal(goodymat[:nameLSI][:cz], expected_nameLSI[:cz])
    assert_equal(goodymat[:descLSI][:en], expected_descLSI[:en])
    assert_equal(goodymat[:descLSI][:cz], expected_descLSI[:cz])
    assert_equal(goodymat[:operationMap].keys.sort, exptected_operations.keys.sort)

    goodymat[:operationMap].each do |o|
      assert_equal(o[1][:customer], exptected_operations[o[0]][:customer])
      assert_equal(o[1][:product], exptected_operations[o[0]][:product])
    end

    assert goodymat[:sys][:cts]
    assert goodymat[:sys][:mts]
  end

  ##
  # Assert attributes of order object.
  #
  def assert_order(response, expected_customer, expected_state, expected_product, expected_goodymat)
    if response.kind_of?(Hash)
      response_body = response
    else
      response_body = response.body
    end
    assert_equal(response_body[:goodymat], expected_goodymat)
    assert_equal(response_body[:product], expected_product)
    assert_equal(response_body[:state], expected_state)
    assert_equal(response_body[:customer], expected_customer)
    assert response_body[:sys][:cts]
    assert response_body[:sys][:mts]
  end

  ##
  # Returns goodymat by code. Nil is returned if goodymat is not found.
  #
  def get_goodymat_by_code(awid, code)
    UclGoodymatMain::Models::Goodymat.instance.get_by_code(awid, code)
  end

end