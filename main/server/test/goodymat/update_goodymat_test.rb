require_relative 'abstract_goodymat_test'

class UpdateGoodymatTest < AbstractGoodymatTest

  ##
  # Simple happy day scenario.
  #
  def test_happy_day

    name_LSI = Hash.new
    name_LSI[:en] = 'goodymat #1'
    name_LSI[:cz] = 'goodymat 1'
    desc_LSI = Hash.new
    desc_LSI[:en] = 'it is goodymat number 1'
    desc_LSI[:cz] = 'to je goodymat číslo 1'
    operations = Hash.new
    operations[:O1] = { customer: VLADIMIR, product: SMOKED_KNEE }
    operations[:O2] = { customer: PES, product: RABBIT_EARS }
    goodymat = create_goodymat(@awid, GM1, name_LSI, desc_LSI, operations)

    new_name_LSI = Hash.new
    name_LSI[:en] = 'first goodymat'
    name_LSI[:cz] = 'prvni goodymat'
    new_state = 'shutdown'


    dto_in = {
      awid: @awid,
      id: goodymat[:id],
      state: new_state,
      nameLSI: new_name_LSI
    }

    UclGoodymatMain::Models::Goodymat.instance.update_goodymat(dto_in)
    goodymat = UclGoodymatMain::Models::Goodymat.instance.get_by_code(@awid, GM1)
    assert_goodymat(goodymat, goodymat[:code], new_state, new_name_LSI, goodymat[:descLSI], goodymat[:operationMap])
  end

  ##
  # Update non-existing goodymat.
  #
  def test_update_non_existing_goodymat

    dto_in = {
      awid: @awid,
      id: '0'
    }

    begin
      UclGoodymatMain::Models::Goodymat.instance.update_goodymat(dto_in)
    rescue Exception => e
      assert_equal(UU_OBJECT_NOT_FOUND_CODE, e.code)
    end
  end

end