require_relative 'abstract_goodymat_test'

class CreateGoodymatTest < AbstractGoodymatTest

  #
  # Simple happy day scenario. Create new goodymat from the parameters.
  #
  def test_happy_day

    initialize_shop

    name_LSI = Hash.new
    name_LSI[:en] = 'goodymat #1'
    name_LSI[:cz] = 'goodymat 1'
    desc_LSI = Hash.new
    desc_LSI[:en] = 'it is goodymat number 1'
    desc_LSI[:cz] = 'to je goodymat číslo 1'
    operations = Hash.new
    operations[:O1] = { customer: VLADIMIR, product: SMOKED_KNEE }
    operations[:O2] = { customer: PES, product: RABBIT_EARS }
    goodymat = create_goodymat(@awid, GM1, name_LSI, desc_LSI, operations)
    assert_goodymat(goodymat, GM1, 'active', name_LSI, desc_LSI, operations)
  end

end
