require 'uu_app/test/abstract_test'
require 'uu_app_workspace'
require_relative '../app/ucl_goodymat_main/models/order'
require_relative '../app/ucl_goodymat_main/models/shop'
require_relative '../app/ucl_goodymat_main/models/customer'

class AbstractUclGoodymatMainTest < UuApp::Test::AbstractTest

  # UC and VUC names
  SET_SHOP_STATE = 'setShopState'
  BUY = 'buy'
  BUY_VIA_IOT = 'buyViaIot'
  FAKE_VUC = 'fakeVUC'
  GET_SHOP_WINDOW = 'getShopWindow'
  LIST_ACTIVE_ORDERS = 'listActiveOrders'
  PROCEED_ORDER = 'proceedOrder'
  DELETE_ALL_ORDERS = 'deleteAllOrders'

  # customer types
  PERSON = UclGoodymatMain::Models::Customer::CUSTOMER_TYPE_PERSON
  UU_EE = UclGoodymatMain::Models::Customer::CUSTOMER_TYPE_UUEE

  # error codes
  UU_OBJECT_NOT_FOUND_CODE = 'uu.appobjectstore/objectNotFound'

  # Model states (order, shop, customer, goodymat)
  ORDERED_STATE = UclGoodymatMain::Models::Order::ORDERED_STATE
  FINISHED_STATE = UclGoodymatMain::Models::Order::FINISHED_STATE
  OPEN_STATE = UclGoodymatMain::Models::Shop::OPEN_SHOP_STATE
  CLOSED_STATE = UclGoodymatMain::Models::Shop::CLOSED_SHOP_STATE

  # test setup (i.e. "overriding" relevant constants)
  CONFIG_RU = File.expand_path('../config.ru', File.dirname(__FILE__))
  ACCESS_CODE_1 = '3yMhD1qzi0JUwsAJ7UOsWX2YtrGH_Olk5DxgWyssNW6e_WC4wjKrXODHES92cQXp'
  ACCESS_CODE_2 = 'Cq%iaz#9w-#z7C8yZlZoC$DsebIK2JGWyDK8lg8J7vLFSKXtYFt5tn3eI3WthZtI'
  HOLLY_HUDSON_UUID = '14-2710-1'
  ART_ID = '77124143975760106'
  ART_URI = "ues:[#{TID}]:[#{ART_ID}]"
  AWID = '00000000000000000000000000000000'
  NSID = '00000000000000000000000000000001'

  # application profiles
  EXECUTIVES = 'Executives'
  AUTHORITIES = 'Authorities'
  CUSTOMERS = 'Customers'
  GOODYMATS = 'Goodymats'
  GUESTS = 'Guests'
  FAKE_PROFILE = 'FakeProfile'

  # role ues uri
  ROLE_URI = 'ues:[84723967990075193]:[74309394208653707]'
  AUTHORITIES_URI = 'ues:[84723967990075193]:[74309394208653707]'
  NULL_URI = 'ues:[-1]:[-1]'
  INVALID_URI = 'ues:FAKE_TENANT[12345]:FAKE_ART[67890]'

  # some (non)existing UUIDs
  UUID_01 = '11-2233-44'
  UUID_02 = '55-6677-88'
  UUID_03 = '99-1011-12'

  # customer codes
  VLADIMIR = '1-1'
  PES = '1111-11-1'
  TEST_CUSTOMER = '9999-9999-9999-9999'
  INVALID_CUSTOMER = { code: '0' }

  # goodymat codes
  GM1 = '101-1'

  # product codes
  SMOKED_KNEE = 'SMOKED_KNEE'
  RABBIT_EARS = 'RABBIT_EARS'
  GUMMY_BEARS = 'GUMMY_BEARS'
  INVALID_PRODUCT = 'PIG_TAIL'
  INVALID_SHOP = 'invalid_shop_id'

  INVALID_MONGO_ID = '000000000000000000000000'

  DEFAULT_PROFILES = {
    code: AUTHORITIES,
    uuIdentityList: HOLLY_HUDSON_UUID
  },
    {
      code: EXECUTIVES,
      uuIdentityList: HOLLY_HUDSON_UUID
    },
    {
      code: GOODYMATS,
      uuIdentityList: HOLLY_HUDSON_UUID
    },
    {
      code: CUSTOMERS,
      uuIdentityList: HOLLY_HUDSON_UUID
    },
    {
      code: GUESTS,
      uuIdentityList: HOLLY_HUDSON_UUID
    }

  HOLLY_HUDSON_LICENSE = {
    organization: {
      name: '<uu5string/>Holly Hudson Organization',
      oId: 'HOLLY_HUDSON_OID',
      web: 'www.holly-hudson-organization.com'
    },
    userList: [
      {
        uuIdentity: HOLLY_HUDSON_UUID,
        name: '<uu5string/>Holly Hudson'
      }
    ]
  }

  DEFAULT_CONFIGURATION = {
    param1: 'Some String',
    param2: 0,
    param3: %w(alpha beta gamma),
    param4: {
      id: 0,
      name: 'Holly Hudson'
    },
    param5: nil
  }

  ##
  # tests setup
  def setup
    super
    @profiles = UuAppWorkspace::ProfileConfig.instance.get_profiles
    UuAppWorkspace::Models::SysAppWorkspace.instance.clear_cache
    UuAppWorkspace::Models::SysAppWorkspaceConfig.instance.clear_cache
  end

  ##
  # methods definition
  def init_aw_all_profiles
    parameters = {
      profileList: [
        {
          code: AUTHORITIES,
          identityUuIdList: HOLLY_HUDSON_UUID
        },
        {
          code: EXECUTIVES,
          identityUuIdList: HOLLY_HUDSON_UUID
        },
        {
          code: GOODYMATS,
          identityUuIdList: HOLLY_HUDSON_UUID
        },
        {
          code: CUSTOMERS,
          identityUuIdList: HOLLY_HUDSON_UUID
        },
        {
          code: GUESTS,
          identityUuIdList: HOLLY_HUDSON_UUID
        }
      ],
      licenseOwner: HOLLY_HUDSON_LICENSE
    }

    rescued = false
    begin
      UuAppWorkspace::Models::SysAppWorkspace.instance.init_app_workspace(@awid, TID, nil, parameters)
    rescue StandardError => e
      rescued = true
    end
    assert !rescued
  end

  def get_customer(code)
    UclGoodymatMain::Models::Customer.instance.get_by_code(@awid, code)
  end

  ##
  # Open shop (set state to open)
  #
  def open_shop
    dto_in = Hash.new
    dto_in[:state] = OPEN_STATE
    UclGoodymatMain::Models::Shop.instance.set_shop_state(@awid, dto_in)
  end

  def get_license
    execute_get_command('sys/getLicenseOwner')
  end

  def set_configuration(request_body)
    execute_post_command('sys/setAppWorkspaceConfig', request_body)
  end

  def get_configuration
    execute_get_command('sys/getAppWorkspaceConfig')
  end

  def clear_configuration
    execute_post_command('sys/clearAppWorkspaceConfig', {})
  end

  ##
  # Initialize shop.
  #
  def initialize_shop(skip_create_permission = false, tid = nil, awid = nil, sys_owner = nil, license_owner = nil, config = nil, skip_status_assert = false, skip_creating_config = false)

    # init app workspace
    init_simple_app_workspace(tid, awid, sys_owner, license_owner, config, skip_status_assert, skip_creating_config)
    # init shop
    request_body = Hash.new
    request_body[:authoritiesUri] = AUTHORITIES_URI
    response = execute_post_command('init', request_body)
    assert_equal('200', response.code)
    # create permission for Holly Hudson on all profile
    unless skip_create_permission
      all_profiles = @profiles.dup
      all_profiles.delete(:SysOwner)
      all_profiles.each do |code|
        dto_in = {
          profileCode: code.to_s,
          uuIdentityList: [HOLLY_HUDSON_UUID]
        }
        UuAppWorkspace::Models::SysPermission.instance.create(awid.nil? ? @awid : awid, dto_in)
      end
    end

  end

  def init_simple_app_workspace(tid = nil, awid = nil, sys_owner = nil, license_owner = nil, config = nil, skip_status_assert = false, skip_creating_config = false)

    tid = tid.nil? ? TID : tid
    @awid = AWID

    request_body = {}
    request_body[:awid] = awid.nil? ? @awid : awid
    request_body[:sysOwner] = sys_owner ? sys_owner : HOLLY_HUDSON_UUID
    request_body[:licenseOwner] = license_owner ? license_owner : HOLLY_HUDSON_LICENSE

    unless skip_creating_config
      request_body[:appWorkspaceConfig] = config ? config : DEFAULT_CONFIGURATION
    end

    response = execute_post_command('sys/initAppWorkspace', request_body, tid, NSID)
    assert_equal('200', response.code) unless skip_status_assert
    response
  end


  ##
  # Assert error from the response.
  #
  def assert_response_error(response, error_class)
    assert_equal('400', response.code)
    prefix = UclGoodymatMain::Errors::GoodymatGeneralError::ERROR_PREFIX
    assert_equal("#{prefix}#{error_class::CODE}", response.body[:code])
    assert_equal("#{error_class::MESSAGE}", response.body[:message])
  end

end