require_relative 'abstract_customer_test'

class CreateCustomerTest < AbstractCustomerTest

  ##
  # Simple happy day scenario. Create new customer from the parameters.
  #
  def test_happy_day

    name = 'Vladimír Kovář'
    create_customer(@awid, VLADIMIR, name, PERSON, VLADIMIR)
    customer =  UclGoodymatMain::Models::Customer.instance.get_by_code(@awid, VLADIMIR)
    assert_customer(customer, VLADIMIR, name, PERSON, 'active', nil)
  end

end
