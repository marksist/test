require_relative '../abstract_ucl_goodymat_main_test'

class AbstractCustomerTest < AbstractUclGoodymatMainTest

  ##
  # Create new customer from the parameters.
  #
  def create_customer(awid, code, name, type, sponsor, state = 'active', image = nil)

    dto_in = Hash.new
    dto_in[:awid] = awid
    dto_in[:code] = code
    dto_in[:name] = name
    dto_in[:type] = type
    dto_in[:state] = state
    dto_in[:sponsor] = sponsor
    dto_in[:state] = state
    dto_in[:image] = image
    UclGoodymatMain::Models::Customer.instance.create_customer(dto_in)
  end

  ##
  # Assert order attributes.
  #
  def assert_order(response, expected_customer, expected_state, expected_product, expected_goodymat)

    if response.kind_of?(Hash)
      response_body = response
    else
      response_body = response.body
    end

    assert_equal(response_body[:goodymat], expected_goodymat)
    assert_equal(response_body[:product], expected_product)
    assert_equal(response_body[:state], expected_state)
    assert_equal(response_body[:customer], expected_customer)
    assert response_body[:sys][:cts]
    assert response_body[:sys][:mts]
  end

  ##
  # Assert costumer attributes.
  #
  def assert_customer(customer, expected_code, expected_name, expected_type, expected_state, expected_img)
    assert_equal(customer[:code], expected_code)
    assert_equal(customer[:name], expected_name)
    assert_equal(customer[:type], expected_type)
    assert_equal(customer[:state], expected_state)
    assert_equal(customer[:image], expected_img)
    assert customer[:sys][:cts]
    assert customer[:sys][:mts]
  end

end