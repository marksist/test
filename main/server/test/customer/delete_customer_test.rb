require_relative 'abstract_customer_test'

class DeleteCostumerTest < AbstractCustomerTest

  ##
  # Simple happy day scenario.
  #
  def test_happy_day

    name = 'Vladimír Kovář'
    create_customer(@awid, VLADIMIR, name, PERSON, VLADIMIR)
    customer = UclGoodymatMain::Models::Customer.instance.get_by_code(@awid, VLADIMIR)
    assert(customer)
    UclGoodymatMain::Models::Customer.instance.delete_customer(customer)
    customer = UclGoodymatMain::Models::Customer.instance.get_by_code(@awid, VLADIMIR)
    assert_nil(customer)
  end

end