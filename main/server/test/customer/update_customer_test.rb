require_relative 'abstract_customer_test'

class UpdateCostumerTest < AbstractCustomerTest

  ##
  # Simple happy day scenario.
  #
  def test_happy_day

    name = 'Alena Nevdaná'
    customer = create_customer(@awid, TEST_CUSTOMER, name, PERSON, TEST_CUSTOMER)
    new_name = 'Alena Provdaná'
    dto_in = {
      awid: @awid,
      id: customer[:id],
      name: new_name
    }

    UclGoodymatMain::Models::Customer.instance.update_customer(dto_in)
    customer = UclGoodymatMain::Models::Customer.instance.get_by_code(@awid, TEST_CUSTOMER)
    assert_customer(customer, TEST_CUSTOMER, new_name, PERSON, 'active', nil)
  end

  ##
  # Update non-existing customer.
  #
  def test_update_non_existing_customer

    dto_in = {
      awid: @awid,
      id: '0',
      name: 'Some new name'
    }

    begin
      UclGoodymatMain::Models::Customer.instance.update_customer(dto_in)
    rescue Exception => e
      assert_equal(UU_OBJECT_NOT_FOUND_CODE, e.code)
    end
  end

end