require_relative 'abstract_customer_test'

class BuyTest < AbstractCustomerTest

  ##
  # Simple happy day scenario.
  #
  def test_happy_day

    initialize_shop
    open_shop
    create_customer(@awid, HOLLY_HUDSON_UUID, 'Holly Hudson', PERSON, HOLLY_HUDSON_UUID)

    dto_in = {
      product: SMOKED_KNEE
    }

    response = execute_post_command(BUY, dto_in)
    assert_equal('200', response.code)
    assert_order(response, HOLLY_HUDSON_UUID, ORDERED_STATE, SMOKED_KNEE, nil)
  end

  ##
  # Buy if product, that does not exist in the shop.
  #
  def test_non_existent_product

    initialize_shop
    open_shop
    create_customer(@awid, HOLLY_HUDSON_UUID, 'Holly Hudson', PERSON, HOLLY_HUDSON_UUID)

    dto_in = {
      product: INVALID_PRODUCT
    }
    response = execute_post_command(BUY, dto_in)
    assert_response_error(response, UclGoodymatMain::Errors::Buy::ProductDoesNotExistError)
  end

  ##
  # Buy if customer, that does not exist in the shop.
  #
  def test_non_existent_customer

    initialize_shop
    open_shop

    dto_in = {
      product: SMOKED_KNEE
    }
    response = execute_post_command(BUY, dto_in)
    assert_response_error(response, UclGoodymatMain::Errors::Buy::CustomerDaoGetByCodeFailedError)
  end

  ##
  # Buy if code of product is invalid (missing).
  #
  def test_invalid_product_code

    initialize_shop
    open_shop
    create_customer(@awid, HOLLY_HUDSON_UUID, 'Holly Hudson', PERSON, HOLLY_HUDSON_UUID)
    response = execute_post_command(BUY, {})
    assert_response_error(response, UclGoodymatMain::Errors::Buy::InvalidKeyError)
  end


  ##
  # Create (buy) order if shop is closed.
  #
  def test_create_order_closed_shop

    initialize_shop # shop is initialized in closed state

    dto_in = {
      product: SMOKED_KNEE
    }

    response = execute_post_command(BUY, dto_in)
    assert_response_error(response, UclGoodymatMain::Errors::Buy::ShopIsClosedError)
  end






end