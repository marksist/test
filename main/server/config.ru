require 'bundler'

Bundler.setup
require 'uu_app_server'
require 'singleton'
require 'uu_app-binarystore-cmd'

set :default_uc_path, '/eshop'
set :default_uc_use_redirect, true

configure :development, :test do
  require 'rack/cors'

  set :response_timeout, -1

  use Rack::Cors do
    allow do
      origins '*'
      resource '*', :headers => :any, :methods => [:get, :post, :options]
    end
  end
end

run!